﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using UltimoApps.DAL;
using UltimoApps.DAL.Helper;

namespace UltimoApps
{


    public partial class frmAMTprintpreview : System.Web.UI.Page
    {
        public PrintDocument Target = new PrintDocument();
        private PrintDocument PDamt = new PrintDocument();
        private Single HeaderLeft;
        public int jy = 0;
        int CTI = 0;
        String PName;

        public DataSet ds = new DataSet();

        //public DataSet ds
        //{
        //    get
        //    {
        //        if (ViewState["DataSet"] == null)
        //        {
        //            ViewState["DataSet"] = new DataSet();
        //        }

        //        return (DataSet)ViewState["DataSet"];
        //    }

        //    set {
        //        ViewState["DataSet"] = value;
        //    }
        //}

        string wkono;
        string wkoqty;
        string wkomtr;
        string wkofgcode;
        string wkoptno;
        string wkorevno;
        string wkoprname;
        string wkoeftdate;
        bool RetPrt;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //var prn = new ClsAMTprint.clsmain();
            string JobNo = Request.QueryString["JobNo"].ToString();

            string restr = "";
            string[] s = JobNo.Split('-');

            if (s.Length > 2)
            {
                restr = "-R";
            }

            // Sementara dibuat komentar dulu, karena tombol cekbox belum ada. Saat ini pake dulu yang di bawah.
            //if (printAMT((s[0].Trim() + ('-' + s[1].Trim())), Global.eWipConnString, "PDFCreator", true))
            //{
            //    MsgBox("Please Collect your print copy in level-1");
            //}
            //else if (printAMT((s[0].Trim() + ('-' + (s[1] + restr).Trim())), Global.eWipConnString, "PDFCreator", false))
            //{
            //    MsgBox("Please Collect your print copy in level-1");
            //}

            if (printAMT((s[0].Trim() + ('-' + (s[1] + restr).Trim())), Global.eWipConnString, "PDFCreator", true))
            {
                MsgBox("Please Collect your print copy in level-1");
            }


            //PaperSize pkSize;
            //for (int i = 0; i <= Target.PrinterSettings.PaperSizes.Count - 1; i++)
            //{
            //    pkSize = Target.PrinterSettings.PaperSizes[i];
            //    if ((pkSize.PaperName == "A4"))
            //    {
            //        Target.PrinterSettings.DefaultPageSettings.PaperSize = pkSize;
            //        Target.DefaultPageSettings.PaperSize = pkSize;
            //        break;
            //    }

            //}

            Margins margins = new Margins(30, 50, 15, 50);
            PDamt.DefaultPageSettings.Margins = margins;
            PDamt.DefaultPageSettings.Landscape = false;
            PDamt.PrinterSettings.DefaultPageSettings.Margins = margins;
            PDamt.PrinterSettings.DefaultPageSettings.Landscape = false;
            HeaderLeft = (margins.Left + 110);

            //this.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height;
            PDamt.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDOC);
            //  Dim Printname As String = PName 'System.Configuration.ConfigurationManager.AppSettings("Printer")
            // PDamt.PrinterSettings.PrinterName = PName 'Printname
            PDamt.PrinterSettings.PrinterName = "Microsoft Print to PDF";

            PDamt.Print();
            }
            catch (Exception)
            {
                
            }
        }


        void printDOC(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // printbackup(sender, e)
            try
            {
                // Font Style
                // Dim f8bar As New System.Drawing.Font("IDAutomationHC39M", 12, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
                System.Drawing.Font f8bar = FontHelper.LoadFont(this.GetType().Assembly, "~/App_LocalResources/IDAutomationHC39M.ttf", 12, FontStyle.Bold, GraphicsUnit.Point);
                System.Drawing.Font f6b = new System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f8b = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f8bu = new System.Drawing.Font("Arial", 8, (System.Drawing.FontStyle.Bold | FontStyle.Underline), System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f10 = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f10b = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f10bar = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f10bu = new System.Drawing.Font("Arial", 10, (System.Drawing.FontStyle.Bold | FontStyle.Underline), System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f16b = new System.Drawing.Font("Arial", 14, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f12b = new System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                System.Drawing.Font f16bu = new System.Drawing.Font("Arial", 14, (System.Drawing.FontStyle.Bold | FontStyle.Underline), System.Drawing.GraphicsUnit.Point);
                // Text Align
                System.Drawing.StringFormat lSF = new System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit);
                lSF.Alignment = StringAlignment.Near;
                System.Drawing.StringFormat rSF = new System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit);
                rSF.Alignment = StringAlignment.Far;
                System.Drawing.StringFormat cSF = new System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit);
                cSF.Alignment = StringAlignment.Center;
                // Pen Style
                Pen SP1 = new Pen(Color.Black, 1);
                Pen SP2 = new Pen(Color.Black, 1);
                Pen SP3 = new Pen(Color.Black, 2);
                float il;
                float it;
                float pw;
                float ph;
                float ct;
                float ch;
                il = e.MarginBounds.X;
                // 30
                it = e.MarginBounds.Y;
                // 15
                pw = e.MarginBounds.Width;
                // 747
                ph = e.MarginBounds.Height;
                // 1104
                int lines;
                int chars;
                int bs = 0;
                int pg = 1;
                int curpg = 1;
                bool lastpgprinted = false;
                float l1;
                float l2;
                float l3;
                float l4;
                float l5;
                float l6;
                float l7;
                float l8;
                float l9;
                float l10;
                float l11;
                l1 = il;
                l2 = (l1 + 30);
                l3 = (l2 + 100);
                l4 = (l3 + 50);
                l5 = (l4 + 50);
                l6 = (l5 + 50);
                l7 = (l6 + 50);
                l8 = (l7 + 50);
                l9 = (l8 + 50);
                l10 = (l9 + 50);
                l11 = (il + pw);
                ct = it;
                //  + (30)
                ch = (ph - 20);
                System.Drawing.RectangleF pa = new System.Drawing.RectangleF(l1, ct, pw, ch);
                e.Graphics.DrawString("ADVANCED MATERIALS TECHNOLOGIES PTE LTD", f16b, Brushes.Black, pa, cSF);
                il = 640;
                // e.MarginBounds.X
                it = e.MarginBounds.Y;
                pw = e.MarginBounds.Width;
                ph = e.MarginBounds.Height;
                pa = new System.Drawing.RectangleF(il, it, pw, ph);
                string logoPath = System.Configuration.ConfigurationManager.AppSettings["Path"];
                if ((logoPath == "E:\\Projects\\2008\\AMT\\Source_20101210\\AMT\\AMT\\Resources"))
                {
                    // Dim img As Image = Image.FromFile(Windows.Forms.Application.StartupPath & "\AMT_Logo.jpg")
                    System.Drawing.Image img = System.Drawing.Image.FromFile(logoPath + "\\AMT_Logo.jpg");

                    e.Graphics.DrawImage(img, il, it);
                }

                ct += 30;
                pa = new System.Drawing.RectangleF(l1, ct, pw, ch);
                if ((wkono.Length > 5))
                {
                    if ((wkono.Substring(4, 1) == "9"))
                    {
                        e.Graphics.DrawString("Engineering Work Order", f12b, Brushes.Black, pa, cSF);
                    }
                    else
                    {
                        e.Graphics.DrawString("WORK ORDER ROUTING", f12b, Brushes.Black, pa, cSF);
                    }

                }
                else
                {
                    e.Graphics.DrawString("WORK ORDER ROUTING", f12b, Brushes.Black, pa, cSF);
                }

                ct += 20;
                pa = new System.Drawing.RectangleF(l1, ct, pw, ch);
                e.Graphics.DrawString(("Date: " + DateTime.Now.Date.ToString("dd-MMM-yyyy")), f8b, Brushes.Black, pa, lSF);
                // pa = New System.Drawing.RectangleF(400, 1100, 200, ch)
                // e.Graphics.DrawString("LEGEND", f8bu, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(480, 1120, 200, ch)
                // e.Graphics.DrawString("CP = Completed", f8b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(560, 1140, 200, ch)
                // e.Graphics.DrawString("RJ = Rejected", f8b, Brushes.Black, pa, lSF)
                pa = new System.Drawing.RectangleF(680, 1020, 80, ch);
                e.Graphics.DrawString("LEGEND", f8bu, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(680, 1040, 100, ch);
                e.Graphics.DrawString("CP = Completed", f8b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(680, 1060, 100, ch);
                e.Graphics.DrawString("RJ = Rejected", f8b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(50, 1020, 250, ch);
                e.Graphics.DrawString("Approved By:_______________(Engrg Mgr)", f8b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(50, 1040, 500, ch);
                e.Graphics.DrawString("Approved By:_______________(QA Head/Mgr) Only products accepted for delivery", f8b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(50, 1060, 500, ch);
                e.Graphics.DrawString("Remarks:", f8b, Brushes.Black, pa, lSF);
                // two point define one line.
                e.Graphics.DrawLine(SP1, l1, (ct + 15), (pw + 50), (ct + 15));
                // wkono = "50SI981117-B01-R"
                ct += 20;
                pa = new System.Drawing.RectangleF(l1, ct, 80, ch);
                e.Graphics.DrawString("Work Order No.", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 80), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 90), ct, 330, ch);
                e.Graphics.DrawString("*" + (wkono + "*"), f8bar, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 420), ct, 100, ch);
                e.Graphics.DrawString("FG Code", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 520), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 530), ct, 100, ch);
                e.Graphics.DrawString(wkofgcode, f10b, Brushes.Black, pa, lSF);
                ct += 20;
                pa = new System.Drawing.RectangleF((l1 + 420), ct, 100, ch);
                e.Graphics.DrawString("DWG No.", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 520), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 530), ct, 250, ch);
                e.Graphics.DrawString(wkoptno, f10b, Brushes.Black, pa, lSF);
                ct += 20;
                pa = new System.Drawing.RectangleF((l1 + 420), ct, 100, ch);
                e.Graphics.DrawString("QP Rev No.", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 520), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 530), ct, 100, ch);
                e.Graphics.DrawString(wkorevno, f10b, Brushes.Black, pa, lSF);
                ct += 20;
                // pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
                // e.Graphics.DrawString("Effective Date", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
                // e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 530, ct, 100, ch)
                // e.Graphics.DrawString(wkoeftdate, f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1, ct, 80, ch)
                // e.Graphics.DrawString("Material", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 80, ct, 10, ch)
                // e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 90, ct, 190, ch)
                // e.Graphics.DrawString(wkomtr, f10b, Brushes.Black, pa, lSF)
                pa = new System.Drawing.RectangleF(l1, ct, 80, ch);
                e.Graphics.DrawString("Total Qty.", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 80), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 90), ct, 190, ch);
                e.Graphics.DrawString(wkoqty, f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 420), ct, 100, ch);
                e.Graphics.DrawString("Effective Date", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 520), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 530), ct, 100, ch);
                e.Graphics.DrawString(wkoeftdate, f10b, Brushes.Black, pa, lSF);
                ct += 20;
                // pa = New System.Drawing.RectangleF(l1, ct, 80, ch)
                // e.Graphics.DrawString("Total Qty.", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 80, ct, 10, ch)
                // e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 90, ct, 190, ch)
                // e.Graphics.DrawString(wkoqty, f10b, Brushes.Black, pa, lSF)
                pa = new System.Drawing.RectangleF(l1, ct, 80, ch);
                e.Graphics.DrawString("Material", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 80), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 90), ct, 250, ch);
                e.Graphics.DrawString(wkomtr, f10b, Brushes.Black, pa, lSF);
                double intLine;
                if ((wkomtr == ""))
                {
                    e.Graphics.MeasureString(":", f10b, new SizeF(250, ch), lSF, out chars, out lines);
                }
                else
                {
                    e.Graphics.MeasureString(wkomtr, f10b, new SizeF(250, ch), lSF, out chars, out lines);
                }

                intLine = lines;
                pa = new System.Drawing.RectangleF((l1 + 420), ct, 100, ch);
                e.Graphics.DrawString("Part Name", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 520), ct, 10, ch);
                e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l1 + 530), ct, 250, ch);
                e.Graphics.DrawString(wkoprname, f10b, Brushes.Black, pa, lSF);
                // pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
                // e.Graphics.DrawString("Effective Date", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
                // e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
                // pa = New System.Drawing.RectangleF(l1 + 530, ct, 100, ch)
                // e.Graphics.DrawString(wkoeftdate, f10b, Brushes.Black, pa, lSF)
                ct = (ct
                            + (f10b.Height * lines));
                // e.Graphics.DrawLine(SP1, l1, ct + 20, pw + 50, ct + 20)
                e.Graphics.DrawLine(SP1, l1, ct, (pw + 50), ct);
                // ct += f10bu.Height * 2.4
                // ch -= f10bu.Height * 2.4
                ct = (ct + f10b.Height);
                ch = (ch - f10b.Height);
                pa = new System.Drawing.RectangleF(l1, ct, (l2 - l1), ch);
                e.Graphics.DrawString("OP", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(l2, ct, (l3 - l2), ch);
                e.Graphics.DrawString("Work Centre Name", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(l3, ct, 150, ch);
                e.Graphics.DrawString("Work Description", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l4 + (100 + 30)), ct, ((l5 - l4)
                                + 60), ch);
                e.Graphics.DrawString("Start Date/Time", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l5 + (150 + 30)), ct, ((l6 - l5)
                                + 60), ch);
                e.Graphics.DrawString("End Date/Time", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l8 + (30 + 100)), ct, 0, ch);
                e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l8 + (30 + 100)), ct, (l8 - l7), ch);
                e.Graphics.DrawString("CP", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l9 + (30 + 100)), ct, (l9 - l8), ch);
                e.Graphics.DrawString("RJ", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l10 + (30 + 100)), ct, (l10 - l9), ch);
                e.Graphics.DrawString("Emp ID", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l10 + 150), ct, (l11 - l10), ch);
                e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l10 + (150 + 30)), ct, 100, ch);
                e.Graphics.DrawString("Remark", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF((l10 + 250), ct, 0, ch);
                e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF);
                pa = new System.Drawing.RectangleF(il, it, (pw + 50), ph);
                e.Graphics.DrawLine(SP1, l1, (ct + 40), (pw + 50), (ct + 40));
                ct = (float)(ct + (f10bu.Height * 2.4) + 10);
                ch = (float)(ch - ((f10bu.Height * 2.4) + 10));
                int j;
                bool prtgry = false;
                int tlin;


                var count = this.ds.Tables.Count;

                for (j = bs; (j <= (ds.Tables[0].Rows.Count - 1)); j++)
                {
                    prtgry = !prtgry;
                    // With...
                    ct = (float)(ct + (f10.Height * 0.5));
                    ch = (float)(ch - (f10.Height * 0.5));
                    // measure the maximumn lines
                    double tempdbl;
                    e.Graphics.MeasureString(ds.Tables[0].Rows[j]["CtlPt"].ToString().ToString().Trim(), f10, new SizeF(150, ch), lSF, out chars, out lines);
                    tempdbl = lines;
                    // include measure the remarks
                    if (ds.Tables[0].Rows[j]["Remark"].ToString() == "")
                    {
                        pa = new System.Drawing.RectangleF(l1, (ct - 5), (pw + 25), (float)(f10.Height * (lines + 0.6)));

                        if ((prtgry == true))
                        {
                            e.Graphics.FillRectangle(Brushes.LightGray, pa);
                        }

                        pa = new System.Drawing.RectangleF(l1, ct, (l2 - l1), ch);
                    }
                    else
                    {
                        e.Graphics.MeasureString(ds.Tables[0].Rows[j]["Remark"].ToString().ToString().Trim(), f10, new SizeF(100, ch), lSF, out chars, out  lines);
                        if ((lines < tempdbl))
                        {
                            lines = (int)tempdbl;
                        }

                        pa = new System.Drawing.RectangleF(l1, (ct - 5), (pw + 25), (float)(f10.Height * (lines + 0.6)));
                        if ((prtgry == true))
                        {
                            e.Graphics.FillRectangle(Brushes.LightGray, pa);
                        }

                        pa = new System.Drawing.RectangleF(l1, ct, (l2 - l1), ch);
                    }

                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["OP"].ToString(), f10, Brushes.Black, pa, cSF);
                    pa = new System.Drawing.RectangleF(l2, ct, (l3 - l2), ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["Work Centre Name"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF(l3, ct, 150, ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["CtlPt"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l4 + (100 + 30)), ct, ((l5 - l4)
                                    + 60), ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["Start Time"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l5 + (150 + 30)), ct, ((l6 - l5)
                                    + 60), ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["End Time"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l8 + (30 + 100)), ct, 0, ch);
                    // e.Graphics.DrawString(("Total Hrs"), f10, Brushes.Black, pa, cSF)
                    e.Graphics.DrawString("", f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l8 + (30 + 100)), ct, (l8 - l7), ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["CP"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l9 + (30 + 100)), ct, (l9 - l8), ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["RJ"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l10 + (30 + 100)), ct, (l10 - l9), ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["Emp ID"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l10 + 150), ct, (l11 - l10), ch);
                    // e.Graphics.DrawString(("OP Completed"), f10, Brushes.Black, pa, lSF)
                    e.Graphics.DrawString("", f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l10 + (150 + 30)), ct, 100, ch);
                    e.Graphics.DrawString(ds.Tables[0].Rows[j]["Remark"].ToString(), f10, Brushes.Black, pa, lSF);
                    pa = new System.Drawing.RectangleF((l10 + 250), ct, 0, ch);
                    e.Graphics.DrawString("", f10, Brushes.Black, pa, lSF);
                    if (lines == 0)
                    {
                        tlin = 1;
                        ct = (ct + (float)(f10.Height * (tlin + 0.5)));
                        ch = (ch - (float)(f10.Height * (tlin + 0.5)));
                    }
                    else
                    {
                        tlin = lines;
                        ct = (ct + (float)(f10.Height * (tlin + 0.5)));
                        ch = (ch - (float)(f10.Height * (tlin + 0.5)));
                    }

                    // tlin = lines
                    // ct += f10.Height * (tlin + 0.4)
                    // ch -= f10.Height * (tlin + 0.4)
                    // If ct >= (it + ph) - f10.Height Then
                    //     Exit For
                    // End If
                    // If ct >= (it + ph) - f10.Height * (tlin + 0.5) Then
                    //     Exit For
                    // End If
                    if ((ct >= 1050))
                    {
                        // (it + ph) - f10.Height * (tlin + 0.5) Then
                        break;
                    }

                    if ((ct >= 1050))
                    {
                        // (it + ph) - f10.Height * (tlin + 0.5) Then
                        break;
                    }

                }

                bs = (j + 1);
                if ((j >= ds.Tables[0].Rows.Count))
                {
                    if ((ct >= 1050))
                    {
                        // (it + ph) - f10.Height * (tlin + 0.5) Then
                        pg = 1;
                    }

                    if ((lastpgprinted == false))
                    {
                        if ((ct >= 1050))
                        {
                            // (it + ph) - f10.Height * 14 Then
                            pg = 1;
                        }
                        else
                        {
                            //  '' '' 'Remove the total
                            //  '' ''e.Graphics.DrawLine(SP1, l1, ct, l11 - l1, ct)
                            // ' ''e.Graphics.DrawLine(SP1, l1, ct, 1150, ct)
                            //  ''ct += f10b.Height * 0.4
                            //  ''ch -= f10b.Height * 0.4
                            //  '' ''e.Graphics.DrawLine(SP2, l1, ct, l11 - l1, ct)
                            // ' ''e.Graphics.DrawLine(SP2, l1, ct, 1150, ct)
                            //  ''ct += f10b.Height * 0.4
                            //  ''ch -= f10b.Height * 0.4
                            //  ''ct += f10b.Height * 2
                            //  ''ch -= f10b.Height * 2
                            //  '' ''e.Graphics.DrawLine(SP1, l1, ct, l11 - l1, ct)
                            // ' ''e.Graphics.DrawLine(SP1, l1, ct, 1150, ct)
                            //  ''ct += f10b.Height * 0.4
                            //  ''ch -= f10b.Height * 0.4
                            // ' ''e.Graphics.DrawLine(SP2, l1, ct, 1150, ct)
                            //  '' ''e.Graphics.DrawLine(SP2, l1, ct, l11 - l1, ct)
                            //  ''ct += f10b.Height * 4
                            //  ''ch -= f10b.Height * 4
                            lastpgprinted = true;
                            pg = 3;
                        }

                    }

                }

                // pa = New RectangleF(l1, (il + ph) - 50, l11 - l1, f10.Height * 2)
                pa = new RectangleF(l1, 1104, (l11 - l1), (f10.Height * 2));
                e.Graphics.DrawString(("Page " + curpg), f10, Brushes.Black, pa, cSF);
                curpg++;
                if (pg == 1)
                {
                    //PPC1.Rows++;
                    //e.HasMorePages = true;
                }
                else
                {
                    bs = 0;
                    pg = 1;
                    curpg = 1;
                    e.HasMorePages = false;
                    lastpgprinted = false;
                }

            }
            catch (Exception ex)
            {
                //MsgBox(("There was an error preparing the document." + ("\r\n" + ("\r\n" + ("Error Message :" + ("\r\n" + ex.Message))))), MsgBoxStyle.Exclamation, "Preparation Error");
                RetPrt = false;
            }

        }

        //MAIN CLASS

        // Public sqls As String
        public string WONo;

        // Dim DB As New clsmain
        public string refprint = "False";

        public string cs;

        // = System.Configuration.ConfigurationManager.AppSettings("AMTconn")
        // Dim config As System.Collections.Specialized.NameValueCollection = System.Configuration.ConfigurationManager.AppSettings()
        // Public SqlString As String = config("AMTconn")
        public System.Data.SqlClient.SqlConnection conn;

        public SqlCommand com = new SqlCommand();

        public SqlDataReader rs;

        // Public Function AMT() As Boolean
        //     Dim obj As New frmAMTPrint
        //     obj.Show()
        //     obj.Close()
        // End Function
        // Dim com As New SqlCommand
        private SqlDataAdapter da = new SqlDataAdapter();


        // Dim conn As New SqlConnection
        public string Connect(string CS)
        {
            try
            {
                if (!(conn.State == ConnectionState.Closed))
                {
                    return "Error : Connection is still open. Unable to connect";

                }

                conn = new SqlConnection(CS);
                conn.Open();
                return "Success";
            }
            catch (Exception ex)
            {
                return ("Error : " + ex.Message);
            }

        }

        public string Disconnect()
        {
            try
            {
                if ((conn.State == ConnectionState.Closed))
                {
                    return "Error : Connection is already closed";

                }

                conn.Close();
                return "Success";
            }
            catch (Exception ex)
            {
                return ("Error : " + ex.Message);
            }

        }

        public bool GetConnectionStatus()
        {
            try
            {
                switch (conn.State)
                {
                    case ConnectionState.Broken:
                        throw new Exception("Connection is broken");
                        break;
                    case ConnectionState.Closed:
                        throw new Exception("Connection is closed");
                        break;
                    case ConnectionState.Connecting:
                        throw new Exception("Connection is connecting");
                        break;
                    case ConnectionState.Executing:
                        throw new Exception("Connection is executing");
                        break;
                    case ConnectionState.Fetching:
                        throw new Exception("Connection is fetching");
                        break;
                    case ConnectionState.Open:
                        throw new Exception("Connection is open");
                        break;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;

        }

        public DataSet GetDataset(string CS, string SQLs, string TableName)
        {
            try
            {
                com = new SqlCommand(SQLs, conn);

                DataSet ds = new DataSet();

                da.SelectCommand = com;
                var result = da.Fill(ds, TableName);
           
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public DataSet GetDatasetConnected(string SQLs, string TableName)
        {
            try
            {
                if ((conn.State == ConnectionState.Closed))
                {
                    return null;

                }
                else if (!(conn.State == ConnectionState.Open))
                {
                    return null;

                }

                DataSet ds = new DataSet();
                da = new SqlDataAdapter(SQLs, conn);
                da.Fill(ds, TableName);
                da.Dispose();
                ds.Dispose();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool UpdateRecord(string SQLs)
        {
            try
            {
                if ((conn.State == ConnectionState.Closed))
                {
                    throw new Exception("Connection is closed. Unable to update");

                }
                else if (!(conn.State == ConnectionState.Open))
                {
                    throw new Exception("Connection is busy. Unable to update");

                }

                com = new SqlCommand(SQLs, conn);
                com.ExecuteNonQuery();
                com.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public DataSet ExecStoredProcedure(string CS, string SPName, string TableName)
        {
            try
            {
                DataSet ds = new DataSet();
                da = new SqlDataAdapter(SPName, CS);
                da.Fill(ds, TableName);
                da.Dispose();
                ds.Dispose();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool deleteData(string SQLs, SqlConnection conn)
        {
            try
            {
                SqlCommand com = new SqlCommand(SQLs, conn);
                com.CommandType = CommandType.Text;

                // With...

                conn.Open();
                com.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                //  conn.Close()
            }

        }

        public string ExecProcedureInsert(string CS, string procedurename, SqlParameter[] parm)
        {
            SqlConnection cn = new SqlConnection(CS);
            try
            {
                SqlCommand com = new SqlCommand(procedurename, conn);
                com.CommandType = CommandType.StoredProcedure;

                // With...
                com.Parameters.Add(parm);

                conn.Open();
                com.ExecuteNonQuery();
                conn.Close();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                cn.Close();
            }

        }

        private void AssignParameters(SqlCommand cmd, SqlParameter[] cmdParameters)
        {
            if ((cmdParameters == null))
            {
                return;
            }

            foreach (SqlParameter p in cmdParameters)
            {
                cmd.Parameters.Add(p);
            }

        }

        public string ExecProcedureUpdate(string CS, string procedurename, SqlParameter[] parm)
        {
            SqlConnection cn = new SqlConnection(CS);
            try
            {
                SqlCommand com = new SqlCommand(procedurename, conn);
                com.CommandType = CommandType.StoredProcedure;

                // With...
                com.Parameters.Add(parm);

                conn.Open();
                com.ExecuteNonQuery();
                conn.Close();
                return "Success";
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                cn.Close();
            }

        }

        public DataSet ExecProcedureSelctTable(string CS, string procedurename, SqlParameter[] parm)
        {
            SqlConnection cn = new SqlConnection(CS);
            DataSet ds;
            ds = new DataSet();
            try
            {
                SqlCommand com = new SqlCommand();
                // With...
                this.AssignParameters(com, parm);
                com.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sqlad = new SqlDataAdapter(com);
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                cn.Close();
            }

        }
        public double gettothrs(double st, double ed)
        {
            return 0;
        }
        private void MsgBox(string Message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(),
                                  "myalert", Message, true);

        }

        public void SetConn(string strc)
        {
            conn = new System.Data.SqlClient.SqlConnection(strc);
        }

        public bool printAMT(string WONo, string str, string PName, bool st)
        {
            string wrknoR = "";
            RetPrt = false;
            this.SetConn(str);
            WONo = WONo.ToUpper();
            string[] RowVal = new string[12];
            double recCnt = 0;
            cs = str;
            try
            {
                SqlConnection conn = new SqlConnection(cs);
                string SQLs;
                conn.Open();
                //frmAMTprintpreview pf = new frmAMTprintpreview();
                var pf = this;

                pf.PName = PName;
                pf.ds.Tables.Add("WO");
                pf.ds.Tables[0].Clear();
                pf.ds.Tables[0].Columns.Add("OP", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("Work Centre Name", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("Trans Date", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("Start Time", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("End Time", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("Total Hrs", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("CP", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("RJ", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("Emp ID", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("OP Completed", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("Remark", System.Type.GetType("System.String"));
                pf.ds.Tables[0].Columns.Add("CtlPt", System.Type.GetType("System.String"));

                var count = pf.ds.Tables.Count;

                // use as word description
                SqlDataAdapter da = new SqlDataAdapter();
                // for rework data format 
                // WONo = "sd33990848"
                WONo = WONo.ToString().Trim();
                DataSet dsroute;
                dsroute = new DataSet();
                DataSet dsjobtrans;
                dsjobtrans = new DataSet();
                DataSet dswkDsc;
                dswkDsc = new DataSet();
                SQLs = "select * from tbwc order by _wc";
                dswkDsc = this.GetDataset(cs, SQLs, "dsc");
                SQLs = "";
                DataSet dsItm;
                dsItm = new DataSet();
                SQLs = "select * from tbitem order by _itemcode";
                dsItm = this.GetDataset(cs, SQLs, "itm");
                SQLs = "";
                DataSet dsJob;
                dsJob = new DataSet();
                SQLs = "select * from tbjob order by _job";
                dsJob = this.GetDataset(cs, SQLs, "job");
                SQLs = "";
                string tempjobno;
                string tempsuffix;
                if (((WONo.IndexOf("-") + 1)
                            > 0))
                {
                    tempjobno = WONo.Split('-')[0];
                    if ((WONo.Split('-')[1] == ""))
                    {
                        // 'tempsuffix = "*"
                        tempsuffix = "M";
                    }
                    else
                    {
                        tempsuffix = WONo.Split('-')[1];
                    }

                    // retrieve job route
                    SQLs = ("Select * from tbjobroute where _job=\'"
                                + (tempjobno + "\' order by _operationNo"));
                    dsroute = this.GetDataset(cs, SQLs, "jobroute");
                    if ((dsroute.Tables["jobroute"].Rows.Count == 0))
                    {
                        MsgBox("No records to Print");
                        // TODO: Exit Function: Warning!!! Need to return the value
                        return false;
                    }

                    // retrieve job suffix
                    SQLs = ("select * from tbjobtrans where _job=\'"
                                + (tempjobno + "\' order by _oper_num"));
                    if ((st == false))
                    {
                        SQLs = ("select * from tbjobtrans where _job=\'"
                                    + (tempjobno + ("\' and _jobsuffix=\'"
                                    + (tempsuffix + "\' order by _oper_num"))));
                    }
                    else
                    {
                        SQLs = ("select * from tbjobtransHis where _job=\'"
                                    + (tempjobno + ("\' and _jobsuffix=\'"
                                    + (tempsuffix + "\' order by _oper_num"))));
                    }

                    dsjobtrans = this.GetDataset(cs, SQLs, "suffix");
                    DataSet DSPause = new DataSet();
                    SQLs = ("select * from tbPause where _refID in(select _tansnum from tbjobtrans where _job=\'"
                                + (tempjobno + ("\' and _jobsuffix=\'"
                                + (tempsuffix + "\')"))));
                    DSPause = this.GetDataset(cs, SQLs, "suffix");
                    if (((WONo.IndexOf("-R") + 1)
                                > 0))
                    {
                        if ((st == false))
                        {
                            SQLs = ("select * from tbjobtrans where _job=\'"
                                        + (tempjobno + ("\' and _jobsuffix=\'"
                                        + (tempsuffix + "\' and _Reworkst=\'R\' order by _oper_num"))));
                        }
                        else
                        {
                            SQLs = ("select * from tbjobtransHis where _job=\'"
                                        + (tempjobno + ("\' and _jobsuffix=\'"
                                        + (tempsuffix + "\' and _Reworkst=\'R\' order by _oper_num"))));
                        }

                        dsjobtrans = new DataSet();
                        dsjobtrans = this.GetDataset(cs, SQLs, "suffix");
                        DSPause = new DataSet();
                        SQLs = ("select * from tbPause where _refID in(select _tansnum from tbjobtrans where _job=\'"
                                    + (tempjobno + ("\' and _jobsuffix=\'"
                                    + (tempsuffix + "\')"))));
                        DSPause = this.GetDataset(cs, SQLs, "suffix");
                        SQLs = ("select * from tbjob a,tbjobroute b where " + ("a._job = b._job " + ("and a._job=\'"
                                    + (WONo.Split('-')[0] + "\' order by _operationNO"))));
                        DataSet dsMasterSTG;
                        dsMasterSTG = new DataSet();
                        dsMasterSTG = this.GetDataset(cs, SQLs, "mst");
                        DataView dvstg = new DataView();
                        dvstg.Table = dsMasterSTG.Tables[0];
                        dvstg.RowFilter = ("_operationNo >= " + dsjobtrans.Tables["suffix"].Rows[0]["_oper_num"]);
                        foreach (DataRowView i in dvstg)
                        {
                            recCnt++;
                            if ((recCnt == 1))
                            {
                                DataRow[] drjob;
                                drjob = dsJob.Tables["job"].Select(("_job=\'"
                                                + (tempjobno + "\'")));
                                if ((drjob.Length > 0))
                                {
                                    // 20090528 if reccnt>1, the means the work qty retrieve already, the release qty get 1st record
                                    if ((recCnt > 1))
                                    {

                                    }
                                    else
                                    {
                                        wkoqty = drjob[0]["_qtyreleased"].ToString();
                                        if ((dsjobtrans.Tables["suffix"].Rows.Count > 0))
                                        {
                                            wkoqty = dsjobtrans.Tables["suffix"].Rows[0]["_qty_rele_qty"].ToString();
                                        }

                                    }

                                    if ((drjob[0]["_stas"] == "R"))
                                    {
                                        wkono = WONo;
                                        // & "-R"
                                        wrknoR = wkono;
                                    }
                                    else
                                    {
                                        wrknoR = wkono;
                                        wkono = WONo;
                                    }

                                    string[] tempm = drjob[0]["_material"].ToString().Split('|');

                                    foreach (string tempi in tempm)
                                    {
                                        if (tempi != string.Empty)
                                        {
                                            wkomtr += tempi + "\r\n";
                                        }
                                    }

                                    // wkomtr = drjob(0)("_material")
                                    wkofgcode = drjob[0]["_item"].ToString();
                                    DataRow[] drprt;
                                    drprt = dsItm.Tables[0].Select(("_itemcode=\'"
                                                    + (drjob[0]["_item"].ToString().ToString().Trim() + "\'")));
                                    if ((drprt.Length == 0))
                                    {
                                        wkoptno = "";
                                        wkorevno = "";
                                        wkoprname = "";
                                    }
                                    else
                                    {
                                        // If InStr(drprt(0)("_itemdescription"), "(") > 0 Then
                                        //     Dim temp() As String
                                        //     temp = Split(Trim(drprt(0)("_itemdescription")), "(")
                                        //     wkoprname = temp(0)
                                        //     If InStr(UCase(Trim(drprt(0)("_itemdescription"))), ")") Then
                                        //         temp = Split(temp(1), ")")
                                        //         wkoptno = temp(0)
                                        //         wkorevno = ""
                                        //     Else
                                        //         wkoptno = temp(1)
                                        //         wkorevno = ""
                                        //     End If
                                        // Else
                                        //     wkoptno = ""
                                        //     wkorevno = ""
                                        //     wkoprname = drprt(0)("_itemdescription")
                                        // End If
                                        // 20100311 change to new fields
                                        wkoptno = drprt[0]["_Drawing_Nbr"].ToString();
                                        wkorevno = drprt[0]["_Revision"].ToString();
                                        wkoprname = drprt[0]["_itemdescription"].ToString();
                                    }

                                }

                                wkoeftdate = DateTime.Now.ToString("dd/MM/yyyy");
                            }

                            // this part not sure need to add or not
                            DataRow[] drstg;
                            drstg = dsjobtrans.Tables["suffix"].Select(("_oper_num="
                                            + (i["_operationno"] + " ")));
                            if ((drstg.Length == 0))
                            {
                                RowVal[0] = i["_operationno"].ToString().ToString().Trim().ToUpper();
                                RowVal[1] = i["_wc"].ToString().ToString().Trim().ToUpper();
                                RowVal[2] = DateTime.FromOADate(double.Parse(i["_jobdate"].ToString())).ToString("dd/MM/yyyy");
                                RowVal[3] = "";
                                RowVal[4] = "";
                                RowVal[5] = "";
                                RowVal[6] = "0";
                                RowVal[7] = "0";
                                RowVal[8] = "";
                                RowVal[9] = "";
                                RowVal[10] = "";
                                if ((dswkDsc.Tables[0].Rows.Count == 0))
                                {
                                    RowVal[11] = "";
                                }
                                else
                                {
                                    DataRow[] drn;
                                    drn = dswkDsc.Tables[0].Select(("_wc=\'"
                                                    + (i["_wc"].ToString().ToString().Trim().ToUpper() + "\'")));
                                    if ((drn.Length == 0))
                                    {
                                        RowVal[11] = "";
                                    }
                                    else
                                    {
                                        RowVal[11] = drn[0]["_description"].ToString();
                                    }

                                }

                            }
                            else
                            {
                                RowVal[0] = drstg[0]["_oper_num"].ToString().ToString().Trim().ToUpper();
                                RowVal[1] = drstg[0]["_wc"].ToString().ToString().Trim().ToUpper();
                                RowVal[2] = DateTime.FromOADate(double.Parse(i["_start_date"].ToString())).ToString("dd/MM/yyyy");
                                RowVal[3] = DateTime.FromOADate(double.Parse(i["_start_date"].ToString())).ToString("dd/MM/yyyy HH:mm");
                                if ((RowVal[3] == "30/12/1899 00:00"))
                                {
                                    RowVal[3] = "";
                                }

                                RowVal[4] = DateTime.FromOADate(double.Parse(i["_end_date"].ToString())).ToString("dd/MM/yyyy HH:mm");
                                if ((RowVal[4] == "30/12/1899 00:00"))
                                {
                                    RowVal[4] = "";
                                }

                                DataRow[] DrPause = DSPause.Tables[0].Select(("_refID=" + drstg[0]["_tansnum"]));
                                double topausehr = 0;
                                int intI = 0;
                                if ((DrPause.Length > 0))
                                {
                                    for (intI = 0; (intI
                                                <= (DrPause.Length - 1)); intI++)
                                    {
                                        topausehr = topausehr + this.gettothrs(double.Parse(DrPause[intI]["_pstart"].ToString()), double.Parse(DrPause[intI]["_pend"].ToString()));
                                    }

                                }

                                double tothrs = this.gettothrs(double.Parse(drstg[0]["_start_date"].ToString()), double.Parse(drstg[0]["_end_date"].ToString()));
                                RowVal[5] = (tothrs - topausehr).ToString();
                                RowVal[6] = drstg[0]["_qty_complete"].ToString().ToString().Trim().ToUpper();
                                RowVal[7] = drstg[0]["_qty_scrapped"].ToString().ToString().Trim().ToUpper();
                                if ((drstg[0]["_emp_num"].ToString().ToString().Trim().ToUpper() != ""))
                                {
                                    if ((drstg[0]["_emp_num"].ToString().ToString().Trim().ToUpper().Length > 4))
                                    {
                                        RowVal[8] = drstg[0]["_emp_num"].ToString().ToString().Trim().ToUpper().Substring(0, 4);
                                    }
                                    else
                                    {
                                        RowVal[8] = drstg[0]["_emp_num"].ToString().ToString().Trim().ToUpper();
                                    }

                                }
                                else
                                {
                                    RowVal[8] = drstg[0]["_emp_num"].ToString().ToString().Trim().ToUpper();
                                }

                                RowVal[9] = "";
                                if ((drstg[0]["_Reworkst"] == "R"))
                                {
                                    RowVal[10] = "Rework";
                                }
                                else
                                {
                                    RowVal[10] = drstg[0]["_remarks"].ToString().ToString().Trim().ToUpper();
                                }

                                if ((dswkDsc.Tables[0].Rows.Count == 0))
                                {
                                    RowVal[11] = "";
                                }
                                else
                                {
                                    DataRow[] drnew;
                                    drnew = dswkDsc.Tables[0].Select(("_wc=\'"
                                                    + (drstg[0]["_wc"].ToString().ToString().Trim().ToUpper() + "\'")));
                                    if ((drnew.Length == 0))
                                    {
                                        RowVal[11] = "";
                                    }
                                    else
                                    {
                                        RowVal[11] = drnew[0]["_description"].ToString();
                                    }

                                }

                            }

                            pf.ds.Tables[0].Rows.Add(RowVal);
                        }

                        conn.Close();
                        if ((pf.ds.Tables[0].Rows.Count > 0))
                        {
                            RetPrt = true;
                            pf.Target = pf.PDamt;
                           
                        }
                        else
                        {
                            MsgBox("There are No Records Found");
                        }

                        return RetPrt;
                        // TODO: Exit Function: Warning!!! Need to return the value
             
                        // for rework --------------------end
                    }

                    // 20090701
                    bool refR = false;
                    int stgno = 0;
                    foreach (DataRow v in dsjobtrans.Tables["suffix"].Rows)
                    {
                        recCnt++;
                        if ((recCnt == 1))
                        {
                            wkoqty = v["_qty_rele_qty"].ToString();
                        }

                        // 20090701
                        if ((v["_reworkst"].ToString() == "R"))
                        {
                            refR = true;
                            break;
                        }

                        if ((v["_Reworkst"].ToString() == "R"))
                        {
                            wkono = (WONo + "-R");
                            wkono = WONo;
                            // & "-R"
                            wrknoR = wkono;
                        }
                        else
                        {
                            wrknoR = wkono;
                            wkono = WONo;
                        }

                        // Dim tempm() As String
                        // tempm = Split(v("_material"), "||")
                        // Dim tempi As Integer
                        // For tempi = 0 To tempm.Length - 1
                        //     wkomtr += tempm(tempi) & vbCrLf
                        // Next
                        wkomtr = v["_item"].ToString();
                        wkofgcode = v["_item"].ToString();
                        DataRow[] drprt;
                        drprt = dsItm.Tables[0].Select(("_itemcode=\'"
                                        + (v["_item"].ToString().ToString().Trim() + "\'")));
                        if ((drprt.Length == 0))
                        {
                            wkoptno = "";
                            wkorevno = "";
                            wkoprname = "";
                        }
                        else
                        {
                            // If InStr(drprt(0)("_itemdescription"), "(") > 0 Then
                            //     Dim temp() As String
                            //     temp = Split(Trim(drprt(0)("_itemdescription")), "(")
                            //     wkoprname = temp(0)
                            //     If InStr(UCase(Trim(drprt(0)("_itemdescription"))), ")") Then
                            //         temp = Split(temp(1), ")")
                            //         wkoptno = temp(0)
                            //         wkorevno = ""
                            //     Else
                            //         wkoptno = temp(1)
                            //         wkorevno = ""
                            //     End If
                            // Else
                            //     wkoptno = ""
                            //     wkorevno = ""
                            //     wkoprname = drprt(0)("_itemdescription")
                            // End If
                            // 20100311 change to new field
                            wkoptno = drprt[0]["_Drawing_Nbr"].ToString();
                            wkorevno = drprt[0]["_Revision"].ToString();
                            wkoprname = drprt[0]["_itemdescription"].ToString();
                        }

                        wkoeftdate = DateTime.Now.ToString("dd/MM/yyyy");
                        RowVal[0] = v["_oper_num"].ToString().ToString().Trim().ToUpper();
                        RowVal[1] = v["_wc"].ToString().ToString().Trim().ToUpper();
                        RowVal[2] = DateTime.FromOADate(double.Parse(v["_start_date"].ToString())).ToString("dd/MM/yyyy");
                        RowVal[3] = DateTime.FromOADate(double.Parse(v["_start_date"].ToString())).ToString("dd/MM/yyyy HH:mm");
                        if ((RowVal[3] == "30/12/1899 00:00"))
                        {
                            RowVal[3] = "";
                        }

                        RowVal[4] = DateTime.FromOADate(double.Parse(v["_end_date"].ToString())).ToString("dd/MM/yyyy HH:mm");
                        if ((RowVal[4] == "30/12/1899 00:00"))
                        {
                            RowVal[4] = "";
                        }

                        DataRow[] DrPause = DSPause.Tables[0].Select(("_refID=" + v["_tansnum"]));
                        double topausehr = 0;
                        int intI = 0;
                        if ((DrPause.Length > 0))
                        {
                            for (intI = 0; (intI
                                        <= (DrPause.Length - 1)); intI++)
                            {
                                topausehr = (topausehr + this.gettothrs(double.Parse(DrPause[intI]["_pstart"].ToString()), double.Parse(DrPause[intI]["_pend"].ToString())));
                            }

                        }

                        double tothrs = this.gettothrs(double.Parse(v["_start_date"].ToString()), double.Parse(v["_end_date"].ToString()));
                        RowVal[5] = (tothrs - topausehr).ToString();
                        RowVal[6] = v["_qty_complete"].ToString().ToString().Trim().ToUpper();
                        RowVal[7] = v["_qty_scrapped"].ToString().ToString().Trim().ToUpper();
                        if ((v["_emp_num"].ToString().ToString().Trim().ToUpper() != ""))
                        {
                            if ((v["_emp_num"].ToString().ToString().Trim().ToUpper().Length > 4))
                            {
                                RowVal[8] = v["_emp_num"].ToString().ToString().Trim().ToUpper().Substring(0, 4);
                            }
                            else
                            {
                                RowVal[8] = v["_emp_num"].ToString().ToString().Trim().ToUpper();
                            }

                        }
                        else
                        {
                            RowVal[8] = v["_emp_num"].ToString().ToString().Trim().ToUpper();
                        }

                        RowVal[9] = "";
                        if ((dsjobtrans.Tables["suffix"].Rows.Count == recCnt))
                        {
                            if ((st == false))
                            {
                                RowVal[10] = "";
                            }
                            else
                            {
                                RowVal[10] = "Rework";
                            }

                            // 20090528 if the rwk sts is R
                            if ((v["_Reworkst"] == "R"))
                            {
                                RowVal[10] = "Rework";
                            }

                        }
                        else
                        {
                            RowVal[10] = v["_remarks"].ToString().Trim().ToUpper();
                        }

                        if ((dswkDsc.Tables[0].Rows.Count == 0))
                        {
                            RowVal[11] = "";
                        }
                        else
                        {
                            DataRow[] dr;
                            dr = dswkDsc.Tables[0].Select(("_wc=\'"
                                            + (v["_wc"].ToString().Trim().ToUpper() + "\'")));
                            if ((dr.Length == 0))
                            {
                                RowVal[11] = "";
                            }
                            else
                            {
                                RowVal[11] = dr[0]["_description"].ToString();
                            }

                        }

                        pf.ds.Tables[0].Rows.Add(RowVal);
                        stgno = int.Parse(v["_oper_num"].ToString());
                    }

                    // 20090701
                    if ((refR == true))
                    {

                    }
                    else
                    {
                        DataSet dsmax;
                        dsmax = new DataSet();
                        if ((st == false))
                        {
                            SQLs = ("select * from tbjobtrans where _jobsuffixparent=\'"
                                        + (tempsuffix + ("\' and _jobsuffix<>\'"
                                        + (tempsuffix + ("\' and _job=\'"
                                        + (tempjobno + "\'"))))));
                        }
                        else
                        {
                            SQLs = ("select * from tbjobtransHis where _jobsuffixparent=\'"
                                        + (tempsuffix + ("\' and _jobsuffix<>\'"
                                        + (tempsuffix + ("\' and _job=\'"
                                        + (tempjobno + "\'"))))));
                        }

                        dsmax = this.GetDataset(cs, SQLs, "max");
                        if ((dsmax.Tables["max"].Rows.Count == 0))
                        {
                            DataView dvopno = new DataView();
                            dvopno.Table = dsroute.Tables["jobroute"];
                            dvopno.RowFilter = ("_operationno> "
                                        + (stgno + ""));
                            foreach (DataRowView v in dvopno)
                            {
                                recCnt++;
                                if ((dsJob.Tables["job"].Rows.Count == 0))
                                {
                                    wkoqty = wkoqty;
                                    // v("_qtyReleased")
                                    if ((wrknoR != ""))
                                    {
                                        wrknoR = wkono;
                                    }
                                    else
                                    {
                                        wkono = WONo;
                                    }

                                    wkomtr = "";
                                    wkofgcode = "";
                                    wkoptno = "";
                                    wkorevno = "";
                                    wkoprname = "";
                                    wkoeftdate = DateTime.Now.ToString("dd/MM/yyyy");
                                    wkoqty = wkoqty;
                                }
                                else
                                {
                                    DataRow[] drjob;
                                    drjob = dsJob.Tables["job"].Select(("_job=\'"
                                                    + (tempjobno + "\'")));
                                    if ((drjob.Length > 0))
                                    {
                                        // 20090528 if reccnt>1 the means before have trans record
                                        if ((recCnt > 1))
                                        {

                                        }
                                        else
                                        {
                                            wkoqty = drjob[0]["_qtyreleased"].ToString();
                                        }

                                        if ((drjob[0]["_stas"] == "R"))
                                        {
                                            wkono = WONo;
                                            // & "-R"
                                        }
                                        else
                                        {
                                            wkono = WONo;
                                        }

                                        string[] tempm = drjob[0]["_material"].ToString().Split('|');

                                        foreach (string tempi in tempm)
                                        {
                                            if (tempi != string.Empty)
                                            {
                                                wkomtr += tempi + "\r\n";
                                            }
                                        }

                                        // wkomtr = drjob(0)("_material")
                                        wkofgcode = drjob[0]["_item"].ToString();
                                        DataRow[] drprt;
                                        drprt = dsItm.Tables[0].Select(("_itemcode=\'"
                                                        + (drjob[0]["_item"].ToString().ToString().Trim() + "\'")));
                                        if ((drprt.Length == 0))
                                        {
                                            wkoptno = "";
                                            wkorevno = "";
                                            wkoprname = "";
                                        }
                                        else
                                        {
                                            // If InStr(drprt(0)("_itemdescription"), "(") > 0 Then
                                            //     Dim temp() As String
                                            //     temp = Split(Trim(drprt(0)("_itemdescription")), "(")
                                            //     wkoprname = temp(0)
                                            //     If InStr(UCase(Trim(drprt(0)("_itemdescription"))), ")") Then
                                            //         temp = Split(temp(1), ")")
                                            //         wkoptno = temp(0)
                                            //         wkorevno = ""
                                            //     Else
                                            //         wkoptno = temp(1)
                                            //         wkorevno = ""
                                            //     End If
                                            // Else
                                            //     wkoptno = ""
                                            //     wkorevno = ""
                                            //     wkoprname = drprt(0)("_itemdescription")
                                            // End If
                                            // 20100311 change to new field
                                            wkoptno = drprt[0]["_Drawing_Nbr"].ToString();
                                            wkorevno = drprt[0]["_Revision"].ToString();
                                            wkoprname = drprt[0]["_itemdescription"].ToString();
                                        }

                                    }

                                }

                                wkoeftdate = DateTime.Now.ToString("dd/MM/yyyy");
                                RowVal[0] = v["_operationno"].ToString().Trim().ToUpper();
                                RowVal[1] = v["_wc"].ToString().Trim().ToUpper();
                                RowVal[2] = "";
                                RowVal[3] = "";
                                RowVal[4] = "";
                                RowVal[5] = "";
                                RowVal[6] = "0";
                                RowVal[7] = "0";
                                RowVal[8] = "";
                                RowVal[9] = "";
                                RowVal[10] = "";
                                if ((dswkDsc.Tables[0].Rows.Count == 0))
                                {
                                    RowVal[11] = "";
                                }
                                else
                                {
                                    DataRow[] dr;
                                    dr = dswkDsc.Tables[0].Select(("_wc=\'"
                                                    + (v["_wc"].ToString().Trim().ToUpper() + "\'")));
                                    if ((dr.Length == 0))
                                    {
                                        RowVal[11] = "";
                                    }
                                    else
                                    {
                                        RowVal[11] = dr[0]["_description"].ToString();
                                    }

                                }

                                pf.ds.Tables[0].Rows.Add(RowVal);
                            }

                        }

                    }

                }
                else
                {
                    tempjobno = WONo;
                    tempsuffix = "";
                    SQLs = ("select * from tbjob a,tbjobroute b where " + ("a._job = b._job " + ("and a._job=\'"
                                + (WONo + "\'"))));
                    SqlCommand com = new SqlCommand(SQLs, conn);
                    SqlDataReader dr = com.ExecuteReader();
                    while (dr.Read())
                    {
                        recCnt++;
                        if ((dsJob.Tables["job"].Rows.Count == 0))
                        {
                            wkoqty = dr["_qtyReleased"].ToString();
                            wkono = WONo;
                            wkomtr = "";
                            wkofgcode = "";
                            wkoptno = "";
                            wkorevno = "";
                            wkoprname = "";
                            wkoeftdate = DateTime.Now.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            DataRow[] drjob;
                            drjob = dsJob.Tables["job"].Select(("_job=\'"
                                            + (tempjobno + "\'")));
                            if ((drjob.Length > 0))
                            {
                                // 20090528 if reccnt>1, the means the work qty retrieve already, the release qty get 1st record
                                if ((recCnt > 1))
                                {

                                }
                                else
                                {
                                    wkoqty = drjob[0]["_qtyreleased"].ToString();
                                }

                                if ((drjob[0]["_stas"].ToString() == "R"))
                                {
                                    wkono = WONo;
                                    // & "-R"
                                    wrknoR = wkono;
                                }
                                else
                                {
                                    wrknoR = wkono;
                                    wkono = WONo;
                                }

                                string[] tempm = drjob[0]["_material"].ToString().Split('|');

                                foreach (string tempi in tempm)
                                {
                                    if (tempi != string.Empty)
                                    {
                                        wkomtr += tempi + "\r\n";
                                    }
                                }

                                // wkomtr = drjob(0)("_material")
                                wkofgcode = drjob[0]["_item"].ToString();
                                DataRow[] drprt;
                                drprt = dsItm.Tables[0].Select(("_itemcode=\'"
                                                + (drjob[0]["_item"].ToString().Trim() + "\'")));
                                if ((drprt.Length == 0))
                                {
                                    wkoptno = "";
                                    wkorevno = "";
                                    wkoprname = "";
                                }
                                else
                                {
                                    // If InStr(drprt(0)("_itemdescription"), "(") > 0 Then
                                    //     Dim temp() As String
                                    //     temp = Split(Trim(drprt(0)("_itemdescription")), "(")
                                    //     wkoprname = temp(0)
                                    //     If InStr(UCase(Trim(drprt(0)("_itemdescription"))), ")") Then
                                    //         temp = Split(temp(1), ")")
                                    //         wkoptno = temp(0)
                                    //         wkorevno = ""
                                    //     Else
                                    //         wkoptno = temp(1)
                                    //         wkorevno = ""
                                    //     End If
                                    // Else
                                    //     wkoptno = ""
                                    //     wkorevno = ""
                                    //     wkoprname = drprt(0)("_itemdescription")
                                    // End If
                                    // 20100311 change to new field
                                    wkoptno = drprt[0]["_Drawing_Nbr"].ToString();
                                    wkorevno = drprt[0]["_Revision"].ToString();
                                    wkoprname = drprt[0]["_itemdescription"].ToString();
                                }

                            }

                        }

                        wkoeftdate = DateTime.Now.ToString("dd/MM/yyyy");
                        RowVal[0] = dr["_operationno"].ToString().Trim().ToUpper();
                        RowVal[1] = dr["_wc"].ToString().Trim().ToUpper();
                        RowVal[2] = DateTime.FromOADate(double.Parse(dr["_jobdate"].ToString())).ToString("dd/MM/yyyy");
                        RowVal[3] = "";
                        RowVal[4] = "";
                        RowVal[5] = "";
                        RowVal[6] = "0";
                        RowVal[7] = "0";
                        RowVal[8] = "";
                        RowVal[9] = "";
                        RowVal[10] = "";
                        if ((dswkDsc.Tables[0].Rows.Count == 0))
                        {
                            RowVal[11] = "";
                        }
                        else
                        {
                            DataRow[] drn;
                            drn = dswkDsc.Tables[0].Select(("_wc=\'"
                                            + (dr["_wc"].ToString().Trim().ToUpper() + "\'")));
                            if ((drn.Length == 0))
                            {
                                RowVal[11] = "";
                            }
                            else
                            {
                                RowVal[11] = drn[0]["_description"].ToString();
                            }

                        }

                        pf.ds.Tables[0].Rows.Add(RowVal);
                    }

                    conn.Close();
                }

                if ((pf.ds.Tables[0].Rows.Count > 0))
                {
                    if (((WONo.IndexOf("-") + 1)
                                > 0))
                    {
                        tempjobno = WONo.Split('-')[0];
                    }
                    else
                    {
                        tempjobno = WONo;
                    }

                    DataSet ds;
                    ds = new DataSet();
                    SQLs = ("select * from tbjob where _job=\'"
                                + (tempjobno + "\'"));
                    ds = this.GetDataset(cs, SQLs, "max");
                    wkomtr = "";
                    if (((ds.Tables[0].Rows[0]["_material"].ToString().IndexOf("||") + 1)
                                > 0))
                    {
                        string[] tempm = ds.Tables[0].Rows[0]["_material"].ToString().Split('|');

                        for (int tempi = 0; tempi <= (tempm.Length - 1); tempi++)
                        {
                            if (tempm[tempi] != string.Empty)
                            {
                                if (tempi == (tempm.Length - 1))
                                {
                                    wkomtr = wkomtr + tempm[tempi];
                                }
                                else
                                {
                                    wkomtr = wkomtr + (tempm[tempi] + ",");
                                }

                                // wkomtr += tempm(tempi) & vbCrLf
                            }
                        }
                    }
                    else
                    {
                        wkomtr = ds.Tables[0].Rows[0]["_material"].ToString();
                    }

                    RetPrt = true;
                    pf.Target = pf.PDamt;

                }
                else
                {
                    MsgBox("There are No Records Found");
                }

            }
            catch (Exception ex)
            {
                MsgBox(("There is an error : " + ex.Message));
            }
            finally
            {
                conn.Close();
            }

            return RetPrt;
        }
    }
}