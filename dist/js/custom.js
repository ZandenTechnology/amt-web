$(document).ready(function() {
    $(".k-grid table").addClass("table table-condensed");

    $(".datepicker").kendoDatePicker({
       

        // display month and year in the input
        format: "dd/MM/yyyy",

    });


    $('.upload-control').each(function (index) {

        var hfUploadID = $(this).attr('hfUploadID');
        var containerID = $(this).attr('containerID');

        $(this).kendoUpload({
            multiple: false,
            async: {
                saveUrl: "/Handler/Media/Upload.ashx?id=" + hfUploadID + "&container=" + containerID,
                removeUrl: "remove",
                autoUpload: true
            },
            success: onSingleSuccess,
            error: onError,
            validation: {
                allowedExtensions: [".jpeg", ".jpg", ".png"]
            }
        });
    });
});

$("#grid").find(".k-grid-toolbar").insertAfter($("#grid .k-grid-content"));


function NotifSuccess(Message) {
    setTimeout(function () {
        // create the notification
        var notification = new NotificationFx({
            message: '<p>'+Message+'</p>',
            layout: 'growl',
            effect: 'genie',
            type: 'notice', // notice, warning or error
            ttl: 5000
        });
        notification.show();
    }, 1200);
};

function NotifError(Message) {
    setTimeout(function () {


        // create the notification
        var notification = new NotificationFx({
            message: '<p>'+Message+'</p>',
            layout: 'growl',
            effect: 'genie',
            type: 'error', // notice, warning or error
            ttl: 5000
        });

        notification.show();

    }, 1200);
};

function sortResults(list, prop, asc) {
    list = list.sort(function (a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
    return list;
}


function onSingleSuccess(e) {

    if (e.response.SuccessMessage != "") {
        NotifSuccess(e.response.SuccessMessage);

        $(e.response.UploadedContainer).html(e.response.UploadedContainerContent);
        $(e.response.UploadedID).val(e.response.UploadedURL);
    }
    else {
        NotifError(e.response.ErrorMessage);
    }
}


function onError(e) {
    alert(e.response);
}