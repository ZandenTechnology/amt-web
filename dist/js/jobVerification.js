﻿//Declare Control
mylocalStorage["JobVerification"] = {
    getJobData: function (fromDate, toDate, job, status) {
        var result;

        $.ajax({
            async: false,
            url: "/JobVerification/getJobData",
            data: "{'fromDate': '" + fromDate + "', 'toDate': '" + toDate + "', 'job': '" + job + "', 'status': '" + status + "'}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    },
    getOperationData: function (job) {
        var result;

        $.ajax({
            async: false,
            url: "/JobVerification/getOperationData",
            data: "{'job': '" + job + "'}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    },
    getTransactionData: function (jobNo, operNum) {
        var result;

        $.ajax({
            async: false,
            url: "/JobVerification/getTransactionData",
            data: "{'jobNo': '" + jobNo + "', 'operNum': '" + operNum + "'}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    }
}

var myMPManager = {
    general: {
        gridview: '',
        gridview1: '',
        gridview2: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.txtFromDate = $("#txtFromDate");
            this.txtToDate = $("#txtToDate");
            this.txtJob = $("#txtJob");
            this.ddlStatus = $("#ddlStatus");

            this.gridview1 = $("#grid-operation-details");
            this.modalform = $("#modal-operation-details");
            this.PartxtJobNo = $("#PartxtJobNo");
            this.PartxtReceived = $("#PartxtReceived");
            this.PartxtItem = $("#PartxtItem");
            this.PartxtCompleted = $("#PartxtCompleted");
            this.PartxtReleased = $("#PartxtReleased");
            this.PartxtRejected = $("#PartxtRejected");
            this.PartxtJobDate = $("#PartxtJobDate");

            this.gridview2 = $("#grid-transaction-details");
            this.modalform1 = $("#modal-transaction-details");
            this.Par1txtJobNo = $("#Par1txtJobNo");
            this.Par1txtReceived = $("#Par1txtReceived");
            this.Par1txtItem = $("#Par1txtItem");
            this.Par1txtCompleted = $("#Par1txtCompleted");
            this.Par1txtReleased = $("#Par1txtReleased");
            this.Par1txtRejected = $("#Par1txtRejected");
            this.Par1txtJobDate = $("#Par1txtJobDate");
            this.Par1txtOperNum = $("#Par1txtOperNum");
            this.Par1txtWC = $("#Par1txtWC");

            this.flagInit1 = 0;
            this.flagInit2 = 0;

            $("#btnSearch").click(function () {
                myMPManager.general.refreshGrid();
            });

            //localTempStorage.Products.sync();
            this.initGrid();
            //this.initGrid1();
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
                ({
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                    transport: {
                        read: function (options) {
                            // Implement read

                            //var skip = options.data.skip == undefined ? 1 : options.data.skip;
                            //var take = options.data.take == undefined ? 1 : options.data.take;

                            var data = mylocalStorage.JobVerification.getJobData(
                                myMPManager.general.txtFromDate.val(),
                                myMPManager.general.txtToDate.val(),
                                myMPManager.general.txtJob.val(),
                                myMPManager.general.ddlStatus.val()
                            );
                            options.success(data);
                        },
                        create: function (options) { }
                        ,
                        update: function (options) { }
                    },
                    page: 1,
                    pageSize: 10,
                    batch: true,

                    schema: {
                        //data: "Data",
                        //total: "Total",
                        //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                        model: {
                            id: "_job",
                            fields:
                                {
                                    _job: { type: "string", editable: false },
                                    _jobsuffix: { type: "number", editable: false },
                                    _jobDate: { type: "string", editable: false },
                                    _jobtype: { type: "string", editable: false },
                                    _item: { type: "string", editable: false },
                                    _qtyReleased: { type: "number", editable: false },
                                    _qtyCompleted: { type: "number", editable: false },
                                    _qtyScrapped: { type: "number", editable: false },
                                    color: { type: "string", editable: false }
                                }
                        }
                    },
                })

            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    change: changeData,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_job",
                            title: "Job"
                        },
                        {
                            field: "_jobsuffix",
                            title: "Suffix"
                        },
                        {
                            field: "_jobDate",
                            title: "Released Dates",
                        },
                        {
                            field: "_jobtype",
                            title: "Job Type"
                        },
                        {
                            field: "_item",
                            title: "Item"
                        },
                        {
                            field: "_qtyReleased",
                            title: "Released Qty."
                        },
                        {
                            field: "_qtyCompleted",
                            title: "Completed Qty.",
                            template: function (dataItem) {
                                if (dataItem._qtyCompleted == 0) {
                                    return "-";
                                }
                                else {
                                    return dataItem._qtyCompleted;
                                }
                            }
                        },
                        {
                            field: "_qtyScrapped",
                            title: "Scrapped Qty.",
                            template: function (dataItem) {
                                if (dataItem._qtyScrapped == 0) {
                                    return "-";
                                }
                                else {
                                    return dataItem._qtyScrapped;
                                }
                            }
                        }
                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");

            }

        },

        initGrid1: function () {

            var list1 = new kendo.data.DataSource
                ({
                    //serverPaging: true,
                    //serverFiltering: true,
                    //serverSorting: true,
                    transport: {

                        read: function (options) {
                            // Implement read
                            var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                            var selectedItem = entityGrid.dataItem(entityGrid.select());

                            //var skip = options.data.skip == undefined ? 1 : options.data.skip;
                            //var take = options.data.take == undefined ? 1 : options.data.take;

                            var data1 = {};
                            if (selectedItem != null) {
                                data1 = mylocalStorage.JobVerification.getOperationData(selectedItem._job);
                            }
                            options.success(data1 == undefined ? [] : data1);

                        },
                        create: function (options) {
                        },
                        update: function (options) {
                        }
                    },
                    page: 1,
                    pageSize: 10,
                    batch: true,

                    schema: {
                        //data: "Data",     
                        //total: "Total",
                        //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                        model: {
                            id: "_job",
                            fields:
                                {
                                    _job: { type: "string", editable: false },
                                    _oper_num: { type: "number", editable: false },
                                    _wc: { type: "string", editable: false },
                                    _jobDate: { type: "string", editable: false },
                                    _start_Date: { type: "string", editable: false },
                                    _end_Date: { type: "string", editable: false },
                                    _qty_Rele_qty: { type: "number", editable: false },
                                    _qtyReceived: { type: "number", editable: false },
                                    _qtyCompleted: { type: "number", editable: false },
                                    _qtyScrapped: { type: "number", editable: false },
                                    received: { type: "number", editable: false },
                                    color: { type: "string", editable: false }

                                }
                        }
                    }
                })

            if (list1 != undefined) {

                myMPManager.general.gridview1.kendoGrid({

                    resizable: true,
                    dataSource: list1,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    change: changeData1,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_oper_num",
                            title: "Operation No",
                        },
                        {
                            field: "_wc",
                            title: "Work Center",
                        },
                        {
                            field: "_start_Date",
                            title: "Date/Time Started",
                        },
                        {
                            field: "_end_Date",
                            title: "Date/Time End",
                        },
                        {
                            field: "_qtyReceived",
                            title: "Received",
                        },
                        {
                            field: "_qtyCompleted",
                            title: "Completed",
                        },
                        {
                            field: "_qtyScrapped",
                            title: "Rejected",
                        }
                    ],
                    excelExport: function (e) {
                        // Prevent the default behavior which will prompt the user to save the generated file.
                        e.preventDefault();
                        // Get the Excel file as a data URL.
                        var dataURL = new kendo.ooxml.Workbook(e.workbook).toDataURL();
                        // Strip the data URL prologue.
                        var base64 = dataURL.split(";base64,")[1];
                        // Post the base64 encoded content to the server which can save it.
                        $.post("/server/save", {
                            base64: base64,
                            fileName: "ExcelExport.xlsx"
                        });
                    }
                });

                var grid1 = myMPManager.general.gridview1.data("kendoGrid");
                myMPManager.general.gridview1.find("table").addClass("table table-condensed");
            }
        },

        initGrid2: function () {

            var list2 = new kendo.data.DataSource
                ({
                    //serverPaging: true,
                    //serverFiltering: true,
                    //serverSorting: true,
                    transport: {

                        read: function (options) {
                            // Implement read
                            var entityGrid = myMPManager.general.gridview1.data("kendoGrid");
                            var selectedItem = entityGrid.dataItem(entityGrid.select());

                            //var skip = options.data.skip == undefined ? 1 : options.data.skip;
                            //var take = options.data.take == undefined ? 1 : options.data.take;

                            var data2 = mylocalStorage.JobVerification.getTransactionData(selectedItem._job, selectedItem._oper_num);
                            options.success(data2);

                        },
                        create: function (options) {
                        },
                        update: function (options) {
                        }
                    },
                    page: 1,
                    pageSize: 10,
                    batch: true,

                    schema: {
                        //data: "Data",
                        //total: "Total",
                        //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                        model: {
                            id: "_oper_num",
                            fields:
                                {
                                    _oper_num: { type: "number", editable: false },
                                    _wc: { type: "string", editable: false },
                                    _job: { type: "string", editable: false },
                                    _emp_num: { type: "string", editable: false },
                                    _machineid: { type: "string", editable: false },
                                    _jobsuffix: { type: "string", editable: false },
                                    _no_oper: { type: "number", editable: false },
                                    _qty_op_qty: { type: "number", editable: false },
                                    _qty_complete: { type: "number", editable: false },
                                    _qty_scrapped: { type: "number", editable: false },
                                    _start_Date: { type: "string", editable: false },
                                    _end_Date: { type: "string", editable: false },
                                    _createDate: { type: "string", editable: false },
                                    color: { type: "string", editable: false }
                                }
                        }
                    }
                })

            if (list2 != undefined) {

                myMPManager.general.gridview2.kendoGrid({

                    resizable: true,
                    dataSource: list2,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_emp_num",
                            title: "Operator ID",
                        },
                        {
                            field: "_machineid",
                            title: "Machine ID",
                        },
                        {
                            field: "_jobsuffix",
                            title: "Suffix",
                        },
                        {
                            field: "_no_oper",
                            title: "No. of Operator",
                        },
                        {
                            field: "_qty_op_qty",
                            title: "Received Qty",
                        },
                        {
                            field: "_qty_complete",
                            title: "Completed Qty",
                        },
                        {
                            field: "_qty_scrapped",
                            title: "Rejected Qty",
                        },
                        {
                            field: "_start_Date",
                            title: "Date/Time Started",
                        },
                        {
                            field: "_end_Date",
                            title: "Date/Time End",
                        },
                        {
                            field: "_createDate",
                            title: "Date/Time Transfer"
                        }
                    ],
                });

                var grid2 = myMPManager.general.gridview2.data("kendoGrid");
                myMPManager.general.gridview2.find("table").addClass("table table-condensed");
            }
        },

        refreshGrid: function () {
            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        },
        refreshGrid1: function () {
            myMPManager.general.gridview1.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview1.data('kendoGrid').refresh();
        },
        refreshGrid2: function () {
            myMPManager.general.gridview2.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview2.data('kendoGrid').refresh();
        },
    }
}

var checkedIds = {};


//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                .addClass("k-state-selected")
                .find(".checkbox")
                .attr("checked", "checked");
        }

        var color = view[i].color;

        if (color != "text-muted") {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']").addClass(color);
        }
    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.refreshGrid();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");
    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

function changeData(arg) {
    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());
    if (myMPManager.general.flagInit1 == 0) {
        myMPManager.general.initGrid1();
        myMPManager.general.flagInit1 = 1;
    }
    else {
        myMPManager.general.refreshGrid1();
    }
    var entityGridDetail = myMPManager.general.gridview1.data("kendoGrid");
    var last_index = entityGridDetail._data.length - 1;
    try {
        myMPManager.general.PartxtJobNo.val(selectedItem._job);
        myMPManager.general.PartxtCompleted.val(selectedItem._qtyCompleted);
        myMPManager.general.PartxtItem.val(selectedItem._item);
        myMPManager.general.PartxtJobDate.val(selectedItem._jobDate);
        myMPManager.general.PartxtReceived.val(entityGridDetail._data[last_index].received);
        myMPManager.general.PartxtRejected.val(selectedItem._qtyScrapped);
        myMPManager.general.PartxtReleased.val(selectedItem._qtyReleased);
        myMPManager.general.modalform.modal("show");
        myMPManager.general.gridview1.data("kendoGrid").dataSource.page(1);
    } catch (e) { }

}

function changeData1(arg) {
    var entityGrid = myMPManager.general.gridview1.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    var entityGrid1 = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem1 = entityGrid.dataItem(entityGrid.select());
    if (myMPManager.general.flagInit2 == 0) {
        myMPManager.general.initGrid2()
        myMPManager.general.flagInit2 = 1;
    }
    else {
        myMPManager.general.refreshGrid2();

    }
    try {
        myMPManager.general.Par1txtJobNo.val(selectedItem._job);
        myMPManager.general.Par1txtCompleted.val(selectedItem._qtyCompleted);
        myMPManager.general.Par1txtItem.val(selectedItem._item);
        myMPManager.general.Par1txtJobDate.val(selectedItem1._jobDate);
        myMPManager.general.Par1txtReceived.val(selectedItem._qtyReceived);
        myMPManager.general.Par1txtRejected.val(selectedItem._qtyScrapped);
        myMPManager.general.Par1txtReleased.val(selectedItem1._qtyReleased);
        myMPManager.general.Par1txtOperNum.val(selectedItem._oper_num);
        myMPManager.general.Par1txtWC.val(selectedItem._wc);

        myMPManager.general.modalform1.modal("show");

    } catch (e) { }

}

$(document).ready(function () {
    myMPManager.general.init();
    myMPManager.general.initGrid1();
    myMPManager.general.initGrid2();
});
