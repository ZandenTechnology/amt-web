﻿//Declare Control

var urManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid-roles");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            this.initGrid();
            //Sort

            //urManager.general.popupOrderNavi = $('#form-order').data('kendoWindow');

            //this.ddlState.kendoDropDownList();

            //this.ddlState.change(function () {
            //    urManager.general.refreshGrid();
            //});

            ////Import

            //urManager.general.popupImportNavi = $('#form-import').data('kendoWindow');

            //$('#Import').click(function () {

            //    urManager.general.popupImportNavi.open().center();
            //});

            ////Checkbox
            $('#checkAll').click(function () {
                if ($(this).attr('checked')) {
                    $('.check-box').attr('checked', 'checked');
                } else {
                    $('.check-box').removeAttr('checked');
                }
            });

            $('#grid-items input[type=checkbox][id!=checkAll]').click(function () {
                var numChkBoxes = $('#grid-roles input[type=checkbox][id!=checkAll]').length;
                var numChkBoxesChecked = $('#grid-roles input[type=checkbox][checked][id!=checkAll]').length;
                if (numChkBoxes == numChkBoxesChecked && numChkBoxes > 0) {
                    $('#checkAll').attr('checked', 'checked');
                }
                else {
                    $('#checkAll').attr('checked', '');
                }
            });

            //$('#btnDelete').click(function () {

            //    //SweetAlert begin
            //    swal({
            //        title: "Are you sure?",
            //        text: "You will not be able to recover this data!",
            //        type: "warning", showCancelButton: true,
            //        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
            //        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
            //    }, function (isConfirm) {
            //        if (isConfirm) {
            //            $.LoadingOverlay("show");

            //            setTimeout(function () {
            //                for (var i in checkedIds) {
            //                    if (checkedIds[i]) {

            //                        $.ajax({
            //                            async: false,
            //                            type: "POST",
            //                            url: "/Handler/Products/DeleteProduct.ashx?id=" + i,
            //                            dataType: "json",
            //                            contentType: "application/json; charset=utf-8",
            //                            dataFilter: function (data) { return data; },
            //                            success: function (data) {
            //                                if (!data) {

            //                                    popIt("alert", data.d);
            //                                }
            //                                else {

            //                                    popIt("notification", data.d);
            //                                }
            //                            },
            //                            error: function (XMLHttpRequest, textStatus, errorThrown) {
            //                                if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

            //                                    popIt("alert", XMLHttpRequest.responseText);
            //                                }
            //                                else {

            //                                    popIt("notification", XMLHttpRequest.responseText);
            //                                }
            //                            }
            //                        })


            //                        swal("Deleted!", "Your data has been deleted.", "success");
            //                        $.LoadingOverlay("hide");



            //                    }
            //                }

            //                checkedIds = {};
            //                urManager.general.refreshGrid();

            //            }, 1500);


            //        } else { swal("Cancelled", "Your data is safe :)", "error"); }
            //    });

            //});
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var data = mylocalStorage.RolesPermission.GetAllRoles();
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "id",
                        fields:
                        {
                            RoleID: { type: "string", editable: false },
                            Role: { type: "string", editable: false }
                        }
                    }
                }

            })



            if (list != undefined) {

                urManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: false,
                    selectable: "multiple",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             title: 'Select All',
                             headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                             template: '<input type="checkbox" id="#: id #" class="k-checkbox"><label class="k-checkbox-label" for="#: id #"></label>',
                             width: 80
                         },
                         {
                             field: "RoleID",
                             title: "Role ID",
                             //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                             width: 100
                         },
                        {
                            field: "Role",
                            title: "Role"
                        }
                    ]
                });

                var grid = urManager.general.gridview.data("kendoGrid");

                //bind click event to the checkbox
                grid.table.on("click", ".k-checkbox", selectRow);
                $('#header-chb').change(function (ev) {

                    var checked = ev.target.checked;
                    $('.k-checkbox').each(function (idx, item) {
                        if (checked) {
                            if (!($(item).closest('tr').is('.k-state-selected'))) {
                                $(item).click();
                            }
                        } else {
                            if ($(item).closest('tr').is('.k-state-selected')) {
                                $(item).click();
                            }
                        }
                    });

                });

            }


        },
        refreshGrid: function () {

            urManager.general.gridview.data("kendoGrid").dataSource.read();
            urManager.general.gridview.data('kendoGrid').refresh();
        }
    }
}


function Edit(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    getProductDetailView(item.id);

};

function Delete(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajax({
                async: false,
                type: "POST",
                url: "/Handler/Products/DeleteProduct.ashx?id=" + item.id,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (!data) {

                        popIt("alert", data.d);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        urManager.general.refreshGrid();
                        popIt("notification", data.d);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

                        popIt("alert", XMLHttpRequest.responseText);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        urManager.general.refreshGrid();
                        popIt("notification", XMLHttpRequest.responseText);
                    }
                }
            })


            swal("Deleted!", "Your data has been deleted.", "success");
        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};


function Duplicate(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "",
        text: "Are you sure want to duplicate this products?",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, duplicate it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            getProductDetailViewDuplicate(item.id);
            swal("Duplicated!", "Your data has been duplicated.", "success");

        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = urManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }

}


$(document).ready(function () {
    urManager.general.init();

});
