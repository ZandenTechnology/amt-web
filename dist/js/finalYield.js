﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridAvailable = $("#grid-available-work");
            this.gridAllocated = $("#grid-allocated-work");

            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.hfItemID = $("#hfItemID");
            this.hflistItemData = $("#listItemData");
            this.txtFromDate = $("#txtFromDate");
            this.txtToDate = $("#txtToDate");

            this.listItemData = [];
            this.listAvailable = [];
            this.listAllocated = [];
            this.flagRead = 0;

            //localTempStorage.Products.sync();
            this.initGridAvailable();
            this.initGridAllocated();

            $("#btnCancel").click(function () {
                myMPManager.general.flagRead = 0;
                myMPManager.general.refreshGrid();
            });

            $("#btnSave").click(function () {

                $.LoadingOverlay("show", {
                    minSize: "20px",
                    maxSize: "30px",
                    image: "",
                    fontawesome: "fa fa-spinner fa-spin"
                });

                var listItem = [];
                $.each(myMPManager.general.listAllocated, function (index, item) {
                    listItem.push(item._itemCode);
                });

                var stringWc = JSON.stringify(listItem);

                $.ajax({
                    async: false,
                    url: "/FinalYield/PrintFinalYield",
                    data: "{ 'itemCode': '" + stringWc + "','dateFrom': '" + myMPManager.general.txtFromDate.val() + "', 'dateTo': '" + myMPManager.general.txtToDate.val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        $.LoadingOverlay("hide");

                        if (data["redirect"] != null) {

                            window.open(
                                  data["redirect"],
                                  '_blank' // <- This is what makes it open in a new window.
                                );

                        }
                        if (data["excel"] != null) {

                            window.open(
                                  data["excel"],
                                  '_blank' // <- This is what makes it open in a new window.
                                );

                        }

                        if (data["success"] == undefined) {
                            NotifError(data["error"]);
                        } else {
                            NotifSuccess(data["success"]);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(errorThrown);
                        $.LoadingOverlay("hide");
                    }
                })
            });

            $("#button-right").click(function () {
                //MOVE ITEM RIGHT
                var entityGrid = myMPManager.general.gridAvailable.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.listAllocated.push(selectedItem);
                    myMPManager.general.listAvailable = $.grep(myMPManager.general.listAvailable, function (e) { return e._itemid != selectedItem._itemid });
                    myMPManager.general.refreshGrid();
                }

            });

            $("#button-all-right").click(function () {
                //MOVE ALL ITEM RIGHT
                jQuery.each(myMPManager.general.listAvailable, function (i, val) {
                    myMPManager.general.listAllocated.push(val);
                    myMPManager.general.listAvailable = $.grep(myMPManager.general.listAvailable, function (e) { return e._itemid != val._itemid });
                });
                myMPManager.general.refreshGrid();

            });

            $("#button-left").click(function () {
                //MOVE ITEM LEFT
                var entityGrid = myMPManager.general.gridAllocated.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.listAvailable.push(selectedItem);
                    myMPManager.general.listAllocated = $.grep(myMPManager.general.listAllocated, function (e) { return e._itemid != selectedItem._itemid });
                    myMPManager.general.refreshGrid();
                }
            });

            $("#button-all-left").click(function () {
                //MOVE ALL ITEM LEFT
                jQuery.each(myMPManager.general.listAllocated, function (i, val) {
                    myMPManager.general.listAvailable.push(val);
                    myMPManager.general.listAllocated = $.grep(myMPManager.general.listAllocated, function (e) { return e._itemid != val._itemid });
                });
                myMPManager.general.refreshGrid();

            });


        },

        initGridAvailable: function () {

            var list = new kendo.data.DataSource
            ({
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridAvailable.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        if (selectedItem == null && myMPManager.general.flagRead == 0) {
                            myMPManager.general.listAvailable = mylocalStorage.Item.GetAllItemData(0);
                            myMPManager.general.flagRead = 1;
                        }
                        else if (myMPManager.general.flagRead == 0) {
                            myMPManager.general.listAvailable = mylocalStorage.Item.GetAllItemData(selectedItem._itemid);
                            myMPManager.general.flagRead = 1;
                        }
                        options.success(sortResults(myMPManager.general.listAvailable, "_itemid", true));
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                //page: 1,
                //pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "_itemid",
                        fields:
                        {
                            _itemid: { type: "number", editable: false },
                            _itemCode: { type: "string", editable: false },
                            _itemdescription: { type: "string", editable: false }
                        }
                    }
                }
            });

            if (list != undefined) {

                myMPManager.general.gridAvailable.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    //change: onChangeResource,

                    columns: [
                        {
                            field: "_itemid",
                            title: "Item Code",
                            template: "#:_itemCode#  -  #:_itemdescription#"
                        }
                    ]
                });

                myMPManager.general.gridAllocated.find("table").addClass("table table-condensed");
            }

        },
        refreshGrid: function () {
            try {
                myMPManager.general.gridAvailable.data("kendoGrid").dataSource.read();
                myMPManager.general.gridAvailable.data('kendoGrid').refresh();
            } catch (e) { }
            try {
                myMPManager.general.gridAllocated.data("kendoGrid").dataSource.read();
                myMPManager.general.gridAllocated.data('kendoGrid').refresh();
            } catch (e) { }

        },

        initGridAllocated: function () {

            var list = new kendo.data.DataSource
          ({
              transport: {

                  read: function (options) {
                      // Implement read
                      var entityGrid = myMPManager.general.gridAvailable.data("kendoGrid");
                      var selectedItem = entityGrid.dataItem(entityGrid.select());

                      //if (selectedItem == null && myMPManager.general.flagRead == 0) {
                      //    myMPManager.general.listAllocated = mylocalStorage.Item.GetAllItemData(0);
                      //}
                      //else if (myMPManager.general.flagRead == 0) {
                      //    myMPManager.general.listAllocated = mylocalStorage.Item.GetAllItemData(selectedItem._itemid);
                      //    myMPManager.general.flagRead = 1;
                      //}
                      options.success(sortResults(myMPManager.general.listAllocated, "_itemid", true));

                  },
                  create: function (options) {
                  },
                  update: function (options) {
                  }
              },
              //page: 1,
              //pageSize: 10,
              batch: true,
              schema: {
                  model: {
                      id: "_itemid",
                      fields:
                      {
                          _itemid: { type: "number", editable: false },
                          _itemCode: { type: "string", editable: false },
                          _itemdescription: { type: "string", editable: false }
                      }
                  },
              }

          })

            if (list != undefined) {

                myMPManager.general.gridAllocated.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: function (e) {
                        myMPManager.general.gridAvailable.data('kendoGrid').refresh();
                    },
                    columns: [
                        {
                            field: "_itemid",
                            title: "Selected Item Code",
                            template: "#:_itemCode#  -  #:_itemdescription#"
                        }
                    ]
                });

                ////Add Class
                myMPManager.general.gridAllocated.find("table").addClass("table table-condensed");
            }
        } //END INITGRIDALLOCATION

    }
}

function ViewResetSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        myMPManager.general.flagRead = 0;
        myMPManager.general.gridResource.data("kendoGrid").dataSource.read();
        myMPManager.general.gridResource.data('kendoGrid').refresh();
        myMPManager.general.refreshGrid();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {
    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}

$(document).ready(function () {
    myMPManager.general.init();
});
