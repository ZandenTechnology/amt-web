﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.ddlState = $('#ddlSearch');
            this.ddlCompany = $('#ddlCompany');
            this.txtSearch = $('#txtSearch');
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.hfUserID = $("#hfUserID");
            
            this.mpUserID = $("#UserID");
            this.mpUserName = $("#UserName");
            //this.mpComp = $("#CompID");
            this.mpFirst = $("#FirstName");
            this.mpLast = $("#LastName");
            this.mpBOD = $("#BirthDate");
            this.mpDivision = $("#Division");
            this.mpJobPosition = $("#JobPosition");
            this.mpWorkPhone = $("#WorkPhone");
            this.mpWorkExt = $("#WorkPhoneExt");
            this.mpPhone = $("#PhoneNo");
            this.mpMobileNo = $("#MobileNo");
            this.mpSecMobileNo = $("#SecMobileNo");
            this.mpCorpEmail = $("#CorpEmail");
            this.mpPersonalEmail = $("#PersonalEmail");
            this.hfIsActive = $("#IsActive");

            this.modalform = $("#modal-form");
            this.modalformnew = $("#modal-form-new");

            //localTempStorage.Products.sync();
            this.initGrid();

            //FILTER
            $('#btnSearch').click(function () {

                myMPManager.general.refreshGrid();
                return false;
            });

        

            //new
            $(".btn-new").click(function () {
                myMPManager.general.modalformnew.modal("show");
                urManager.general.initNew();               
                
            });

            //edit
            $(".btn-edit").click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem == null) {
                    alert("Please choose a row from the table");
                }
                else {

                    myMPManager.general.hfUserID.val(selectedItem.UserID);
                    myMPManager.general.mpUserID.val(selectedItem.UserID);
                    myMPManager.general.mpUserName.val(selectedItem.UserName);
                    myMPManager.general.mpFirst.val(selectedItem.FirstName);
                    myMPManager.general.mpLast.val(selectedItem.LastName);

                    var BOD = Date.parse(selectedItem.BirthDate);
                    var date = new Date(BOD).toDateString("yyyy-MM-dd");
                    myMPManager.general.mpBOD.val(date);
                    myMPManager.general.mpDivision.val(selectedItem.Division);
                    myMPManager.general.mpJobPosition.val(selectedItem.JobPosition);
                    myMPManager.general.mpWorkPhone.val(selectedItem.WorkPhone);
                    myMPManager.general.mpWorkExt.val(selectedItem.WorkPhoneExt);
                    myMPManager.general.mpPhone.val(selectedItem.PhoneNo);
                    myMPManager.general.mpMobileNo.val(selectedItem.MobileNo);
                    myMPManager.general.mpSecMobileNo.val(selectedItem.SecMobileNo);
                    myMPManager.general.mpCorpEmail.val(selectedItem.CorpEmail);
                    myMPManager.general.mpPersonalEmail.val(selectedItem.PersonalEmail);
                    myMPManager.general.hfIsActive.val(selectedItem.IsActive);

                    myMPManager.general.modalform.modal("show");

                    if (selectedItem.Gender == "M") {
                        var rbGenderM = document.getElementById("rbGenderM");
                        rbGenderM.checked = true;
                    } else {
                        var rbGenderF = document.getElementById("rbGenderF");
                        rbGenderF.checked = true;
                    }

                    if (selectedItem.IsActive == true) {
                        $("#status-is-active").removeClass("fa-times-circle-o");
                        $("#status-is-active").removeClass("status-inactive");
                        $("#status-is-active").addClass("status-active");
                        $("#status-is-active").addClass("fa-check-circle-o");

                    } else {
                        $("#status-is-active").removeClass("fa-check-circle-o");
                        $("#status-is-active").removeClass("status-active");
                        $("#status-is-active").addClass("status-inactive");
                        $("#status-is-active").addClass("fa-times-circle-o");
                    }
                    urManager.general.init();
                }
            });

            //status changer
            $(".btn-change-active").click(function () {
                if (myMPManager.general.hfIsActive.val() == "true") {
                    myMPManager.general.hfIsActive.val(false);
                    $("#status-is-active").removeClass("status-active");
                    $("#status-is-active").removeClass("fa-check-circle-o");
                    $("#status-is-active").addClass("status-inactive");
                    $("#status-is-active").addClass("fa-times-circle-o");
                } else {
                    myMPManager.general.hfIsActive.val(true);
                    $("#status-is-active").removeClass("status-inactive");
                    $("#status-is-active").removeClass("fa-times-circle-o");
                    $("#status-is-active").addClass("fa-check-circle-o");
                    $("#status-is-active").addClass("status-active");
                }
            });
            
            //btn delete
            $('#btnDelete').click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());
                if (selectedItem == null) {
                    alert("Please choose a row from the table");
                }
                else {
                    //SweetAlert begin
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning", showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            $.LoadingOverlay("show");

                            setTimeout(function () {                       
                                        $.ajax({
                                            async: false,
                                            url: "/UserRole/DeleteUserRole",
                                            data: "{ 'userID': " + selectedItem.UserID + ", 'url': '" + window.location.pathname.replace("/", "") + "' }",
                                            dataType: "json",
                                            type: "POST",
                                            contentType: "application/json; charset=utf-8",
                                            dataFilter: function (data) { return data; },
                                            success: function (data) {
                                                if (data["success"] != undefined && data["success"] == "success") {
                                                    ;
                                                } else if (data["error"] != undefined) {
                                                    alert(data["error"]);
                                                }
                                                myMPManager.general.refreshGrid();
                                            },
                                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                alert(errorThrown);
                                            }
                                        })
                                        swal("Deleted!", "Your data has been deleted.", "success");
                                        $.LoadingOverlay("hide");

                                urManager.general.refreshGrid();

                            }, 1500);


                        } else { swal("Cancelled", "Your data is safe :)", "error"); }
                    });
                }
            });

            //import
            $('#import-excel').click(function () {
                var displayedData = myMPManager.general.gridview.data().kendoGrid.dataSource.view();

                var tbExcel = "<table id='tbExcel' style='display:none;'>" +
                "<tr><th>User ID</th><th>Company Name</th><th>UserName</th><th>FullName</th><th>Gender</th><th>Job Position</th>" +
                "<th>Work No</th><th>Ext No</th><th>Is Active</th><th>Input Date</th><th>Input User</th><th>Modif Date</th><th>Modif User</th></tr>";

                jQuery.each(displayedData, function (i, val) {

                    var InputDateString = val.InputDate == null ? "-" : val.InputDate.toLocaleDateString();
                    var ModifDateString = val.ModifDate == null ? "-" : val.ModifDate.toLocaleDateString();

                    tbExcel = tbExcel + "<tr><td>" + val.UserID + "</td><td>" + val.Company + "</td><td>" + val.UserName  + "</td><td>" + val.FullName + " " + val.LastName + "</td><td>" + val.Gender +
                    "</td><td>" + val.JobPosition + "</td><td>" + val.WorkPhone + "</td><td>" + val.WorkPhoneExt + "</td><td>" + val.IsActive + "</td><td>" + InputDateString +
                    "</td><td>" + val.InputUser + "</td><td>" + val.ModifDateString + "</td><td>" + val.ModifUser + "</td></tr>";
                });

                tbExcel = tbExcel + "</table>";

                $('table#tbExcel').remove();
                $('body').append(tbExcel);

                var date = new Date();

                $('table#tbExcel').table2excel({
                    exclude: ".noExl",
                    name: "Job Export " + date.toLocaleDateString(),
                    filename: "Job Export " + date.toLocaleDateString(),
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            });

            },

                initGrid: function () {

                    var list = new kendo.data.DataSource
                    ({
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        transport: {

                            read: function (options) {
                                // Implement read

                                var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                                var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                                var page = options.data.page == undefined ? 1 : options.data.page;
                                var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                                var skip = options.data.skip == undefined ? 1 : options.data.skip;
                                var take = options.data.take == undefined ? 1 : options.data.take;
                                var searchKey = myMPManager.general.txtSearch.val();

                                var data = mylocalStorage.UserList.GetUserListData( myMPManager.general.ddlState.val(), searchKey, skip, take);
                                options.success(data);
                            },
                            create: function (options) {
                            },
                            update: function (options) {
                            }
                        },
                        page: 1,
                        pageSize: 10,
                        batch: true,
                        schema: {
                            data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                            total: "Total",
                            model: {
                                id: "id",
                                fields:
                                {
                                    UserID: { type: "string", editable: false },
                                    RegistID: { type: "string", editable: false },
                                    UserName: { type: "string", editable: false },
                                    FirstName: { type: "string", editable: false },
                                    LastName: { type: "string", editable: false },
                                    BirthDate: { type: "date", editable: false },
                                    Gender: { type: "string", editable: false },
                                    Division: { type: "string", editable: false },
                                    JobPosition: { type: "string", editable: false },
                                    WorkPhone: { type: "string", editable: false },
                                    WorkPhoneExt: { type: "string", editable: false },
                                    PhoneNo: { type: "string", editable: false },
                                    MobileNo: { type: "string", editable: false },
                                    SecMobileNo: { type: "string", editable: false },
                                    CorpEmail: { type: "string", editable: false },
                                    PersonalEmail: { type: "string", editable: false },
                                    IsActive: { type: "boolean", editable: false },
                                    InputDate: { type: "date", editable: false },
                                    InputUser: { type: "string", editable: false },
                                    ModifDate: { type: "date", editable: false },
                                    ModifUser: { type: "string", editable: false }
                                }
                            }
                        }

                    })



                    if (list != undefined) {

                        myMPManager.general.gridview.kendoGrid({

                            resizable: true,
                            dataSource: list,
                            sortable: false,
                            selectable: "row",
                            editable: "inline",
                            dataBound: onDataBound,
                            filterable: false,
                            //filterable: {
                            //    extra: false,
                            //    operators: {
                            //        string: {
                            //            contains: "Contains"

                            //        }
                            //    }
                            //},
                            pageable: {
                                pageSizes: [10, 25, 50, 100],
                                refresh: true
                            },
                            columns: [
                                //{
                                //    title: 'Select All',
                                //    headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                                //    template: '<input type="checkbox" id="#: id #" class="k-checkbox"><label class="k-checkbox-label" for="#: id #"></label>',
                                //    width: 80
                                //},
                                 {
                                     field: "UserID",
                                     title: "User ID",
                                     //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                                     width: 100
                                 },
                                {
                                    field: "UserName",
                                    title: "UserName"
                                },
                                {
                                    field: "FirstName",
                                    title: "FullName",
                                    template: "#:FirstName# #:LastName#"
                                },
                                {
                                    field: "Gender",
                                    title: "Gender"
                                },
                                 {
                                     field: "JobPosition",
                                     title: "Job Position"
                                 },
                                {
                                    field: "WorkPhone",
                                    title: "Work No"
                                },
                                {
                                    field: "WorkPhoneExt",
                                    title: "Ext No"
                                },
                                {
                                    field: "IsActive",
                                    title: "Is Active"
                                },
                                {
                                    field: "InputDate",
                                    title: "Input Date",
                                    format: "{0:dd/MM/yyyy HH:mm}"
                                },
                                {
                                    field: "InputUser",
                                    title: "Input User"
                                },
                                {
                                    field: "ModifDate",
                                    title: "Modif Date",
                                    format: "{0:dd/MM/yyyy HH:mm}"
                                },
                                {
                                    field: "ModifUser",
                                    title: "Modif User"
                                }
                            ]
                        });

                        var grid = myMPManager.general.gridview.data("kendoGrid");


                        //bind click event to the checkbox
                        grid.table.on("click", ".k-checkbox", selectRow);
                        $('#header-chb').change(function (ev) {

                            var checked = ev.target.checked;
                            $('.k-checkbox').each(function (idx, item) {
                                if (checked) {
                                    if (!($(item).closest('tr').is('.k-state-selected'))) {
                                        $(item).click();
                                    }
                                } else {
                                    if ($(item).closest('tr').is('.k-state-selected')) {
                                        $(item).click();
                                    }
                                }
                            });

                        });

                    }


                },
            refreshGrid: function () {
                myMPManager.general.gridview.data("kendoGrid").dataSource.read();
                myMPManager.general.gridview.data('kendoGrid').refresh();
            }
        }
    }

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }
}


//function bindDTP() {
//    $(".datepicker").kendoDatePicker();
//}



function ViewResetSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.refreshGrid();

        if (myMPManager.general.modalformnew != undefined)
            myMPManager.general.modalformnew.modal("hide");
        if (myMPManager.general.modalform != undefined)
            myMPManager.general.modalform.modal("hide");

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {
    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}

$(document).ready(function () {
    myMPManager.general.init();

});
