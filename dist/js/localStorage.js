
//Declare Control
var mylocalStorage = {
    Item: {
        GetAllItemData: function () {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/Item/GetAllItemData",
                //data: "{ 'itemID' : " + itemID + " }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    Machine: {
        GetMachineByWC: function (wc) {
            var result;

            $.ajax({
                async: false,
                url: "/Machine/GetMachineByWC",
                data: "{ 'wc' : '" + wc + "' }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        GetAllAvailableMachine: function (wc) {
            $.ajax({
                async: false,
                url: "/Machine/GetAllAvailableMachine",
                data: "{ 'wc' : '" + wc + "' }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        GetAllAllocatedMachine: function (wc) {
            $.ajax({
                async: false,
                url: "/Machine/GetAllAllocatedMachine ",
                data: "{ 'wc' : '" + wc + "' }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    JobRoute: {
        getRejectedCodeAll: function (wc, item) {
            var result;

            $.ajax({
                async: false,
                url: "/JobRoute/getRejectedCodeAll",
                data: "{ 'wc' : '" + wc + "', 'item':'" + item + "' }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        getRejectedTransData: function (tansnum) {
            var result;

            $.ajax({
                async: false,
                url: "/JobRoute/getRejectedTransData",
                data: "{ 'tansnum' : " + tansnum + " }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = data;
                    //result["Data"] = jQuery.parseJSON(data["Data"]);
                    //result["Total"] = jQuery.parseJSON(data["Total"]);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        getAllJobLineNumber: function (jobNo) {
            var result;

            $.ajax({
                async: false,
                url: "/JobRoute/getAllJobLineNumber",
                data: "{ 'jobNo' : '" + jobNo + "' }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        getAllJobLineDetail: function (jobNo, lineNo) {
            var result;

            $.ajax({
                async: false,
                url: "/JobRoute/getAllJobLineDetail",
                data: "{ 'jobNo' : '" + jobNo + "', 'lineNo' : " + lineNo + " }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    Print: {
        PrintCOByRG: function () {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/Print/PrintCOByRG",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    }
                    else {
                        NotifError(data["error"]);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    Operator: {
        getAllOperatorData: function (searchString) {
            var result;

            $.ajax({
                async: false,
                url: "/Operator/getAllOperatorData",
                data: "{ 'searchKey' : '" + searchString + "' }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] === undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        console.log(data["error"]);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        getAlloperatorName: function () {
            var result;

            $.ajax({
                async: false,
                url: "/Operator/getAlloperatorName",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    ResourceGroup: {
        GetAllResourceGroupData: function () {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/ResourceGroup/GetAllResourceGroupData",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    Print: {
        PrintCOByRG: function () {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/Print/PrintCOByRG",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },

    WorkCenter: {
        GetAllWorkCenterData: function () {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/WorkCenter/GetAllWorkCenterData",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        GetAllAllocatedWorkCenter: function (_rid) {
            var result;

            $.ajax({
                async: false,
                url: "/WorkCenter/GetAllAllocatedWorkCenter",
                data: "{ '_rid' : " + _rid + " }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        GetAllAvailableWorkCenter: function (_rid) {
            var result;

            $.ajax({
                async: false,
                url: "/WorkCenter/GetAllAvailableWorkCenter",
                data: "{ '_rid' : " + _rid + " }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    RejectedDesc: {
        GetAllRejectedDescData: function (filter, searchKey, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/WorkCenter/GetAllAvailableWorkCenter",
                data: "{ '_rid' : " + _rid + " }",
                type: "POSTFres",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    RejectedDesc: {
        GetAllRejectedDescData: function (_wc, _itemCode) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/RejectedDesc/GetAllRejectedDescData",
                data: "{ '_wc' : '" + _wc + "','_itemCode':'" + _itemCode + "' }",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        getRejectedGrouping: function (wcCode, itemCode) {
            $.ajax({
                async: false,
                url: "/RejectedGrouping/getRejectedGrouping",
                data: "{ '_wc' : '" + wcCode + "','_itemCode' : '" + itemCode + "'}",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    JobOrder: {
        GetAllJobOrderData: function (jobNo, fromDate, toDate, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/JobOrder/GetAllJobOrderData",
                data: "{ 'jobNo': '" + jobNo + "', 'fromDate' : '" + fromDate + "', 'toDate' : '" + toDate +
                      "', 'skip':" + skip + ",'take':" + take + "}",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        GetAllJobDetailsData: function (job, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/JobOrder/GetAllJobDetailsData",
                data: "{ 'job' : '" + job + "', 'skip': '" + skip + "', 'take': '" + take + "'}",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    JobProcess: {
        GetAllJobProcessData: function (jobNo, fromDate, toDate, WC, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/JobProcess/GetAllJobProcessData",
                data: "{ 'jobNo': '" + jobNo + "', 'fromDate' : '" + fromDate + "', 'toDate' : '" + toDate + "' ,'WC' : '" + WC
                    + "', 'skip':" + skip + ",'take':" + take + "}",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    StockList: {
        GetAllStockListData: function (jobNo, fromDate, toDate) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/StockList/GetAllStockListData",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        }
    },
    FormControl: {
        GetAllFormData: function (filter, searchKey, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/FormControl/GetAllFormData",
                data: "{ 'filter': '" + filter + "', 'searchKey': '" + searchKey +
                                "', 'skip' : " + skip +
                                ", 'take' : " + take + ", 'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });

            return result;
        },
        GetSpecificFormCtrl: function (formid) {

            var result;

            $.ajax({
                async: false,
                url: "/FormControl/GetSpecificFormCtrl",
                data: "{ 'FormID': '" + formid + "' ,'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        NotifError(data["error"])
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetSpecificFormCtrlByRolePermission: function (roleid, formid) {

            var result;

            $.ajax({
                async: false,
                url: "/FormControl/GetSpecificFormCtrlByRolePermission",
                data: "{ 'roleID': '" + roleid + "' ,'formID': '" + formid + "' ,'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        NotifError(data["error"])
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetAllControlTypeEnum: function (FormID, btn_type) {
            var result;

            $.ajax({
                async: false,
                url: "/FormControl/GetAllControlTypeEnum",
                data: "{ 'FormID' : '" + FormID + "', 'btn_type': '" + btn_type + "' ,'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        NotifError(data["error"])
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        }
    },
    MenuForm: {
        GetAllMenuFormData: function (filter, searchKey, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/Menu/GetAllMenuFormData",
                data: "{ 'filter': '" + filter + "', 'searchKey': '" + searchKey +
                                "', 'skip' : " + skip +
                                ", 'take' : " + take + ",'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        NotifError(data["error"])
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetAllMenu: function () {
            var result;

            $.ajax({
                async: false,
                url: "/Menu/GetAllMenuForm",
                data: "{ 'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetAllMenuFormByRoleID: function (roleId) {
            var result;

            $.ajax({
                async: false,
                url: "/Menu/GetAllMenuFormByRoleID",
                data: "{ 'url': '" + window.location.pathname.replace("/", "") + "', 'roleId' : '" + roleId + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetAllParentMenu: function () {
            var result;

            $.ajax({
                async: false,
                url: "/Menu/GetAllParentMenu",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        NotifError(data["error"])
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetAllSpecificFormbyMenuID: function (MenuID, RoleID) {
            var result;

            $.ajax({
                async: false,
                url: "/Form/GetAllSpecificFormbyMenuID",
                data: "{ 'MenuID': '" + MenuID + "', 'RoleID' : '" + RoleID + "', 'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        NotifError(data["error"])
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetSpecificFormbyMenuID: function (MenuID, hasForm) {
            var result;

            $.ajax({
                async: false,
                url: "/Form/GetSpecificFormbyMenuID",
                data: "{ 'MenuID': '" + MenuID + "', 'hasForm' : " + hasForm + ", 'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["error"] == undefined) {
                        result = jQuery.parseJSON(data);
                    } else {
                        NotifError(data["error"])
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        }
    },
    RolesPermission: {
        GetAllRolesData: function (filter, searchKey, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/Roles/GetAllRolesData",
                data: "{ 'url': '" + window.location.pathname.replace("/", "") + "', 'filter': '" + filter +
                        "', 'searchKey': '" + searchKey + "', 'skip' : " + skip + ", 'take' : " + take + " }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetAllRoles: function () {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/Services/AMTService.asmx/GetAllRole",
                data: "{ 'filter': '', 'searchKey': '', 'skip' : 0, 'take' : 100 }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = data.d;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        }
    },

    UserRegistration: {
        GetAllRegistrationData: function (company, status, filter, searchKey, skip, take) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/Registration/GetAllRegistrationData",
                data: "{ 'company': '" + company + "', 'status': '" + status + "', 'filter': '" +
                                filter + "', 'searchKey': '" + searchKey +
                                "', 'skip' : " + skip +
                                ", 'take' : " + take + ", 'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        }
    },
    UserList: {
        GetUserListData: function (filter, searchKey, skip, take) {

            var result;

            $.ajax({
                async: false,
                url: "/UserRole/GetUserListData",
                data: "{  'filter': '" +
                                filter + "', 'searchKey': '" + searchKey +
                                "', 'skip' : " + skip +
                                ", 'take' : " + take + ",  'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        },
        GetAllUserRoleDataByUserID: function (userID) {
            //return jQuery.grep(this.array, function (n, i) {
            //    return (n.published.toString() == Published.toString());
            //});
            var result;

            $.ajax({
                async: false,
                url: "/UserRole/GetAllUserRoleDataByUserID",
                data: "{ 'userID': " + userID + " }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    result = jQuery.parseJSON(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    NotifError(errorThrown);
                }
            });
            return result;
        }
    }
}

