﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridResource = $("#grid-work-center");
            this.gridAllocated = $("#grid-allocated-machine");
            this.gridAvailable = $("#grid-available-machine");

            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.txtWcID = $("#txtWcID");
            this.txtWcDesc = $("#txtWcDesc");
            //this.hfmgID = $("#hfmgID");
            this.hfWcID = $("#hfWcID");
            this.hfWcDesc = $("#hfWcDesc");
            this.hflistMachine = $("#listMachine");

            this.listAvailable = [];
            this.listAllocated = [];
            this.flagRead = 0;

            //localTempStorage.Products.sync();
            this.initGridWC();
            this.initGridAllocated();
            this.initGridAvailable();

            $("#btnCancel").click(function () {
                myMPManager.general.flagRead = 0;
                myMPManager.general.refreshGrid();
            });

            $("#btnSave").click(function () {
                var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());
                //jQuery.map:  
                var listMachine = [];
                $.each(myMPManager.general.listAllocated, function (index, item) {
                    listMachine.push(item._mid);
                });

                myMPManager.general.hflistMachine.val(JSON.stringify(listMachine));
                return true;
            });

            $("#button-right").click(function () {
                //MOVE WORK CENTER RIGHT
                var entityGrid = myMPManager.general.gridAvailable.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.listAllocated.push(selectedItem);
                    myMPManager.general.listAvailable = $.grep(myMPManager.general.listAvailable, function (e) { return e._machineID != selectedItem._machineID });
                    myMPManager.general.refreshGrid();
                }

            });

            $("#button-all-right").click(function () {
                //MOVE ALL WORK CENTER RIGHT
                jQuery.each(myMPManager.general.listAvailable, function (i, val) {
                    myMPManager.general.listAllocated.push(val);
                    myMPManager.general.listAvailable = $.grep(myMPManager.general.listAvailable, function (e) { return e._machineID != val._machineID });
                });
                myMPManager.general.refreshGrid();

            });

            $("#button-left").click(function () {
                //MOVE WORK CENTER LEFT
                var entityGrid = myMPManager.general.gridAllocated.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.listAvailable.push(selectedItem);
                    myMPManager.general.listAllocated = $.grep(myMPManager.general.listAllocated, function (e) { return e._machineID != selectedItem._machineID });
                    myMPManager.general.refreshGrid();
                }
            });

        },

        initGridWC: function () {

            var list = new kendo.data.DataSource
           ({
               transport: {

                   read: function (options) {
                       // Implement read
                       if (myMPManager.general.flagRead == 0) {
                           var data = mylocalStorage.WorkCenter.GetAllWorkCenterData();
                       }
                       options.success(data);
                   },
                   create: function (options) {
                   },
                   update: function (options) {
                   }
               },
               batch: true,
               schema: {
                   model: {
                       id: "_wcid",
                       fields:
                       {
                           _wcid: { type: "number", editable: false },
                           _wc: { type: "string", editable: false },
                           _description: { type: "string", editable: false }
                       }
                   }
               }

           })



            if (list != undefined) {

                myMPManager.general.gridResource.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: onChangeResource,

                    columns: [
                        {
                            field: "_description",
                            title: "Work Center",
                            template: "#:_wc# - #:_description#"
                        }
                    ]
                });

                myMPManager.general.gridResource.find("table").addClass("table table-condensed");
            }

        },
        refreshGrid: function () {
            try {
                myMPManager.general.gridAvailable.data("kendoGrid").dataSource.read();
                myMPManager.general.gridAvailable.data('kendoGrid').refresh();
            } catch (e) { }

            try {
                myMPManager.general.gridAllocated.data("kendoGrid").dataSource.read();
                myMPManager.general.gridAllocated.data('kendoGrid').refresh();
            } catch (e) { }

        },

        initGridAvailable: function () {

            var list = new kendo.data.DataSource
            ({
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        if (selectedItem == null && myMPManager.general.flagRead == 0) {
                            myMPManager.general.listAvailable = mylocalStorage.Machine.GetAllAvailableMachine(0);
                        }
                        else if (myMPManager.general.flagRead == 0) {
                            myMPManager.general.listAvailable = mylocalStorage.Machine.GetAllAvailableMachine(selectedItem._wc);
                        }
                        options.success(sortResults(myMPManager.general.listAvailable, "_mid", true));
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                //page: 1,
                //pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "_mid",
                        fields:
                        {
                            _mid: { type: "number", editable: false },
                            _machineID: { type: "string", editable: false },
                            _MachineDesc: { type: "string", editable: false },
                        }
                    }
                }

            })

            if (list != undefined) {

                myMPManager.general.gridAvailable.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: function (e) {
                        myMPManager.general.gridAllocated.data('kendoGrid').refresh();
                    },
                    columns: [
                        {
                            field: "_machineID",
                            title: "Available Machine"
                        }
                    ]
                });

                ////Add Class
                myMPManager.general.gridAvailable.find("table").addClass("table table-condensed");
            }
        }, //END INITGRIDALLOCATION

        initGridAllocated: function () {

            var list = new kendo.data.DataSource
            ({
              transport: {

                  read: function (options) {
                      // Implement read
                      var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
                      var selectedItem = entityGrid.dataItem(entityGrid.select());

                      if (selectedItem == null && myMPManager.general.flagRead == 0) {
                          myMPManager.general.listAllocated = mylocalStorage.Machine.GetAllAllocatedMachine(0);
                      }
                      else if (myMPManager.general.flagRead == 0) {
                          myMPManager.general.listAllocated = mylocalStorage.Machine.GetAllAllocatedMachine(selectedItem._wc);
                          myMPManager.general.flagRead = 1;
                      }
                      options.success(sortResults(myMPManager.general.listAllocated, "_mid", true));

                  },
                  create: function (options) {
                  },
                  update: function (options) {
                  }
              },
              //page: 1,
              //pageSize: 10,
              batch: true,
              schema: {
                  model: {
                      id: "_mid",
                      fields:
                      {
                          _mid: { type: "number", editable: false },
                          _machineID: { type: "string", editable: false },
                          _MachineDesc: { type: "string", editable: false },
                      }
                  }
              }

          })

            if (list != undefined) {

                myMPManager.general.gridAllocated.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: function (e) {
                        myMPManager.general.gridAvailable.data('kendoGrid').refresh();
                    },
                    columns: [
                        {
                            field: "_machineID",
                            title: "Allocated Machine"
                        }
                    ]
                });

                ////Add Class
                myMPManager.general.gridAllocated.find("table").addClass("table table-condensed");
            }
        } //END INITGRIDALLOCATION

    }
}

function ViewResetSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {



        myMPManager.general.flagRead = 0;
        myMPManager.general.gridResource.data("kendoGrid").dataSource.read();
        myMPManager.general.gridResource.data('kendoGrid').refresh();
        myMPManager.general.refreshGrid();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {
    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}

function onChangeResource(arg) {
    var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());
    myMPManager.general.flagRead = 0;

    myMPManager.general.txtWcID.val(selectedItem._wc);
    myMPManager.general.txtWcDesc.val(selectedItem._description);
    myMPManager.general.hfWcID.val(selectedItem._wc);
    myMPManager.general.hfWcDesc.val(selectedItem._description);
    myMPManager.general.initGridAvailable();
    myMPManager.general.initGridAllocated();

    $("#button-right").removeAttr("disabled");
    $("#button-all-right").removeAttr("disabled");
    $("#button-left").removeAttr("disabled");
    $("#btnSave").removeAttr("disabled");
}

$(document).ready(function () {
    myMPManager.general.init();

    $("#button-right").attr("disabled", "disabled");
    $("#button-all-right").attr("disabled", "disabled");
    $("#button-left").attr("disabled", "disabled");
    $("#btnSave").attr("disabled", "disabled");

});
