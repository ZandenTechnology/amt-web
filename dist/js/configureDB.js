﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            $("#btnSave").click(function () {
                var flagWrong = false;
                swal({
                    title: "Are you sure you want to change the DB Name?",
                    text: "If you are sure, type in your password:",
                    type: "input",
                    inputType: "password",
                    showCancelButton: true,
                    closeOnConfirm: false
                }, function (typedPassword) {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "/Configure/CheckPassword/",
                        data: "{'password':'"+typedPassword+"'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            if (data["success"] != undefined) {
                                $("#btnExecute").click();
                                swal.close()
                            }
                            else {
                                flagWrong = true;
                                
                            }                            
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {
                                popIt("alert", XMLHttpRequest.responseText);
                            }
                            else {
                                popIt("notification", XMLHttpRequest.responseText);
                            }
                        }
                    })
                    if (flagWrong == true) {
                        swal("Wrong Password!", "Wrong Password Inserted!", "error");
                        flagWrong = false;
                    }
                    //console.log(typedPassword);
                });
            });
        },
    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        //swal("Success!", "Database References Changed!", "success");
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);
    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {

    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

$(document).ready(function () {
    myMPManager.general.init();
});
