﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.txtSearch = $("#txtSearchID");
            this.txtId = $("#txtId");
            this.txtOpID = $("#txtOpID");
            this.txtOpName = $("#txtOpName");
            this.hfSkip = $("#_skip");
            this.hfSplit = $("#_split");
            this.hfPrint = $("#_print");
            this.cbSkip = $("#cbSkip");
            this.cbSplit = $("#cbSplit");
            this.cbPrint = $("#cbPrint");
            this.cbActive = $("#cbActive");


            //localTempStorage.Products.sync();
            this.initGrid();

            $("#txtSearchID").change(function () {
                myMPManager.general.gridview.data("kendoGrid").dataSource.read();
                myMPManager.general.gridview.data('kendoGrid').refresh();
            });

            $("#btnCancel").click(function () {
                $("#btnSave").removeAttr("disabled");
                myMPManager.general.txtId.val("0");
                myMPManager.general.txtOpID.val("");
                myMPManager.general.txtOpID.removeAttr("readonly");
                myMPManager.general.txtOpName.val("");
                myMPManager.general.hfSkip.val("N");
                myMPManager.general.hfSplit.val("N");
                myMPManager.general.hfPrint.val("N");
                myMPManager.general.cbPrint.removeAttr("checked");
                myMPManager.general.cbSkip.removeAttr("checked");
                myMPManager.general.cbSplit.removeAttr("checked");
                myMPManager.general.cbActive.removeAttr("checked");

                myMPManager.general.refreshGrid();
            });

            myMPManager.general.cbSkip.change(function () {
                if ($("#cbSkip").is(":checked")) {
                    myMPManager.general.hfSkip.val('Y');
                } else {
                    myMPManager.general.hfSkip.val('N');
                }
            });

            myMPManager.general.cbSplit.change(function () {
                if ($("#cbSplit").is(":checked")) {
                    myMPManager.general.hfSplit.val('Y');
                } else {
                    myMPManager.general.hfSplit.val('N');
                }
            });

            myMPManager.general.cbPrint.change(function () {
                if ($("#cbPrint").is(":checked")) {
                    myMPManager.general.hfPrint.val('Y');
                } else {
                    myMPManager.general.hfPrint.val('N');

                }
            });

            $("#btnDelete").click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());
                if (selectedItem == null) {
                    alert("Please choose a row from the table");
                }
                else {
                    //SweetAlert begin
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning", showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            $.LoadingOverlay("show");

                            setTimeout(function () {
                                $.ajax({
                                    async: false,
                                    url: "/Operator/deleteOperator",
                                    data: "{ 'oid': '" + selectedItem._oid + "', 'url': '" + window.location.pathname.replace("/", "") + "' }",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataFilter: function (data) { return data; },
                                    success: function (data) {
                                        if (data["success"] != undefined) {
                                            swal("Deleted!", data["success"], "success");
                                            $("#btnCancel").trigger('click');
                                        } else if (data["error"] != undefined) {
                                            swal("Oops!", data["error"], "error");
                                        }
                                        myMPManager.general.refreshGrid();
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        alert(errorThrown);
                                    }
                                })
                                $.LoadingOverlay("hide");
                            }, 1500);


                        } else { swal("Cancelled", "Your data is safe :)", "error"); }
                    });
                }
            });
        },

        initGrid: function () {

            var list = new kendo.data.DataSource

            ({
                transport: {

                    read: function (options) {
                        // Implement read
                        var searchString = myMPManager.general.txtSearch.val();
                        var data = mylocalStorage.Operator.getAllOperatorData(searchString);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "_oid",
                        fields:
                        {
                            _oid: { type: "numeric", editable: false },
                            _operatorID: { type: "string", editable: false },
                            _operatorName: { type: "string", editable: false },
                            _skip: { type: "string", editable: false },
                            _split: { type: "string", editable: false },
                            _print: { type: "string", editable: false },
                            _active: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    change: onChange,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_operatorID",
                            title: "Operator ID"
                        },
                        {
                            field: "_operatorName",
                            title: "Operator Name"
                        },
                        {
                            field: "_skip",
                            title: "Skip"
                        },
                        {
                            field: "_split",
                            title: "Split"
                        },
                        {
                            field: "_print",
                            title: "Print"
                        },
                        {
                            field: "_active",
                            title: "Active"
                        }
                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");
            }


        },
        refreshGrid: function () {
            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        },
    }
}

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');
    
    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        $("#btnCancel").trigger("click");

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {

    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

function onChange(arg) {
    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    $("#btnSave").attr("disabled", "disabled")

    myMPManager.general.txtId.val(selectedItem._oid);
    myMPManager.general.txtOpID.attr("readonly", "readonly");
    myMPManager.general.txtOpID.val(selectedItem._operatorID);
    myMPManager.general.txtOpName.val(selectedItem._operatorName);
    myMPManager.general.hfPrint.val(selectedItem._print)
    myMPManager.general.hfSkip.val(selectedItem._skip)
    myMPManager.general.hfSplit.val(selectedItem._split)

    if (selectedItem._skip == "Y") {
        myMPManager.general.cbSkip.attr('checked', 'checked')
    } else {
        myMPManager.general.cbSkip.removeAttr("checked")
    }

    if (selectedItem._split == "Y") {
        myMPManager.general.cbSplit.attr('checked', 'checked')
    } else {
        myMPManager.general.cbSplit.removeAttr("checked")
    }

    if (selectedItem._print == "Y") {
        myMPManager.general.cbPrint.attr('checked', 'checked')
    } else {
        myMPManager.general.cbPrint.removeAttr("checked")
    }
}

$(document).ready(function () {
    myMPManager.general.init();

});
