﻿//Declare Control

var fcManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        btnType: '',

        init: function () {
            this.gridview = $("#control-grid");
            this.gridviewnew = $("#control-grid-new");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            if (fcManager.general.btnType == "btn_edit")
            {
                this.initGrid();
            }
            else 
            {
                this.initGridNew();
            }

            this.hfListControl = $("#ListControl");
            this.hfListControlnew = $("#ListControlNew");

            $('#btnSave').live('click', function (e) {

                //jQuery.map:
                var data = $.grep(fcManager.general.gridview.data("kendoGrid").dataSource.data(), function (e) { return e.ctrlActive });

                var listMenu = [];
                $.each(data, function (index, item) {
                    var obj = {
                        Ctrl: item.Ctrl,
                        CtrlType: item.CtrlType,
                        Label: item.Label
                    }
                    listMenu.push(obj);
                });

                fcManager.general.hfListControl.val(JSON.stringify(listMenu));
                return true;
            });

            $('#btnSave-new').live('click', function (e) {

                    //jQuery.map:
                var data = $.grep(fcManager.general.gridviewnew.data("kendoGrid").dataSource.data(), function (e) { return e.ctrlActive });

                    var listMenu = [];
                    $.each(data, function (index, item) {
                        var obj = {
                            Ctrl: item.Ctrl,
                            CtrlType: item.CtrlType,
                            Label: item.Label
                        }
                        listMenu.push(obj);
                    });

                    fcManager.general.hfListControlnew.val(JSON.stringify(listMenu));
                    return true;
                });

        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        var data = mylocalStorage.FormControl.GetAllControlTypeEnum(selectedItem.FormID, fcManager.general.btnType);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "Ctrl",
                        fields:
                        {
                            ctrlActive: { type: "boolean", editable: true },
                            Ctrl: { type: "string", editable: false },
                            CtrlType: { type: "string", editable: false },
                            Label: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (list != undefined) {

                fcManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                    editable: "inline",
                    selectable: "row",
                    filterable: false,
                    selectable: "multiple",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            title: 'Select All',
                            field: "ctrlActive",
                            headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: "<input type=\"checkbox\" id=\"#: Ctrl #\" class=\"k-checkbox check_row\" #= ctrlActive ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"#: Ctrl #\"></label>",
                            width: 50,
                            filterable: false
                        },
                        {
                            field: "Ctrl",
                            title: "Ctrl",
                            width: 80
                        },
                        {
                            field: "CtrlType",
                            title: "CtrlType",
                            width: 80
                        }                
                    ]
                });

                //Adding click event on check box 
                $('.check_row').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, fcManager.general.gridview);
                });

                $("#header-chb").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = fcManager.general.gridview.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = fcManager.general.gridview.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("ctrlActive", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        fcManager.general.gridview.data("kendoGrid").clearSelection();
                    }

                });

                var grid = fcManager.general.gridview.data("kendoGrid");          

                ////Add Class
                fcManager.general.gridview.find("table").addClass("table table-condensed");
            }


        },

        initGridNew: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        var data = mylocalStorage.FormControl.GetAllControlTypeEnum('', fcManager.general.btnType);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "Ctrl",
                        fields:
                        {
                            ctrlActive: { type: "boolean", editable: true },
                            Ctrl: { type: "string", editable: false },
                            CtrlType: { type: "string", editable: false },
                            Label: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (list != undefined) {

                fcManager.general.gridviewnew.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                    editable: "inline",
                    selectable: "row",
                    filterable: false,
                    selectable: "multiple",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            title: 'Select All',
                            field: "ctrlActive",
                            headerTemplate: '<input type="checkbox" id="header-chb-new" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: "<input type=\"checkbox\" id=\"#: Ctrl #\" class=\"k-checkbox check_row\" #= ctrlActive ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"#: Ctrl #\"></label>",
                            width: 50,
                            filterable: false
                        },
                        {
                            field: "Ctrl",
                            title: "Ctrl",
                            width: 80
                        },
                        {
                            field: "CtrlType",
                            title: "CtrlType",
                            width: 80
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, fcManager.general.gridviewnew);
                });

                $("#header-chb-new").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = fcManager.general.gridviewnew.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = fcManager.general.gridviewnew.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("ctrlActive", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        fcManager.general.gridviewnew.data("kendoGrid").clearSelection();
                    }

                });

                var grid = fcManager.general.gridviewnew.data("kendoGrid");

                ////Add Class
                fcManager.general.gridviewnew.find("table").addClass("table table-condensed");
            }


        },
        refreshGridNew: function () {

            fcManager.general.gridviewnew.data("kendoGrid").dataSource.read();
            fcManager.general.gridviewnew.data('kendoGrid').refresh();
        }
    }
}

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = fcManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }

}

function setValueToGridData(val, grid) {
    //Selecting Grid
    var grid = grid.data("kendoGrid");
    //Getting selected Item(s)
    var selectedItems = grid.select();
    selectedItems.each(function (index, row) {
        var selectedItem = grid.dataItem(row);
        //Setting value to data item; so that we can read updated data
        selectedItem.set('ctrlActive', val);
    });
}

$(document).ready(function () {


});
