
//Declare Control

var rmManager = {
    general: {
        gridview: '',
        gridviewupdate: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        lblformtitle: '',
        gridviewcontrol: '',
        lblformtitleupdate: '',
        gridviewcontrolupdate: '',

        hflistmenu: '',
        hflistcontrol: '',

        hflistmenuupdate: '',
        hflistcontrolupdate: '',

        btnSave: '',
        btnUpdate: '',

        listControls: [],

        init: function () {
            this.gridview = $("#grid-menu");
            this.gridviewupdate = $("#grid-menu-update");

            this.lblformtitle = $("#lbl-form-title");
            this.gridviewcontrol = $("#grid-ctrl");

            this.lblformtitleupdate = $("#lbl-form-title-update");
            this.gridviewcontrolupdate = $("#grid-ctrl-update");

            this.hflistmenu = $("#hf-list-menu");
            this.hflistcontrol = $("#hf-list-control");

            this.hflistmenuupdate = $("#hf-list-menu-update");
            this.hflistcontrolupdate = $("#hf-list-control-update");

            this.btnSave = $("#btnSave");
            this.btnUpdate = $("#btnUpdate");


            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            this.initGrid();

            //Biz Source 
            bindBizSource();

            //Area
            bindAreaProcess();

            //Adding click event on btn
            this.btnSave.live('click', function (e) {

                //jQuery.map:
                //var data = $.grep(rmManager.general.gridview.data("kendoGrid").dataSource.data(), function (e) { return e.IsMenuActive });

                var listMenu = [];
                $.each(rmManager.general.gridview.data("kendoGrid").dataSource.data(), function (index, item) {
                    var obj = {
                        RoleID: myMPManager.general.hfRoleID.val(),
                        MenuID: item.MenuID,
                        IsMenuActive: item.IsMenuActive
                    }
                    listMenu.push(obj);
                });


                var listControl = [];

                $.each(rmManager.general.listControls, function (index, item) {
                    var obj = {
                        RoleID: myMPManager.general.hfRoleID.val(),
                        CtrlID: item.CtrlID,
                        IsActive: item.IsActive
                    }
                    listControl.push(obj);
                });

                rmManager.general.hflistcontrol.val(JSON.stringify(listControl));
                rmManager.general.hflistmenu.val(JSON.stringify(listMenu));

                return true;
            });

            this.btnUpdate.live('click', function (e) {

                //jQuery.map:
                //var data = $.grep(rmManager.general.gridview.data("kendoGrid").dataSource.data(), function (e) { return e.IsMenuActive });

                var listMenu = [];
                $.each(rmManager.general.gridviewupdate.data("kendoGrid").dataSource.data(), function (index, item) {
                    var obj = {
                        RoleID: myMPManager.general.hfRoleID.val(),
                        MenuID: item.MenuID,
                        IsMenuActive: item.IsMenuActive
                    }
                    listMenu.push(obj);
                });


                var listControl = [];

                $.each(rmManager.general.listControls, function (index, item) {
                    var obj = {
                        RoleID: myMPManager.general.hfRoleID.val(),
                        CtrlID: item.CtrlID,
                        IsActive: item.IsActive
                    }
                    listControl.push(obj);
                });

                rmManager.general.hflistcontrolupdate.val(JSON.stringify(listControl));
                rmManager.general.hflistmenuupdate.val(JSON.stringify(listMenu));

                return true;
            });


        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var data = mylocalStorage.MenuForm.GetAllMenuFormByRoleID("");
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "MenuID",
                        fields:
                        {
                            IsMenuActive: { type: "boolean", editable: true },
                            MenuID: { type: "string", editable: false },
                            ParentID: { type: "string", editable: false },
                            MenuLvl: { type: "number", editable: false },
                            Menu: { type: "string", editable: false }
                        }
                    }
                }

            })



            if (list != undefined) {
                if (rmManager.general.gridview.data("kendoGrid") != undefined) {
                    rmManager.general.gridview.data("kendoGrid").destroy();
                }

                if (rmManager.general.gridviewcontrol.data("kendoGrid") != undefined) {
                    rmManager.general.initGridCtrl("");
                }

                rmManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,


                    selectable: "row",
                    editable: "inline",
                    change: onChange,

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            title: 'Select All',
                            field: "IsMenuActive",
                            headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: "<input type=\"checkbox\" id=\"#: MenuID #\" class=\"k-checkbox check_row\" #= IsMenuActive ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"#: MenuID #\"></label>",
                            width: 50,
                            filterable: false
                        },

                        {
                            field: "MenuID",
                            title: "ID",
                            width: 60
                        },
                        {
                            field: "MenuLvl",
                            title: "Level",
                            width: 80
                        },
                        {
                            field: "Menu",
                            title: "Menu Name"
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, rmManager.general.gridview);
                });

                $("#header-chb").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = rmManager.general.gridview.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = rmManager.general.gridview.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("IsMenuActive", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        rmManager.general.gridview.data("kendoGrid").clearSelection();
                    }

                });

                //Add Class
                rmManager.general.gridview.find("table").addClass("table table-condensed");
            }


        },
        refreshGrid: function () {

            if (rmManager.general.gridview.data("kendoGrid") != undefined) {
                rmManager.general.gridview.data("kendoGrid").dataSource.read();
                rmManager.general.gridview.data('kendoGrid').refresh();
            }
        },

        //UPDATE
        initGridUpdate: function () {
            if (rmManager.general.gridviewupdate.data("kendoGrid") != undefined) {
                rmManager.general.gridviewupdate.data("kendoGrid").destroy();
            }
            if (rmManager.general.gridviewcontrolupdate.data("kendoGrid") != undefined) {
                rmManager.general.initGridCtrlUpdate("");
            }



            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var data = mylocalStorage.MenuForm.GetAllMenuFormByRoleID(myMPManager.general.hfRoleID.val());
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "MenuID",
                        fields:
                        {
                            IsMenuActive: { type: "boolean", editable: true },
                            MenuID: { type: "string", editable: false },
                            ParentID: { type: "string", editable: false },
                            MenuLvl: { type: "number", editable: false },
                            Menu: { type: "string", editable: false }
                        }
                    }
                }

            })



            if (list != undefined)
            {

                //UPDATE
                rmManager.general.gridviewupdate.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,

                    selectable: "row",
                    editable: "inline",
                    change: onChangeUpdate,

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             title: 'Select All',
                             field: "IsMenuActive",
                             headerTemplate: '<input type="checkbox" id="header-chb-update" class="k-checkbox"><label class="k-checkbox-label" for="header-chb-update"></label>',
                             template: "<input type=\"checkbox\" id=\"update-#: MenuID #\" class=\"k-checkbox check_row_update\" #= IsMenuActive ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"update-#: MenuID #\"></label>",
                             width: 50,
                             filterable: false
                         },
                         {
                             field: "MenuID",
                             title: "ID",
                             width: 60
                         },
                        {
                            field: "MenuLvl",
                            title: "Level",
                            width: 80
                        },
                        {
                            field: "Menu",
                            title: "Menu Name"
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row_update').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, rmManager.general.gridviewupdate);
                });

                $("#header-chb-update").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = rmManager.general.gridviewupdate.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = rmManager.general.gridviewupdate.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("IsMenuActive", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        rmManager.general.gridviewupdate.data("kendoGrid").clearSelection();
                    }

                });


                //Add Class
                rmManager.general.gridviewupdate.find("table").addClass("table table-condensed");
            }

        },
        refreshGridUpdate: function () {

            if (rmManager.general.gridviewupdate.data("kendoGrid") != undefined) {
                rmManager.general.gridviewupdate.data("kendoGrid").dataSource.read();
                rmManager.general.gridviewupdate.data('kendoGrid').refresh();
            }
        },

        //ADD GRID CONTROL
        initGridCtrl: function (formId) {

            var list = new kendo.data.DataSource
            ({
                transport: {

                    read: function (options) {
                        // Implement read
                        var data = mylocalStorage.FormControl.GetSpecificFormCtrlByRolePermission(myMPManager.general.hfRoleID.val(), formId);

                        $.each(rmManager.general.listControls, function (index, item) {
                            //Setting value to data item; so that we can read updated data
                            item.set('IsActive', item.IsActive);

                            var getdata = $.grep(data, function (e) { return e.CtrlID == item.CtrlID });

                            if (getdata[0] != undefined) {
                                getdata[0].IsActive = item.IsActive;
                            }
                        });


                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "CtrlID",
                        fields:
                        {
                            IsActive: { type: "boolean", editable: true },
                            CtrlID: { type: "string", editable: false },
                            Ctrl: { type: "string", editable: false },
                            Label: { type: "string", editable: false },
                        }
                    }
                }

            })



            if (list != undefined) {

                rmManager.general.gridviewcontrol.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,

                    selectable: "row",
                    editable: "inline",

                    filterable: true,
                    //pageable: {
                    //    pageSizes: [10, 25, 50, 100],
                    //    refresh: true
                    //},
                    columns: [
                         {
                             title: 'Select All',
                             field: "IsActive",
                             headerTemplate: '<input type="checkbox" id="header-chb-ctrl" class="k-checkbox"><label class="k-checkbox-label" for="header-chb-ctrl"></label>',
                             template: "<input type=\"checkbox\" id=\"ctrl-#: CtrlID #\" class=\"k-checkbox check_row_ctrl\" #= IsActive ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"ctrl-#: CtrlID #\"></label>",
                             width: 50,
                             filterable: false
                         },
                         {
                             field: "CtrlID",
                             title: "CtrlID",
                             width: 100
                         },
                        {
                            field: "Ctrl",
                            title: "Ctrl Name"
                        },
                        {
                            field: "Label",
                            title: "Ctrl Label"
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row_ctrl').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridCtrlData(checked, rmManager.general.gridviewcontrol);
                });

                $("#header-chb-ctrl").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = rmManager.general.gridviewcontrol.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = rmManager.general.gridviewcontrol.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("IsActive", checked);

                        rmManager.general.listControls = $.grep(rmManager.general.listControls, function (e) { return e.CtrlID != item.CtrlID });

                        rmManager.general.listControls.push(item);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        rmManager.general.gridviewcontrol.data("kendoGrid").clearSelection();
                    }

                });


                //Add Class
                rmManager.general.gridviewcontrol.find("table").addClass("table table-condensed");

            }


        },
        refreshGridCtrl: function () {

            if (rmManager.general.gridviewcontrol.data("kendoGrid") != undefined) {
                rmManager.general.gridviewcontrol.data("kendoGrid").dataSource.read();
                rmManager.general.gridviewcontrol.data('kendoGrid').refresh();
            }
        },


        //UPDATE GRID CONTROL
        initGridCtrlUpdate: function (formId) {

            var list = new kendo.data.DataSource
            ({
                transport: {

                    read: function (options) {
                        // Implement read
                        var data = mylocalStorage.FormControl.GetSpecificFormCtrlByRolePermission(myMPManager.general.hfRoleID.val(), formId);

                        $.each(rmManager.general.listControls, function (index, item) {
                            //Setting value to data item; so that we can read updated data
                            item.set('IsActive', item.IsActive);

                            var getdata = $.grep(data, function (e) { return e.CtrlID == item.CtrlID });

                            if (getdata[0] != undefined) {
                                getdata[0].IsActive = item.IsActive;
                            }
                        });


                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "CtrlID",
                        fields:
                        {
                            IsActive: { type: "boolean", editable: true },
                            CtrlID: { type: "string", editable: false },
                            Ctrl: { type: "string", editable: false },
                            Label: { type: "string", editable: false },
                        }
                    }
                }

            })



            if (list != undefined) {

                rmManager.general.gridviewcontrolupdate.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,

                    selectable: "row",
                    editable: "inline",

                    filterable: true,
                    //pageable: {
                    //    pageSizes: [10, 25, 50, 100],
                    //    refresh: true
                    //},
                    columns: [
                         {
                             title: 'Select All',
                             field: "IsActive",
                             headerTemplate: '<input type="checkbox" id="header-chb-ctrl-update" class="k-checkbox"><label class="k-checkbox-label" for="header-chb-ctrl-update"></label>',
                             template: "<input type=\"checkbox\" id=\"ctrl-update-#: CtrlID #\" class=\"k-checkbox check_row_ctrl_update\" #= IsActive ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"ctrl-update-#: CtrlID #\"></label>",
                             width: 50,
                             filterable: false
                         },
                         {
                             field: "CtrlID",
                             title: "CtrlID",
                             width: 100
                         },
                        {
                            field: "Ctrl",
                            title: "Ctrl Name"
                        },
                        {
                            field: "Label",
                            title: "Ctrl Label"
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row_ctrl_update').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridCtrlData(checked, rmManager.general.gridviewcontrolupdate);
                });

                $("#header-chb-ctrl-update").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = rmManager.general.gridviewcontrolupdate.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = rmManager.general.gridviewcontrolupdate.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("IsActive", checked);

                        rmManager.general.listControls = $.grep(rmManager.general.listControls, function (e) { return e.CtrlID != item.CtrlID });

                        rmManager.general.listControls.push(item);


                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        rmManager.general.gridviewcontrolupdate.data("kendoGrid").clearSelection();
                    }

                });


                //Add Class
                rmManager.general.gridviewcontrolupdate.find("table").addClass("table table-condensed");

            }


        },
        refreshGridCtrlUpdate: function () {

            if (rmManager.general.gridviewcontrolupdate.data("kendoGrid") != undefined) {
                rmManager.general.gridviewcontrolupdate.data("kendoGrid").dataSource.read();
                rmManager.general.gridviewcontrolupdate.data('kendoGrid').refresh();

            }
        }

    }
}


function setValueToGridData(val, grid) {
    //Selecting Grid
    var grid = grid.data("kendoGrid");
    //Getting selected Item(s)
    var selectedItems = grid.select();
    selectedItems.each(function (index, row) {
        var selectedItem = grid.dataItem(row);
        //Setting value to data item; so that we can read updated data
        selectedItem.set('IsMenuActive', val);

    });
}

function setValueToGridCtrlData(val, grid) {
    //Selecting Grid
    var grid = grid.data("kendoGrid");
    //Getting selected Item(s)
    var selectedItems = grid.select();
    selectedItems.each(function (index, row) {
        var selectedItem = grid.dataItem(row);
        //Setting value to data item; so that we can read updated data
        selectedItem.set('IsActive', val);

        rmManager.general.listControls = $.grep(rmManager.general.listControls, function (e) { return e.CtrlID != selectedItem.CtrlID });

        //if (val) {
        //    rmManager.general.listControls.push(selectedItem);
        //}
        
        rmManager.general.listControls.push(selectedItem);

    });
}

$(document).ready(function () {


});

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        if (myMPManager.general.modalform.hasClass('in')) {
            rmManager.general.refreshGridCtrl();
            rmManager.general.refreshGrid();
            myMPManager.general.modalform.modal("hide");
        }

        if (myMPManager.general.modalformupdate.hasClass('in')) {
            rmManager.general.refreshGridCtrlUpdate();
            rmManager.general.refreshGridUpdate();
            myMPManager.general.modalformupdate.modal("hide");
        }

        myMPManager.general.refreshGrid();
        rmManager.general.listControls = [];

        NotifSuccess(data["success"]);


    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {

    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}


//On Change
function onChange(arg) {
    var grid = rmManager.general.gridview.data("kendoGrid");
    var item = this.dataItem(grid.select()[0]);

    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    var form = mylocalStorage.MenuForm.GetAllSpecificFormbyMenuID(item.MenuID, selectedItem.RoleID);
    if (form.length > 0) {
        $("#header-chb-ctrl").removeAttr('checked');
        rmManager.general.lblformtitle.html(form[0].Caption);
        rmManager.general.initGridCtrl(form[0].FormID);
    }
    else {

        rmManager.general.lblformtitle.html('');
        rmManager.general.initGridCtrl('');
    }
}

function onChangeUpdate(arg) {
    var grid = rmManager.general.gridviewupdate.data("kendoGrid");
    var item = this.dataItem(grid.select()[0]);

    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    var form = mylocalStorage.MenuForm.GetAllSpecificFormbyMenuID(item.MenuID, selectedItem.RoleID);

    if (form.length > 0) {
        $("#header-chb-ctrl-update").removeAttr('checked');
        rmManager.general.lblformtitleupdate.html(form[0].Caption);
        rmManager.general.initGridCtrlUpdate(form[0].FormID);
    }
    else {

        rmManager.general.lblformtitleupdate.html('');
        rmManager.general.initGridCtrlUpdate('');
    }
}


//List Control

function Update(e) {

    var item = e.data.models[0];

    if (item.ID == "") {
        item.ID = NewGuid();
    }
    else {
        myPOSManager.general.listVariants = $.grep(myPOSManager.general.listVariants, function (e) { return e.ID != item.ID });
    }

    myPOSManager.general.listVariants.push(item);
    myPOSManager.general.refreshGrid();


    return true;
}