﻿//Declare Control
mylocalStorage["Machine"] = {
    GetAllMachineData: function (filter, searchKey, skip, take) {
        var result;

        $.ajax({
            async: false,
            url: "/Machine/GetAllMachineData",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    }
}

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.txtMId = $("#txtMId");
            this.txtMachineID = $("#txtMachineID");
            this.txtMachineDesc = $("#txtMachineDesc");
            this.initGrid();
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
                transport: {
                    read: function (options) {
                        // Implement read
                        //var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                        //var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                        //var page = options.data.page == undefined ? 1 : options.data.page;
                        //var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                        //var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        //var take = options.data.take == undefined ? 1 : options.data.take;
                        //var searchKey = myMPManager.general.txtSearch.val();
                        var data = mylocalStorage.Machine.GetAllMachineData();
                        options.success(data);
                    },
                    create: function (options) { },
                    update: function (options) { }
                },
                page: 1,
                pageSize: 10,
                batch: true,

                schema: {
                    //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "_mid",
                        fields:
                        {
                            _machineID: { type: "string", editable: true },
                            _MachineDesc: { type: "string", editable: true }
                        }
                    }
                },



            })

            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    change: changeData,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_machineID",
                            title: "Machine ID"
                        },
                        {
                            field: "_MachineDesc",
                            title: "Description",
                        }
                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");

            }

        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        },
    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.refreshGrid();
        $("#btnCancel").click();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

function changeData(arg) {
    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());
    try {
        myMPManager.general.txtMId.val(selectedItem._mid);
        myMPManager.general.txtMachineID.val(selectedItem._machineID);
        myMPManager.general.txtMachineID.attr("disabled", true);
        myMPManager.general.txtMachineDesc.val(selectedItem._MachineDesc);
    } catch (e) { }
}

$(document).ready(function () {
    myMPManager.general.init();
    $('#btnCancel').click(function () {
        myMPManager.general.txtMachineID.removeAttr("disabled");
        myMPManager.general.txtMId.val("");
        $("#grid").data("kendoGrid").clearSelection();
    });
});