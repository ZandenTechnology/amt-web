﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.ddlState = $('#ddlSearch');
            this.ddlCompany = $('#ddlCompany');
            this.ddlStatus = $('#ddlStatus');
            this.txtSearch = $('#txtSearch');
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.hfRegistID = $('#RegistID');
            this.mpFirst = $('#FirstName');
            this.mpCorp = $('#CorpEmail');
            this.mpGender = $('#Gender');
            this.mpComp = $('#txt-comp');
            this.mpDivision = $('#Division');
            this.mpApply = $('#ApplyDate');
            this.mpApvRem = $('#ApvRemarks');

            this.hfRegistIDDec = $('#id-regist-dec');
            this.mpFirstDec = $('#first-dec');
            this.mpCorpDec = $('#corp-dec');
            this.mpGenderDec = $('#gender-dec');
            this.mpCompDec = $('#txt-comp-dec');
            this.mpDivisionDec = $('#div-dec');
            this.mpApplyDec = $('#apply-dec');
            this.mpApvRemDec = $('#apv-dec');

            this.modalformaccept = $('#modal-form-accept');
            this.modalformdecline = $('#modal-form-decline');

            //localTempStorage.Products.sync();
            this.initGrid();

            //FILTER
            $('#btnSearch').click(function () {

                myMPManager.general.refreshGrid();
                return false;
            });

            //Checkbox
            $('#checkAll').click(function () {
                if ($(this).attr('checked')) {
                    $('.check-box').attr('checked', 'checked');
                } else {
                    $('.check-box').removeAttr('checked');
                }
            });

            $('#grid-items input[type=checkbox][id!=checkAll]').click(function () {
                var numChkBoxes = $('#grid-items input[type=checkbox][id!=checkAll]').length;
                var numChkBoxesChecked = $('#grid-items input[type=checkbox][checked][id!=checkAll]').length;
                if (numChkBoxes == numChkBoxesChecked && numChkBoxes > 0) {
                    $('#checkAll').attr('checked', 'checked');
                }
                else {
                    $('#checkAll').attr('checked', '');
                }
            });

            //accept registration
            $("#btn-accept").click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.modalformaccept.modal("show");

                    var Apply = Date.parse(selectedItem.ApplyDate);
                    var date = new Date(Apply).toDateString("yyyy-MM-dd");
                    myMPManager.general.hfRegistID.val(selectedItem.RegistID);
                    myMPManager.general.mpFirst.val(selectedItem.FirstName);
                    myMPManager.general.mpCorp.val(selectedItem.CorpEmail);
                    myMPManager.general.mpGender.val(selectedItem.Gender);
                    myMPManager.general.mpComp.val(selectedItem.FullName);
                    myMPManager.general.mpDivision.val(selectedItem.Division);
                    myMPManager.general.mpApply.val(date);
                    myMPManager.general.mpApvRem.val(selectedItem.ApvRemarks);
                }
                else {
                    alert("Please select a row");
                }
            });

            //decline registration
            $("#btn-decline").click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.modalformdecline.modal("show");

                    var Apply = Date.parse(selectedItem.ApplyDate);
                    var date = new Date(Apply).toDateString("yyyy-MM-dd");
                    myMPManager.general.hfRegistIDDec.val(selectedItem.RegistID);
                    myMPManager.general.mpFirstDec.val(selectedItem.FirstName);
                    myMPManager.general.mpCorpDec.val(selectedItem.CorpEmail);
                    myMPManager.general.mpGenderDec.val(selectedItem.Gender);
                    myMPManager.general.mpCompDec.val(selectedItem.FullName);
                    myMPManager.general.mpDivisionDec.val(selectedItem.Division);
                    myMPManager.general.mpApplyDec.val(date);
                    myMPManager.general.mpApvRemDec.val(selectedItem.ApvRemarks);
                }
                else {
                    alert("Please select a row");
                }
            });

            //import
            $('#import-excel').click(function () {
                var displayedData = myMPManager.general.gridview.data().kendoGrid.dataSource.view();

                var tbExcel = "<table id='tbExcel' style='display:none;'>" +
                "<tr><th>Company Name</th><th>Corp Email</th><th>Full Name</th><th>Gender</th><th>Division</th><th>Job Position</th><th>Apply Date</th><th>Status</th></tr>";

                jQuery.each(displayedData, function (i, val) {
                    var ApplyDateString = val.ApplyDate == null ? "-" : val.ApplyDate.toLocaleDateString();

                    tbExcel = tbExcel + "<tr><td>" + val.FullName + "</td><td>" + val.CorpEmail + "</td><td>" + val.FirstName + " " + val.LastName + "</td>" +
                        "<td>" + val.Gender + "</td><td>" + val.Division + "</td>" +
                        "<td>" + val.JobPosition + "</td><td>" + ApplyDateString + "</td><td>" + val.Status + "</td></tr>";
                });

                tbExcel = tbExcel + "</table>";

                $('table#tbExcel').remove();
                $('body').append(tbExcel);

                var date = new Date();

                $('table#tbExcel').table2excel({
                    exclude: ".noExl",
                    name: "Job Export " + date.toLocaleDateString(),
                    filename: "Job Export " + date.toLocaleDateString(),
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            });

         



        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read

                        var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                        var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                        var page = options.data.page == undefined ? 1 : options.data.page;
                        var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;
                        var searchKey = myMPManager.general.txtSearch.val();

                        var data = mylocalStorage.UserRegistration.GetAllRegistrationData(myMPManager.general.ddlCompany.val(), myMPManager.general.ddlStatus.val(), myMPManager.general.ddlState.val(), searchKey, skip, take);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "Total",
                    model: {
                        id: "id",
                        fields:
                        {
                            FullName: { type: "string", editable: false },
                            RegistID: { type: "string", editable: false },
                            CompID: { type: "string", editable: false },
                            FirstName: { type: "string", editable: false },
                            LastName: { type: "string", editable: false },
                            Gender: { type: "string", editable: false },
                            Division: { type: "string", editable: false },
                            JobPosition: { type: "string", editable: false },
                            CorpEmail: { type: "string", editable: false },
                            ApplyDate: { type: "date", editable: false },
                            Status: { type: "string", editable: false },
                            ApvBy: { type: "string", editable: false },
                            ApvDate: { type: "date", editable: false },
                            ApvRemarks: { type: "string", editable: false },
                            InputDate: { type: "date", editable: false },
                            InputUser: { type: "string", editable: false },
                            ModifDate: { type: "date", editable: false },
                            ModifUser: { type: "string", editable: false }
                        }
                    }
                }

            })



            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: false,
                    //filterable: {
                    //    extra: false,
                    //    operators: {
                    //        string: {
                    //            contains: "Contains"

                    //        }
                    //    }
                    //},
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    selectable: "row",
                    columns: [
                        //{
                        //    title: 'Select All',
                        //    headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                        //    template: '<input type="checkbox" id="#: id #" class="k-checkbox"><label class="k-checkbox-label" for="#: id #"></label>',
                        //    width: 80
                        //},
                         {
                             field: "FullName",
                             title: "Company Name",
                             //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                             width: 100
                         },
                        {
                            field: "CorpEmail",
                            title: "Corp Email"
                        },
                        {
                            field: "FirstName",
                            title: "Full Name",
                            template: "#:FirstName# #:LastName#"
                        },
                        {
                            field: "Gender",
                            title: "Gender"
                        },
                        {
                            field: "Division",
                            title: "Division"
                        },
                         {
                             field: "JobPosition",
                             title: "Job Position"
                         },
                        {
                            field: "ApplyDate",
                            title: "Apply Date",
                            format: "{0:dd/MM/yyyy}"
                        },
                        {
                            field: "Status",
                            title: "Status"
                        }
                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");


                //bind click event to the checkbox
                grid.table.on("click", ".k-checkbox", selectRow);
                $('#header-chb').change(function (ev) {

                    var checked = ev.target.checked;
                    $('.k-checkbox').each(function (idx, item) {
                        if (checked) {
                            if (!($(item).closest('tr').is('.k-state-selected'))) {
                                $(item).click();
                            }
                        } else {
                            if ($(item).closest('tr').is('.k-state-selected')) {
                                $(item).click();
                            }
                        }
                    });

                });

            }


        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        }
    }
}


function Edit(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    getProductDetailView(item.id);

};

function Delete(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajax({
                async: false,
                type: "POST",
                url: "/Handler/Products/DeleteProduct.ashx?id=" + item.id,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (!data) {

                        popIt("alert", data.d);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", data.d);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

                        popIt("alert", XMLHttpRequest.responseText);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", XMLHttpRequest.responseText);
                    }
                }
            })


            swal("Deleted!", "Your data has been deleted.", "success");
        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};


function Duplicate(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "",
        text: "Are you sure want to duplicate this products?",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, duplicate it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            getProductDetailViewDuplicate(item.id);
            swal("Duplicated!", "Your data has been duplicated.", "success");

        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }

}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.modalformaccept.modal("hide");
        myMPManager.general.modalformdecline.modal("hide");
        myMPManager.general.refreshGrid();

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {

    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}


function bindCompDDL() {
    try {
        $('.ddl-comp').kendoDropDownList({
            dataTextField: "Company",
            dataValueField: "CompID",
            dataSource: mylocalStorage.CompData.GetAllCompany("btn_edit"),
            filter: "contains"
        })
    } catch (e) {

    }
}

$(document).ready(function () {
    myMPManager.general.init();

    bindCompDDL();
});
