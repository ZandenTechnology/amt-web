﻿//Declare Control

var rmManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid-menu");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            this.initGrid();

            //Checkbox
            $('#checkAll').click(function () {
                if ($(this).attr('checked')) {
                    $('.check-box').attr('checked', 'checked');
                } else {
                    $('.check-box').removeAttr('checked');
                }
            });

            $('#grid-items input[type=checkbox][id!=checkAll]').click(function () {
                var numChkBoxes = $('#grid-items input[type=checkbox][id!=checkAll]').length;
                var numChkBoxesChecked = $('#grid-items input[type=checkbox][checked][id!=checkAll]').length;
                if (numChkBoxes == numChkBoxesChecked && numChkBoxes > 0) {
                    $('#checkAll').attr('checked', 'checked');
                }
                else {
                    $('#checkAll').attr('checked', '');
                }
            });
           
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var data = mylocalStorage.MenuForm.GetAllMenu();
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,                
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "MenuID",
                        fields:
                        {
                            MenuID: { type: "string", editable: false },
                            ParentID: { type: "string", editable: false },
                            MenuLvl: { type: "integer", editable: false },
                            Menu: { type: "string", editable: false }
                        }
                    }
                }

            })



            if (list != undefined) {

                rmManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                            title: 'Select All',
                            headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: '<input type="checkbox" id="#: MenuID #" class="k-checkbox"><label class="k-checkbox-label" for="#: MenuID #"></label>',
                            width: 50
                        },
                         {
                             field: "MenuID",
                             title: "ID",
                             width: 80
                         },
                        {
                            field: "ParentID",
                            title: "Parent",
                            width: 80
                        },
                        {
                            field: "MenuLvl",
                            title: "Level",
                            width: 50
                        },
                        {
                            field: "Menu",
                            title: "Menu Name"
                        }
                    ]
                });

                var grid = rmManager.general.gridview.data("kendoGrid");

                //bind click event to the checkbox
                grid.table.on("click", ".k-checkbox", selectRow);
                $('#header-chb').change(function (ev) {

                    var checked = ev.target.checked;
                    $('.k-checkbox').each(function (idx, item) {
                        if (checked) {
                            if (!($(item).closest('tr').is('.k-state-selected'))) {
                                $(item).click();
                            }
                        } else {
                            if ($(item).closest('tr').is('.k-state-selected')) {
                                $(item).click();
                            }
                        }
                    });

                });

                //Add Class
                rmManager.general.gridview.find("table").addClass("table table-condensed");

            }


        },
        refreshGrid: function () {

            rmManager.general.gridview.data("kendoGrid").dataSource.read();
            rmManager.general.gridview.data('kendoGrid').refresh();
        }
    }
}


function Edit(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    getProductDetailView(item.id);

};

function Delete(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajax({
                async: false,
                type: "POST",
                url: "/Handler/Products/DeleteProduct.ashx?id=" + item.id,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (!data) {

                        popIt("alert", data.d);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        rmManager.general.refreshGrid();
                        popIt("notification", data.d);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

                        popIt("alert", XMLHttpRequest.responseText);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        rmManager.general.refreshGrid();
                        popIt("notification", XMLHttpRequest.responseText);
                    }
                }
            })


            swal("Deleted!", "Your data has been deleted.", "success");
        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};


function Duplicate(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "",
        text: "Are you sure want to duplicate this products?",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, duplicate it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            getProductDetailViewDuplicate(item.id);
            swal("Duplicated!", "Your data has been duplicated.", "success");

        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = rmManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }

}


$(document).ready(function () {
    

});
