﻿var wcData = undefined,
    itemData = undefined,
    rejectData = undefined,
    rejectedGroupingData = undefined
isThisUpdatingData = false;

function addOptionToSelect(data, selectId, id, desc) {
    if (selectId == "#wcSelect") {
        wcData = data;
    }
    else if (selectId == "#itemSelect") {
        itemData = data;
    }
    else if (selectId == "#rejectSelect") {
        rejectData = data;
    }

    if (data != undefined) {
        $.each(data, function (index, value) {
            $(selectId).append($("<option></option>").attr("value", index)
                                                     .text(value[id] +
                                                           " : " +
                                                           value[desc]));
        });
    }
}

$(function () {
    $("#wcSelect").on("change", function () {
        if (wcData != undefined) {
            var key = $(this).val(),
                wc = "",
                description = "";

            if (key != "-Select-") {
                var some_wc = wcData[key];
                description = some_wc["_description"];
                wc = some_wc["_wc"];
            }
            else if (key == "-Select-" && isThisUpdatingData == false) {
                myMPManager.general.txtRgId.val("");
                myMPManager.general.txtWC.val("");
            }

            if (isThisUpdatingData == false) {
                RejectedGrouping.getRejectedGrouping(wc, myMPManager.general.txtItemCode.val());
            }

            myMPManager.general.txtWC.val(wc);
            myMPManager.general.wcDescription.val(description);
        }
    });

    $("#itemSelect").on("change", function () {
        if (itemData != undefined) {
            var key = $(this).val(),
                itemDescription = "",
                itemCode = "";

            if (key != "-Select-") {
                var some_item = itemData[key];
                itemDescription = some_item["_itemdescription"];
                itemCode = some_item["_itemCode"];
            }
            else {
                myMPManager.general.txtItemCode.val("");
            }

            if (isThisUpdatingData == false) {
                RejectedGrouping.getRejectedGrouping(myMPManager.general.txtWC.val(), itemCode);
            }

            myMPManager.general.txtItemCode.val(itemCode);
            myMPManager.general.itemDescription.val(itemDescription);
        }
    });

    $("#rejectSelect").on("change", function () {
        if (rejectData != undefined) {
            var key = $(this).val(),
                RejID = "",
                RejectedDesc = null;

            if (key != "-Select-") {
                var some_reject = rejectData[key];
                RejID = some_reject["_RejID"];
                RejectedDesc = some_reject["_RejectedDesc"];
            }
            else {
                myMPManager.general.txtRejID.val("");
            }

            myMPManager.general.txtRejID.val(RejID);
            myMPManager.general.rejectDescription.val(RejectedDesc);
        }
    });
});

var RejectedGrouping = {
    init: function () {
        addOptionToSelect(this.getWC(), "#wcSelect", "_wc", "_description");
        addOptionToSelect(this.getItem(), "#itemSelect", "_itemCode", "_itemdescription");
        addOptionToSelect(this.getReject(), "#rejectSelect", "_RejectedCode", "_RejectedDesc");
    },
    getData: function (url, attributes) {
        var result;

        $.ajax({
            async: false,
            url: url,
            data: attributes != undefined ? attributes : "{}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) {
                return data;
            },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    },
    getWC: function () {
        return this.getData("/RejectedGrouping/getWC");
    },
    getItem: function () {
        return this.getData("/RejectedGrouping/getItem");
    },
    getReject: function () {
        return this.getData("/RejectedGrouping/getReject");
    },
    getRejectedGrouping: function (_wc, _itemCode) {
        var data = "{'_wc': '" + _wc + "', '_itemCode': '" + _itemCode + "'}";
        rejectedGroupingData = this.getData("/RejectedGrouping/getRejectedGrouping", data);
        myMPManager.general.initGrid();
        myMPManager.general.refreshGrid();
        isThisUpdatingData = false;
        $("#btnSave").attr("disabled", false);
        $("#btnUpdate").attr("disabled", true);
        $("#btnDelete").attr("disabled", true);
    }
}

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        init: function () {
            RejectedGrouping.init();
            this.gridWorkCenter = $("#grid-work-center");
            this.gridItemCode = $("#grid-item-code");
            this.gridRejectedCode = $("#grid-rejected-code");
            this.gridTableReject = $("#grid-table-reject");

            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.hfWorkCenter = $("#hfWorkCenter");
            this.hfitemCode = $("#hfitemCode");
            this.hfRejectedCode = $("#hfRejectedCode");
            this.hfRgId = $("#hfRgId");

            this.listWorkCenter = [];
            this.listItemCode = [];
            this.listRejectedCode = [];
            this.flagRead = 0;

            //localTempStorage.Products.sync();
            this.initGridWorkCenter();
            this.initGridItemCode();
            this.initGrid();
            this.initGridRejectedCode();


            $('#btnCancel').click(function () {
                myMPManager.general.flagRead = 0;
                myMPManager.general.refreshGrid();
            });

            $("#btnSave").click(function () {
                //jQuery.map:  
                var entityGrid = myMPManager.general.gridWorkCenter.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                var entityGrid2 = myMPManager.general.gridItemCode.data("kendoGrid");
                var selectedItem2 = entityGrid2.dataItem(entityGrid2.select());

                var entityGrid3 = myMPManager.general.gridRejectedCode.data("kendoGrid");
                var selectedItem3 = entityGrid3.dataItem(entityGrid3.select());

                var itemCode = "";
                var wcCode = "";
                var rejcode = "";

                if (selectedItem != undefined) {
                    wcCode = selectedItem._wc;
                }

                if (selectedItem2 != undefined) {
                    itemCode = selectedItem2._itemCode;
                }
                else if (selectedItem2 == undefined){
                    itemCode = "";
                }

                if (selectedItem3 != undefined) {
                    rejcode = selectedItem3._RejID;
                }
                else if (selectedItem3 == undefined) {
                    rejcode = 0;
                }

                $(document).ready(function () {
                    myMPManager.general.initGrid();
                });

                try {
                    myMPManager.general.gridTableReject.data("kendoGrid").dataSource.read();
                    myMPManager.general.gridTableReject.data('kendoGrid').refresh();
                } catch (e) { }


                return true;
            });

        },
        initGrid: function () {
            var list = new kendo.data.DataSource
            ({
                //serverPaging: false,
                //serverFiltering: false,
                //serverSorting: false,
                transport: {
                    read: function (options) {
                        // Implement read
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;

                        var entityGrid = myMPManager.general.gridWorkCenter.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        var entityGrid2 = myMPManager.general.gridItemCode.data("kendoGrid");
                        var selectedItem2 = entityGrid2.dataItem(entityGrid2.select());

                        var wcCode = "";
                        var itemCode = "";
                        if (selectedItem != undefined) {
                            wcCode = selectedItem._wc;
                        }

                        if (selectedItem2 != undefined) {
                            itemCode = selectedItem2._itemCode;
                        }

                        var data = mylocalStorage.RejectedDesc.getRejectedGrouping(wcCode, itemCode);

                        //var data = rejectedGroupingData;
                        options.success(data);
                    },
                    create: function (options) { },
                    update: function (options) { }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "_rgid",
                        fields:
                        {
                            _wc: { type: "string", editable: false },
                            _itemCode: { type: "string", editable: false },
                            _RejectedCode: { type: "number", editable: false },
                            _RejectedDesc: { type: "string", editable: false },
                            _RejID: { type: "number", editable: false }
                        }
                    }
                },
            })

            if (list != undefined) {
                myMPManager.general.gridTableReject.kendoGrid({
                    height: 400,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    change: changeData,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_wc",
                            title: "WC Code"
                        },
                        {
                            field: "_itemCode",
                            title: "Item Code"
                        },
                        {
                            field: "_RejectedCode",
                            title: "Rejected Code"
                        },
                        {
                            field: "_RejectedDesc",
                            title: "Rejected Description"
                        }
                    ]
                });

                var grid = myMPManager.general.gridTableReject.data("kendoGrid");
            }
        },

        initGridWorkCenter: function () {

            var listWC = new kendo.data.DataSource
            ({
               transport: {

                   read: function (options) {
                       // Implement read
                       if (myMPManager.general.flagRead == 0) {
                           var data = mylocalStorage.WorkCenter.GetAllWorkCenterData('', '', 0, 10);
                       }
                       options.success(sortResults(data, "_wc", true));
                   },
                   create: function (options) {
                   },
                   update: function (options) {
                   }
               },
               //page: 1,
               //pageSize: 10,
               batch: true,
               schema: {
                   model: {
                       id: "_wcid",
                       fields:
                       {
                           _wcid: { type: "number", editable: false },
                           _wc: { type: "string", editable: false },
                           _description: { type: "string", editable: false }
                       }
                   }
               }

           })

            if (listWC != undefined) {

                myMPManager.general.gridWorkCenter.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: listWC,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: onChangeWC,

                    columns: [
                        {
                            field: "_wcid",
                            title: "Work Center",
                            template: "#:_wc#  -  #:_description#"
                        }
                    ]
                });

            }

        },

        initGridItemCode: function () {

            var listIC = new kendo.data.DataSource
           ({
               transport: {

                   read: function (options) {
                       // Implement read
                       if (myMPManager.general.flagRead == 0) {
                           var data = mylocalStorage.Item.GetAllItemData('', '', 0, 10);
                       }
                       options.success(sortResults(data, "_itemCode", true));
                   },
                   create: function (options) {
                   },
                   update: function (options) {
                   }
               },
               //page: 1,
               //pageSize: 10,
               batch: true,
               schema: {
                   model: {
                       id: "_itemid",
                       fields:
                       {
                           _itemid: { type: "number", editable: false },
                           _itemCode: { type: "string", editable: false },
                           _itemdescription: { type: "string", editable: false }
                       }
                   }
               }

           })

            if (listIC != undefined) {

                myMPManager.general.gridItemCode.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: listIC,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: onChangeWC,

                    columns: [
                        {
                            field: "_itemid",
                            title: "Item Code",
                            template: "#:_itemCode#  -  #:_itemdescription#"
                        }
                    ]
                });

                myMPManager.general.gridItemCode.find("table").addClass("table table-condensed");
            }

        },
        initGridRejectedCode: function () {

            var listRC = new kendo.data.DataSource
            ({
                transport: {

                    read: function (options) {
                        var entityGrid = myMPManager.general.gridWorkCenter.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        var entityGrid2 = myMPManager.general.gridItemCode.data("kendoGrid");
                        var selectedItem2 = entityGrid2.dataItem(entityGrid2.select());
                        var itemCode = "";
                        var wcCode = "";
                        var dataTemp = myMPManager.general.gridRejectedCode.data("kendoGrid");
                        if (myMPManager.general.flagRead == 0) {
                            if (selectedItem != undefined) {
                                wcCode = selectedItem._wc;
                            }

                            if (selectedItem2 != undefined) {
                                itemCode = selectedItem2._itemCode;
                            }
                            dataTemp = mylocalStorage.RejectedDesc.GetAllRejectedDescData(wcCode, itemCode);
                            myMPManager.general.flagRead = 1;
                        }

                        var data = dataTemp;

                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                //page: 1,
                //pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "_RejID",
                        fields:
                        {
                            _RejID: { type: "number", editable: false },
                            _RejectedCode: { type: "string", editable: false },
                            _RejectedDesc: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (listRC != undefined) {

                myMPManager.general.gridRejectedCode.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: listRC,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: onChangeRC,

                    columns: [
                        {
                            field: "_RejID",
                            title: "Rejected Code",
                            template: "#:_RejectedDesc#"
                        }
                    ]
                });

                myMPManager.general.gridRejectedCode.find("table").addClass("table table-condensed");
            }

        },

        refreshGrid: function () {
            try {
                myMPManager.general.gridWorkCenter.data("kendoGrid").dataSource.read();
                myMPManager.general.gridWorkCenter.data('kendoGrid').refresh();
            } catch (e) { }

            try {
                myMPManager.general.gridItemCode.data("kendoGrid").dataSource.read();
                myMPManager.general.gridItemCode.data('kendoGrid').refresh();
            } catch (e) { }

            try {
                myMPManager.general.gridRejectedCode.data("kendoGrid").dataSource.read();
                myMPManager.general.gridRejectedCode.data('kendoGrid').refresh();
            } catch (e) { }

            try {
                myMPManager.general.gridTableReject.data("kendoGrid").dataSource.read();
                myMPManager.general.gridTableReject.data('kendoGrid').refresh();
            } catch (e) { }
        },
    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                      .addClass("k-state-selected")
                      .find(".checkbox")
                      .attr("checked", "checked");
        }
    }
}

function ViewSuccess(data) {
    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.flagRead = 0;

        myMPManager.general.gridTableReject.data("kendoGrid").dataSource.read();
        myMPManager.general.gridTableReject.data('kendoGrid').refresh();

        myMPManager.general.gridRejectedCode.data("kendoGrid").dataSource.read();
        myMPManager.general.gridRejectedCode.data('kendoGrid').refresh();
        myMPManager.general.flagRead = 1;
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);
    }
}

function ViewError(data) {
    $.LoadingOverlay("hide");
    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show",
                    {
                        minSize: "20px",
                        maxSize: "30px",
                        image: "",
                        fontawesome: "fa fa-spinner fa-spin"
                    });

}
function onChangeWC(arg) {
    var entityGrid = myMPManager.general.gridWorkCenter.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    var entityGrid2 = myMPManager.general.gridItemCode.data("kendoGrid");
    var selectedItem2 = entityGrid2.dataItem(entityGrid2.select());

    if (selectedItem != undefined) {
        myMPManager.general.hfWorkCenter.val(selectedItem._wc);
    } else {
        myMPManager.general.hfWorkCenter.val("");
    }

    if (selectedItem2 != undefined) {
        myMPManager.general.hfitemCode.val(selectedItem2._itemCode);
    } else {
        myMPManager.general.hfitemCode.val("");
    }

    myMPManager.general.flagRead = 0;

    myMPManager.general.gridTableReject.data("kendoGrid").dataSource.read();
    myMPManager.general.gridTableReject.data('kendoGrid').refresh();

    myMPManager.general.gridRejectedCode.data("kendoGrid").dataSource.read();
    myMPManager.general.gridRejectedCode.data('kendoGrid').refresh();
}

function onChangeRC(arg) {
    var entityGrid3 = myMPManager.general.gridRejectedCode.data("kendoGrid");
    var selectedItem3 = entityGrid3.dataItem(entityGrid3.select());

    if (selectedItem3 != undefined) {
        myMPManager.general.hfRejectedCode.val(selectedItem3._RejID);
    } else {
        myMPManager.general.hfRejectedCode.val("");
    }
}

function changeData(arg) {
    var entityGridTable = myMPManager.general.gridTableReject.data("kendoGrid");
    var selectedItemTable = entityGridTable.dataItem(entityGridTable.select());
    
    try {
        if (selectedItemTable._rgid) {
            isThisUpdatingData = true;
        }

        myMPManager.general.hfRgId.val(selectedItemTable._rgid);
        myMPManager.general.hfWorkCenter.val(selectedItemTable._wc);
        myMPManager.general.hfitemCode.val(selectedItemTable._itemCode);
        myMPManager.general.hfRejectedCode.val(selectedItemTable._RejID);
    }
    catch (e) { }
}

$(document).ready(function () {
    myMPManager.general.init();
});