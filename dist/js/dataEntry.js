﻿mylocalStorage["DataEntry"] = {
    GetJobNoPrint: function (JobNo) {
        var result;

        $.ajax({
            async: false,
            url: "/JobRoute/GetJobNoPrint",
            data: "{'JobNo': '" + JobNo + "'}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    }
}

//Declare Control
var detailRejManager = {
    general: {
        init: function () {
            this.cmbRejCode = $("#cmbRejCode");
            this.txtRejQty = $("#txtRejQty");
            this.txtRejDesc = $("#txtRejDesc");
            this.hfRefID = $("#hfRefID");
            this.hfrid = $("#hfrid");
            this.hfListReject = $("#hfListReject");
            this.gridrejecteddetail = $("#grid-rejected-detail");
            this.txtRejReceivedQTY = $("#txtRejReceivedQty");
            this.txtRejCompQty = $("#txtRejCompQty");
            this.txtRejRejectedQty = $("#txtRejRejectedQty");
            this.lastTansum = 0;

            $("#btnRejCancel").click(function () {
                detailRejManager.general.hfrid.val("");
                detailRejManager.general.cmbRejCode.val("");
                detailRejManager.general.txtRejDesc.val("");
                detailRejManager.general.txtRejQty.val("");
                detailRejManager.general.hfListReject.val("");
                detailRejManager.general.cmbRejCode.removeAttr("readonly");
                detailRejManager.general.txtRejDesc.removeAttr("readonly");
                detailRejManager.general.txtRejQty.removeAttr("readonly");
                myMPManager.general.resetGrid = 1;
                bindRejecCode();

                detailRejManager.general.gridrejecteddetail.data("kendoGrid").dataSource.read();
                detailRejManager.general.gridrejecteddetail.data('kendoGrid').refresh();

                var ddlRej = $("#cmbRejCode").data("kendoDropDownList");
                ddlRej.enable(true);
                $("#btnRejRemove").attr("disabled", "disabled");
                $("#btnRejAdd").removeAttr("disabled");
            });

            $("#btnRejAdd").click(function () {
                if (detailRejManager.general.cmbRejCode.val() != "" && detailRejManager.general.txtRejDesc.val() != "" && (detailRejManager.general.txtRejQty.val() != "" || detailRejManager.general.txtRejQty.val() != 0)) {
                    var listItem = {
                        _rejid: 0,
                        _refID: detailRejManager.general.hfRefID.val(),
                        _RejectedCode: detailRejManager.general.cmbRejCode.val(),
                        _RejectedDesc: detailRejManager.general.txtRejDesc.val(),
                        _RejectedQty: detailRejManager.general.txtRejQty.val()
                    }

                    myMPManager.general.listTotal = parseInt(myMPManager.general.listTotal) + parseInt(detailRejManager.general.txtRejQty.val());
                    myMPManager.general.listReject.push(listItem);
                    updateList();

                    detailRejManager.general.hfrid.val("");
                    detailRejManager.general.cmbRejCode.val("");
                    detailRejManager.general.txtRejDesc.val("");
                    detailRejManager.general.txtRejQty.val("");
                    detailRejManager.general.cmbRejCode.removeAttr("readonly");
                    detailRejManager.general.txtRejDesc.removeAttr("readonly");
                    detailRejManager.general.txtRejQty.removeAttr("readonly");
                    bindRejecCode();

                    var ddlRej = $("#cmbRejCode").data("kendoDropDownList");
                    ddlRej.enable(true);
                    detailRejManager.general.gridrejecteddetail.data("kendoGrid").dataSource.read();
                    detailRejManager.general.gridrejecteddetail.data('kendoGrid').refresh();

                }
            });

            $("#btnRejRemove").click(function () {
                //MOVE WORK CENTER RIGHT
                var entityGrid = detailRejManager.general.gridrejecteddetail.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                myMPManager.general.listTotal = parseInt(myMPManager.general.listTotal) - parseInt(selectedItem._RejectedQty);
                myMPManager.general.listReject = $.grep(myMPManager.general.listReject, function (e) { return e._rejid != selectedItem._rejid });
                updateList();

                detailRejManager.general.hfrid.val("");
                detailRejManager.general.cmbRejCode.val("");
                detailRejManager.general.txtRejDesc.val("");
                detailRejManager.general.txtRejQty.val("");
                detailRejManager.general.cmbRejCode.removeAttr("readonly");
                detailRejManager.general.txtRejDesc.removeAttr("readonly");
                detailRejManager.general.txtRejQty.removeAttr("readonly");
                bindRejecCode();

                detailRejManager.general.gridrejecteddetail.data("kendoGrid").dataSource.read();
                detailRejManager.general.gridrejecteddetail.data('kendoGrid').refresh();
            });
        }
    }
}

var jobSuffix = undefined;


var myMPManager = {
    general: {
        gridlinenumber: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        roleUser: '',
        userName: '',
        flagChange: 0,
        flagRefreshJob: 0,
        flagRefreshNo: 0,

        init: function () {
            this.gridlinenumber = $("#grid-line-number");
            this.gridjobline = $("#grid-job-line");
            this.gridrejected = $(".grid-rejected");
            this.roleUser = "";
            this.userName = "";
            this.listReject = [];
            this.resetGrid = 1;
            this.listTotal = 0;

            this.modal = $("#modal-reject-detail");

            //localTempStorage.Products.sync();
            this.txtJobNo = $("#txtJobNo");
            this.lblItemCode = $("#lblItemCode");
            this.lblDescription = $("#lblDescription");
            this.lblReleasedQty = $("#lblReleasedQty");
            this.lblJobDate = $("#lblJobDate");
            this.lblStatus = $("#lblStatus");
            this.lblRework = $("#lblRework");
            this.txtOpNo = $("#txtOpNo");
            this.txtWorkCenter = $("#txtWorkCenter");
            this.cbMachineID = $("#cbMachineID");
            this.cbOperator = $("#cbOperator");
            this.cbAction = $("#cbAction");
            this.txtNumberOP = $("#txtNumberOP");
            this.txtRemarks = $("#txtRemarks");
            this.ckbRework = $("#ckbRework");

            this.txtTotal = $("#txtTotal");
            this.hfTotalRejected = $("#hfTotalRejected");
            this.lblTotal = $(".label-total-reject");
            this.txtReleasedQty = $("#txtReleasedQty");
            this.txtStartDate = $("#txtStartDate");
            this.txtEndDate = $("#txtEndDate");

            this.txtReceivedQty = $("#txtReceived");
            this.txtCompletedQty = $("#txtCompleted");
            this.txtRejectedQty = $("#txtRejected");
            this.dtpStartDate = $("#txtStartDate");
            this.dtpEndDate = $("#txtEndDate");
            this.hfTansum = $("#hfTansum");
            this.hfActionType = $("#hfActionType");
            this.hfjobSuffix = $("#hfjobSuffix");
            this.hfjobSuffixParent = $("#hfjobSuffixParent");
            this.hfStatus = $("#hfStatus");
            this.hfItem = $("#hfItem");

            this.btnPrintLine = $("#btnPrintLine");

            $("#addReject").click(function () {
                detailRejManager.general.cmbRejCode.val("");
                detailRejManager.general.txtRejQty.val("");
                detailRejManager.general.txtRejDesc.val("");
                detailRejManager.general.hfrid.val("");
                detailRejManager.general.txtRejReceivedQTY.html(myMPManager.general.txtReceivedQty.val());
                detailRejManager.general.txtRejCompQty.html(myMPManager.general.txtCompletedQty.val());
                detailRejManager.general.txtRejRejectedQty.html(myMPManager.general.txtRejectedQty.val());

                detailRejManager.general.gridrejecteddetail.data("kendoGrid").dataSource.read();
                detailRejManager.general.gridrejecteddetail.data('kendoGrid').refresh();

                $("#btnRejRemove").attr("disabled", "disabled")
                bindRejecCode();
                myMPManager.general.modal.modal("show");
            });

            $("#btnSearch").click(function () {
                $("#btnPrint").removeAttr("disabled");

                $.ajax({
                    async: false,
                    url: "/JobRoute/getJobInformation",
                    data: "{ 'jobNo': '" + myMPManager.general.txtJobNo.val() + "' }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        if (data != null) {
                            var temp = jQuery.parseJSON(data);
                            myMPManager.general.lblItemCode.html(temp._itemCode);
                            myMPManager.general.hfItem.val(temp._itemCode);
                            myMPManager.general.lblDescription.html(temp._description);
                            myMPManager.general.lblReleasedQty.html(temp._qty_Rele_qty);
                            myMPManager.general.lblJobDate.html(temp._jobDateParse);
                        }
                        myMPManager.general.refreshGrid();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                })

                myMPManager.general.gridlinenumber.data("kendoGrid").dataSource.read();
                myMPManager.general.gridlinenumber.data('kendoGrid').refresh();
            });

            myMPManager.general.cbAction.change()

            myMPManager.general.cbAction.change(function () {
                myMPManager.general.hfActionType.val(myMPManager.general.cbAction.val());
                //alert(myMPManager.general.hfActionType.val());
            });

            $("#btnPrint").click(function () {
            });

            $("#btnPrintLine").click(function () {
                $.LoadingOverlay("show", {
                    minSize: "20px",
                    maxSize: "30px",
                    image: "",
                    fontawesome: "fa fa-spinner fa-spin"
                });

                var entityGrid = myMPManager.general.gridlinenumber.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                $.ajax({
                    async: false,
                    url: "/Print/PrintDataEntry",
                    data: "{ 'jobID': '" + myMPManager.general.txtJobNo.val() + "', 'operNum': " + selectedItem._operationNo + " }",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        $.LoadingOverlay("hide");

                        if (data["redirect"] != null) {

                            window.open(
                                  data["redirect"],
                                  '_blank' // <- This is what makes it open in a new window.
                                );
                        }
                        if (data["excel"] != null) {

                            window.open(
                                  data["excel"],
                                  '_blank' // <- This is what makes it open in a new window.
                                );
                        }

                        NotifSuccess(data["success"]);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                })

                //if (jobSuffix != undefined)
                //{
                //    JobNo = JobNo + '-' + jobSuffix;
                //}

                //mylocalStorage.DataEntry.GetJobNoPrint(JobNo);

            });

            $("#btnCancel").click(function () {
                myMPManager.general.flagChange = 0;

                myMPManager.general.hfActionType.val("");
                myMPManager.general.hfTansum.val("");
                myMPManager.general.hfjobSuffix.val("");
                myMPManager.general.hfjobSuffixParent.val("");
                myMPManager.general.hfStatus.val("");
                myMPManager.general.txtReleasedQty.val("");
                myMPManager.general.cbMachineID.attr("disabled", "disabled");
                myMPManager.general.cbOperator.attr("disabled", "disabled");
                myMPManager.general.txtNumberOP.val("");
                myMPManager.general.txtRemarks.val("");
                myMPManager.general.lblRework.hide();
                myMPManager.general.lblStatus.html("");
                myMPManager.general.txtReceivedQty.val("");
                myMPManager.general.txtCompletedQty.val("");
                myMPManager.general.txtRejectedQty.val("");
                myMPManager.general.txtStartDate.val(null);
                myMPManager.general.txtEndDate.val(null);
                myMPManager.general.hfTotalRejected.val(0);
                detailRejManager.general.hfListReject.val("");

                myMPManager.general.resetGrid = 1;

                var ddlOP = $("#cbOperator").data("kendoDropDownList");
                ddlOP.enable(false);

                var ddlAction = $("#cbAction").data("kendoDropDownList");
                ddlAction.enable(false);

                bindActionDDL();

                $("#addReject").attr("disabled", "disabled");
                $("#btnPrint").attr("disabled", "disabled");
                $("#btnPrintLine").attr("disabled", "disabled");
                $("#btnSave").attr("disabled", "disabled");

                myMPManager.general.refreshGrid();
            });

            this.initGridLineNo();
            this.initGridJobLine();
            this.initGridRejected();
        },

        initGridLineNo: function () {
            var list = new kendo.data.DataSource

           ({
               transport: {

                   read: function (options) {
                       var jobNo = myMPManager.general.txtJobNo.val();
                       var data = mylocalStorage.JobRoute.getAllJobLineNumber(jobNo);
                       options.success(data);
                   },
                   create: function (options) {
                   },
                   update: function (options) {
                   }
               },
               batch: true,
               schema: {
                   model: {
                       id: "_oid",
                       fields:
                       {
                           _job: { type: "string", editable: false },
                           _operationNo: { type: "string", editable: false },
                           _wc: { type: "string", editable: false },
                           _runlbrhrs: { type: "numeric", editable: false },
                           _runmchhrs: { type: "numeric", editable: false },
                       }
                   }
               }

           })

            if (list != undefined) {

                myMPManager.general.gridlinenumber.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    change: onChange,
                    filterable: false,
                    selectable: "row",
                    pageable: false,
                    height: "506px",
                    columns: [
                        {
                            field: "_operationNo",
                            title: "Operation No"
                        }
                    ]
                });

                myMPManager.general.gridlinenumber.find("table").addClass("table table-condensed");
            }

        },
        initGridJobLine: function () {
            var list = new kendo.data.DataSource

           ({
               transport: {

                   read: function (options) {
                       var entityGrid = myMPManager.general.gridlinenumber.data("kendoGrid");
                       var selectedItem = entityGrid.dataItem(entityGrid.select());

                       var jobNo = myMPManager.general.txtJobNo.val();
                       var lineNo = 0;
                       if (myMPManager.general.flagChange == 1) {
                           lineNo = selectedItem._operationNo;
                           myMPManager.general.flagChange = 0;
                       }

                       var data = mylocalStorage.JobRoute.getAllJobLineDetail(jobNo, lineNo);
                       options.success(sortResults(data, "_jobsuffix", true));
                   },
                   create: function (options) {
                   },
                   update: function (options) {
                   }
               },
               batch: true,
               schema: {
                   model: {
                       id: "_oid",
                       fields:
                       {
                           _tansnum: { type: "numeric", editable: false },
                           _job: {
                               type: "string", editable: false
                           },
                           _jobsuffix: { type: "string", editable: false },
                           _jobsuffixParent: { type: "string", editable: false },
                           _wc: { type: "string", editable: false },
                           _qty_Rele_qty: { type: "numeric", editable: false },
                           _qty_op_qty: { type: "numeric", editable: false },
                           _qty_scrapped: { type: "numeric", editable: false },
                           _qty_complete: { type: "numeric", editable: false },
                           _jobDateParse: { type: "string", editable: false },
                           _itemCode: { type: "string", editable: false },
                           _description: { type: "string", editable: false },
                           _status: { type: "string", editable: false },
                           _reworkst: { type: "string", editable: false },
                           _numberOp: { type: "numeric", editable: false },
                           startDate: { type: "date", editable: false },
                           endDate: { type: "date", editable: false }
                       }
                   }
               }

           })

            if (list != undefined) {

                myMPManager.general.gridjobline.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    filterable: false,
                    selectable: "row",
                    pageable: false,
                    height: "506px",
                    change: onJobChange,
                    columns: [
                        {
                            field: "_job",
                            title: "Job Number",
                            template: "#:_job # - #:_jobsuffix# #:_reworkst#"
                        }
                    ]
                });

                myMPManager.general.gridlinenumber.find("table").addClass("table table-condensed");
            }

        },
        initGridRejected: function () {
            var list = new kendo.data.DataSource

           ({
               transport: {

                   read: function (options) {
                       var entityGrid = myMPManager.general.gridjobline.data("kendoGrid");
                       var selectedItem = entityGrid.dataItem(entityGrid.select());

                       var tansnum = 0;
                       if (selectedItem != null) {
                           tansnum = selectedItem._tansnum;
                       }

                       if (myMPManager.general.resetGrid == 1) {
                           var data = mylocalStorage.JobRoute.getRejectedTransData(tansnum);
                           myMPManager.general.listTotal = data.List.Total;
                           myMPManager.general.listReject = jQuery.parseJSON(data.List.Data);
                           myMPManager.general.resetGrid = 0;
                       }
                       myMPManager.general.lblTotal.html(myMPManager.general.listTotal);
                       options.success(myMPManager.general.listReject);
                   },
                   create: function (options) {
                   },
                   update: function (options) {
                   }
               },
               batch: true,
               schema: {
                   model: {
                       id: "_RejID",
                       fields:
                       {
                           _rejid: { type: "numeric", editable: false },
                           _refID: { type: "numeric", editable: false },
                           _RejectedCode: { type: "string", editable: false },
                           _RejectedDesc: { type: "string", editable: false },
                           _RejectedQty: { type: "numeric", editable: false }
                       }
                   }
               }

           })

            if (list != undefined) {

                myMPManager.general.gridrejected.kendoGrid({
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    filterable: false,
                    selectable: "row",
                    pageable: false,
                    height: "150px",
                    change: onRejectChange,
                    columns: [
                        {
                            field: "_RejectedCode",
                            title: "RJ-Code"
                        },
                        {
                            field: "_RejectedDesc",
                            title: "Description"
                        },
                        {
                            field: "_RejectedQty",
                            title: "QTY"
                        }
                    ]
                });

                myMPManager.general.gridrejected.find("table").addClass("table table-condensed");
            }

        },
        refreshGrid: function () {

            myMPManager.general.gridjobline.data("kendoGrid").dataSource.read();
            myMPManager.general.gridjobline.data('kendoGrid').refresh();

            myMPManager.general.gridlinenumber.data("kendoGrid").dataSource.read();
            myMPManager.general.gridlinenumber.data('kendoGrid').refresh();

            myMPManager.general.gridrejected.data("kendoGrid").dataSource.read();
            myMPManager.general.gridrejected.data('kendoGrid').refresh();
        },
    }
}

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        if (myMPManager.general.hfActionType.val() === "COMPLETED" || myMPManager.general.hfActionType.val() === "SPLIT" || myMPManager.general.hfActionType.val() === "REWORK" || myMPManager.general.hfActionType.val() === "UPDATE") {
            //SweetAlert begin
            swal({
                title: "Auto Posting to SL9?",
                //text: "You will not be able to recover this data!",
                type: "warning", showCancelButton: true,
                confirmButtonColor: "#3d34bb", confirmButtonText: "Yes",
                cancelButtonText: "No", closeOnConfirm: false, closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.LoadingOverlay("show");
                    var ddlAction = $("#cbAction").data("kendoDropDownList");
                    setTimeout(function () {
                        $.ajax({
                            async: false,
                            url: "/AutoPosting/UserExportToSL9",
                            data: "{ 'tansnum':" + myMPManager.general.lastTansum + ",'job': '" + myMPManager.general.txtJobNo.val() + "','oper_num': '" + myMPManager.general.txtOpNo.val()
                                + "', 'wc': '" + myMPManager.general.txtWorkCenter.val() + "', 'action':'" + ddlAction.value() + "', 'url': '" + window.location.pathname.replace("/", "") + "' }",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataFilter: function (data) { return data; },
                            success: function (data) {
                                if (data["success"] != undefined) {
                                    swal("Data Posted Succesfully!", data["success"], "success");
                                    //$("#btnCancel").trigger('click');
                                } else if (data["error"] != undefined) {
                                    swal("Posting Canceled!", data["error"], "error");
                                }
                                myMPManager.general.refreshGrid();
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(errorThrown);
                            }
                        })

                    }, 1500);
                } else {
                    swal('Information', "Data will not be Posted", "info");
                }
                $.LoadingOverlay("hide");
            })
        }

        myMPManager.general.hfActionType.val("");
        myMPManager.general.hfTansum.val("");
        myMPManager.general.hfjobSuffix.val("");
        myMPManager.general.hfjobSuffixParent.val("");
        myMPManager.general.hfStatus.val("");
        myMPManager.general.txtReleasedQty.val("");
        myMPManager.general.cbMachineID.attr("disabled", "disabled");
        myMPManager.general.cbOperator.attr("disabled", "disabled");
        myMPManager.general.txtNumberOP.val("");
        myMPManager.general.txtRemarks.val("");
        myMPManager.general.lblRework.hide();
        myMPManager.general.lblStatus.html("");
        myMPManager.general.txtReceivedQty.val("");
        myMPManager.general.txtCompletedQty.val("");
        myMPManager.general.txtRejectedQty.val("");
        myMPManager.general.txtStartDate.val(null);
        myMPManager.general.txtEndDate.val(null);
        myMPManager.general.hfTotalRejected.val(0);
        detailRejManager.general.hfListReject.val("");

        myMPManager.general.resetGrid = 1;

        var ddlOP = $("#cbOperator").data("kendoDropDownList");
        ddlOP.enable(false);

        var ddlAction = $("#cbAction").data("kendoDropDownList");
        ddlAction.enable(false);

        bindActionDDL();

        $("#btnPrint").attr("disabled", "disabled");
        $("#btnPrintLine").attr("disabled", "disabled");
        $("#btnSave").attr("disabled", "disabled");

        myMPManager.general.refreshGrid();

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {

    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}

function onChange(arg) {
    var entityGrid = myMPManager.general.gridlinenumber.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    myMPManager.general.txtOpNo.val(selectedItem._operationNo);
    myMPManager.general.txtWorkCenter.val(selectedItem._wc);

    myMPManager.general.txtReleasedQty.val("");
    myMPManager.general.cbMachineID.attr("disabled", "disabled");
    myMPManager.general.cbOperator.attr("disabled", "disabled");
    myMPManager.general.txtNumberOP.val("");
    myMPManager.general.txtRemarks.val("");
    myMPManager.general.lblRework.hide();
    myMPManager.general.lblStatus.html("");
    myMPManager.general.txtReceivedQty.val("");
    myMPManager.general.txtCompletedQty.val("");
    myMPManager.general.txtRejectedQty.val("");
    myMPManager.general.txtStartDate.val(null);
    myMPManager.general.txtEndDate.val(null);

    var ddlOP = $("#cbOperator").data("kendoDropDownList");
    ddlOP.enable(false);

    var ddlAction = $("#cbAction").data("kendoDropDownList");
    ddlAction.enable(false);

    myMPManager.general.flagChange = 1;
    myMPManager.general.gridjobline.data("kendoGrid").dataSource.read();
    myMPManager.general.gridjobline.data('kendoGrid').refresh();

    bindActionDDL();
}

function bindMachineDDL() {
    var entityGrid = myMPManager.general.gridjobline.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    var _wc = ''
    try {
        if (selectedItem._wc != undefined) {
            _wc = selectedItem._wc;
        }
    } catch (ex) {
    }

    myMPManager.general.cbMachineID.kendoDropDownList({
        optionLabel: "-",
        dataTextField: "_machineID",
        dataValueField: "_machineID",
        dataSource: mylocalStorage.Machine.GetMachineByWC(_wc),
        filter: "contains"
    })
}

function bindOperatorDDL() {

    myMPManager.general.cbOperator.kendoDropDownList({
        //optionLabel: "---",
        dataTextField: "_operatorName",
        dataValueField: "_operatorID",
        dataSource: mylocalStorage.Operator.getAlloperatorName(),
        filter: "contains"
    })

}

function onRejectChange(arg) {
    var entityGrid = detailRejManager.general.gridrejecteddetail.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    detailRejManager.general.hfrid.val(selectedItem._rejid);
    detailRejManager.general.cmbRejCode.val(selectedItem._RejectedCode);
    detailRejManager.general.txtRejDesc.val(selectedItem._RejectedDesc);
    detailRejManager.general.txtRejQty.val(selectedItem._RejectedQty);

    var ddlRej = $("#cmbRejCode").data("kendoDropDownList");
    ddlRej.enable(false);

    detailRejManager.general.txtRejDesc.attr("readonly", "readonly");
    detailRejManager.general.txtRejQty.attr("readonly", "readonly");
    $("#btnRejAdd").attr("disabled", "disabled");
    $("#btnRejRemove").removeAttr("disabled");

    bindRejecCode();
}

function onJobChange(arg) {
    myMPManager.general.cbMachineID.removeAttr("disabled");
    var ddlOP = $("#cbOperator").data("kendoDropDownList");
    ddlOP.enable(true);

    var ddlAction = $("#cbAction").data("kendoDropDownList");
    ddlAction.enable(true);

    $("#addReject").removeAttr("disabled");
    $("#btnPrintLine").removeAttr("disabled");
    $("#btnSave").removeAttr("disabled");

    var entityGrid = myMPManager.general.gridjobline.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    myMPManager.general.lblStatus.html(selectedItem._status);
    if (selectedItem._reworkst != "") {
        myMPManager.general.lblRework.show();
    } else {
        myMPManager.general.lblRework.hide();
    }

    jobSuffix = selectedItem._jobsuffix;

    myMPManager.general.lastTansum = selectedItem._tansnum;
    myMPManager.general.hfTansum.val(selectedItem._tansnum);
    myMPManager.general.hfjobSuffix.val(selectedItem._jobsuffix);
    myMPManager.general.hfjobSuffixParent.val(selectedItem._jobsuffixParent);
    myMPManager.general.hfStatus.val(selectedItem._status);
    myMPManager.general.txtReleasedQty.val(selectedItem._qty_Rele_qty);
    myMPManager.general.txtNumberOP.val(selectedItem._numberOp);
    myMPManager.general.txtOpNo.val(selectedItem._operationNo);
    myMPManager.general.txtWorkCenter.val(selectedItem._wc);
    myMPManager.general.txtReceivedQty.val(selectedItem._qty_op_qty);
    myMPManager.general.txtRejectedQty.val(selectedItem._qty_scrapped);
    myMPManager.general.txtCompletedQty.val(selectedItem._qty_complete);


    if (selectedItem.startDate != null) {
        var date = new Date(selectedItem.startDate);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12;
        //var dateString = (date.getMonth() + 1).toString() + "/" + date.getDate().toString() + "/" + date.getFullYear().toString() + " " + date.getHours() + ":" + date.getMinutes();
        var dateString = ('0' + (date.getDate())).slice(-2).toString() + '/'
                         + ('0' + (date.getMonth() + 1)).slice(-2).toString() + '/'
                         + date.getFullYear().toString() + " "
                         + ('0' + hours).slice(-2).toString()
                         + ":" + ('0' + minutes).slice(-2).toString() + " "
                         + ampm;
        myMPManager.general.txtStartDate.val(dateString);
    }
    else {
        myMPManager.general.txtStartDate.val(null);
    }

    if (selectedItem.endDate != null) {
        date = new Date(selectedItem.endDate);
        //dateString = (date.getMonth() + 1).toString() + "/" + date.getDate().toString() + "/" + date.getFullYear().toString() + " " + date.getHours() + ":" + date.getMinutes();
        dateString =  ('0' + (date.getDate())).slice(-2).toString() + '/'
                         + ('0' + (date.getMonth() + 1)).slice(-2).toString() + '/'
                         + date.getFullYear().toString() + " "
                         + ('0' + (date.getHours())).slice(-2).toString()
                         + ":" + ('0' + (date.getMinutes())).slice(-2).toString()
                         + " " + (date.getHours() >= 12 ? 'PM' : 'AM');
        myMPManager.general.txtEndDate.val(dateString);
    }
    else {
        myMPManager.general.txtEndDate.val(null);
    }
    myMPManager.general.resetGrid = 1;

    myMPManager.general.gridrejected.data("kendoGrid").dataSource.read();
    myMPManager.general.gridrejected.data('kendoGrid').refresh();

    detailRejManager.general.hfRefID.val(selectedItem._tansnum);
    bindActionDDL();
    bindMachineDDL();
    //bindDateTimePicker();

    var entityGrid = myMPManager.general.gridjobline.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    //myMPManager.general.btnPrintLine.attr("href", ("/frmAMTprintpreview.aspx?JobNo=" + selectedItem._job + (selectedItem._jobsuffix != "" ? "-" + selectedItem._jobsuffix : "") + (selectedItem._reworkst != "" ? selectedItem._reworkst : "")));
}

function bindDateTimePicker() {
    $("#txtStartDate").kendoDateTimePicker({
        dateInput: true,
        format: "dd/MM/yyyy HH:mm tt",
    });

    $("#txtEndDate").kendoDateTimePicker({
        dateInput: true,
        format: "dd/MM/yyyy HH:mm tt",
    });
};

function bindActionDDL() {
    myMPManager.general.cbAction.kendoDropDownList({
        dataSource: getActionDDL(),
        filter: "contains"
    })
}

function getActionDDL() {
    try {
        var entityGridLineNo = myMPManager.general.gridlinenumber.data("kendoGrid");
        var entityGrid = myMPManager.general.gridjobline.data("kendoGrid");
        var selectedItem = entityGrid.dataItem(entityGrid.select());
        var s = null;
        if (entityGrid.select().index() != -1) {
            var temp = selectedItem._job + '-' + selectedItem._jobsuffix + ' ' + selectedItem._reworkst;
            s = temp.split('-');
        }
        var result = ["CHOOSE ACTION"];

        if (selectedItem._status == "NEW") {
            if (myMPManager.general.roleUser.toUpperCase() == "REWORK") {
                result.push("REWORK");
                myMPManager.general.cbOperator.val(myMPManager.general.userName);
                bindOperatorDDL();
            }
            else {
                if (selectedItem._status == "NEW" && entityGridLineNo.select().index() != 0) {
                    if (selectedItem._tansnum != 0) {
                        result = ["CHOOSE ACTION", "REWORK", "SKIP", "START"];
                    }
                } else {
                    result = ["CHOOSE ACTION", "REWORK", "SKIP", "START", "UPDATE"];
                }

            }
        } else if (selectedItem._status == "START") {
            if (myMPManager.general.roleUser.toUpperCase() == "REWORK") {
                result.push("REWORK");
                myMPManager.general.cbOperator.val(myMPManager.general.userName);
                bindOperatorDDL();
            } else {
                if (entityGridLineNo.select().index() != 0) {
                    result.push("REWORK");
                    result.push("COMPLETED");
                    if (s.length <= 2) {
                        result.push("SPLIT");
                    }
                    result.push("PAUSE");
                    if (selectedItem._tansnum != 0 || selectedItem != null) {
                        result.push("UPDATE");
                    }
                }
                else {
                    result.push("COMPLETED");
                    if (s.length <= 2) {
                        result.push("SPLIT");
                    }
                    result.push("PAUSE");
                    if (selectedItem._tansnum != 0 || selectedItem != null) {
                        result.push("UPDATE");
                    }
                }
            }

        } else if (selectedItem._status == "COMPLETED" && myMPManager.general.roleUser.toUpperCase() != "REWORK") {
            if (selectedItem._tansnum != 0 || selectedItem != null) {
                result.push("UPDATE");
            }
        } else if (selectedItem._status == "PAUSE" && myMPManager.general.roleUser.toUpperCase() != "REWORK") {
            result.push("RESUME");
            if (selectedItem._tansnum != 0 || selectedItem != null) {
                result.push("UPDATE");
            }
        }

        //if (selectedItem._status == "NEW" && entityGrid.select().index() != 0) {
        //    result = ["CHOOSE ACTION", "REWORK", "PAUSE", "START", "UPDATE"];
        //} else if (selectedItem._status == "NEW" && selectedItem._rework == 1) {
        //    result = ["CHOOSE ACTION", "SKIP", "START", "UPDATE"];
        //} else if (selectedItem._status == "NEW") {
        //    result = ["CHOOSE ACTION", "SKIP", "START"];
        //} else if (selectedItem._status == "START") {
        //    result = ["CHOOSE ACTION", "REWORK", "COMPLETED", "SPLIT", "PAUSE", "UPDATE"]
        //} else if (selectedItem._status == "COMPLETED") {
        //    result = ["CHOOSE ACTION", "UPDATE"]
        //} else if (selectedItem._status == "PAUSE") {
        //    result = ["CHOOSE ACTION", "RESUME", "UPDATE"]
        //} else {
        //    result = ["CHOOSE ACTION"]
        //}
    } catch (ex) {
        result = ["PLEASE CHOOSE A LINE"];
    }

    return result;
}

function bindRejecCode() {
    $('#cmbRejCode').kendoDropDownList({
        optionLabel: " ",
        dataTextField: "customRej",
        dataValueField: "_RejectedCode",
        change: function () {
            var dropdownlist = $("#cmbRejCode").data("kendoDropDownList");
            detailRejManager.general.txtRejDesc.val(dropdownlist.text());
        }
        ,
        dataSource: mylocalStorage.JobRoute.getRejectedCodeAll(myMPManager.general.txtWorkCenter.val(), myMPManager.general.lblItemCode.html()),
        filter: "contains"
    })
}

function getCurrentRole() {
    $.ajax({
        async: false,
        url: "/JobRoute/getCurrentRole",
        //data: "{ 'itemID' : " + itemID + " }",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            myMPManager.general.roleUser = data.role;
            myMPManager.general.userName = data.name.Data;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            NotifError(errorThrown);
        }
    });
}

function updateList() {
    var listRejected = [];
    $.each(myMPManager.general.listReject, function (index, item) {
        var obj = {
            _rejid: item._rejid,
            _refID: item._refID,
            _RejectedCode: item._RejectedCode,
            _RejectedDesc: item._RejectedDesc,
            _RejectedQty: item._RejectedQty
        }
        listRejected.push(obj);
    });
    myMPManager.general.hfTotalRejected.val(myMPManager.general.listTotal);
    detailRejManager.general.hfListReject.val(JSON.stringify(listRejected));
    return true;
}

$(document).ready(function () {
    myMPManager.general.init();
    detailRejManager.general.init();
    bindMachineDDL();
    bindOperatorDDL();
    bindDateTimePicker();
    getCurrentRole();

    myMPManager.general.txtStartDate.val(null);
    myMPManager.general.txtEndDate.val(null);

    bindActionDDL();

    var ddlOP = $("#cbOperator").data("kendoDropDownList");
    ddlOP.enable(false);

    var ddlAction = $("#cbAction").data("kendoDropDownList");
    ddlAction.enable(false);

    $("#addReject").attr("disabled", "disabled");
    $("#btnPrint").attr("disabled", "disabled");
    $("#btnPrintLine").attr("disabled", "disabled");
    $("#btnSave").attr("disabled", "disabled");
});
