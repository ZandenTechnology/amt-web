﻿mylocalStorage["JobTransfer"] = {
    GetJobTransfer: function (datestart, dateend, jobnumber, skip, take) {
        var result;

        $.ajax({
            async: false,
            // url: "/Machine/GetAllMachineData",
            data: "{ 'start': '" + datestart + "','end': '" + dateend + "', 'JobNo':'" + jobnumber + "', 'skip':'" + skip + "' , 'take':'" + take + "'}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    }
}

var MyPos = {
    general: {
        grid: '',
        datestart: '',
        dateend: '',
        JobNumber: '',
        search:'',
        init: function () {
            this.JobNumber = $("#jobnum");
            this.datestart = $("#start");
            this.dateend = $("#end");
            this.grid = $("#gridview");
            this.search = $("#search");
        },
        initform:function()
        {
            var todayDate = kendo.toString(kendo.parseDate(new Date()), 'MM/dd/yyyy');
            MyPos.general.datestart.kendoDatePicker({ format: "MM/dd/yyyy" });
            MyPos.general.dateend.kendoDatePicker({ format: "MM/dd/yyyy" });

            MyPos.general.datestart.data("kendoDatePicker").value(todayDate);
            MyPos.general.dateend.data("kendoDatePicker").value(todayDate);
        },
        initGrid: function () {
            var Totalvar = 0;
            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var a = $("#ddlSession").val();
                        var data = mylocalStorage.JobTransfer.GetJobTransfer(MyPos.general.datestart.val(), MyPos.general.dateend.val(), MyPos.general.JobNumber.val(), options.data.take == undefined ? 1 : options.data.take, options.data.skip == undefined ? 1 : options.data.skip);
                        Totalvar = data==undefined?0: data.Total;
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "Queueno",
                        fields:
                        {
                          


                        }
                    }
                }
            })



            if (list != undefined) {
                if (MyPos.general.grid.data("kendoGrid") != undefined) {
                    MyPos.general.grid.data("kendoGrid").destroy();
                }

                //if (rmManager.general.gridviewcontrol.data("kendoGrid") != undefined) {
                //    rmManager.general.initGridCtrl("");
                //}

                MyPos.general.grid.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,


                    selectable: "row",
                    editable: "inline",
                    //change: onChange,

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [{
                        field: "Job",
                        title: "Job"
                    }, {
                        field: "suffix",
                        title: "Suffix"
                    },
                    {
                        field: "item",
                        title: "Item"
                    },
                    {
                        field: "quantity",
                        title: "Quantity"
                    },
                    {
                        field: "complete",
                        title: "Completed Date"
                    }

                    ]
                });


            }


        },
        RefreshGrid: function () {
            myPOS.general.grid.data("kendoGrid").dataSource.read();
            myPOS.general.grid.data('kendoGrid').refresh();
        }
    }
}


$(document).ready(function () {
    MyPos.general.init();
    MyPos.general.initform();

    MyPos.general.initGrid();


})

MyPos.general.search.click(function () {
    MyPos.general.RefreshGrid();
});