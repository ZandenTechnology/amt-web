﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        btnedit: '',
        btncreate: '',
        btndelete: '',
        modalform: '',

        init: function () {
            this.hfRoleID = $("#RoleID");
            this.hfRole = $("#Role");
            this.hfBSID = $("#BizSourceID");
            this.hfRoleDesc = $("#RoleDesc");
            this.hfBiz = $("#txtBizSource");
            this.hfArea = $("#txtAreaID");
            this.mpActive = $("#IsActive");
            this.mpActiveNew = $("#IsActive-new");
            this.rbINT = $("#rbINT");
            this.rbEXT = $("#rbEXT");

            this.gridview = $("#grid");
            this.ddlState = $('#ddlSearch');
            this.txtSearch = $('#txtSearch');
            this.btnedit = $('#btn-edit');
            this.btncreate = $('#btn-create');
            this.btndelete = $('#btn-delete');
            this.modalform = $('#modal-form');
            this.modalformupdate = $('#modal-form-update');
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            this.initGrid();

            //FILTER
            $('#btnSearch').click(function () {

                myMPManager.general.refreshGrid();
                return false;
            });


            
            //edit
            myMPManager.general.btnedit.click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    rmManager.general.listControls = [];
                    myMPManager.general.modalformupdate.modal("show");

                    myMPManager.general.hfRoleID.val(selectedItem.RoleID);
                    myMPManager.general.hfRole.val(selectedItem.Role);
                    myMPManager.general.hfRoleDesc.val(selectedItem.RoleDesc);
                    myMPManager.general.mpActive.val(selectedItem.IsActive);

                    bindBizSource();
                    bindAreaProcess();

                    if (selectedItem.RoleType == "INT") {
                        radiobtn = document.getElementById("rbINT");
                        radiobtn.checked = true;
                    }
                    else {
                        radiobtn = document.getElementById("rbEXT");
                        radiobtn.checked = true;
                    }

                    if (selectedItem.IsActive == true) {
                        $("#status-is-active").removeClass("fa-times-circle-o");
                        $("#status-is-active").removeClass("status-inactive");
                        $("#status-is-active").addClass("status-active");
                        $("#status-is-active").addClass("fa-check-circle-o");

                    } else {
                        $("#status-is-active").removeClass("fa-check-circle-o");
                        $("#status-is-active").removeClass("status-active");
                        $("#status-is-active").addClass("status-inactive");
                        $("#status-is-active").addClass("fa-times-circle-o");
                    }

                    rmManager.general.initGridUpdate();


                    rmManager.general.btnUpdate.live('click', function (e) {

                        //jQuery.map
                        var data = $.grep(rmManager.general.gridviewupdate.data("kendoGrid").dataSource.data(), function (e) { return e.IsMenuActive });

                        var listMenu = [];
                        $.each(data, function (index, item) {
                            var obj = {
                                RoleID: myMPManager.general.hfRoleID.val(),
                                MenuID: item.MenuID,
                                IsMenuActive: item.IsMenuActive
                            }
                            listMenu.push(obj);
                        });


                        var listControl = [];

                        $.each(rmManager.general.listControls, function (index, item) {
                            var obj = {
                                RoleID: myMPManager.general.hfRoleID.val(),
                                CtrlID: item.CtrlID,
                                IsActive: item.IsActive
                            }
                            listControl.push(obj);
                        });

                        rmManager.general.hflistcontrolupdate.val(JSON.stringify(listControl));
                        rmManager.general.hflistmenuupdate.val(JSON.stringify(listMenu));

                        return true;
                    });
                }

            });


            $(".btn-change-active").click(function () {
                if (myMPManager.general.mpActive.val() == "true") {
                    myMPManager.general.mpActive.val(false);
                    $("#status-is-active").removeClass("status-active");
                    $("#status-is-active").removeClass("fa-check-circle-o");
                    $("#status-is-active").addClass("status-inactive");
                    $("#status-is-active").addClass("fa-times-circle-o");
                } else {
                    myMPManager.general.mpActive.val(true);
                    $("#status-is-active").removeClass("status-inactive");
                    $("#status-is-active").removeClass("fa-times-circle-o");
                    $("#status-is-active").addClass("fa-check-circle-o");
                    $("#status-is-active").addClass("status-active");
                }
            });

            $('#import-excel').click(function () {
                var displayedData = myMPManager.general.gridview.data().kendoGrid.dataSource.view();

                var tbExcel = "<table id='tbExcel' style='display:none;'>" +
                "<tr><th>RoleID</th><th>Role</th><th>Area Name</th><th>Biz Source</th><th>Role Type</th><th>Description</th><th>Is Active</th> " +
                "<th>Input Date</th><th>Input User</th><th>Modif Date</th><th>Modif User</th></tr>";

                jQuery.each(displayedData, function (i, val) {

                    //var CalcSpecDueDateString = val.CalcSpecDueDateString == null ? "-" : val.CalcSpecDueDateString.toLocaleDateString();
                    var InputDateString = val.InputDate == null ? "-" : val.InputDate.toLocaleDateString();
                    var ModifDateString = val.ModifDate == null ? "-" : val.ModifDate.toLocaleDateString();

                    tbExcel = tbExcel + "<tr><td>" + val.RoleID + "</td><td>" + val.Role + "</td><td>" + val.Area + "</td><td>" + val.BizSource + "</td>" +
                        "<td>" + val.RoleType + "</td><td>" + val.RoleDesc + "</td><td>" + val.IsActive + "</td><td>" + InputDateString + "</td><td>" + val.InputUser + "</td>" +
                        "<td>" + ModifDateString + "</td><td>" + val.ModifUser + "</td></tr>";
                });

                tbExcel = tbExcel + "</table>";

                $('table#tbExcel').remove();
                $('body').append(tbExcel);

                var date = new Date();

                $('table#tbExcel').table2excel({
                    exclude: ".noExl",
                    name: "Job Export " + date.toLocaleDateString(),
                    filename: "Job Export " + date.toLocaleDateString(),
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            });

            //create
            myMPManager.general.btncreate.click(function () {

                rmManager.general.listControls = [];

                myMPManager.general.hfRoleID.val("");
                myMPManager.general.hfRole.val("");
                myMPManager.general.hfRoleDesc.val("");
                myMPManager.general.hfArea.val("");
                myMPManager.general.hfBiz.val("");
                myMPManager.general.mpActiveNew.val(true);

                myMPManager.general.modalform.modal("show");

            });


            //delete
            myMPManager.general.btndelete.click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    Delete(selectedItem.RoleID);
                }
            });



        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read

                        var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                        var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                        var page = options.data.page == undefined ? 1 : options.data.page;
                        var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;
                        var searchKey = myMPManager.general.txtSearch.val();

                        var data = mylocalStorage.RolesPermission.GetAllRolesData(myMPManager.general.ddlState.val(), searchKey, skip, take);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "Total",
                    model: {
                        id: "id",
                        fields:
                        {
                            RoleID: { type: "string", editable: false },
                            RoleType: { type: "string", editable: false },
                            Role: { type: "string", editable: false },
                            RoleDesc: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                            InputDate: { type: "date", editable: false },
                            InputUser: { type: "string", editable: false },
                            ModifDate: { type: "date", editable: false },
                            ModifUser: { type: "string", editable: false },
                        }
                    }
                }

            })



            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    dataBound: onDataBound,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [

                        {
                            field: "RoleID",
                            title: "Role ID",
                            //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                            width: 100
                        },
                        {
                            field: "Role",
                            title: "Role"
                        },
                        {
                            field: "RoleType",
                            title: "Role Type"
                        },
                        {
                            field: "RoleDesc",
                            title: "Description"
                        },
                        {
                            field: "IsActive",
                            title: "Is Active"
                        },
                        {
                            field: "InputDate",
                            title: "Input Date",
                            format: "{0:dd/MM/yyyy HH:mm}"
                        },
                        {
                            field: "InputUser",
                            title: "Input User"
                        },
                        {
                            field: "ModifDate",
                            title: "Modif Date",
                            format: "{0:dd/MM/yyyy HH:mm}"
                        },
                        {
                            field: "ModifUser",
                            title: "Modif User"
                        }

                    ]
                });

                //Add Class
                myMPManager.general.gridview.find("table").addClass("table table-condensed");

            }


        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        }
    }
}



function Delete(ID) {

    //SweetAlert begin
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            var result;

            $.ajax({
                async: false,
                url: "/Roles/DeleteRole",
                data: "{ 'RoleID': '" + ID + "' ,'url': '" + window.location.pathname.replace("/", "") + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data["success"] != undefined) {
                        swal("Deleted!", data["success"], "success");

                        myMPManager.general.refreshGrid();
                    }
                    else {
                        swal("Deleted!", data["error"], "error");
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });

            
        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};


function Duplicate(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "",
        text: "Are you sure want to duplicate this products?",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, duplicate it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            getProductDetailViewDuplicate(item.id);
            swal("Duplicated!", "Your data has been duplicated.", "success");

        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }

}

function bindBizSource() {
    //BindBsource
    try {
        $('.txtBizSource').kendoDropDownList({
            dataTextField: "BizSource",
            dataValueField: "BizSourceID",
            dataSource: mylocalStorage.BizSource.GetAllBizSource("btn_create"),
            filter: "contains"
        })
    } catch (e) {

    }
}


function bindAreaProcess() {
    //BindBsource
    try {
        $('.txtAreaID').kendoDropDownList({
            dataTextField: "Area",
            dataValueField: "AreaID",
            dataSource: mylocalStorage.AreaProcess.GetDDLAreaProcess("btn_edit"),
            filter: "contains"
        })
    } catch (e) {

    }
}

$('#status-is-active').click(function () {

});

$(document).ready(function () {
    myMPManager.general.init();
    rmManager.general.init();

});
