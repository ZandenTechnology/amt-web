﻿/// <reference path="CObyItem.js" />
var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridAvailable = $("#grid-available-work");

            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.hfItemID = $("#hfItemID");
            this.hflistItemData = $("#listItemData");
            this.txtFromDate = $("#txtFromDate");
            this.txtToDate = $("#txtToDate");

            this.listItemData = [];
            this.listAvailable = [];
            this.flagRead = 0;

            //localTempStorage.Products.sync();
            this.initGridAvailable();

            $("#btnCancel").click(function () {
                myMPManager.general.flagRead = 0;
                myMPManager.general.refreshGrid();
            });

            $("#btnSave").click(function () {
                var entityGrid = myMPManager.general.gridAvailable.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != undefined) {
                    $.LoadingOverlay("show", {
                        minSize: "20px",
                        maxSize: "30px",
                        image: "",
                        fontawesome: "fa fa-spinner fa-spin"
                    });
                    $.ajax({
                        async: false,
                        url: "/COItem/PrintCObyItem",
                        data: "{ 'itemCode': '" + selectedItem._itemCode + "','dateFrom': '" + myMPManager.general.txtFromDate.val() + "', 'dateTo': '" + myMPManager.general.txtToDate.val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {

                            if (data["redirect"] != null) {

                                window.open(
                                      data["redirect"],
                                      '_blank' // <- This is what makes it open in a new window.
                                    );

                            }
                            if (data["excel"] != null) {

                                window.open(
                                      data["excel"],
                                      '_blank' // <- This is what makes it open in a new window.
                                    );

                            }
                            if (data["success"] == undefined) {
                                NotifError(data["error"]);
                            } else {
                                NotifSuccess(data["success"]);
                            }

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(errorThrown);
                        }

                    })
                    $.LoadingOverlay("hide");

                } else {
                    NotifError("Please select item code");
                }
            });

        },

        initGridAvailable: function () {

            var list = new kendo.data.DataSource
            ({
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridAvailable.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        if (selectedItem == null && myMPManager.general.flagRead == 0) {
                            myMPManager.general.listAvailable = mylocalStorage.Item.GetAllItemData(0);
                        }

                        options.success(sortResults(myMPManager.general.listAvailable, "_itemid", true));
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                //page: 1,
                //pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "_itemid",
                        fields:
                        {
                            _itemid: { type: "number", editable: false },
                            _itemCode: { type: "string", editable: false },
                            _itemdescription: { type: "string", editable: false }
                        }
                    }
                }
            });

            if (list != undefined) {

                myMPManager.general.gridAvailable.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,

                    columns: [
                        {
                            field: "_itemid",
                            title: "Item Code",
                            template: "#:_itemCode#  -  #:_itemdescription#"
                        }
                    ]
                });

            }

        },
        refreshGrid: function () {
            try {
                myMPManager.general.gridAvailable.data("kendoGrid").dataSource.read();
                myMPManager.general.gridAvailable.data('kendoGrid').refresh();
            } catch (e) { }
        }
    }
}

function ViewResetSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        myMPManager.general.flagRead = 0;
        myMPManager.general.gridResource.data("kendoGrid").dataSource.read();
        myMPManager.general.gridResource.data('kendoGrid').refresh();
        myMPManager.general.refreshGrid();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {
    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}

$(document).ready(function () {
    myMPManager.general.init();
});