﻿mylocalStorage["Import"] = {
    GetImportData: function (datestart, dateend, jobnumber, skip, take) {
        var result;

        $.ajax({
            async: false,
            // url: "/Machine/GetAllMachineData",
            data: "{ 'start': '" + datestart + "','end': '" + dateend + "', 'JobNo':'" + jobnumber + "', 'skip':'" + skip + "' , 'take':'" + take + "'}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    }
}

var MyPos = {
    general: {
        grid: '',
        fileupload: '',
        save: '',
        cancel: '',
        init: function () {
            this.grid = $("#grid");
            this.fileupload = $("#fileupload");
            this.save = $("#save");
            this.cancel = $("#cancel");
        },

        initGrid: function () {
            var Totalvar = 0;
            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        //var a = $("#ddlSession").val();
                        //var data = mylocalStorage.JobTransfer.GetJobTransfer(MyPos.general.datestart.val(), MyPos.general.dateend.val(), MyPos.general.JobNumber.val(), options.data.take == undefined ? 1 : options.data.take, options.data.skip == undefined ? 1 : options.data.skip);
                        //Totalvar = data == undefined ? 0 : data.Total;
                        //options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data",
                    //total: "Total",
                    model: {
                        id: "Queueno",
                        fields:
                        {



                        }
                    }
                }
            })



            //if (list != undefined) {
                if (MyPos.general.grid.data("kendoGrid") != undefined) {
                    MyPos.general.grid.data("kendoGrid").destroy();
                }

                //if (rmManager.general.gridviewcontrol.data("kendoGrid") != undefined) {
                //    rmManager.general.initGridCtrl("");
                //}

                MyPos.general.grid.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,


                    selectable: "row",
                    editable: "inline",
                    //change: onChange,

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [{
                        field: "txntyp",
                        title: "Txn Type"
                    }, {
                        field: "txndate",
                        title: "Transaction Date"
                    },
                    {
                        field: "docref",
                        title: "Doc Ref"
                    },
                    {
                        field: "customer",
                        title: "Customer"
                    },
                    {
                        field: "pn",
                        title: "P/N"
                    },
                    {
                        field: "partdesc",
                        title: "Part Description"
                    },
                    {
                        field: "qty",
                        title: "Quantity"
                    },
                    {
                        field: "remarks",
                        title: "Remarks"
                    }

                    ]
                });


            //}
        },
        initCtrl: function () {

            var ALLOWED_EXTENSIONS = [".xlsx", ".csv", ".txt", ".json"];

           MyPos.general.fileupload.kendoUpload({
                async: {
                    saveUrl: "/csvs"
                },
                multiple: false,
                localization: {
                    "select": "Select file to import..."
                },
                select: function (e) {
                    var extension = e.files[0].extension.toLowerCase();
                    if (ALLOWED_EXTENSIONS.indexOf(extension) == -1) {
                        alert("Please, select a supported file format");
                        e.preventDefault();
                    }
                },
                success: function (e) {
                    // Load the converted document into the spreadsheet
                    spreadsheet.fromJSON(e.response);
                }
            });
        }

    }
}

$(document).ready(function () {
    MyPos.general.init();
    MyPos.general.initCtrl();
    MyPos.general.initGrid();
})


MyPos.general.save.click(function () {
    var data = MyPos.general.grid.data().kendoGrid.dataSource.view();
    var jsoned = JSON.stringify(data);

    var result = '';

    $.ajax({
        async: false,
        // url: "/Machine/GetAllMachineData",
        data: "{ 'data': '" +jsoned+ "'}",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            result = jQuery.parseJSON(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            NotifError(errorThrown);
        }
    });
})