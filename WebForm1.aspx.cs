﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UltimoApps
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReportDocument crystalReport = new ReportDocument();
            crystalReport.Load(Server.MapPath("CrystalReport1.rpt"));
            //var data = CrystalReportClass.GetView_AnnualStatement(int.Parse(Request.QueryString["ID"]));
            //crystalReport.SetDataSource(data);
            CrystalReportViewer1.ReportSource = crystalReport;
        }
    }
}