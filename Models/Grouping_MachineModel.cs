﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Grouping_MachineModel
    {
        [Display(Name = "Work Center ID")]
        [Required(ErrorMessage = "Work Center ID can't be empty.")]
        public string _wc {get;set;}

        [Display(Name = "Work Center Name")]
        [Required(ErrorMessage = "Work Center Name can't be empty.")]
        public string _wcdesc {get;set;}

        [Display(Name = "list Machine")]
        public string listMachine {get;set;}
    }
}