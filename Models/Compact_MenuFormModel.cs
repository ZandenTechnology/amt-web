﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Compact_MenuFormModel
    {
        [Display(Name = "Menu ID")]
        [Required(ErrorMessage = "Menu ID can't be empty.")]
        public string MenuID { get; set; }

        [Display(Name = "Parent ID")]
        [Required(ErrorMessage = "Parent ID can't be empty.")]
        public string ParentID { get; set; }

        [Display(Name = "Menu Level")]
        [Required(ErrorMessage = "Menu Level can't be empty.")]
        public string MenuLvl { get; set; }

        [Display(Name = "Menu")]
        [Required(ErrorMessage = "Menu can't be empty.")]
        public string Menu { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description can't be empty.")]
        public string MenuDesc { get; set; }

        [Display(Name = "Is Active")]
        [Required(ErrorMessage = "Is Active can't be empty.")]
        public bool IsActive { get; set; }

        [Display(Name = "Is Hide")]
        [Required(ErrorMessage = "Is Hide can't be empty.")]
        public bool IsHide { get; set; }

        [Display(Name = "Menu URL")]
        [Required(ErrorMessage = "Menu can't be empty.")]
        public string MenuURL { get; set; }

        [Display(Name = "List Form")]
        public string ListForm { get; set; }
    }
}