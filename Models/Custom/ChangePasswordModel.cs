﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models.Custom
{
    public class ChangePasswordModel
    {
        
        [Required]
        [Display(Name = "User ID")]
        public long UserID { get; set; }

        [Display(Name = "Old Password")]
        [Required(ErrorMessage = "Old Password can't be empty.")]
        public string OldPassword { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password can't be empty.")]
        //[StringLength(int.MaxValue, MinimumLength = 6)]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }
    }
}