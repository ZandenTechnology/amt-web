﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models.Custom
{
    public class RoleModel
    {
        [Display(Name = "Role ID")]
        [Required(ErrorMessage = "Role ID can't be empty.")]
        public string RoleID { get; set; }

        [Display(Name = "Role")]
        [Required(ErrorMessage = "Role can't be empty.")]
        public string Role { get; set; }

        [Display(Name = "Biz Source")]
        [Required(ErrorMessage = "Biz Source can't be empty.")]
        public string BizSourceID { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Area ID")]
        [Required(ErrorMessage = "Area ID can't be empty.")]
        public string AreaID { get; set; }

        [Display(Name = "Role Type")]
        [Required(ErrorMessage = "Role Type can't be empty.")]
        public string RoleType { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Menu")]
        public string ListMenuID { get; set; }

        [Display(Name = "Control")]
        public string ListControlID { get; set; }
    }
}