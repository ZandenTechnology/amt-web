﻿using UltimoApps.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class MenuBindingModel
    {
        private List<GetAllMenuFormDataResult> _Data;

        public List<GetAllMenuFormDataResult> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }
        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }
}