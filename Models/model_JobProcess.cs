﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class model_JobProcess
    {
        private List<JobProcessModel> _Data;

        public List<JobProcessModel> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }
}