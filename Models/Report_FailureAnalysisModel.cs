﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Report_FailureAnalysisModel
    {
        [Display(Name = "ItemCode")]
        [Required(ErrorMessage = "ItemCode can't be empty.")]
        public string ItemCode { get; set; }

        [Display(Name = "FromDate")]
        [Required(ErrorMessage = "FromDate can't be empty.")]
        public DateTime FromDate { get; set; }

        [Display(Name = "ToDate")]
        [Required(ErrorMessage = "ToDate can't be empty.")]
        public DateTime ToDate { get; set; }
    }
}