﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Compact_BizSourceModel
    {
        [Display(Name = "CompID")]
        [Required(ErrorMessage = "CompID can't be empty.")]
        public string CompID { get; set; }

        [Display(Name = "OldCompID")]
        [Required(ErrorMessage = "OldCompID can't be empty.")]
        public string OldCompID { get; set; }

        [Display(Name = "CompTypeID")]
        [Required(ErrorMessage = "CompTypeID can't be empty.")]
        public string CompTypeID { get; set; }

        [Display(Name = "Company")]
        [Required(ErrorMessage = "Company can't be empty.")]
        public string Company { get; set; }

        [Display(Name = "FullName")]
        [Required(ErrorMessage = "FullName can't be empty.")]
        public string FullName { get; set; }

        [Display(Name = "CityID")]
        [Required(ErrorMessage = "CityID can't be empty.")]
        public int CityID { get; set; }

        [Display(Name = "Building")]
        public string Building { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Is Active")]
        [Required(ErrorMessage = "Is Active can't be empty.")]
        public bool IsActive { get; set; }

        [Display(Name = "List Biz")]
        public string ListBiz { get; set; }
    }
}