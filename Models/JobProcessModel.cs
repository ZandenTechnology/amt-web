﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class JobProcessModel
    {
        public string _job;
        public string job
        {
            get { return _job; }
            set { _job = value; }
        }

        public string _item { get; set; }
        public int _oper_num { get; set; }
        public string _wc { get; set; }
        public double _jobDate { get; set; }
        public string _jobsuffix { get; set; }
        public string _jobsuffixParent { get; set; }
        public double _qty_Rele_qty { get; set; }
        public double _qty_op_qty { get; set; }
        public double _qty_complete { get; set; }
        public double _qty_scrapped { get; set; }
        public double _Start_Date { get; set; }
        public double _end_Date { get; set; }

        public DateTime jobDateParsed { get; set; }
        public DateTime jobStartParsed { get; set; }
        public DateTime jobEndParsed { get; set; }
    }
}