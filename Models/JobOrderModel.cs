﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class JobOrderModel
    {
        public string _job;
        public string job
        {
            get { return _job;}
            set { _job = value;}
        }

        public int _jobsuffix { get; set; }
        public double _jobDate { get; set; }
        public string _jobtype { get; set; }
        public string _item { get; set; }
        public double _qtyReleased { get; set; }
        public double _qtyCompleted { get; set; }
        public double _qtyScrapped { get; set; }
        public string _status { get; set; }
        public DateTime jobDateParsed { get; set; }
    }

    public class JobDetailModel
    {
        public string _job;
        public string job
        {
            get { return _job; }
            set { _job = value; }
        }

        public int _jobsuffix { get; set; }
        public double _jobDate { get; set; }
        public string _jobtype { get; set; }
        public string _item { get; set; }
        public double _qtyReleased { get; set; }
        public double _qtyCompleted { get; set; }
        public double _qtyScrapped { get; set; }
        public string _status { get; set; }
        public DateTime jobDateParsed { get; set; }
        public string _wc { get; set; }
        public string _description { get; set; }
        public string _dept { get; set; }
    }

    public class model_JobOrder
    {
        private List<JobOrderModel> _Data;

        public List<JobOrderModel> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }

    public class model_JobDetailsOrder
    {
        private List<JobDetailModel> _Data;

        public List<JobDetailModel> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }
}