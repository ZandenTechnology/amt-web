﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class ImportJobModel
    {
        [Display(Name = "From")]
        [Required(ErrorMessage = "From can't be empty.")]
        public string From { get; set; }

        [Display(Name = "To")]
        [Required(ErrorMessage = "To can't be empty.")]
        public string To { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "Type can't be empty.")]
        public string Type { get; set; }


        [Display(Name = "JobNo")]
        [Required(ErrorMessage = "JobNo can't be empty.")]
        public string JobNo { get; set; }

    }
}