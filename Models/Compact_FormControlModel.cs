﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Compact_FormControlModel
    {
        [Display(Name = "Form ID")]
        [Required(ErrorMessage = "FormID can't be empty.")]
        public string FormID { get; set; }

        [Display(Name = "WebForm")]
        [Required(ErrorMessage = "WebForm can't be empty.")]
        public string WebForm { get; set; }

        [Display(Name = "FormType")]
        [Required(ErrorMessage = "Caption can't be empty.")]
        public string FormType { get; set; }

        [Display(Name = "Caption")]
        [Required(ErrorMessage = "Caption can't be empty.")]
        public string Caption { get; set; }

        [Display(Name = "FileName")]
        public string FileName { get; set; }

        [Display(Name = "IsActive")]
        [Required(ErrorMessage = "IsActive can't be empty.")]
        public bool IsActive { get; set; }

        [Display(Name = "InputDate")]
        public DateTime InputDate { get; set; }

        [Display(Name = "InputUser")]
        public string InputUser { get; set; }

        [Display(Name = "ModifDate")]
        public DateTime ModifDate { get; set; }

        [Display(Name = "ModifUser")]
        public string ModifUser { get; set; }

        [Display(Name = "ListControl")]
        public string ListControl { get; set; }
    }
}