﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Grouping_WorkCenterModel
    {
        [Display(Name = "Resource ID")]
        [Required(ErrorMessage = "FormID can't be empty.")]
        public long _rid { get; set; }

        [Display(Name = "Work Center")]
        [Required(ErrorMessage = "Work Center can't be empty.")]
        public long _wc { get; set; }

        [Display(Name = "Resource Group ID")]
        [Required(ErrorMessage = "Resource Group ID can't be empty.")]
        public string _rGroupID {get;set;}

        [Display(Name = "Resource Group Name")]
        [Required(ErrorMessage = "Resource Group Name can't be empty.")]
        public string _rGroupName{get;set;}

        [Display(Name = "list Work Center")]
        public string listWorkCenter {get;set;}
    }
}