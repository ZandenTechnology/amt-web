﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.DAL;

namespace UltimoApps.Models
{
    public class UserRegistrationBindingModel
    {
        private List<GetAllRegistrationDataResult> _Data;

        public List<GetAllRegistrationDataResult> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }
        private List<GetAllRegistrationDataTotalResult> _Total;

        public List<GetAllRegistrationDataTotalResult> Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }
}