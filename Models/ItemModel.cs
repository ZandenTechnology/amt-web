﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class ItemModel
    {
        public int _itemid { get; set; }
        public string _itemCode{get; set;}
        public string _itemdescription { get; set; }
        public string _type { get; set; }
        public string _Drawing_Nbr { get; set; }
        public string _Revision { get; set; }
    }
}