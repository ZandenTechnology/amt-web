﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.DAL;

namespace UltimoApps.Models
{
    public class FormControlModel
    {
        private List<GetAllFormDataResult> _Data;

        public List<GetAllFormDataResult> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }
        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }
}