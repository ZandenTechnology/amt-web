﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class UserRoleModel
    {
        [Display(Name = "User ID")]
        [Required(ErrorMessage = "Role ID can't be empty.")]
        public long UserID {get;set;}

        [Display(Name = "Username")]
        [Required(ErrorMessage = "Username can't be empty.")]
        public string UserName { get; set; }

        [Display(Name = "Company")]
        [Required(ErrorMessage = "Company can't be empty.")]
        public string CompID { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Firstname can't be empty.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "You must select a gender")]
        public char Gender { get; set; }

        [Display(Name = "Birthdate")]
        [Required(ErrorMessage = "Birthdate can't be empty.")]
        public DateTime BirthDate { get; set; }

        [Display(Name = "Division")]
        [Required(ErrorMessage = "Division can't be empty.")]
        public string Division { get; set; }

        [Display(Name = "Job Position")]
        [Required(ErrorMessage = "Job Position can't be empty.")]
        public string JobPosition { get; set; }

        [Display(Name = "Work Phone")]
        [Required(ErrorMessage = "Work Phone can't be empty.")]
        public string WorkPhone { get; set; }

        [Display(Name = "Work Phone Ext")]
        [Required(ErrorMessage = "Work Phone Ext can't be empty.")]
        public string WorkPhoneExt { get; set; }

        [Display(Name = "Phone No")]
        public string PhoneNo { get; set; }

        [Display(Name = "Mobile No")]
        public string MobileNo { get; set; }

        [Display(Name = "Sec Mobile No")]
        public string SecMobileNo { get; set; }

        [Display(Name = "Corp Email")]
        [Required(ErrorMessage = "Corp Email  can't be empty.")]
        public string CorpEmail { get; set; }

        [Display(Name = "Personal Email")]
        public string PersonalEmail { get; set; }

        [Display(Name = "Is Active")]
        [Required(ErrorMessage = "IsActive can't be empty.")]
        public bool IsActive { get; set; }

        [Display(Name = "Roles")]
        public string ListRolesID { get; set; }
    }
}