﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class DB_Parameter
    {
        public string name { get; set; }
        public dynamic value { get; set; }
        public SqlDbType type_data { get; set; }
    }
}