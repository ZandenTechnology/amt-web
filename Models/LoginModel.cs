﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace UltimoApps.Models
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password can't be empty.")]
        public string Password { get; set; }

        [Display(Name = "Remember me")]
        [Required(ErrorMessage = "Remember me can't be empty.")]
        public bool RememberMe { get; set; }
    }


}