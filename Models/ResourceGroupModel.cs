﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;

namespace UltimoApps.Models
{
    [Table("tbResourceGroup")]

    public class ResourceGroupModel 
    {
        [Required]
        [Display(Name = "rID")]
        public int _rid { get; set; } 

        [Display(Name = "Group ID")]
        [Required(ErrorMessage = "Group Name can't be empty.")]
        public string _rGroupID { get; set; }

        [Display(Name = "Group Name")]
        [Required(ErrorMessage = "Group Name can't be empty.")]
        public string _rGroupName  { get; set; }

        [Display(Name = "Transfer")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public char _transfer { get; set; }

        [Display(Name = "Email To")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public string _to { get; set; }

        [Display(Name = "Email CC")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public string _cc { get; set; }

        [Display(Name = "Yield Per")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public float _yieldper { get; set; }
    }
}