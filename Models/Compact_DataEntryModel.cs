﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Compact_DataEntryModel
    {
        public int tansnum { get; set; }

        public string jobNo { get; set; }

        public string jobSuffix { get; set; }

        public string jobSuffixParent { get; set; }

        public string emp_num { get; set; }

        public string end_Date { get; set; }

        public string start_Date { get; set; }

        public string item { get; set; }

        public string wc { get; set; }

        public float qty_op_qty { get; set; }

        public float qty_complete { get; set; }

        public float qty_scrapped { get; set; }

        public float total_rejected { get;set;}

        public float ARelQty {get;set;}

        public int oper_num { get; set; }

        public int number_of_operator { get; set; }

        public string status { get; set; }

        public string machineId { get; set; }

        public string remarks { get; set; }

        public string actionType { get; set; }

        public int ckbRemarks { get; set; }

        public string listRejected { get; set; }
    }
}