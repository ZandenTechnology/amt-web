﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;


namespace UltimoApps.Models
{
    [Table("tbwc")]

    public class WorkCenterModel
    {
        [Required]
        [Display(Name = "wcID")]
        public int _wcid { get; set; }

        [Display(Name = "Work Center ID")]
        [Required(ErrorMessage = "Work Center can't be empty.")]
        public string _wc { get; set; }

        [Display(Name = "Group Name")]
        [Required(ErrorMessage = "Group Name can't be empty.")]
        public string _rGroupName { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description can't be empty.")]
        public string _description { get; set; }

        [Display(Name = "Eficiency")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public bool _efficiency { get; set; }

        [Display(Name = "Dept")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public string _dept { get; set; }

        [Display(Name = "Transfer")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public string _transfer { get; set; }

        [Display(Name = "Yield Per")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public float _labour { get; set; }

        [Display(Name = "Run Rate")]
        //[Required(ErrorMessage = "Transfer can't be empty.")]
        public float _Run_Rate { get; set; }
    }
}