﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class JobVerificationModel
    {
        public string _job { get; set; }
        public string _jobsuffix { get; set; }
        public double _jobDate { get; set; }
        public string _jobtype { get; set; }
        public string _item { get; set; }
        public double _qtyReleased { get; set; }
        public double _qtyCompleted { get; set; }
        public double _qtyScrapped { get; set; }
        public string color { get; set; }

        public DateTime jobDateParsed { get; set; }
    }

    public class OperationDetailModel
    {
        public string _job;
        public string job
        {
            get { return _job; }
            set { _job = value; }
        }

        public int _oper_num { get; set; }
        public string _item { get; set; }
        public string _wc { get; set; }
        public string _end_Date { get; set; }
        public string _start_Date { get; set; }
        public string _jobDate { get; set; }
        public double _qtyCompleted { get; set; }
        public double _qtyScrapped { get; set; }
        public double _qtyReleased { get; set; }
        public double _qtyReceived { get; set; }
        public double _qty_Rele_qty { get; set; }

        public double received { get; set; }
        public string color { get; set; }
    }

    public class TransactionDetailModel
    {
        public string _job { get; set; }
        public int _oper_num { get; set; }
        public string _emp_num { get; set; }
        public string _machineid { get; set; }
        public string _trans { get; set; }
        public int _no_oper { get; set; }
        public string _jobsuffix { get; set; }
        public string _end_Date { get; set; }
        public string _start_Date { get; set; }
        public string _createDate { get; set; }
        public double _jobDate { get; set; }
        public string _jobtype { get; set; }
        public string _item { get; set; }
        public double _qty_op_qty { get; set; }
        public double _qty_Rele_qty { get; set; }
        public double _qty_complete { get; set; }
        public double _qty_scrapped { get; set; }
        public string _status { get; set; }
        public string _wc { get; set; }
        public string _description { get; set; }
        public string _dept { get; set; }

        public string color { get; set; }
        public DateTime jobDateParsed { get; set; }
    }

    public class model_JobVerification
    {
        private List<JobVerificationModel> _Data;

        public List<JobVerificationModel> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }

    public class model_JobOperationDetail
    {
        private List<OperationDetailModel> _Data;

        public List<OperationDetailModel> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }

    public class model_JobTransactionDetail
    {
        private List<TransactionDetailModel> _Data;

        public List<TransactionDetailModel> Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        private int _Total;

        public int Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }
}