﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UltimoApps.Models
{
    public class Grouping_RejectedModel
    {
        [Display(Name = "Resource Group ID")]
        public decimal _rgid { get; set; }

        [Display(Name = "Work Center")]
        public string _wc { get; set; }

        [Display(Name = "Item Code")]
        public string _itemCode { get; set; }

        [Display(Name = "Rejected ID")]
        public int _RejID { get; set; }

        [Display(Name = "Rejected Code")]
        public string _RejectedCode { get; set; }

        [Display(Name = "Rejected Description")]
        public string _RejectedDesc { get; set; }
    }
}