﻿//Declare Control

var BizSourceManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        btnType: '',

        init: function () {
            this.gridview = $("#biz-grid");
            this.gridviewnew = $("#biz-grid-new");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            this.hfListBiz = $("#ListBiz");
            this.hfListBiznew = $("#ListBizNew");

            if (this.btnType == "btn_edit") {
                this.initGrid();
            } else {
                this.initGridNew();
            }

            $('#btnSave').live('click', function (e) {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                var listMenu = [];
                $.each(BizSourceManager.general.gridview.data("kendoGrid").dataSource.data(), function (index, item) {
                    var obj = {
                        isSelected: item.isSelected,
                        BizSourceID: item.BizSourceID,
                        CompID: selectedItem.CompID
                    }
                    listMenu.push(obj);
                });

                BizSourceManager.general.hfListBiz.val(JSON.stringify(listMenu));
                return true;
            });

            $('#btnSave-new').live('click', function (e) {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                var listMenu = [];
                $.each(BizSourceManager.general.gridviewnew.data("kendoGrid").dataSource.data(), function (index, item) {
                    var obj = {
                        isSelected: item.isSelected,
                        BizSourceID: item.BizSourceID,
                        CompID: "0000"
                    }
                    listMenu.push(obj);
                });

                BizSourceManager.general.hfListBiznew.val(JSON.stringify(listMenu));
                return true;
            });

        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        var data = mylocalStorage.BizSource.GetAllBizSource("btn_edit", selectedItem.CompID);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "BizSourceID",
                        fields:
                        {
                            isSelected: { type: "boolean", editable: true },
                            BizSourceID: { type: "string", editable: false },
                            BizCategory: { type: "string", editable: false },
                            BizGroup: { type: "string", editable: false },
                            BizSource: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                            InputDate: { type: "date", editable: false },
                            InputUser: { type: "string", editable: false },
                            ModifDate: { type: "date", editable: false },
                            ModifUser: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (list != undefined) {

                BizSourceManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                    editable: "inline",
                    filterable: false,
                    selectable: "multiple",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            title: 'Select All',
                            field: "isSelected",
                            headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: "<input type=\"checkbox\" id=\"#: BizSourceID #\" class=\"k-checkbox check_row\" #= isSelected ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"#: BizSourceID #\"></label>",
                            width: 50,
                            filterable: false
                        },
                        {
                            field: "BizSourceID",
                            title: "BizSourceID",
                            width: 80
                        },
                        {
                            field: "BizSource",
                            title: "BizSource",
                            width: 80
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, BizSourceManager.general.gridview);
                });

                $("#header-chb").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = BizSourceManager.general.gridview.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = BizSourceManager.general.gridview.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("isSelected", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        BizSourceManager.general.gridview.data("kendoGrid").clearSelection();
                    }

                });

                var grid = BizSourceManager.general.gridview.data("kendoGrid");

                ////Add Class
                BizSourceManager.general.gridview.find("table").addClass("table table-condensed");
            }


        },

        initGridNew: function () {

            var list = new kendo.data.DataSource
            ({
                transport: {

                    read: function (options) {

                        var data = mylocalStorage.BizSource.GetAllBizSource("btn_create", "");
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "BizSourceID",
                        fields:
                        {
                            isSelected: { type: "boolean", editable: true },
                            BizSourceID: { type: "string", editable: false },
                            BizCategory: { type: "string", editable: false },
                            BizGroup: { type: "string", editable: false },
                            BizSource: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                            InputDate: { type: "date", editable: false },
                            InputUser: { type: "string", editable: false },
                            ModifDate: { type: "date", editable: false },
                            ModifUser: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (list != undefined) {

                BizSourceManager.general.gridviewnew.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                    editable: "inline",
                    filterable: false,
                    selectable: "multiple",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            title: 'Select All',
                            field: "isSelected",
                            headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: "<input type=\"checkbox\" id=\"#: BizSourceID #\" class=\"k-checkbox check_row\" #= isSelected ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"#: BizSourceID #\"></label>",
                            width: 50,
                            filterable: false
                        },
                        {
                            field: "BizSourceID",
                            title: "BizSourceID",
                            width: 80
                        },
                        {
                            field: "BizSource",
                            title: "BizSource",
                            width: 80
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, BizSourceManager.general.gridviewnew);
                });

                $("#header-chb").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = BizSourceManager.general.gridviewnew.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = BizSourceManager.general.gridviewnew.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("isSelected", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        BizSourceManager.general.gridviewnew.data("kendoGrid").clearSelection();
                    }

                });

                var grid = BizSourceManager.general.gridviewnew.data("kendoGrid");

                ////Add Class
                BizSourceManager.general.gridviewnew.find("table").addClass("table table-condensed");
            }


        }
    }
}

function setValueToGridData(val, grid) {
    //Selecting Grid
    var grid = grid.data("kendoGrid");
    //Getting selected Item(s)
    var selectedItems = grid.select();
    selectedItems.each(function (index, row) {
        var selectedItem = grid.dataItem(row);
        //Setting value to data item; so that we can read updated data
        selectedItem.set('isSelected', val);
    });
}

$(document).ready(function () {


});
