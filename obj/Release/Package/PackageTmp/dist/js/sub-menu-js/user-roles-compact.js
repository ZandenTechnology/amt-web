﻿//Declare Control

var urManager = {
    general: {
        gridview: '',
        gridviewnew: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        //init edit grid
        init: function () {
            this.gridview = $(".grid-roles");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");
            
            //localTempStorage.Products.sync();
            this.initGrid();
     
            //Checkbox
            this.hfListRolesID = $("#ListRolesID");

            //Adding click event on btn
            $('#btnSave').live('click', function (e) {

                //jQuery.map:
                //var data = $.grep(urManager.general.gridview.data("kendoGrid").dataSource.data(), function (e) { return e.IsHasRole });

                var listMenu = [];
                $.each(urManager.general.gridview.data("kendoGrid").dataSource.data(), function (index, item) {
                    var obj = {
                        UserID: myMPManager.general.mpUserID.val(),
                        RoleID: item.RoleID,
                        IsHasRole: item.IsHasRole
                    }
                    listMenu.push(obj);
                });

                urManager.general.hfListRolesID.val(JSON.stringify(listMenu));

                return true;
            });
        },
        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        var data = mylocalStorage.UserList.GetAllUserRoleDataByUserID(selectedItem.UserID);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "RoleID",
                        fields:
                        {
                            IsHasRole: { type: "boolean", editable: true },
                            RoleID: { type: "string", editable: false },
                            Role: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (list != undefined) {

                urManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                    editable: "inline",
                    filterable: false,
                    selectable: "multiple",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            title: 'Select All',
                            field: "IsHasRole",
                            headerTemplate: '<input type="checkbox" id="header-chb" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: "<input type=\"checkbox\" id=\"#: RoleID #\" class=\"k-checkbox check_row\" #= IsHasRole ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"#: RoleID #\"></label>",
                            width: 50,
                            filterable: false
                        },
                        {
                            field: "RoleID",
                            title: "Role ID",
                            //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                            width: 55
                        },
                        {
                            field: "Role",
                            title: "Role"
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, urManager.general.gridview);
                });

                $("#header-chb").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = urManager.general.gridview.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = urManager.general.gridview.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("IsHasRole", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        urManager.general.gridview.data("kendoGrid").clearSelection();
                    }

                });


                //Add Class
                urManager.general.gridview.find("table").addClass("table table-condensed");
            }
        },
        refreshGrid: function () {

            urManager.general.gridview.data("kendoGrid").dataSource.read();
            urManager.general.gridview.data('kendoGrid').refresh();
        },

        //init new grid
        initNew: function () {
            this.gridviewnew = $(".grid-roles-new");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            this.initGridNew();

            this.hfListRolesIDnew = $("#ListRolesID-new");

            //Adding click event on btn
            $('#btnSave-new').live('click', function (e) {

                //jQuery.map:
                var data = $.grep(urManager.general.gridviewnew.data("kendoGrid").dataSource.data(), function (e) { return e.IsHasRole });

                var listMenunew = [];
                $.each(data, function (index, item) {
                    var obj = {
                        UserID: 0,
                        RoleID: item.RoleID
                    }
                    listMenunew.push(obj);
                });

                urManager.general.hfListRolesIDnew.val(JSON.stringify(listMenunew));
                return true;
            });
        },
        initGridNew: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var data = mylocalStorage.RolesPermission.GetAllRolesData('', '', 0, 1000);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    data: "Data", 
                    total: "Total",
                    model: {
                        id: "RoleID",
                        fields:
                        {
                            IsHasRole: { type: "boolean", editable: true },
                            RoleID: { type: "string", editable: false },
                            Role: { type: "string", editable: false }
                        }
                    }
                }

            })

            if (list != undefined) {

                urManager.general.gridviewnew.kendoGrid({
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,
                    editable: "inline",
                    selectable: "row",
                    filterable: false,
                    selectable: "multiple",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            title: 'Select All',
                            field: "IsHasRole",
                            headerTemplate: '<input type="checkbox" id="header-chb-new" class="k-checkbox"><label class="k-checkbox-label" for="header-chb"></label>',
                            template: "<input type=\"checkbox\" id=\"#: RoleID #\" class=\"k-checkbox check_row\" #= IsHasRole ? checked='checked' : '' # ><label class=\"k-checkbox-label\" for=\"#: RoleID #\"></label>",
                            width: 50,
                            filterable: false
                        },
                        {
                            field: "RoleID",
                            title: "Role ID",
                            //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                            width: 55
                        },
                        {
                            field: "Role",
                            title: "Role"
                        }
                    ]
                });

                //Adding click event on check box 
                $('.check_row').live('click', function (e) {
                    //Getting checkbox
                    var $cb = $(this);
                    //Getting checkbox value
                    var checked = $cb.is(':checked');
                    //Setting checkbox value to data Item
                    setValueToGridData(checked, urManager.general.gridviewnew);
                });

                $("#header-chb-new").live('click', function (e) {

                    var $cb = $(this);
                    var checked = $cb.is(':checked');
                    var grid = urManager.general.gridviewnew.data('kendoGrid');
                    grid.table.find("tr").find("td:last input").attr("checked", checked);

                    //now making all the rows value to true or false
                    var items = urManager.general.gridviewnew.data("kendoGrid").dataSource.data();
                    for (i = 0; i < items.length; i++) {
                        var item = items[i];
                        item.set("IsHasRole", checked);

                    }

                    //If checkbox is unchecked them clear all rows
                    if (!checked) {
                        urManager.general.gridviewnew.data("kendoGrid").clearSelection();
                    }

                });


                //Add Class
                urManager.general.gridviewnew.find("table").addClass("table table-condensed");
            }
        },
        refreshGridNew: function () {

            urManager.general.gridviewnew.data("kendoGrid").dataSource.read();
            urManager.general.gridviewnew.data('kendoGrid').refresh();
        },
    }
}


function setValueToGridData(val, grid) {
    //Selecting Grid
    var grid = grid.data("kendoGrid");
    //Getting selected Item(s)
    var selectedItems = grid.select();
    selectedItems.each(function (index, row) {
        var selectedItem = grid.dataItem(row);
        //Setting value to data item; so that we can read updated data
        selectedItem.set('IsHasRole', val);
    });
}

$(document).ready(function () {
});
