﻿//Declare Control

var mcManager = {
    menuForm: {
        gridview: '',
        gridviewAll: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        dataMenu: [],

        init: function () {
            this.gridview = $("#menu-form-grid");
            this.gridviewAll = $("#all-form-grid");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.hfListForm = $("#ListForm");
            this.btnLeft = $("#to-left");
            this.btnRight = $("#to-right");

            //localTempStorage.Products.sync();
            this.flagRead = 0;

            this.dataMenu = [];
            this.dataMenuAll = [];

            this.initGrid();
            this.initGridAll();

            mcManager.menuForm.btnLeft.click(function () {
                var entityGrid = mcManager.menuForm.gridviewAll.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {

                    var popItem = mcManager.menuForm.dataMenu.pop();

                    if (popItem != null) {
                        mcManager.menuForm.dataMenuAll.push(popItem);
                    }

                    mcManager.menuForm.dataMenu.push(selectedItem);
                    mcManager.menuForm.dataMenuAll = $.grep(mcManager.menuForm.dataMenuAll, function (e) { return e.FormID != selectedItem.FormID });
                    mcManager.menuForm.refreshGrid();
                }
            });

            mcManager.menuForm.btnRight.click(function () {
                var entityGrid = mcManager.menuForm.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    mcManager.menuForm.dataMenuAll.push(selectedItem);
                    mcManager.menuForm.dataMenu = $.grep(mcManager.menuForm.dataMenu, function (e) { return e.FormID != selectedItem.FormID });
                    mcManager.menuForm.refreshGrid();
                }
            });

            //Adding click event on btn
            $('#btnSave').live('click', function (e) {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());
                //jQuery.map:  
                var listMenu = [];
                $.each(mcManager.menuForm.dataMenu, function (index, item) {
                    var obj = {
                        MenuID: selectedItem.MenuID,
                        FormID: item.FormID
                    }
                    listMenu.push(obj);
                });

                mcManager.menuForm.hfListForm.val(JSON.stringify(listMenu));
                return true;
            });

        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        if (mcManager.menuForm.flagRead == 0) {
                            mcManager.menuForm.dataMenu = mylocalStorage.MenuForm.GetSpecificFormbyMenuID(selectedItem.MenuID, 1);
                        }

                        options.success(sortResults(mcManager.menuForm.dataMenu, "FormID", true));
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "FormID",
                        fields:
                        {
                            isSelected: { type: "boolean", editable: true },
                            hasForm: { type: "boolean", editable: false },
                            FormID: { type: "string", editable: false },
                            WebForm: { type: "string", editable: false },
                            FormType: { type: "string", editable: false },
                            Caption: { type: "string", editable: false },
                            FileName: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                        }
                    }
                }

            })

            if (list != undefined) {

                mcManager.menuForm.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,

                    selectable: "row",
                    editable: "inline",

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             field: "FormID",
                             title: "Form ID",
                             width: 80
                         },
                        {
                            field: "Caption",
                            title: "FormName",
                            width: 80
                        }
                    ]
                });

                var grid = mcManager.menuForm.gridview.data("kendoGrid");

                ////Add Class
                mcManager.menuForm.gridview.find("table").addClass("table table-condensed");
            }


        },
        initGridAll: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());

                        if (mcManager.menuForm.flagRead == 0) {
                            mcManager.menuForm.dataMenuAll = mylocalStorage.MenuForm.GetSpecificFormbyMenuID(selectedItem.MenuID, 0);
                            mcManager.menuForm.flagRead = 1;
                        }

                        options.success(sortResults(mcManager.menuForm.dataMenuAll, "FormID", true));
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "FormID",
                        fields:
                        {
                            isSelected: { type: "boolean", editable: true },
                            hasForm: { type: "boolean", editable: false },
                            FormID: { type: "string", editable: false },
                            WebForm: { type: "string", editable: false },
                            FormType: { type: "string", editable: false },
                            Caption: { type: "string", editable: false },
                            FileName: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                        }
                    }
                }

            })

            if (list != undefined) {

                mcManager.menuForm.gridviewAll.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,


                    selectable: "row",
                    editable: "inline",

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             field: "FormID",
                             title: "Form ID",
                             width: 80
                         },
                        {
                            field: "Caption",
                            title: "FormName",
                            width: 80
                        }
                    ]
                });

                ////Add Class
                mcManager.menuForm.gridviewAll.find("table").addClass("table table-condensed");

            }
        },
        refreshGrid: function () {
            mcManager.menuForm.gridview.data("kendoGrid").dataSource.read();
            mcManager.menuForm.gridview.data('kendoGrid').refresh();
            mcManager.menuForm.gridviewAll.data("kendoGrid").dataSource.read();
            mcManager.menuForm.gridviewAll.data('kendoGrid').refresh();
        }
    },
    newMenuForm: {
        gridview: '',
        gridviewAll: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        DataMenu: '',
        DataMenuAll: '',

        init: function () {
            this.gridview = $("#menu-form-grid-new");
            this.gridviewAll = $("#all-form-grid-new");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.hfListForm = $("#ListForm-new");
            this.mpMenuID = $("#MenuID-new");
            this.btnLeft = $("#to-left-new");
            this.btnRight = $("#to-right-new");

            //localTempStorage.Products.sync();
            this.flagReadNew = 0;

            this.DataMenu = [];
            this.DataMenuAll = [];

            //localTempStorage.Products.sync();
            this.initGrid();
            this.initGridAll();

            mcManager.newMenuForm.btnLeft.click(function () {
                var entityGrid = mcManager.newMenuForm.gridviewAll.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    var popItem = mcManager.newMenuForm.DataMenu.pop();

                    if (popItem != null) {
                        mcManager.newMenuForm.DataMenuAll.push(popItem);
                    }

                    mcManager.newMenuForm.DataMenu.push(selectedItem);
                    mcManager.newMenuForm.DataMenuAll = $.grep(mcManager.newMenuForm.DataMenuAll, function (e) { return e.FormID != selectedItem.FormID });
                    mcManager.newMenuForm.refreshGrid();
                }
            });

            mcManager.newMenuForm.btnRight.click(function () {
                var entityGrid = mcManager.newMenuForm.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    mcManager.newMenuForm.DataMenuAll.push(selectedItem);
                    mcManager.newMenuForm.DataMenu = $.grep(mcManager.newMenuForm.DataMenu, function (e) { return e.FormID != selectedItem.FormID });
                    mcManager.newMenuForm.refreshGrid();
                }
            });

            //Adding click event on btn
            $('#btnSave-new').live('click', function (e) {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());
                //jQuery.map:  
                var listMenu = [];
                $.each(mcManager.newMenuForm.DataMenu, function (index, item) {
                    var obj = {
                        MenuID: mcManager.newMenuForm.mpMenuID.val(),
                        FormID: item.FormID
                    }
                    listMenu.push(obj);
                });

                mcManager.newMenuForm.hfListForm.val(JSON.stringify(listMenu));
                return true;
            });

        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());


                        if (mcManager.newMenuForm.flagReadNew == 0) {
                            mcManager.newMenuForm.DataMenu = mylocalStorage.MenuForm.GetSpecificFormbyMenuID("new", 1);
                        }
                        options.success(sortResults(mcManager.newMenuForm.DataMenu, "FormID", true));

                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "FormID",
                        fields:
                        {
                            isSelected: { type: "boolean", editable: true },
                            hasForm: { type: "boolean", editable: false },
                            FormID: { type: "string", editable: false },
                            WebForm: { type: "string", editable: false },
                            FormType: { type: "string", editable: false },
                            Caption: { type: "string", editable: false },
                            FileName: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                        }
                    }
                }

            })

            if (list != undefined) {

                mcManager.newMenuForm.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,


                    selectable: "row",
                    editable: "inline",

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             field: "FormID",
                             title: "Form ID",
                             width: 80
                         },
                        {
                            field: "Caption",
                            title: "FormName",
                            width: 80
                        }
                    ]
                });

                var grid = mcManager.newMenuForm.gridview.data("kendoGrid");

                ////Add Class
                mcManager.newMenuForm.gridview.find("table").addClass("table table-condensed");

            }


        },
        initGridAll: function () {

            var list = new kendo.data.DataSource
            ({
                //serverPaging: true,
                //serverFiltering: true,
                //serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        if (mcManager.newMenuForm.flagReadNew == 0) {
                            mcManager.newMenuForm.DataMenuAll = mylocalStorage.MenuForm.GetSpecificFormbyMenuID('new', 0);
                            mcManager.newMenuForm.flagReadNew = 1;
                        }
                        options.success(sortResults(mcManager.newMenuForm.DataMenuAll, "FormID", true));
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    //data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "FormID",
                        fields:
                        {
                            isSelected: { type: "boolean", editable: true },
                            hasForm: { type: "boolean", editable: false },
                            FormID: { type: "string", editable: false },
                            WebForm: { type: "string", editable: false },
                            FormType: { type: "string", editable: false },
                            Caption: { type: "string", editable: false },
                            FileName: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                        }
                    }
                }

            })

            if (list != undefined) {

                mcManager.newMenuForm.gridviewAll.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    scrollable: true,


                    selectable: "row",
                    editable: "inline",

                    filterable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             field: "FormID",
                             title: "Form ID",
                             width: 80
                         },
                        {
                            field: "Caption",
                            title: "FormName",
                            width: 80
                        }
                    ]
                });

                var grid = mcManager.newMenuForm.gridviewAll.data("kendoGrid");



                ////Add Class
                mcManager.newMenuForm.gridviewAll.find("table").addClass("table table-condensed");

            }


        },
        refreshGrid: function () {

            mcManager.newMenuForm.gridview.data("kendoGrid").dataSource.read();
            mcManager.newMenuForm.gridview.data('kendoGrid').refresh();
            mcManager.newMenuForm.gridviewAll.data("kendoGrid").dataSource.read();
            mcManager.newMenuForm.gridviewAll.data('kendoGrid').refresh();
        }
    }
}


function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

//var checkedIds = {};

////on click of the checkbox:
//function selectRow() {
//    var checked = this.checked,
//        row = $(this).closest("tr"),
//        grid = mcManager.general.gridview.data("kendoGrid"),
//        dataItem = grid.dataItem(row);

//    checkedIds[dataItem.id] = checked;
//    if (checked) {
//        //-select the row
//        row.addClass("k-state-selected");
//    } else {
//        //-remove selection
//        row.removeClass("k-state-selected");
//    }
//}

////on dataBound event restore previous selected rows:
//function onDataBound(e) {
//    var view = this.dataSource.view();
//    for (var i = 0; i < view.length; i++) {
//        if (checkedIds[view[i].id]) {
//            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
//              .addClass("k-state-selected")
//              .find(".checkbox")
//              .attr("checked", "checked");
//        }
//    }

//}

$(document).ready(function () {
});
