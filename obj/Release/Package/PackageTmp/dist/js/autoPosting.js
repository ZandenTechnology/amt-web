﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.cbAutoPosting = $("#cbAutoPosting");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");
            this.hffreeze_post = $("#hffreeze_post");

            $('#cbAutoPosting').click(function () {
                var flagWrong = false;

                swal({
                    title: "Are you sure you want to change the Freeze Period?",
                    text: "If you are sure, type in your password:",
                    type: "input",
                    inputType: "password",
                    showCancelButton: true,
                    closeOnConfirm: false,
                }, function (typedPassword) {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "/AutoPosting/CheckPassword/",
                        data: "{'password':'" + typedPassword + "', 'freeze_post':'" + myMPManager.general.hffreeze_post.val() + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            if (data["success"] != undefined) {
                                if (myMPManager.general.hffreeze_post.val() == 1) {
                                    myMPManager.general.cbAutoPosting.attr('checked', 'checked')
                                    myMPManager.general.hffreeze_post.val('0');
                                    swal("Freeze Period!", "Status Changed!", "info");
                                }
                                else {
                                    myMPManager.general.cbAutoPosting.removeAttr("checked");
                                    myMPManager.general.hffreeze_post.val('1');
                                    swal("Unfreeze Period!", "Status Changed!", "info");
                                }
                            }
                            else {
                                flagWrong = true;
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {
                                popIt("alert", XMLHttpRequest.responseText);
                            }
                            else {
                                popIt("notification", XMLHttpRequest.responseText);
                            }
                        }
                    })
                    if (flagWrong == true) {
                        swal("Wrong Password!", "Wrong Password Inserted!", "error");
                        flagWrong = false;
                    }
                    //console.log(typedPassword);
                });
                return false;
            });

        },

    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.refreshGrid();
        $("#btnCancel").click();

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

$(document).ready(function () {
    myMPManager.general.init();

    $.ajax({
        async: false,
        type: "POST",
        url: "/AutoPosting/getDataFreeze",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            myMPManager.general.hffreeze_post.val(data)
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    })

});
