﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        btnPushed: '',

        init: function () {
            this.gridview = $("#grid");
            this.txtJobID = $("#txtJobID");
            this.txtFromDate = $("#txtFromDate");
            this.txtToDate = $("#txtToDate");
            this.txtJobNo = $("#txtJobNo");
            this.txtWorkCenter = $("#txtWorkCenter");

            //localTempStorage.Products.sync();
            this.initGrid();

            $('#btnSearch').click(function () {
                myMPManager.general.btnPushed = "btnSearch";
                myMPManager.general.refreshGrid();
            });
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read

                        var tempJob = myMPManager.general.txtJobNo.val();
                        var tempFrom = myMPManager.general.txtFromDate.val();
                        var tempTo = myMPManager.general.txtToDate.val();
                        var tempWC = myMPManager.general.txtWorkCenter.val();
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;

                        var data = mylocalStorage.JobProcess.GetAllJobProcessData(tempJob, tempFrom, tempTo, tempWC, skip, take);
                        myMPManager.general.btnPushed = "";
                        options.success(data == undefined ? [] : data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,

                schema: {
                    data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "Total",
                    //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {

                        id: "_job",
                        fields:
                        {
                            _job: { type: "string", editable: false },
                            _item: { type: "string", editable: false },
                            _oper_num: { type: "number", editable: false },
                            _wc: { type: "string", editable: false },
                            _jobsuffixParent: { type: "string", editable: false },
                            _jobsuffix: { type: "string", editable: false },
                            _qty_Rele_qty: { type: "number", editable: false },
                            _qty_op_qty: { type: "number", editable: false },
                            _qty_complete: { type: "number", editable: false },
                            _qty_scrapped: { type: "number", editable: false },
                            jobStartParsed: { type: "date", editable: false },
                            jobEndParsed: { type: "date", editable: false }
                        }
                    }
                }
            })

            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         //{
                         //    field: "tempID",
                         //    title: "ID",
                         //    //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                         //    width: 100
                         //},
                        {
                            field: "_job",
                            title: "Job No",
                        },
                        {
                            field: "_item",
                            title: "Item",
                        },
                        {
                            field: "_oper_num",
                            title: "OP",
                        },
                        {
                            field: "_wc",
                            title: "Work Center",
                        },
                        {
                            field: "_jobsuffixParent",
                            title: "Parent Suff",
                        },
                        {
                            field: "_jobsuffix",
                            title: "Child Suff",
                        },
                        {
                            field: "_qty_Rele_qty",
                            title: "Rel Qty",
                        },
                        {
                            field: "_qty_op_qty",
                            title: "Racd Qty",
                        },
                        {
                            field: "_qty_complete",
                            title: "Comp City",
                        },
                        {
                            field: "_qty_scrapped",
                            title: "Rej Qty",
                        },
                        {
                            field: "jobStartParsed",
                            format: "{0:dd/MM/yyyy}",
                            title: "Start Date",
                            template: "#if($.date(jobStartParsed) == '30/12/1899'){# #='-'# #}else{# #=kendo.toString(jobStartParsed, 'dd/MM/yyyy')# #}#"
                        },
                        {
                            field: "jobEndParsed",
                            format: "{0:dd/MM/yyyy}",
                            title: "End Date",
                            template: "#if($.date(jobEndParsed) == '30/12/1899'){# #='-'# #}else{# #=kendo.toString(jobEndParsed, 'dd/MM/yyyy')# #}#"
                        }

                    ],
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");
            }

        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        },
    }
}

$.date = function (dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

function Edit(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    getProductDetailView(item.id);

};

function Delete(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajax({
                async: false,
                type: "POST",
                url: "/Handler/Products/DeleteProduct.ashx?id=" + item.id,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (!data) {

                        popIt("alert", data.d);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", data.d);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

                        popIt("alert", XMLHttpRequest.responseText);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", XMLHttpRequest.responseText);
                    }
                }
            })


            swal("Deleted!", "Your data has been deleted.", "success");
        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end
};


function Duplicate(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "",
        text: "Are you sure want to duplicate this products?",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, duplicate it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            getProductDetailViewDuplicate(item.id);
            swal("Duplicated!", "Your data has been duplicated.", "success");

        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end

};

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
        if ($.date(view[i].jobEndParsed) == '30/12/1899') {
            $(this.tbody.find("tr[data-uid='" + view[i].uid + "']")).css('color', 'orange');
        }
        else {
            $(this.tbody.find("tr[data-uid='" + view[i].uid + "']")).css('color', 'green');
        }
    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.refreshGrid();
        $("#btnCancel").click();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

$(document).ready(function () {
    myMPManager.general.init();
    //$('#btnCancel').click(function () {
    //    myMPManager.general.txtCode.removeAttr("disabled");
    //    myMPManager.general.txtWcID.val("");
    //    $("#grid").data("kendoGrid").clearSelection();
    //});
});
