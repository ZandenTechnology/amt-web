﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.ddlState = $('#ddlSearch');
            this.txtSearch = $('#txtSearch');
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.mpParent = $("#txtParentID");
            this.mpID = $("#MenuID");
            this.mpName = $("#Menu");
            this.mpDesc = $("#MenuDesc");
            this.mpMenuURL = $("#MenuURL");
            this.hfActive = $("#IsActive");
            this.hfIsHide = $("#IsHide");
            this.hfIsHideUpdate = $("#IsHideUpdate");

            this.modalform = $('#modal-form');
            this.modalformnew = $('#modal-form-new');

            //localTempStorage.Products.sync();
            this.initGrid();

            //FILTER
            $('#btnSearch').click(function () {
                myMPManager.general.refreshGrid();
                return false;
            });

            $('.btn-new').click(function () {
                myMPManager.general.modalformnew.modal("show");

                
                bindParentMenu();
                mcManager.newMenuForm.init();
            });

            $('.btn-edit').click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem == null) {
                    alert("Please choose a row from the table");
                }
                else {
                    myMPManager.general.modalform.modal("show");

                    myMPManager.general.mpID.val(selectedItem.MenuID);
                    myMPManager.general.mpParent.val(selectedItem.ParentID);
                    myMPManager.general.mpName.val(selectedItem.Menu);
                    myMPManager.general.mpDesc.val(selectedItem.MenuDesc);
                    myMPManager.general.hfActive.val(selectedItem.IsActive);
                    myMPManager.general.mpMenuURL.val(selectedItem.MenuURL);

                    if (selectedItem.IsActive == true) {
                        $("#status-is-active").removeClass("status-inactive");
                        $("#status-is-active").removeClass("fa-times-circle-o");
                        $("#status-is-active").addClass("fa-check-circle-o");
                        $("#status-is-active").addClass("status-active");
                    } else {
                        $("#status-is-active").removeClass("status-active");
                        $("#status-is-active").removeClass("fa-check-circle-o");
                        $("#status-is-active").addClass("status-inactive");
                        $("#status-is-active").addClass("fa-times-circle-o");
                    }

                    if (selectedItem.IsHide == true) {
                        $("#hide-is-active-update").removeClass("status-inactive");
                        $("#hide-is-active-update").removeClass("fa-times-circle-o");
                        $("#hide-is-active-update").addClass("fa-check-circle-o");
                        $("#hide-is-active-update").addClass("status-active");
                    } else {
                        $("#hide-is-active-update").removeClass("status-active");
                        $("#hide-is-active-update").removeClass("fa-check-circle-o");
                        $("#hide-is-active-update").addClass("status-inactive");
                        $("#hide-is-active-update").addClass("fa-times-circle-o");
                    }

                    mcManager.menuForm.init();
                    bindParentMenu();
                }
            });

            //status changer
            $(".btn-change-active").click(function () {
                if (myMPManager.general.hfActive.val() == "true") {
                    myMPManager.general.hfActive.val(false);
                    $("#status-is-active").removeClass("status-active");
                    $("#status-is-active").removeClass("fa-check-circle-o");
                    $("#status-is-active").addClass("status-inactive");
                    $("#status-is-active").addClass("fa-times-circle-o");
                } else {
                    myMPManager.general.hfActive.val(true);
                    $("#status-is-active").removeClass("status-inactive");
                    $("#status-is-active").removeClass("fa-times-circle-o");
                    $("#status-is-active").addClass("fa-check-circle-o");
                    $("#status-is-active").addClass("status-active");
                }
            });

            $(".btn-hide-change-active").click(function () {
                if (myMPManager.general.hfIsHide.val() == "true") {
                    myMPManager.general.hfIsHide.val(false);
                    $("#hide-is-active").removeClass("status-active");
                    $("#hide-is-active").removeClass("fa-check-circle-o");
                    $("#hide-is-active").addClass("status-inactive");
                    $("#hide-is-active").addClass("fa-times-circle-o");
                } else {
                    myMPManager.general.hfIsHide.val(true);
                    $("#hide-is-active").removeClass("status-inactive");
                    $("#hide-is-active").removeClass("fa-times-circle-o");
                    $("#hide-is-active").addClass("fa-check-circle-o");
                    $("#hide-is-active").addClass("status-active");
                }
            });

            $(".btn-hide-change-active-update").click(function () {
                if (myMPManager.general.hfIsHideUpdate.val() == "true") {
                    myMPManager.general.hfIsHideUpdate.val(false);
                    $("#hide-is-active-update").removeClass("status-active");
                    $("#hide-is-active-update").removeClass("fa-check-circle-o");
                    $("#hide-is-active-update").addClass("status-inactive");
                    $("#hide-is-active-update").addClass("fa-times-circle-o");
                } else {
                    myMPManager.general.hfIsHideUpdate.val(true);
                    $("#hide-is-active-update").removeClass("status-inactive");
                    $("#hide-is-active-update").removeClass("fa-times-circle-o");
                    $("#hide-is-active-update").addClass("fa-check-circle-o");
                    $("#hide-is-active-update").addClass("status-active");
                }
            });

            $('.btn-delete').click(function () {
                var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());
                if (selectedItem == null) {
                    alert("Please choose a row from the table");
                }
                else {
                    //SweetAlert begin
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this data!",
                        type: "warning", showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            $.LoadingOverlay("show");

                            setTimeout(function () {
                                $.ajax({
                                    async: false,
                                    url: "/Menu/deleteMenuForm",
                                    data: "{ 'ID': '" + selectedItem.MenuID + "', 'url': '" + window.location.pathname.replace("/", "") + "' }",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataFilter: function (data) { return data; },
                                    success: function (data) {
                                        if (data["success"] != undefined && data["success"] == "success") {
                                            swal("Deleted!", data["success"], "success");
                                        } else if (data["error"] != undefined) {
                                            swal("Delete Failed!", data["error"], "error");
                                        }
                                        myMPManager.general.refreshGrid();
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        alert(errorThrown);
                                    }
                                })

                               
                                $.LoadingOverlay("hide");
                                urManager.general.refreshGrid();

                            }, 1500);


                        } else { swal("Cancelled", "Your data is safe :)", "error"); }
                    });
                }
            });

            //import-excel
            //import
            $('#import-excel').click(function () {
                var displayedData = myMPManager.general.gridview.data().kendoGrid.dataSource.view();

                var tbExcel = "<table id='tbExcel' style='display:none;'>" +
                "<tr><th>Menu ID</th><th>Parent ID</th><th>Menu Level</th><th>Menu Name</th><th>Description</th><th>Is Active</th> " +
                "<th>Input Date</th><th>Input User</th><th>Modif Date</th><th>Modif User</th></tr>";



                jQuery.each(displayedData, function (i, val) {

                    //var CalcSpecDueDateString = val.CalcSpecDueDateString == null ? "-" : val.CalcSpecDueDateString.toLocaleDateString();
                    var InputDateString = val.InputDate == null ? "-" : val.InputDate.toLocaleDateString();
                    var ModifDateString = val.ModifDate == null ? "-" : val.ModifDate.toLocaleDateString();

                    tbExcel = tbExcel + "<tr><td>" + val.MenuID + "</td><td>" + val.ParentID + "</td><td>" + val.MenuLvl + "</td><td>" + val.Menu +
                        "</td><td>" + val.MenuDesc + "</td>" +
                        "<td>" + val.IsActive + "</td><td>" + InputDateString + "</td><td>" + val.InputUser + "</td>" +
                        "<td>" + ModifDateString + "</td><td>" + val.ModifUser + "</td></tr>";
                });

                tbExcel = tbExcel + "</table>";

                $('table#tbExcel').remove();
                $('body').append(tbExcel);

                var date = new Date();

                $('table#tbExcel').table2excel({
                    exclude: ".noExl",
                    name: "Job Export " + date.toLocaleDateString(),
                    filename: "Job Export " + date.toLocaleDateString(),
                    fileext: ".xls",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            });
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read

                        var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                        var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                        var page = options.data.page == undefined ? 1 : options.data.page;
                        var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;
                        var searchKey = myMPManager.general.txtSearch.val();

                        var data = mylocalStorage.MenuForm.GetAllMenuFormData(myMPManager.general.ddlState.val(), searchKey, skip, take);
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "Total",
                    model: {
                        id: "id",
                        fields:
                        {
                            MenuID: { type: "string", editable: false },
                            ParentID: { type: "string", editable: false },
                            MenuLvl: { type: "integer", editable: false },
                            Menu: { type: "string", editable: false },
                            MenuDesc: { type: "string", editable: false },
                            MenuURL: { type: "string", editable: false },
                            IsActive: { type: "boolean", editable: false },
                            InputDate: { type: "date", editable: false },
                            InputUser: { type: "string", editable: false },
                            ModifDate: { type: "date", editable: false },
                            ModifUser: { type: "string", editable: false }
                        }
                    }
                }

            })



            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    //toolbar: [{
                    //    template:
                    //        '<a id="New" class="btn btn-success" href="javascript:getProductDetailView(0);"><i class="fa-plus-circle fa"></i>&nbsp;&nbsp;Add Product</a>' +
                    //        '<a id="btnDelete" class="btn btn-default" style=\"margin-left:10px;\"><i class="fa-remove fa"></i>&nbsp;&nbsp;Delete</a>' +
                    //        '<a id="Import" class="btn btn-default" style=\"margin-left:10px;\"><i class="fa-download fa"></i>&nbsp;&nbsp;Import</a>' +
                    //        '<a id="btnOrderNavi" href="javascript:getSortView(\'Products\',\'javascript:getProductsView();\');" class="btn btn-default pull-right" ><i class="fa-sort fa"></i>&nbsp;&nbsp;Sort</a>'
                    //}],
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             field: "MenuID",
                             title: "Menu ID",
                             //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                             width: 100
                         },
                        {
                            field: "ParentID",
                            title: "Parent ID"
                        },
                        {
                            field: "MenuLvl",
                            title: "Menu Level"
                        },
                        {
                            field: "Menu",
                            title: "Menu Name"
                        },
                        {
                            field: "MenuDesc",
                            title: "Description"
                        },
                        {
                            field: "IsActive",
                            title: "Is Active"
                        },
                        {
                            field: "InputDate",
                            title: "Input Date",
                            format: "{0:dd/MM/yyyy HH:mm}"
                        },
                        {
                            field: "InputUser",
                            title: "Input User"
                        },
                        {
                            field: "ModifDate",
                            title: "Modif Date",
                            format: "{0:dd/MM/yyyy HH:mm}"
                        },
                        {
                            field: "ModifUser",
                            title: "Modif User"
                        }

                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");


                //bind click event to the checkbox
                //grid.table.on("click", ".k-checkbox", selectRow);
                //$('#header-chb').change(function (ev) {

                //    var checked = ev.target.checked;
                //    $('.k-checkbox').each(function (idx, item) {
                //        if (checked) {
                //            if (!($(item).closest('tr').is('.k-state-selected'))) {
                //                $(item).click();
                //            }
                //        } else {
                //            if ($(item).closest('tr').is('.k-state-selected')) {
                //                $(item).click();
                //            }
                //        }
                //    });

                //});

            }


        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        }
    },
    compactMenu: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid-menu");
            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            //localTempStorage.Products.sync();
            this.initGrid();
            //Sort

            //myMPManager.general.popupOrderNavi = $('#form-order').data('kendoWindow');

            //this.ddlState.kendoDropDownList();

            //this.ddlState.change(function () {
            //    myMPManager.general.refreshGrid();
            //});

            ////Import

            //myMPManager.general.popupImportNavi = $('#form-import').data('kendoWindow');

            //$('#Import').click(function () {

            //    myMPManager.general.popupImportNavi.open().center();
            //});

            ////Checkbox
            //$('#checkAll').click(function () {
            //    if ($(this).attr('checked')) {
            //        $('.check-box').attr('checked', 'checked');
            //    } else {
            //        $('.check-box').removeAttr('checked');
            //    }
            //});

            //$('#grid-items input[type=checkbox][id!=checkAll]').click(function () {
            //    var numChkBoxes = $('#grid-items input[type=checkbox][id!=checkAll]').length;
            //    var numChkBoxesChecked = $('#grid-items input[type=checkbox][checked][id!=checkAll]').length;
            //    if (numChkBoxes == numChkBoxesChecked && numChkBoxes > 0) {
            //        $('#checkAll').attr('checked', 'checked');
            //    }
            //    else {
            //        $('#checkAll').attr('checked', '');
            //    }
            //});

            //$('#btnDelete').click(function () {

            //    //SweetAlert begin
            //    swal({
            //        title: "Are you sure?",
            //        text: "You will not be able to recover this data!",
            //        type: "warning", showCancelButton: true,
            //        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
            //        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
            //    }, function (isConfirm) {
            //        if (isConfirm) {
            //            $.LoadingOverlay("show");

            //            setTimeout(function () {
            //                for (var i in checkedIds) {
            //                    if (checkedIds[i]) {

            //                        $.ajax({
            //                            async: false,
            //                            type: "POST",
            //                            url: "/Handler/Products/DeleteProduct.ashx?id=" + i,
            //                            dataType: "json",
            //                            contentType: "application/json; charset=utf-8",
            //                            dataFilter: function (data) { return data; },
            //                            success: function (data) {
            //                                if (!data) {

            //                                    popIt("alert", data.d);
            //                                }
            //                                else {

            //                                    popIt("notification", data.d);
            //                                }
            //                            },
            //                            error: function (XMLHttpRequest, textStatus, errorThrown) {
            //                                if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

            //                                    popIt("alert", XMLHttpRequest.responseText);
            //                                }
            //                                else {

            //                                    popIt("notification", XMLHttpRequest.responseText);
            //                                }
            //                            }
            //                        })


            //                        swal("Deleted!", "Your data has been deleted.", "success");
            //                        $.LoadingOverlay("hide");



            //                    }
            //                }

            //                checkedIds = {};
            //                myMPManager.general.refreshGrid();

            //            }, 1500);


            //        } else { swal("Cancelled", "Your data is safe :)", "error"); }
            //    });

            //});
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read

                        var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                        var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                        var page = options.data.page == undefined ? 1 : options.data.page;
                        var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;
                        var searchKey = myMPManager.general.txtSearch.val();

                        var data = mylocalStorage.MenuForm.GetAllMenu();
                        options.success(data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,
                schema: {
                    data: "Data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "id",
                        fields:
                        {
                            MenuID: { type: "string", editable: false },
                            ParentID: { type: "string", editable: false },
                            MenuLvl: { type: "integer", editable: false },
                            Menu: { type: "string", editable: false }
                        }
                    }
                }

            })



            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    //toolbar: [{
                    //    template:
                    //        '<a id="New" class="btn btn-success" href="javascript:getProductDetailView(0);"><i class="fa-plus-circle fa"></i>&nbsp;&nbsp;Add Product</a>' +
                    //        '<a id="btnDelete" class="btn btn-default" style=\"margin-left:10px;\"><i class="fa-remove fa"></i>&nbsp;&nbsp;Delete</a>' +
                    //        '<a id="Import" class="btn btn-default" style=\"margin-left:10px;\"><i class="fa-download fa"></i>&nbsp;&nbsp;Import</a>' +
                    //        '<a id="btnOrderNavi" href="javascript:getSortView(\'Products\',\'javascript:getProductsView();\');" class="btn btn-default pull-right" ><i class="fa-sort fa"></i>&nbsp;&nbsp;Sort</a>'
                    //}],
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: false,
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                             field: "MenuID",
                             title: "Menu ID",
                             //template: "<div class='photo' style='background-image: url(\"/ImageGen.ashx?image=#:ImageString#&width=50\");'></div>",
                             width: 100
                         },
                        {
                            field: "ParentID",
                            title: "Parent ID"
                        },
                        {
                            field: "MenuLvl",
                            title: "Menu Level"
                        },
                        {
                            field: "Menu",
                            title: "Menu Name"
                        }
                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");


                //bind click event to the checkbox
                //grid.table.on("click", ".k-checkbox", selectRow);
                //$('#header-chb').change(function (ev) {

                //    var checked = ev.target.checked;
                //    $('.k-checkbox').each(function (idx, item) {
                //        if (checked) {
                //            if (!($(item).closest('tr').is('.k-state-selected'))) {
                //                $(item).click();
                //            }
                //        } else {
                //            if ($(item).closest('tr').is('.k-state-selected')) {
                //                $(item).click();
                //            }
                //        }
                //    });

                //});

            }


        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        }
    }
}

function Delete(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajax({
                async: false,
                type: "POST",
                url: "/Handler/Products/DeleteProduct.ashx?id=" + item.id,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (!data) {

                        popIt("alert", data.d);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", data.d);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

                        popIt("alert", XMLHttpRequest.responseText);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", XMLHttpRequest.responseText);
                    }
                }
            })


            swal("Deleted!", "Your data has been deleted.", "success");
        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};


function Duplicate(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "",
        text: "Are you sure want to duplicate this products?",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, duplicate it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            getProductDetailViewDuplicate(item.id);
            swal("Duplicated!", "Your data has been duplicated.", "success");

        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end



};

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }

}

function bindParentMenu() {
    //BindBsource
    try {
        $('.txtParentID').kendoDropDownList({
            dataTextField: "Menu",
            dataValueField: "MenuID",
            dataSource: mylocalStorage.MenuForm.GetAllParentMenu(),
            filter: "contains"
        })
    } catch (e) {

    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.modalform.modal("hide");
        myMPManager.general.modalformnew.modal("hide");
        myMPManager.general.refreshGrid();

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {

    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

$(document).ready(function () {
    myMPManager.general.init();

});
