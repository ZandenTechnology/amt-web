﻿//Declare Control
mylocalStorage["UserAMT"] = {
    GetAllUserAMTData: function (filter, searchKey, skip, take) {
        var result;

        $.ajax({
            async: false,
            url: "/UserAMT/GetAllUserAMTData",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    }
}

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.txtIdno = $("#txtIdno");
            this.txtUserID = $("#txtUserID");
            this.txtName = $("#txtName");
            this.txtUserName = $("#txtUserName");
            this.txtPassword = $("#txtPassword");
            this.txtPrivilege = $("#txtPrivilege");
            this.initGrid();
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
                transport: {

                    read: function (options) {
                        // Implement read
                        //var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                        //var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                        //var page = options.data.page == undefined ? 1 : options.data.page;
                        //var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                        //var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        //var take = options.data.take == undefined ? 1 : options.data.take;
                        //var searchKey = myMPManager.general.txtSearch.val();
                        var data = mylocalStorage.UserAMT.GetAllUserAMTData();
                        options.success(data);
                    },
                    create: function (options) {},
                    update: function (options) {}
                },
                page: 1,
                pageSize: 10,
                batch: true,

                schema: {
                    //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "_idno",
                        fields:
                        {
                            _userID: { type: "string", editable: true },
                            _name: { type: "string", editable: true },
                            _userName: { type: "string", editable: true },
                            _password: { type: "string", editable: true },
                            _privilege: { type: "string", editable: true }
                        }
                    }
                },

                

            })

            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    change: changeData,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                         {
                            field: "_userID",
                            title: "User ID"
                        },
                        {
                            field: "_name",
                            title: "Name",
                        },
                        {
                            field: "_userName",
                            title: "User Name",
                        },
                        {
                            field: "_password",
                            title: "Password",
                        },
                        {
                            field: "_privilege",
                            title: "Privilege",
                        }
                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");

            }

        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        },
    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.refreshGrid();
        $("#btnCancel").click();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}

function changeData(arg) {
    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());
    try {
        myMPManager.general.txtIdno.val(selectedItem._idno);
        myMPManager.general.txtUserID.val(selectedItem._userID);
        myMPManager.general.txtUserID.attr("disabled", true);
        myMPManager.general.txtName.val(selectedItem._name);
        myMPManager.general.txtUserName.val(selectedItem._userName);
        myMPManager.general.txtPassword.val(selectedItem._password);
        myMPManager.general.txtPrivilege.val(selectedItem._privilege);
    } catch (e) { }
}

$(document).ready(function () {
    myMPManager.general.init();
    $('#btnCancel').click(function () {
        myMPManager.general.txtUserID.removeAttr("disabled");
        myMPManager.general.txtIdno.val("");
        $("#grid").data("kendoGrid").clearSelection();
    });
});