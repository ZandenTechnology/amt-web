﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridResource = $("#grid-resource");
            this.gridAllocated = $("#grid-allocated-work");
            this.gridAvailable = $("#grid-available-work");

            this.notification = $("#popupNotification").kendoNotification().data("kendoNotification");

            this.txtResourceID = $("#txtResourceID");
            this.txtResourceDesc = $("#txtResourceDesc");
            this.hfRID = $("#hfRID");
            this.hfGroupID = $("#hfGroupID");
            this.hfGroupName = $("#hfGroupName");
            this.hfListWorkCenter = $("#listWorkCenter");

            this.listAvailable = [];
            this.listAllocated = [];
            this.flagRead = 0;

            //localTempStorage.Products.sync();
            this.initGridResource();
            this.initGridAllocated();
            this.initGridAvailable();

            $("#btnCancel").click(function () {
                myMPManager.general.flagRead = 0;
                myMPManager.general.refreshGrid();
            });
            
            $("#btnSave").click(function () {
                var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());
                //jQuery.map:  
                var listWorkCenter = [];
                $.each(myMPManager.general.listAllocated, function (index, item) {
                    listWorkCenter.push(item._wc);
                });

                myMPManager.general.hfListWorkCenter.val(JSON.stringify(listWorkCenter));
                return true;
            });

            $("#button-right").click(function () {
                //MOVE WORK CENTER RIGHT
                var entityGrid = myMPManager.general.gridAvailable.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.listAllocated.push(selectedItem);
                    myMPManager.general.listAvailable = $.grep(myMPManager.general.listAvailable, function (e) { return e._wcid != selectedItem._wcid });
                    myMPManager.general.refreshGrid();
                }

            });

            $("#button-all-right").click(function () {
                //MOVE ALL WORK CENTER RIGHT
                jQuery.each(myMPManager.general.listAvailable, function (i, val) {
                    myMPManager.general.listAllocated.push(val);
                    myMPManager.general.listAvailable = $.grep(myMPManager.general.listAvailable, function (e) { return e._wcid != val._wcid });
                });
                myMPManager.general.refreshGrid();

            });
            
            $("#button-left").click(function () {
                //MOVE WORK CENTER LEFT
                var entityGrid = myMPManager.general.gridAllocated.data("kendoGrid");
                var selectedItem = entityGrid.dataItem(entityGrid.select());

                if (selectedItem != null) {
                    myMPManager.general.listAvailable.push(selectedItem);
                    myMPManager.general.listAllocated = $.grep(myMPManager.general.listAllocated, function (e) { return e._wcid != selectedItem._wcid });
                    myMPManager.general.refreshGrid();
                }
            });

        },

        initGridResource: function () {

            var list = new kendo.data.DataSource
           ({
               transport: {

                   read: function (options) {
                       // Implement read
                       if (myMPManager.general.flagRead == 0) {
                           var data = mylocalStorage.ResourceGroup.GetAllResourceGroupData('', '', 0, 10);
                       }
                       options.success(data);
                   },
                   create: function (options) {
                   },
                   update: function (options) {
                   }
               },
               //page: 1,
               //pageSize: 10,
               batch: true,
               schema: {
                   model: {
                       id: "id",
                       fields:
                       {
                           _rid: { type: "number", editable: false },
                           _rGroupID: { type: "string", editable: false },
                           _rGroupName: { type: "string", editable: false }
                       }
                   }
               }

           })



            if (list != undefined) {

                myMPManager.general.gridResource.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: onChangeResource,

                    columns: [
                        {
                            field: "_rGroupID",
                            title: "Resource Group ID",
                            template: "#:_rGroupID# #:_rGroupName#"
                        }
                    ]
                });

                myMPManager.general.gridResource.find("table").addClass("table table-condensed");
            }

        },
        refreshGrid: function () {
            try {
                myMPManager.general.gridAvailable.data("kendoGrid").dataSource.read();
                myMPManager.general.gridAvailable.data('kendoGrid').refresh();
            } catch (e) { }

            try {
                myMPManager.general.gridAllocated.data("kendoGrid").dataSource.read();
                myMPManager.general.gridAllocated.data('kendoGrid').refresh();
            } catch (e) { }

        },

        initGridAvailable: function () {

            var list = new kendo.data.DataSource
            ({
              transport: {

                  read: function (options) {
                      // Implement read
                      var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
                      var selectedItem = entityGrid.dataItem(entityGrid.select());

                      if (selectedItem == null && myMPManager.general.flagRead == 0) {
                          myMPManager.general.listAvailable = mylocalStorage.WorkCenter.GetAllAvailableWorkCenter(0);
                      }
                      else if (myMPManager.general.flagRead == 0) {
                          myMPManager.general.listAvailable = mylocalStorage.WorkCenter.GetAllAvailableWorkCenter(selectedItem._rid);
                      }
                      options.success(sortResults(myMPManager.general.listAvailable,"_wc",true));
                  },
                  create: function (options) {
                  },
                  update: function (options) {
                  }
              },
              //page: 1,
              //pageSize: 10,
              batch: true,
              schema: {
                  model: {
                      id: "_wc",
                      fields:
                      {
                          _wcid: { type: "number", editable: false},
                          _wc: { type: "string", editable: false },
                          _description: { type: "string", editable: false },
                      }
                  }
              }

          })

            if (list != undefined) {

                myMPManager.general.gridAvailable.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: function(e){
                        myMPManager.general.gridAllocated.data('kendoGrid').refresh();
                    },
                    columns: [
                        {
                            field: "_wc",
                            title: "Available Work Station",
                            template: "#:_wc#  -  #:_description#"
                        }
                    ]
                });

                ////Add Class
                myMPManager.general.gridAvailable.find("table").addClass("table table-condensed");
            }
        }, //END INITGRIDALLOCATION

        initGridAllocated: function () {

            var list = new kendo.data.DataSource
            ({
              transport: {

                  read: function (options) {
                      // Implement read
                      var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
                      var selectedItem = entityGrid.dataItem(entityGrid.select());

                      if (selectedItem == null && myMPManager.general.flagRead == 0) {
                          myMPManager.general.listAllocated = mylocalStorage.WorkCenter.GetAllAllocatedWorkCenter(0);
                      }
                      else if (myMPManager.general.flagRead == 0) {
                          myMPManager.general.listAllocated = mylocalStorage.WorkCenter.GetAllAllocatedWorkCenter(selectedItem._rid);
                          myMPManager.general.flagRead = 1;
                      }
                      options.success(sortResults(myMPManager.general.listAllocated, "_wc", true));

                  },
                  create: function (options) {
                  },
                  update: function (options) {
                  }
              },
              //page: 1,
              //pageSize: 10,
              batch: true,
              schema: {
                  model: {
                      id: "_wc",
                      fields:
                      {
                          _wcid: { type: "number", editable: false },
                          _wc: { type: "string", editable: false },
                          _description: { type: "string", editable: false },
                      }
                  }
              }

          })

            if (list != undefined) {

                myMPManager.general.gridAllocated.kendoGrid({
                    height: 350,
                    reorderable: true,
                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    selectable: "row",
                    editable: "inline",
                    filterable: false,
                    pageable: false,
                    change: function (e) {
                        myMPManager.general.gridAvailable.data('kendoGrid').refresh();
                    },
                    columns: [
                        {
                            field: "_wc",
                            title: "Allocated Work Station",
                            template: "#:_wc#  -  #:_description#"
                        }
                    ]
                });

                ////Add Class
                myMPManager.general.gridAllocated.find("table").addClass("table table-condensed");
            }
        } //END INITGRIDALLOCATION

    }
}

function ViewResetSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        

        
        myMPManager.general.flagRead = 0;
        myMPManager.general.gridResource.data("kendoGrid").dataSource.read();
        myMPManager.general.gridResource.data('kendoGrid').refresh();
        myMPManager.general.refreshGrid();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {
    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}

function onChangeResource(arg) {
    var entityGrid = myMPManager.general.gridResource.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());
    myMPManager.general.flagRead = 0;

    myMPManager.general.txtResourceID.val(selectedItem._rGroupID);
    myMPManager.general.txtResourceDesc.val(selectedItem._rGroupName);
    myMPManager.general.hfGroupID.val(selectedItem._rGroupID);
    myMPManager.general.hfRID.val(selectedItem._rid);
    myMPManager.general.hfGroupName.val(selectedItem._rGroupName);
    myMPManager.general.initGridAvailable();
    myMPManager.general.initGridAllocated();

    $("#button-right").removeAttr("disabled");
    $("#button-all-right").removeAttr("disabled");
    $("#button-left").removeAttr("disabled");
    $("#btnSave").removeAttr("disabled");
}

$(document).ready(function () {
    myMPManager.general.init();

    $("#button-right").attr("disabled", "disabled");
    $("#button-all-right").attr("disabled", "disabled");
    $("#button-left").attr("disabled", "disabled");
    $("#btnSave").attr("disabled", "disabled");

});
