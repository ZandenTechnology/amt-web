﻿//Declare Control

var myMPManager = {
    general: {
        gridview: '',
        gridview1: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',
        btnPushed: '',

        init: function () {
            this.gridview = $("#grid");
            this.txtJobID = $("#txtJobID");
            this.txtFromDate = $("#txtFromDate");
            this.txtToDate = $("#txtToDate");
            this.txtJobNo = $("#txtJobNo");

            this.modalform = $("#modal-form-update");
            this.gridview1 = $("#grid-menu-update");
            this.PartxtJobNo = $("#PartxtJobNo");
            this.PartxtItem = $("#PartxtItem");
            this.PartxtSuffix = $("#PartxtSuffix");
            this.PartxtReleased = $("#PartxtReleased");
            this.PartxtDate = $("#PartxtDate");
            this.PartxtCompleted = $("#PartxtCompleted");
            this.PartxtScrapped = $("#PartxtScrapped");

            this.ddlType = $("#ddlType");
            this.txtFrom = $("#txtFrom");
            this.txtTo = $("#txtTo");
            this.txtJobNumber = $("#txtJobNumber");

            //localTempStorage.Products.sync();
            this.initGrid();

            $('#btnSearch').click(function () {
                myMPManager.general.btnPushed = "btnSearch"
                myMPManager.general.refreshGrid();
            });
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read

                        var tempJob = myMPManager.general.txtJobNo.val();
                        var tempFrom = myMPManager.general.txtFromDate.val();
                        var tempTo = myMPManager.general.txtToDate.val();
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;

                        var data = mylocalStorage.JobOrder.GetAllJobOrderData(tempJob, tempFrom, tempTo, skip, take);
                        myMPManager.general.btnPushed = "";
                        options.success(data == undefined ? [] : data);
                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,

                schema: {
                    data: "Data",
                    total: "Total",
                    //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "_job",
                        fields:
                        {
                            _job: { type: "string", editable: false },
                            _jobsuffix: { type: "string", editable: false },
                            _jobDate: { type: "string", editable: false },
                            _jobtype: { type: "string", editable: false },
                            _item: { type: "string", editable: false },
                            _qtyReleased: { type: "number", editable: false },
                            _qtyCompleted: { type: "number", editable: false },
                            _qtyScrapped: { type: "number", editable: false },
                            _status: { type: "string", editable: false },
                            jobDateParsed: { type: "date", editable: false }
                        }
                    }
                }
            })

            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    change: changeData,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_job",
                            title: "Job",
                        },
                        {
                            field: "_jobsuffix",
                            title: "Suffix",
                        },
                        {
                            field: "jobDateParsed",
                            format: "{0:dd/MM/yyyy}",
                            title: "Date",
                        },
                        {
                            field: "_jobtype",
                            title: "Job Type",
                        },
                        {
                            field: "_item",
                            title: "Item",
                        },
                        {
                            field: "_qtyReleased",
                            title: "Quantity Released",
                        },
                        {
                            field: "_qtyCompleted",
                            title: "Quantity Completed",
                        },
                        {
                            field: "_qtyScrapped",
                            title: "Quantity Scrapped",
                        },
                        {
                            field: "_status",
                            title: "Status",
                        }
                    ],
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");
                myMPManager.general.gridview.find("table").addClass("table table-condensed");
            }
        },

        initGrid1: function () {

            var list1 = new kendo.data.DataSource
            ({
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                transport: {

                    read: function (options) {
                        // Implement read
                        var entityGrid = myMPManager.general.gridview.data("kendoGrid");
                        var selectedItem = entityGrid.dataItem(entityGrid.select());
                        var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        var take = options.data.take == undefined ? 1 : options.data.take;

                        var data1 = mylocalStorage.JobOrder.GetAllJobDetailsData(selectedItem._job, skip, take);
                        options.success(data1 == undefined ? [] : data1);

                    },
                    create: function (options) {
                    },
                    update: function (options) {
                    }
                },
                page: 1,
                pageSize: 10,
                batch: true,

                schema: {
                    data: "Data",
                    total: "Total",
                    //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "_job",
                        fields:
                        {
                            _job: { type: "string", editable: false },
                            _jobsuffix: { type: "string", editable: false },
                            _jobDate: { type: "string", editable: false },
                            _item: { type: "string", editable: false },
                            _qtyReleased: { type: "number", editable: false },
                            _wc: { type: "string", editable: false },
                            _description: { type: "string", editable: false },
                            _dept: { type: "string", editable: false }
                        }
                    }
                }
            })

            if (list1 != undefined) {

                myMPManager.general.gridview1.kendoGrid({

                    resizable: true,
                    dataSource: list1,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_job",
                            title: "Job",
                        },
                        {
                            field: "_jobsuffix",
                            title: "Suffix",
                        },
                        {
                            field: "_qtyReleased",
                            title: "Operator Released",
                        },
                        {
                            field: "_wc",
                            title: "Work Center",
                        },
                        {
                            field: "_description",
                            title: "Description",
                        },
                        {
                            field: "_dept",
                            title: "Department",
                        }
                    ],
                });

                var grid1 = myMPManager.general.gridview1.data("kendoGrid");
                myMPManager.general.gridview1.find("table").addClass("table table-condensed");
            }
        },

        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        },

        refreshGrid1: function () {

            myMPManager.general.gridview1.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview1.data('kendoGrid').refresh();
        },

    }
}

function getPrint(txtFrom, txtTo, txtjobNo, cmbType) {
    $.ajax({
        async: false,
        data: "{'txtFrom': '" + myMPManager.general.txtFrom.val() + "', 'txtTo': '" + myMPManager.general.txtTo.val() +
                      "', 'txtjobNo':'" + myMPManager.general.txtJobNumber.val() + "', 'cmbType': '" + myMPManager.general.ddlType.val() + "'}",
        url: "/ImportJob/PrintInsufficientMaterial/",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {

            if (data["redirect"] != null) {

                window.open(
                      data["redirect"],
                      '_blank' // <- This is what makes it open in a new window.
                    );

            }
            if (data["excel"] != null) {

                window.open(
                      data["excel"],
                      '_blank' // <- This is what makes it open in a new window.
                    );
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    })
    return true;
}

function Edit(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    getProductDetailView(item.id);

};

function Delete(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajax({
                async: false,
                type: "POST",
                url: "/Handler/Products/DeleteProduct.ashx?id=" + item.id,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (!data) {

                        popIt("alert", data.d);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", data.d);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.responseText.toLowerCase().indexOf("sorry") != -1) {

                        popIt("alert", XMLHttpRequest.responseText);
                    }
                    else {

                        //localTempStorage.Products.sync();
                        myMPManager.general.refreshGrid();
                        popIt("notification", XMLHttpRequest.responseText);
                    }
                }
            })


            swal("Deleted!", "Your data has been deleted.", "success");
        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end
};


function Duplicate(e) {

    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));

    //SweetAlert begin
    swal({
        title: "",
        text: "Are you sure want to duplicate this products?",
        type: "warning", showCancelButton: true,
        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, duplicate it!",
        cancelButtonText: "No, cancel!", closeOnConfirm: false, closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            getProductDetailViewDuplicate(item.id);
            swal("Duplicated!", "Your data has been duplicated.", "success");

        } else { swal("Cancelled", "Your data is safe :)", "error"); }
    });

    //SweetAlert end

};

function onGridDataBoundWithMark(arg) {

    //var item = arg.sender.table.find('.aState');


    //get the index of the WorkingDrawing cell
    var columns = arg.sender.columns;
    var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "published" + "]").index();

    // iterate the table rows and apply custom row and cell styling
    var rows = arg.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var dataItem = arg.sender.dataItem(row);

        var published = dataItem.get("published");


        if (!published) {
            row.addClass("unpublish");
        }

    }
}

function changeData(arg) {
    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());

    myMPManager.general.initGrid1();
    try {
        myMPManager.general.PartxtJobNo.val(selectedItem._job);
        myMPManager.general.PartxtItem.val(selectedItem._item);
        myMPManager.general.PartxtSuffix.val(selectedItem._jobsuffix);
        myMPManager.general.PartxtDate.val(selectedItem._jobDate);
        myMPManager.general.PartxtReleased.val(selectedItem._qtyReleased);
        myMPManager.general.PartxtCompleted.val(selectedItem._qtyCompleted);
        myMPManager.general.PartxtScrapped.val(selectedItem._qtyScrapped);
        myMPManager.general.modalform.modal("show");

    } catch (e) { }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }
}

function ViewSuccess(data) {
    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        NotifSuccess(data["success"]);

        if (data["redirect"] != null) {

            window.open(
                  data["redirect"],
                  '_blank' // <- This is what makes it open in a new window.
                );

        }
        if (data["excel"] != null) {

            window.open(
                  data["excel"],
                  '_blank' // <- This is what makes it open in a new window.
                );

        }
    }
    else {
        NotifError(data["error"]);
    }
}

function ViewError(data) {
    $.LoadingOverlay("hide");
    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}


$(document).ready(function () {
    myMPManager.general.init();

});
