

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {

        NotifSuccess(data["success"]);

        $('#change-pass').modal("hide");

    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {

    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });
}



var passProgress;
$(document).ready(function () {

    passProgress = $("#passStrength").kendoProgressBar({

        type: "value",
        max: 9,
        animation: false,
        change: onChange

    }).data("kendoProgressBar");

    passProgress.progressStatus.text("Empty");

});

$("#userPass").keyup(function () {

    passProgress.value(this.value.length);

});

$("#submitButton").click(function () {

    var strength = passProgress.progressStatus.first().text();
    alert(strength + " password!");

});

function onChange(e) {

    this.progressWrapper.css({

        "background-image": "none",
        "border-image": "none"

    });

    if (e.value < 1) {

        this.progressStatus.text("Empty");

    } else if (e.value <= 3) {

        this.progressStatus.text("Weak");

        this.progressWrapper.css({

            "background-color": "#EE9F05",
            "border-color": "#EE9F05"

        });

    } else if (e.value <= 6) {

        this.progressStatus.text("Good");

        this.progressWrapper.css({

            "background-color": "#428bca",
            "border-color": "#428bca"

        });

    } else {

        this.progressStatus.text("Strong");

        this.progressWrapper.css({

            "background-color": "#8EBC00",
            "border-color": "#8EBC00"

        });

    }

}