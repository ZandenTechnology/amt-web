﻿//Declare Control
mylocalStorage["ActionControl"] = {
    GetAllActionControlData: function (filter, searchKey, skip, take) {
        var result;

        $.ajax({
            async: false,
            url: "/ActionControl/GetAllActionControlData",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                result = jQuery.parseJSON(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                NotifError(errorThrown);
            }
        });

        return result;
    }
}

var myMPManager = {
    general: {
        gridview: '',
        notification: '',
        popupNotification: '',
        ddlSearch: '',

        init: function () {
            this.gridview = $("#grid");
            this.txtIdno = $("#txtIdno");
            this.txtSkip = $("#txtSkip");
            this.txtSplit = $("#txtSplit");
            this.txtTransfer = $("#txtTransfer");
            this.initGrid();
        },

        initGrid: function () {

            var list = new kendo.data.DataSource
            ({
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false,
                transport: {
                    read: function (options) {
                        // Implement read
                        //var filter = options.data.filter == undefined ? "" : JSON.stringify(options.data.filter);
                        //var sort = options.data.sort == undefined ? "" : JSON.stringify(options.data.sort);
                        //var page = options.data.page == undefined ? 1 : options.data.page;
                        //var pageSize = options.data.pageSize == undefined ? 1 : options.data.pageSize;
                        //var skip = options.data.skip == undefined ? 1 : options.data.skip;
                        //var take = options.data.take == undefined ? 1 : options.data.take;
                        //var searchKey = myMPManager.general.txtSearch.val();
                        var data = mylocalStorage.ActionControl.GetAllActionControlData();
                        options.success(data);
                    },
                    create: function (options) { },
                    update: function (options) { }
                },
                page: 1,
                pageSize: 10,
                batch: true,

                schema: {
                    //data: "data", // web methods return JSON in the following format { "d": <result> }. Specify how to get the result.
                    model: {
                        id: "_idno",
                        fields:
                        {
                            _skip: { type: "number", editable: true },
                            _Split: { type: "number", editable: true },
                            _transfer: { type: "number", editable: true }
                        }
                    }
                },



            })

            if (list != undefined) {

                myMPManager.general.gridview.kendoGrid({

                    resizable: true,
                    dataSource: list,
                    sortable: false,
                    editable: "inline",
                    dataBound: onDataBound,
                    change: changeData,
                    filterable: false,
                    selectable: "row",
                    pageable: {
                        pageSizes: [10, 25, 50, 100],
                        refresh: true
                    },
                    columns: [
                        {
                            field: "_skip",
                            title: "Skip"
                        },
                        {
                            field: "_Split",
                            title: "Split",
                        },
                        {
                            field: "_transfer",
                            title: "Transfer",
                        }
                    ]
                });

                var grid = myMPManager.general.gridview.data("kendoGrid");

            }

        },
        refreshGrid: function () {

            myMPManager.general.gridview.data("kendoGrid").dataSource.read();
            myMPManager.general.gridview.data('kendoGrid').refresh();
        },
    }
}

var checkedIds = {};

//on click of the checkbox:
function selectRow() {
    var checked = this.checked,
        row = $(this).closest("tr"),
        grid = myMPManager.general.gridview.data("kendoGrid"),
        dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
    } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}

//on dataBound event restore previous selected rows:
function onDataBound(e) {
    var view = this.dataSource.view();
    for (var i = 0; i < view.length; i++) {
        if (checkedIds[view[i].id]) {
            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
              .addClass("k-state-selected")
              .find(".checkbox")
              .attr("checked", "checked");
        }
    }
}

function ViewSuccess(data) {

    $.LoadingOverlay("hide");

    if (data["success"] != undefined) {
        myMPManager.general.refreshGrid();
        $("#btnCancel").click();
        NotifSuccess(data["success"]);
    }
    else {
        NotifError(data["error"]);

    }
}

function ViewError(data) {

    $.LoadingOverlay("hide");

    NotifError(data["error"]);
}

function ViewBegin() {
    $.LoadingOverlay("show", {
        minSize: "20px",
        maxSize: "30px",
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
    });

}

function changeData(arg) {
    var entityGrid = myMPManager.general.gridview.data("kendoGrid");
    var selectedItem = entityGrid.dataItem(entityGrid.select());
    try {
        myMPManager.general.txtIdno.val(selectedItem._idno);
        myMPManager.general.txtSkip.val(selectedItem._skip);
        myMPManager.general.txtSplit.val(selectedItem._Split);
        myMPManager.general.txtTransfer.val(selectedItem._transfer);

        if (selectedItem._skip == 1) {
            $("#cbSkip").attr('checked', true);
        }
        else {
            $("#cbSkip").attr('checked', false);
        }

        if (selectedItem._Split == 1) {
            $("#cbSplit").attr('checked', true);
        }
        else {
            $("#cbSplit").attr('checked', false);
        }

        if (selectedItem._transfer == 1) {
            $("#cbTransfer").attr('checked', true);
        }
        else {
            $("#cbTransfer").attr('checked', false);
        }
    } catch (e) { }
}

$("#cbSkip").change(function () {
    if ($("#cbSkip").is(":checked")) {
        myMPManager.general.txtSkip.val(1);
    } else {
        myMPManager.general.txtSkip.val(0);
    }
});

$("#cbSplit").change(function () {
    if ($("#cbSplit").is(":checked")) {
        myMPManager.general.txtSplit.val(1);
    } else {
        myMPManager.general.txtSplit.val(0);
    }
});

$("#cbTransfer").change(function () {
    if ($("#cbTransfer").is(":checked")) {
        myMPManager.general.txtTransfer.val(1);
    } else {
        myMPManager.general.txtTransfer.val(0);
    }
});

$(document).ready(function () {
    myMPManager.general.init();
    $("#grid").data("kendoGrid").select('tr:eq(0)');
});