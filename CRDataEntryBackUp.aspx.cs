﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UltimoApps.DAL;

namespace UltimoApps
{
    public partial class CRDataEntryBackUp1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReportDocument crystalReport = new ReportDocument();
            crystalReport.Load(Server.MapPath("~/CRDataEntryBackUp.rpt"));

            var data = CrystalReportClass.GetAllDataEntry();
            crystalReport.SetDataSource(data);

            CrystalReportViewer1.ReportSource = crystalReport;
        }
    }
}