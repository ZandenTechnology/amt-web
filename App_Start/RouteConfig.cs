﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UltimoApps
{
    //public class RouteConfig
    //{
    //    //public static void RegisterRoutes(RouteCollection routes)
    //    //{
    //    //    routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

    //    //    routes.MapRoute(
    //    //        name: "Default",
    //    //        url: "{controller}/{action}/{id}",
    //    //        defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
    //    //    );
    //    //}
    //}

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("Services/{resource}.asmx/{*pathInfo}");
            routes.IgnoreRoute("dist/js/{resource}.js");
            routes.IgnoreRoute("{resource}.aspx/{*pathInfo}");
            routes.IgnoreRoute("{resource}.rpt");
            routes.IgnoreRoute("COResourceGroup/{*pathInfo}");

            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.MapPageRoute("Tickets", "Reports/Tickets", "~/WebForms/Tickets/Report.aspx");

            routes.MapLocalizedRoute("Menu",
                                                 "{Menu}",
                                                 new { controller = "Menu", action = "Index"},
                                                 new[] { "UltimoApps.Controllers" });
            //routes.MapLocalizedRoute("MenuLevel2",
            //                                    "{Parent}/{Menu}/{id}",
            //                                    new { controller = "Menu", action = "Index", id = UrlParameter.Optional  },
            //                                    new[] { "UltimoApps.Controllers" });
            //routes.MapLocalizedRoute("MenuLevel3",
            //                                    "{Parent}/{subParent}/{Menu}/{id}",
            //                                    new { controller = "Menu", action = "Index", id = UrlParameter.Optional },
            //                                    new[] { "UltimoApps.Controllers" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );
        }


    }
    public static class LocalizedRouteExtensionMethod
    {
        public static Route MapLocalizedRoute(this RouteCollection routes, string name, string url, object defaults, string[] namespaces)
        {
            return MapLocalizedRoute(routes, name, url, defaults, null /* constraints */, namespaces);
        }
        public static Route MapLocalizedRoute(this RouteCollection routes, string name, string url, object defaults, object constraints, string[] namespaces)
        {
            if (routes == null)
            {
                throw new ArgumentNullException("routes");
            }
            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            var route = new clsRouteData(url, new MvcRouteHandler())
            {
                Defaults = new RouteValueDictionary(defaults),
                Constraints = new RouteValueDictionary(constraints),
                DataTokens = new RouteValueDictionary()
            };

            if ((namespaces != null) && (namespaces.Length > 0))
            {
                route.DataTokens["Namespaces"] = namespaces;
            }

            routes.Add(name, route);

            return route;
        }
    }
    public class clsRouteData : Route
    {
        public clsRouteData(string url, IRouteHandler routeHandler)
            : base(url, routeHandler)
        {
        }
        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            RouteData data = base.GetRouteData(httpContext);
            if (data != null)
            {
                var SeoFriendliyName = data.Values["Menu"] as string;
                var controller = data.Values["controller"] as string;
                //get here from Database;
                var ObjDataRep = new DAL.Repository();
                var Resutls = ObjDataRep.GetMenuDetailBySeoUrl(SeoFriendliyName);
                if (Resutls != null && Resutls.FormID != string.Empty )
                {
                    data.Values["controller"] = "Dashboard";
                    data.Values["action"] = "Controller";
                    data.Values["Id"] = Resutls.WebForm;
                }
                else
                {
                    // Add Error page here.
                    data.Values["controller"] = "Dashboard";
                    data.Values["action"] = "Controller";
                    data.Values["Id"] = "error-not-found"; 
                }
            }
            return data;
        }

    }
}