﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Services
{
    /// <summary>
    /// Summary description for BankService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BankService : System.Web.Services.WebService
    {
        //[WebMethod]
        //public CompBankBindingModel GetAllCompBankData(string filter, string searchKey, int skip, int take)
        //{
        //    CompBankBindingModel result = new CompBankBindingModel { Data = Ms_CompBank.GetAllCompBankData(filter, searchKey, skip, take), Total = Ms_CompBank.GetAllCompBankDataTotal(filter, searchKey) };


        //    return result;
        //}

        //[WebMethod]
        //public VABindingModel GetAllVAData(string filter, string searchKey, int skip, int take)
        //{
        //    VABindingModel result = new VABindingModel { Data = Ms_MyVA.GetAllVAData(filter, searchKey, skip, take), Total = Ms_MyVA.GetAllVADataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public FormControlModel GetAllFormData(string filter, string searchKey, int skip, int take)
        //{
        //    FormControlModel result = new FormControlModel { Data = Sc_Form.GetAllFormData(filter, searchKey, skip, take), Total = Sc_Form.GetAllFormDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public MenuBindingModel GetAllMenuFormData(string filter, string searchKey, int skip, int take)
        //{
        //    MenuBindingModel result = new MenuBindingModel { Data = Sc_Menu.getAllMenuFormData(filter, searchKey, skip, take), Total = Sc_Menu.GetAllMenuFormDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public RolesBindingModel GetAllRoleData(string filter, string searchKey, int skip, int take)
        //{
        //    RolesBindingModel result = new RolesBindingModel { Data = Sc_Role.GetAllRolesData(filter, searchKey, skip, take), Total = Sc_Role.GetAllRolesDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public List<GetAllRolesDataResult> GetAllRole(string filter, string searchKey, int skip, int take)
        //{
        //    return Sc_Role.GetAllRolesData(filter, searchKey, skip, take) ;
        //}

        //[WebMethod]
        //public UserRegistrationBindingModel GetAllRegistrationData(string company, string status, string filter, string searchKey, int skip, int take)
        //{
        //    UserRegistrationBindingModel result = new UserRegistrationBindingModel { Data = Sc_Register.GetAllRegistrationData(company, status, filter, searchKey, skip, take), Total = Sc_Register.GetAllRegistrationDataTotal(company, status, filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public UserListBindingModel GetUserListData(string company, string filter, string searchKey, int skip, int take)
        //{
        //    UserListBindingModel result = new UserListBindingModel { Data = Sc_User.GetUserListData(company, filter, searchKey, skip, take), Total = Sc_User.GetUserListDataTotal(company, filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public AttachPortalPathBindingModel GetAllPortalAttachPathData(string filter, string searchKey, int skip, int take)
        //{
        //    AttachPortalPathBindingModel result = new AttachPortalPathBindingModel { Data = Ms_AttachPortalPath.GetAllPortalAttachPathData(filter, searchKey, skip, take), Total = Ms_AttachPortalPath.GetAllPortalAttachPathDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public AttachCloudPathBindingModel GetAllCloudAttachPathData(string filter, string searchKey, int skip, int take)
        //{
        //    AttachCloudPathBindingModel result = new AttachCloudPathBindingModel { Data = Ms_AttachCloudPath.GetAllCloudAttachPathData(filter, searchKey, skip, take), Total = Ms_AttachCloudPath.GetAllCloudAttachPathDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public CompBindingModel GetAllCompData(string filter, string searchKey, int skip, int take)
        //{
        //    CompBindingModel result = new CompBindingModel { Data = Ms_Comp.GetAllCompData(filter, searchKey, skip, take), Total = Ms_Comp.GetAllCompDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public ReinsMethodDataBindingModel GetAllReinsMethodData(string filter, string searchKey, int skip, int take)
        //{
        //    ReinsMethodDataBindingModel result = new ReinsMethodDataBindingModel { Data = Ms_RIMethod.GetAllReinsMethodData(filter, searchKey, skip, take), Total = Ms_RIMethod.GetAllReinsMethodDataTotal(filter, searchKey) };
        //    return result;
        //}        

        //[WebMethod]
        //public ReinsLayerDataBindingModel GetAllReinsLayerData(string filter, string searchKey, int skip, int take)
        //{
        //    ReinsLayerDataBindingModel result = new ReinsLayerDataBindingModel { Data = Ms_RILayer.GetAllReinsLayerData(filter, searchKey, skip, take), Total = Ms_RILayer.GetAllReinsLayerTotalData(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public COBBindingModel GetAllClassOfBusinessData(string filter, string searchKey, int skip, int take)
        //{
        //    COBBindingModel result = new COBBindingModel { Data = Ms_COB.GetAllClassOfBusinessData(filter, searchKey, skip, take), Total = Ms_COB.GetAllClassOfBusinessDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public COBSubClassBindingModel GetAllCOBSubData(string filter, string searchKey, int skip, int take)
        //{
        //    COBSubClassBindingModel result = new COBSubClassBindingModel { Data = Ms_COBSub.GetAllCOBSubData(filter, searchKey, skip, take), Total = Ms_COBSub.GetAllCOBSubDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public CurrencyBindingModel GetAllCurrencyData(string filter, string searchKey, int skip, int take)
        //{
        //    CurrencyBindingModel result = new CurrencyBindingModel { Data = Ms_Currency.GetAllCurrencyData(filter, searchKey, skip, take), Total = Ms_Currency.GetAllCurrencyDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public BankBindingModel GetAllBankData(string filter, string searchKey, int skip, int take)
        //{
        //    BankBindingModel result = new BankBindingModel { Data = Ms_Bank.GetAllBankData(filter, searchKey, skip, take), Total = Ms_Bank.GetAllBankDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public ModulBindingModel GetAllModulData(string filter, string searchKey, int skip, int take)
        //{
        //    ModulBindingModel result = new ModulBindingModel { Data = Ms_Modul.GetAllModulData(filter, searchKey, skip, take), Total = Ms_Modul.GetAllModulDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public ModulSubBindingModel GetAllModulSubData(string filter, string searchKey, int skip, int take)
        //{
        //    ModulSubBindingModel result = new ModulSubBindingModel { Data = Ms_ModulSub.GetAllModulSubData(filter, searchKey, skip, take), Total = Ms_ModulSub.GetAllModulSubDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public CategoryBindingModel GetAllCategoryData(string filter, string searchKey, int skip, int take)
        //{
        //    CategoryBindingModel result = new CategoryBindingModel { Data = Ms_Ctgr.GetAllCategoryData(filter, searchKey, skip, take), Total = Ms_Ctgr.GetAllCategoryDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public CategorySubBindingModel GetAllCategorySubData(string filter, string searchKey, int skip, int take)
        //{
        //    CategorySubBindingModel result = new CategorySubBindingModel { Data = Ms_CtgrSub.GetAllCategorySubData(filter, searchKey, skip, take), Total = Ms_CtgrSub.GetAllCategorySubDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public FileVerificationBindingModel GetAllFileVerificationData(string filter, string searchKey, int skip, int take)
        //{
        //    FileVerificationBindingModel result = new FileVerificationBindingModel { Data = Ms_PortalAttach.GetAllFileVerificationData(filter, searchKey, skip, take), Total = Ms_PortalAttach.GetAllFileVerificationDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public FileVerificationCategoryBindingModel GetFileVerificationDataByCategory(string category, string filter, string searchKey, int skip, int take)
        //{
        //    FileVerificationCategoryBindingModel result = new FileVerificationCategoryBindingModel { Data = Ms_PortalAttach.GetFileVerificationDataByCategory(category, filter, searchKey, skip, take), Total = Ms_PortalAttach.GetFileVerificationDataByCategoryTotal(category, filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public TreatyQuoteBindingModel GetAllTreatyQuoteData(string filter, string searchKey, int skip, int take)
        //{
        //    TreatyQuoteBindingModel result = new TreatyQuoteBindingModel { Data = Ms_TreatyProg.GetAllTreatyQuoteData(filter, searchKey, skip, take), Total = Ms_TreatyProg.GetAllTreatyQuoteDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public FacultativeOfferBindingData GetAllFacultativeOfferData(string filter, string searchKey, int skip, int take)
        //{
        //    FacultativeOfferBindingData result = new FacultativeOfferBindingData { Data = Tr_FacOffer.GetAllFacultativeOfferData(filter, searchKey, skip, take), Total = Tr_FacOffer.GetAllFacultativeOfferDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public PreliminaryLossAdviceBindingModel GetAllPreliminaryLossAdviceData(string filter, string searchKey, int skip, int take)
        //{
        //    PreliminaryLossAdviceBindingModel result = new PreliminaryLossAdviceBindingModel { Data = Tr_ClaimMaster.GetAllPreliminaryLossAdviceData(filter, searchKey, skip, take), Total = Tr_ClaimMaster.GetAllPreliminaryLossAdviceDataTotal(filter, searchKey) };
        //    return result;
        //}

        //[WebMethod]
        //public BizSourceBindingModel GetDDLBizSource()
        //{
        //    BizSourceBindingModel result = new BizSourceBindingModel { Data = Ms_BizSource.GetDDLBizSource('0000') };
        //    return result;
        //}

        //[WebMethod]
        //public AreaProcessBindingModel GetDDLAreaProcess()
        //{
        //    AreaProcessBindingModel result = new AreaProcessBindingModel { Data = Ms_AreaProcess.GetDDLAreaProcess() };
        //    return result;
        //}

        //[WebMethod]
        //public AreaProcessBindingModel GetAllMenu()
        //{
        //    AreaProcessBindingModel result = new AreaProcessBindingModel { Data = Ms_AreaProcess.GetDDLAreaProcess() };
        //    return result;
        //}

        //[WebMethod]
        //public MyBankBindingModel GetAllMyBankData(string filter, string searchKey, int skip, int take)
        //{
        //    MyBankBindingModel result = new MyBankBindingModel { Data = Ms_MyBank.GetAllMyBankData(filter, searchKey, skip, take), Total = Ms_MyBank.GetAllMyBankDataTotal(filter, searchKey) };
        //    return result;
        //}

        [WebMethod]     
        public List<GetSpecificFormCtrlResult> GetSpecificFormCtrl(string FormID)
        {
            return Sc_FormControl.GetSpecificFormCtrl(FormID);
        }
    }
}
