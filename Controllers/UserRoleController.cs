﻿using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace UltimoApps.Controllers
{
    public class UserRoleController : Controller
    {
        //
        // GET: /UserRole/

        public ActionResult GetAllUserRoleDataByUserID(long userID)
        {
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(Sc_UserRole.GetAllUserRoleDataByUserID(userID));

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUserListData( string filter, string searchKey, int skip, int take, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_search", url))
            {
                UserListBindingModel model = new UserListBindingModel { Data = Sc_User.GetUserListData(filter, searchKey, skip, take), Total = Sc_User.GetUserListDataTotal( filter, searchKey) };
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(model);

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult newUserList(UserRoleModel model, string url)
        {

            if (Sc_RolePermission.IsHasPermission("btn_create", url))
            {
                model.IsActive = true;
                {
                    if (Sc_UserRole.insertUserList(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = "Update Error : " + Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { error = "Update Error : " + Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult updateUserList(UserRoleModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                if (ModelState.IsValid)
                {
                    if (Sc_UserRole.UpdateFromUserRole(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = "Update Error : " + Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { error = "Update Error : " + Global.FillMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = "Update Error : " + Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteUserRole(long userID, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_delete", url))
            {
                try
                {
                    Sc_UserRole.deleteUserRole(userID);
                    return Json(new { success = "success" }, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(new { error = "error deleting data from database" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = "Update Error : " + Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
