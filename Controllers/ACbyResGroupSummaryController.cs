﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class ACbyResGroupSummaryController : Controller
    {
        //
        // GET: /ACbyResGroupSummary/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetACResGroupWCMachine dt;

        // Print Work Center Yield 
        public ActionResult PrintACResGroupWCMachine(string itemCode, string dateFrom, string dateTo, string rid, string wc)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetACResGroupWCMachine ds = new DataSetACResGroupWCMachine();
            dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
            dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(itemCode);

            var stringItem = "";
            foreach (var list in listItem)
            {
                stringItem += "'" + list + "',";
            }

            if (!stringItem.Equals(""))
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }

            var getData = CrystalReportClass.getdataACbyResGroupWCMachine(dateFrom, dateTo, stringItem, rid, wc);

            List<dataACbyResGroupWCMachine> detailData = new List<dataACbyResGroupWCMachine>();

            foreach (var detail in getData)
            {
                dataACbyResGroupWCMachine temp = new dataACbyResGroupWCMachine();
                temp._rGroupID = detail._rGroupID;
                temp._description = detail._description;
                temp._wc = detail._wc;
                temp._machineid = detail._machineid;
                temp._rGroupName = detail._rGroupName;
                temp.sumHour = Convert.ToDouble(detail.sumHour);
                temp.sumMinute = Convert.ToDouble(detail.sumMinute);
                temp.dateFrom = Convert.ToDateTime(detail.dateFrom);
                temp.dateTo = Convert.ToDateTime(detail.dateTo);

                detailData.Add(temp);
            }

            DataSetACResGroupWCMachine SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetACResGroupWCMachine();

            dt = (DataSetACResGroupWCMachine)Session["sourcetableGroupMAchine"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRACbyRGDetails.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-ACResGroupSummary" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-ACResGroupSummary" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetACResGroupWCMachine SessionDataTable
        {
            get
            {
                if (Session["sourcetableGroupMAchine"] == null)
                {
                    Session["sourcetableGroupMAchine"] = new DataSetACResGroupWCMachine();
                }

                return (DataSetACResGroupWCMachine)Session["sourcetableGroupMAchine"];
            }
            set { Session["sourcetableGroupMAchine"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataACbyResGroupWCMachine
        {
            public string _rGroupID { get; set; }

            public string _rGroupName { get; set; }

            public string _wc { get; set; }

            public string _description { get; set; }

            public string _machineid { get; set; }

            public double sumHour { get; set; }

            public double sumMinute { get; set; }

            public DateTime dateFrom { get; set; }

            public DateTime dateTo { get; set; }
        }

    }
}
