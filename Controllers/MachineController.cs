﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class MachineController : Controller
    {
        //
        // GET: /Machine/

        [HttpPost]
        public ActionResult processMachine(tbMachine model, string command)
        {
            var jsonResult = Json(new { error = Global.ErrorMessage },
                                  JsonRequestBehavior.AllowGet);
            var isSuccess = false;
            var success = Global.SuccessMessage;

            try
            {
                if (command.Equals("saveMachine"))
                {
                    isSuccess = tbMachine.saveMachine(model);
                    success = Global.InsertMessage;
                }
                else if (command.Equals("updateMachine"))
                {
                    isSuccess = tbMachine.updateMachine(model);
                    success = Global.UpdateMessage;
                }
                else if (command.Equals("deleteMachine"))
                {
                    isSuccess = tbMachine.deleteMachine(model);
                    success = Global.DeleteMessage;
                }
            }
            catch (Exception) { }

            if (isSuccess)
            {
                jsonResult = Json(new { success = success },
                                  JsonRequestBehavior.AllowGet);
            }

            return jsonResult;
        }

        public ActionResult GetAllMachineData()
        {
            var db = Global.ewipDB;
            var MachineData = from machine in db.tbMachines select machine;
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(MachineData);
            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMachineByWC(string wc)
        {
            var db = Global.ewipDB;
            var MachineData = (from mg in db.tbMachineGroups
                              join m in db.tbMachines on mg._Mid equals m._mid
                              where mg._wc == wc
                              select new
                              {
                                  _mid = m._mid,
                                  _machineID = m._machineID,
                                  _MachineDesc = m._MachineDesc
                              }).ToList();
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(MachineData);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllAvailableMachine(string wc)
        {
            try
            {
                var db = Global.ewipDB;
                var listActive = (from x in db.tbMachineGroups
                                  where x._wc == wc
                                  select x).ToList();

                var AvailableMachine = (from y in db.tbMachines
                                   where !(from z in listActive select z._Mid).Contains(y._mid)
                                   select y).ToList();

                var serializer = new JavaScriptSerializer();
                var serializeResult = serializer.Serialize(AvailableMachine);

                return Json(serializeResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllAllocatedMachine(string wc)
        {
            var db = Global.ewipDB;
            var getMachine = (from x in db.tbMachineGroups
                              join y in db.tbMachines on x._Mid equals y._mid
                              where x._wc == wc
                              select y).ToList();

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(getMachine);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult updateMachineGrouping(Grouping_MachineModel model, string url)
        {
            try
            {
                var serializer = new JavaScriptSerializer();
                var serializedList = serializer.Deserialize<List<int>>(model.listMachine);

                var db = Global.ewipDB;
                var getMachineGroup = db.tbMachineGroups.Where(x => x._wc == model._wc).ToList(); ;
                var getMaxMG = db.tbMachineGroups.OrderByDescending(z => z._mgid).FirstOrDefault()._mgid + 1;
                var list = new List<tbMachineGroup>();
                foreach (var work in serializedList)
                {
                    var checkMachine = getMachineGroup.Where(y => y._Mid == work).FirstOrDefault();

                    if (checkMachine == null)
                    {
                        var newMG = new tbMachineGroup();
                        newMG._mgid = getMaxMG;
                        newMG._wc = model._wc;
                        newMG._Mid = work;
                        getMaxMG += 1;

                        list.Add(newMG);
                        db.tbMachineGroups.InsertOnSubmit(newMG);
                    }
                    else
                    {
                        getMachineGroup.Remove(checkMachine);
                    }
                }

                foreach (var deselected in getMachineGroup)
                {
                    db.tbMachineGroups.DeleteOnSubmit(deselected);
                }

                db.SubmitChanges();
                return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
