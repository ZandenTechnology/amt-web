﻿using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace UltimoApps.Controllers
{
    public class FormControlController : Controller
    {
        //
        // GET: /FormControl/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllFormData(string filter, string searchKey, int skip, int take, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_search", url))
            {
                FormControlModel result = new FormControlModel { Data = Sc_Form.GetAllFormData(filter, searchKey, skip, take), Total = Sc_Form.GetAllFormDataTotal(filter, searchKey) };
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(result);

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSpecificFormCtrlByRolePermission(string url, string roleID, string formID)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_FormControl.GetSpecificFormCtrlByRolePermission(roleID, formID));

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult updateForm(Compact_FormControlModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                try
                {
                    Sc_User activeUser = Sc_User.getUserbyUserName(mpMembership.GetCurrentUsername());
                    model.ModifUser = activeUser.FirstName + " " + activeUser.LastName;
                    model.ModifDate = DateTime.Now;

                    if (Sc_Form.validateCompactModel(model))
                    {
                        if (Sc_Form.updateFromCompactModel(model))
                        {
                            return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { error = "Update Error : " + Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { error = "Update Error : " + Global.FillMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = ex.Message + " : " + ex.InnerException }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = "Update Error : " + Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult newForm(Compact_FormControlModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_create", url))
            {
                if (Sc_Form.insertCompactModel(model))
                {
                    return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "Update Error : " + Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult deleteFormData(string FormID, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_delete", url))
            {
                try
                {
                    Sc_Form.DeleteFormData(FormID);
                    return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSpecificFormCtrl(string FormID, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_FormControl.GetSpecificFormCtrl(FormID));

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllControlTypeEnum(string FormID, string btn_type, string url)
        {
            if (Sc_RolePermission.IsHasPermission(btn_type, url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_FormControl.GetAllControlTypeEnum(FormID));

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
