﻿using UltimoApps.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace UltimoApps.Controllers
{
    public class FormController : Controller
    {
        //
        // GET: /Form/

        public ActionResult GetSpecificFormbyMenuID(string MenuID, bool hasForm, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_MenuForm.GetSpecificFormbyMenuID(MenuID, hasForm));

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetAllSpecificFormbyMenuID(string MenuID, string RoleID, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_MenuForm.GetAllSpecificFormbyMenuID(MenuID, RoleID));

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
