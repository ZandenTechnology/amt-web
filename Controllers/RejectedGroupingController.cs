﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class RejectedGroupingController : Controller
    {
        [HttpPost]
        public ActionResult processRejectedGrouping(Grouping_RejectedModel model, string command, string _rgid)
        {
            var jsonResult = Json(new { error = Global.ErrorMessage },
                                  JsonRequestBehavior.AllowGet);
            var isSuccess = false;
            var success = Global.SuccessMessage;

            if (command.Equals("saveRejectedGrouping"))
            {
                isSuccess = tbRejectedGroup.saveRejectedGrouping(model);
                success = Global.InsertMessage;
            }
            else if(command.Equals("deleteRejectedGrouping")) 
            {
                isSuccess = tbRejectedGroup.deleteRejectedGrouping(model);
                success = Global.DeleteMessage;
            }

            if (isSuccess)
            {
                jsonResult = Json(new { success = success },
                                  JsonRequestBehavior.AllowGet);
            }

            return jsonResult;
        }

        private JsonResult jsonResult(IQueryable<dynamic> data)
        {
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(data);
            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getWC()
        {
            var db = Global.ewipDB;
            var wcData = from wc in db.tbWcs
                         orderby wc._wc
                         select new { wc._wc, wc._description };
            return jsonResult(wcData);
        }

        public ActionResult getItem()
        {
            var db = Global.ewipDB;
            var itemData = from item in db.tbItems
                           orderby item._itemCode
                           select new { item._itemCode, item._itemdescription };
            return jsonResult(itemData);
        }

        public ActionResult getReject()
        {
            var db = Global.ewipDB;
            var rejectData = from reject in db.tbRejecteds
                             orderby reject._RejectedCode,
                                     reject._RejectedDesc
                             select reject;
            return jsonResult(rejectData);
        }

        [HttpPost]
        public ActionResult getRejectedGrouping(string _wc, string _itemCode)
        {
            var db = Global.ewipDB;
            List<RejGrouping> rejectedGroupingData = new List<RejGrouping>();

            if (_itemCode == string.Empty)
            {
                var tempGroupingData = (from tbRejectedGroups in db.tbRejectedGroups
                                       join tbRejecteds in db.tbRejecteds
                                       on tbRejectedGroups._RejID equals tbRejecteds._RejID
                                       where tbRejectedGroups._wc == _wc
                                       select new
                                       {
                                           tbRejectedGroups._rgid,
                                           tbRejectedGroups._wc,
                                           tbRejectedGroups._itemCode,
                                           tbRejecteds._RejectedCode,
                                           tbRejecteds._RejectedDesc,
                                           tbRejectedGroups._RejID
                                       }).ToList();

                foreach (var item in tempGroupingData)
                {
                    RejGrouping temp = new RejGrouping()
                    {
                        _RejectedCode = item._RejectedCode,
                        _itemCode = item._itemCode,
                        _RejectedDesc = item._RejectedDesc,
                        _wc = item._wc,
                        _RejID = item._RejID,
                        _rgid = item._rgid
                    };

                    rejectedGroupingData.Add(temp);
                }
            }
            else
            {
                var tempGroupingData = (from tbRejectedGroups in db.tbRejectedGroups
                                       join tbRejecteds in db.tbRejecteds
                                       on tbRejectedGroups._RejID equals tbRejecteds._RejID
                                       where tbRejectedGroups._wc == _wc &&
                                             tbRejectedGroups._itemCode == _itemCode
                                       select new
                                       {
                                           tbRejectedGroups._rgid,
                                           tbRejectedGroups._wc,
                                           tbRejectedGroups._itemCode,
                                           tbRejecteds._RejectedCode,
                                           tbRejecteds._RejectedDesc,
                                           tbRejectedGroups._RejID
                                       }).ToList();

                foreach (var item in tempGroupingData)
                {
                    RejGrouping temp = new RejGrouping()
                    {
                        _RejectedCode = item._RejectedCode,
                        _itemCode = item._itemCode,
                        _RejectedDesc = item._RejectedDesc,
                        _wc = item._wc,
                        _RejID = item._RejID,
                        _rgid = item._rgid
                    };

                    rejectedGroupingData.Add(temp);
                }
            }

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(rejectedGroupingData);
            return Json(serializedResult, JsonRequestBehavior.AllowGet); 
        }

        internal class RejGrouping
        {
            public decimal _rgid { get; set; }

            public string _wc { get; set; }

            public string _itemCode { get; set; }

            public string _RejectedCode { get; set; }

            public string _RejectedDesc { get; set; }

            public int _RejID { get; set; }
        }
    }
}
