﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class ActionControlController : Controller
    {
        //
        // GET: /ActionControl/

        [HttpPost]
        public ActionResult processActionControl(tbOperationStatus model, string command)
        {
            var jsonResult = Json(new { error = Global.ErrorMessage },
                                  JsonRequestBehavior.AllowGet);
            var isSuccess = false;

            try
            {
                if (command.Equals("updateActionControl"))
                {
                    isSuccess = tbOperationStatus.updateActionControl(model);
                }
            }
            catch (Exception) { }

            if (isSuccess)
            {
                jsonResult = Json(new { success = Global.UpdateMessage },
                                  JsonRequestBehavior.AllowGet);
            }

            return jsonResult;
        }

        public ActionResult GetAllActionControlData()
        {
            var db = Global.ewipDB;
            var ActionControlData = from actionControl in db.tbOperationStatus select actionControl;
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(ActionControlData);
            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }
    }
}
