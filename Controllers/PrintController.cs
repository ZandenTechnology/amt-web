﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class PrintController : Controller
    {
        //
        // GET: /Print/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetDataEntry dt;

        public static List<Report_dataEntryPerLineResult> getDataEntryPerLineReportResult(string jobID)
        {
            var db = Global.ewipDB;
            return db.Report_dataEntryPerLine(jobID).ToList();
        }

        // Print Data Entry
        public ActionResult PrintDataEntry(string jobID, int operNum)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
                var connection = connectionString;
                var Report = "";
                DataSetDataEntry ds = new DataSetDataEntry();
                var db = Global.ewipDB;

                var lineJobName = (from x in db.tbJobTrans
                                   where x._oper_num == operNum && x._job == jobID
                                   select x).FirstOrDefault();

                var jobName = lineJobName._job + '-' + lineJobName._jobsuffix + (lineJobName._Reworkst != "" ? '-' + lineJobName._Reworkst : "");

                var getData = PrintController.getDataEntryPerLineReportResult(jobID).ToList();

                List<dataentry_binding> detailData = new List<dataentry_binding>();

                //try
                //{
                    foreach (var detail in getData)
                    {
                        dataentry_binding temp = new dataentry_binding();
                        temp._job = jobName;
                        temp._item = detail._item;
                        temp._qtyReleased = Convert.ToDouble(detail._qtyReleased);

                        if (detail._material != null)
                        {
                            var tempSplit = detail._material.Split('|');
                            string holderMaterial = "";
                            foreach (var itemSplit in tempSplit)
                            {
                                if (itemSplit != "")
                                {
                                    holderMaterial += itemSplit + ", ";
                                }
                            }

                            temp._material = holderMaterial;
                        }
                        else
                        {
                            temp._material = "-";
                        }

                        
                        temp._Drawing_Nbr = detail._Drawing_Nbr;
                        temp._Revision = detail._Revision;
                        temp._itemdescription = detail._itemdescription;
                        temp._description = detail._description;
                        temp._operationNo = detail._operationNo;
                        temp._wc = detail._wc;
                        temp.sumCom = Convert.ToDouble(detail.sumCom);
                        temp.sumRej = Convert.ToDouble(detail.sumRej);

                        if (detail._emp_num != "0" && detail._emp_num != null && detail._emp_num != "")
                        {
                            temp._emp_num = detail._emp_num;
                            var splitEMP = detail._emp_num.Substring(detail._emp_num.Length - 4, 4);
                            temp._emp_num = splitEMP;
                        }
                        else if (detail._emp_num == "0" || detail._emp_num == null || detail._emp_num == "")
                        {
                            temp._emp_num = detail._emp_num;
                        }

                        temp._Remarks = detail._Remarks;
                        temp._start_Date = detail._start_Date;
                        temp._end_Date = detail._end_Date;
                        
                        detailData.Add(temp);
                    }
                //}
                //catch (Exception ex)
                //{

                //}

                DataSetDataEntry SOPDS = ds;

                parameters = new Hashtable();
                rptDoc = new ReportDocument();
                dt = new DataSetDataEntry();

                dt = (DataSetDataEntry)Session["sourcetable"];
                parameters = (Hashtable)Session["parameters"];
                SessionDataTable = SOPDS;
                rptName = (string)Session["rptName"];
                Report = "~/CRDataEntryBackUp.rpt";

                rptDoc.Load(Server.MapPath(Report));
                rptDoc.SetDataSource(detailData);

                SetParameter();
                string fileDir = Server.MapPath("/Uploads/");
                string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-" + jobID + ".pdf";
                string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-" + jobID + ".xls";
                string filePath = fileDir + fileName;
                string filePathExcel = fileDir + fileNameExcel;

                if (!System.IO.Directory.Exists(fileDir))
                {
                    System.IO.Directory.CreateDirectory(fileDir);
                }

                if (detailData.Count() != 0)
                {
                    rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                    rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                    return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.StackTrace.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetDataEntry SessionDataTable
        {
            get
            {
                if (Session["sourcetable"] == null)
                {
                    Session["sourcetable"] = new DataSetDataEntry();
                }

                return (DataSetDataEntry)Session["sourcetable"];
            }
            set { Session["sourcetable"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        //[WebMethod(EnableSession=true)]

    }

    internal class dataentry_binding
    {
        public string _job { get; set; }

        public string _item { get; set; }

        public double _qtyReleased { get; set; }

        public string _material { get; set; }

        public string _Drawing_Nbr { get; set; }

        public string _Revision { get; set; }

        public string _itemdescription { get; set; }

        public decimal _operationNo { get; set; }

        public string _wc { get; set; }

        public string _description { get; set; }

        public double sumRej { get; set; }

        public double sumCom { get; set; }

        public string _Remarks { get; set; }

        public string _emp_num { get; set; }

        public string _start_Date { get; set; }

        public string _end_Date { get; set; }
    }
}
