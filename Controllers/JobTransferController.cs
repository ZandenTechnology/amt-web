﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class JobTransferController : Controller
    {
        //
        // GET: /JobTransfer/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetJobTransfer()
        {
            var db = Global.ewipDB;
            var MachineData = from machine in db.tbMachines select machine;
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(MachineData);
            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

    }
}
