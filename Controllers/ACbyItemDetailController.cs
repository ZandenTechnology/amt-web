﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class ACbyItemDetailController : Controller
    {
        //
        // GET: /ACbyItemDetail/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetACbyItemDetail dt;

        // Print Work Center Yield 
        public ActionResult PrintACbyItemDetail(string itemCode, string dateFrom, string dateTo, string rid, string wc)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetACbyItemDetail ds = new DataSetACbyItemDetail();
            dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
            dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(itemCode);

            var stringItem = "";
            foreach (var list in listItem)
            {
                stringItem += "'" + list + "',";
            }

            if (!stringItem.Equals(""))
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }

            var getData = CrystalReportClass.getdataACbyItemDetail(dateFrom, dateTo, stringItem, rid, wc);

            List<dataACbyItemDetail> detailData = new List<dataACbyItemDetail>();

            foreach (var detail in getData)
            {
                dataACbyItemDetail temp = new dataACbyItemDetail();
                temp._description = detail._description;
                temp._item = detail._item;
                temp._job = detail._job;
                temp._oper_num = detail._oper_num;
                temp._wc = detail._wc;
                temp.sumHour = Convert.ToDouble(detail.sumHour);
                temp.sumMinute = Convert.ToDouble(detail.sumMinute);
                temp.sumTotal = Convert.ToDouble(detail.sumTotal);
                temp.dateFrom = Convert.ToDateTime(detail.dateFrom);
                temp.dateTo = Convert.ToDateTime(detail.dateTo);

                detailData.Add(temp);
            }

            DataSetACbyItemDetail SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetACbyItemDetail();

            dt = (DataSetACbyItemDetail)Session["sourcetableItemDetail"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRACbyItemDetails.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-ACbyItemDetail" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-ACbyItemDetail" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            } 

        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetACbyItemDetail SessionDataTable
        {
            get
            {
                if (Session["sourcetableItemDetail"] == null)
                {
                    Session["sourcetableItemDetail"] = new DataSetACbyItemDetail();
                }

                return (DataSetACbyItemDetail)Session["sourcetableItemDetail"];
            }
            set { Session["sourcetableItemDetail"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataACbyItemDetail
        {
            public string _item { get; set; }

            public string _job { get; set; }

            public double _oper_num { get; set; }

            public string _wc { get; set; }

            public string _description { get; set; }

            public double sumTotal { get; set; }

            public double sumHour { get; set; }

            public double sumMinute { get; set; }

            public DateTime dateFrom { get; set; }

            public DateTime dateTo { get; set; }
        }

    }
}
