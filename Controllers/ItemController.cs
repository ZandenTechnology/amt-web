﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class ItemController : Controller
    {
        //
        // GET: /Item/

        public ActionResult GetAllItemData()
        {
            var db = Global.ewipDB;
            var ItemData = from item in db.tbItems select item;
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(ItemData);
            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

    }
}
