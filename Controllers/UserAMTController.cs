﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class UserAMTController : Controller
    {
        //
        // GET: /User/

        [HttpPost]
        public ActionResult processUserAMT(tbUser model, string command)
        {
            if (command.Equals("saveUserAMT"))
            {
                try
                {
                    if (tbUser.saveUserAMT(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (command.Equals("updateUserAMT"))
            {

                try
                {
                    if (tbUser.updateUserAMT(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (command.Equals("deleteUserAMT"))
            {

                try
                {
                    if (tbUser.deleteUserAMT(model))
                    {
                        return Json(new { success = Global.DeleteMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllUserAMTData()
        {
            var db = Global.ewipDB;
            var UserData = from user in db.tbUsers select user;

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(UserData);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }
    }
}