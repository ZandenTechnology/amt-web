﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class OperatorController : Controller
    {
        //
        // GET: /Operator/
        [HttpPost]
        public ActionResult getAllOperatorData(string searchKey)
        {
            try
            {
                var db = Global.ewipDB;
                var dataOP = (from x in db.tbOperators select x).ToList();
                if (searchKey != "")
                {
                    dataOP = dataOP.Where(y => y._operatorID.Contains(searchKey)).ToList();
                }

                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(dataOP);

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                return Json(new { error = exception.Message}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getAlloperatorName()
        {
            var db = Global.ewipDB;
            var dataOP = (from x in db.tbOperators
                          select new
                              {
                                  _operatorID = x._operatorID,
                                  _operatorName = x._operatorName
                              }).ToList();

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(dataOP);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult processOperator(tbOperator model, string command, string url)
        {
            var db = Global.ewipDB;
            if (command.Equals("saveOperator"))
            {
                try
                {
                    tbOperator newOperator = new tbOperator();
                    tbOperator maxUser = db.tbOperators.OrderByDescending(x => x._oid).FirstOrDefault();
                    newOperator._oid = maxUser._oid + 1;
                    newOperator._operatorID = model._operatorID;
                    newOperator._operatorName = model._operatorName;
                    newOperator._skip = (model._skip == 'Y') ? 'Y' : 'N';
                    newOperator._print = (model._print == 'Y') ? 'Y' : 'N';
                    newOperator._split = (model._split == 'Y') ? 'Y' : 'N';
                    db.tbOperators.InsertOnSubmit(newOperator);

                    db.SubmitChanges();
                    return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (command.Equals("updateOperator"))
            {

                try
                {
                    tbOperator get = db.tbOperators.FirstOrDefault(y => y._oid == model._oid);
                    get._operatorID = model._operatorID;
                    get._operatorName = model._operatorName;

                    get._skip = model._skip;
                    get._print = model._print;
                    get._split = model._split;
                    db.SubmitChanges();
                    return Json(new { success = Global.UpdateMessage }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult deleteOperator(int oid, string url)
        {
            try
            {
                var db = Global.ewipDB;
                tbOperator get = db.tbOperators.FirstOrDefault(y => y._oid == oid);
                db.tbOperators.DeleteOnSubmit(get);
                db.SubmitChanges();

                return Json(new { success = Global.DeleteMessage }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
