﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UltimoApps.Controllers
{
    public class MediaController : Controller
    {
        //
        // GET: /Media/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadMedia(HttpPostedFileBase file)
        {

            if (file.ContentLength >= 100000)
            {
                try
                {
                    // Find filename
                    string _text = file.FileName;
                    string filename;
                    string _fullFilePath;

                    _text = _text.Replace("+", "plus");

                    filename = _text.Substring(_text.LastIndexOf("\\") + 1, _text.Length - _text.LastIndexOf("\\") - 1).ToLower();

                    //remove invalid char / * & # ?
                    filename = filename.Replace("/", "").Replace("*", "").Replace("&", "").Replace("?", "");


                    string path = "/Media/" + filename;

                    _fullFilePath = System.Web.HttpContext.Current.Server.MapPath(path);

                    file.SaveAs(_fullFilePath);

                    return Json(new { success = path }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { error = "Max file size 100kb" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
