﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class ResourceGroupController : Controller
    {
        //
        // GET: /ResourceGroup/

        [HttpPost]
        public ActionResult prosesResourceGroup(tbResourceGroup model, string command, string url)
        {
            var db = Global.ewipDB;
            if (command.Equals("saveRG"))
            {
                try
                {
                    if (tbResourceGroup.insertResourceGroup(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception ex)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (command.Equals("updateRG"))
            {

                try
                {
                    if (tbResourceGroup.updateResourceGroup(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (command.Equals("deleteRG"))
            {

                try
                {
                    if (tbResourceGroup.deleteResourceGroup(model))
                    {
                        return Json(new { success = Global.DeleteMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult deleteRG(int _rid, string url)
        //{
        //    try
        //    {
        //        var db = Global.ewipDB;
        //        tbResourceGroup get = db.tbResourceGroups.FirstOrDefault(y => y._rid == _rid);
        //        db.tbResourceGroups.DeleteOnSubmit(get);
        //        db.SubmitChanges();

        //        return Json(new { success = Global.DeleteMessage }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch
        //    {
        //        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public ActionResult GetAllResourceGroupData()
        {
            var db = Global.ewipDB;
            var RGData = from rg in db.tbResourceGroups
                         select rg;

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(RGData);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            //ewipDBDataContext ewipDB = new ewipDBDataContext();
            //List<tbResourceGroup> ResGroup = ewipDB.tbResourceGroups.ToList();

            ResourceGroupModel model = new ResourceGroupModel();

            return View(model);
        }
    }
}

