﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class JobVerificationController : Controller
    {
        //
        // GET: /JobVerification/
        [HttpPost]
        public ActionResult getJobData(DateTime fromDate, DateTime toDate, string job, string status)
        {
            var db = Global.ewipDB;
            dynamic jobData = new List<dynamic>(),
                    jobTransData = new List<dynamic>(),
                    jobRouteData = new List<dynamic>();
            DataTable tbjobs = new DataTable(),
                      tbJobTrans = new DataTable(),
                      tbJobRoutes = new DataTable(),
                      jobTransDataTable = new DataTable(),
                      jobRouteDataTable = new DataTable();
            DB_Parameter parameter = new DB_Parameter();
            List<DB_Parameter> parameters = new List<DB_Parameter>();

            if (job != string.Empty)
            {
                var jobs = (from data in db.tbJobs
                            where data._job == job
                            orderby data._job, data._jobDate
                            select new
                            {
                                _job = data._job,
                                _jobsuffix = data._jobsuffix,
                                _jobDate = DateTime.FromOADate(data._jobDate).ToString("d-MMM-yyyy"),
                                _jobtype = data._jobtype,
                                _item = data._item,
                                _qtyReleased = data._qtyReleased,
                                _qtyCompleted = data._qtyCompleted,
                                _qtyScrapped = data._qtyScrapped
                            } ).ToList();
                jobData = jobs;

                //var jobTrans = (from data in db.tbJobTrans
                //                where data._job == job
                //                group data by new
                //                {
                //                    data._job,
                //                    data._oper_num
                //                }
                //                into dummy_table
                //                orderby dummy_table.Key._job, dummy_table.Key._oper_num
                //                select new
                //                {
                //                    _job = dummy_table.Key._job,
                //                    _oper_num = dummy_table.Key._oper_num,
                //                    _qty_scrapped = dummy_table.Sum(t => t._qty_scrapped),
                //                    _qty_complete = dummy_table.Sum(t => t._qty_complete)
                //                }).ToList();
                //jobTransData = jobTrans;

                //var jobRoute = (from data in db.tbJobRoutes
                //                where data._job == job
                //                group data by new
                //                {
                //                    data._job
                //                }
                //                into dummy_table
                //                orderby dummy_table.Key._job
                //                select new
                //                {
                //                    _job = dummy_table.Key._job,
                //                    _operationNo = dummy_table.Max(t => t._operationNo)
                //                }).ToList();
                //jobRouteData = jobRoute;

                //parameter.name = "@job";
                //parameter.type_data = SqlDbType.NVarChar;
                //parameter.value = job;
                //parameters.Add(parameter);
                //tbjobs = Global.getDataFrom_eWipDB("select * from tbJob where _job=@job order by _job, _jobDate", parameters);
            }
            else
            {
                var jobs = ( from data in db.tbJobs
                             where data._jobDate >= fromDate.ToOADate() &&
                                    data._jobDate <= toDate.ToOADate()
                             orderby data._job, data._jobDate
                             select new
                            {
                                _job = data._job,
                                _jobsuffix = data._jobsuffix,
                                _jobDate = DateTime.FromOADate(data._jobDate).ToString("d-MMM-yyyy"),
                                _jobtype = data._jobtype,
                                _item = data._item,
                                _qtyReleased = data._qtyReleased,
                                _qtyCompleted = data._qtyCompleted,
                                _qtyScrapped = data._qtyScrapped
                            } ).ToList();
                jobData = jobs;

                //parameter = new DB_Parameter();
                //parameter.name = "@job";
                //parameter.type_data = SqlDbType.NVarChar;
                //parameter.value = jobString;
                //parameters.Add(parameter);
      
                //var jobTrans = (from data in db.tbJobTrans
                //                where jobNameList.Contains(data._job)
                //                group data by new
                //                {
                //                    data._job,
                //                    data._oper_num
                //                }
                //                    into dummy_table
                //                    orderby dummy_table.Key._job, dummy_table.Key._oper_num
                //                    select new
                //                    {
                //                        _job = dummy_table.Key._job,
                //                        _oper_num = dummy_table.Key._oper_num,
                //                        _qty_complete = dummy_table.Sum(t => t._qty_complete)
                //                    }).ToList();
                //jobTransData = jobTrans;

                //var jobRoute = (from data in db.tbJobRoutes
                //                where jobNameList.Contains(data._job)
                //                group data by new
                //                {
                //                    data._job
                //                }
                //                into dummy_table
                //                orderby dummy_table.Key._job
                //                select new
                //                {
                //                    _job = dummy_table.Key._job,
                //                    _operationNo = dummy_table.Max(t => t._operationNo)
                //                }).ToList();
                //jobRouteData = jobRoute;

                //parameter = new DB_Parameter();
                //parameter.name = "@job";
                //parameter.type_data = SqlDbType.NVarChar;
                //parameter.value = jobString;
                //parameters.Add(parameter);
            }

            var jobNameList = new List<string>();

            foreach (var data in jobData)
            {
                jobNameList.Add("'" + data._job + "'");
            }

            var jobString = string.Join(",", jobNameList);
            jobTransDataTable = getDataFrom_eWipDBWithMaxString("select _job, _oper_num, Sum(_qty_scrapped) as _qty_scrapped, SUM(_qty_complete) as _qty_complete from(select * from tbJobTrans where _job in (@job))temp group by _job, _oper_num order by _job, _oper_num", jobString);
            jobRouteDataTable = getDataFrom_eWipDBWithMaxString("select _job, Max(_operationNo) as _operationNo from(	select * from tbJobRoute where _job in (@job))temp group by _job order by _job", jobString);
            List<dynamic> dummy_list = new List<dynamic>();

            foreach (var some_data in jobData)
            {
                var oper_num = 0;

                foreach (DataRow data in jobRouteDataTable.Rows)
                {
                    if (data["_job"].ToString() == some_data._job.ToString())
                    {
                        oper_num = int.Parse(data["_operationNo"].ToString());
                        break;
                    }
                }

                double totalScrapped = 0,
                       totalComplete = 0;
                var isProgress = false;

                foreach (DataRow data in jobTransDataTable.Rows)
                {
                    if (data["_job"].ToString() == some_data._job.ToString())
                    {
                        isProgress = true;
                        totalScrapped += float.Parse(data["_qty_scrapped"].ToString());

                        if (oper_num == int.Parse(data["_oper_num"].ToString()))
                        {
                            totalComplete += float.Parse(data["_qty_complete"].ToString()); 
                        }
                    }
                }

                Dictionary<string, dynamic> dummy_data = new Dictionary<string, dynamic>()
                {
                    {"_job", some_data._job},
                    {"_jobsuffix", some_data._jobsuffix},
                    {"_jobDate", some_data._jobDate},
                    {"_jobtype", some_data._jobtype},
                    {"_item", some_data._item},
                    {"_qtyReleased", some_data._qtyReleased}
                };

                if (isProgress == true)
                {
                    dummy_data.Add("_qtyCompleted", totalComplete);
                    dummy_data.Add("_qtyScrapped", totalScrapped);
                }
                else
                {
                    dummy_data.Add("_qtyCompleted", some_data._qtyCompleted);
                    dummy_data.Add("_qtyScrapped", some_data._qtyScrapped);
                }

                var isComplete = false;

                if (some_data._qtyReleased <= some_data._qtyCompleted + some_data._qtyScrapped)
                {
                    isComplete = true;
                }

                var color = "text-muted"; // Bootstrap color for text.

                if ((some_data._qtyReleased == (totalComplete + totalScrapped)) || (isComplete == true))
                {
                    if (status != "All" && status != "Completed")
                    {
                        continue;
                    }

                    color = "text-success";
                }
                else if (some_data._qtyReleased < (totalComplete + totalScrapped))
                {
                    if (status != "All" && status != "Miscalculation")
                    {
                        continue;
                    }

                    color = "text-danger";
                }
                else if ((some_data._qtyReleased > (totalComplete + totalScrapped)) && (isProgress == true))
                {
                    if (status != "All" && status != "Progress")
                    {
                        continue;
                    }

                    color = "text-warning";
                }
                else if (isProgress == false && status != "All" && status != "New")
                {
                    continue;
                }

                dummy_data.Add("color", color);
                dummy_list.Add(dummy_data);

                //List<JobVerificationModel> ParsedModel = new List<JobVerificationModel>();

                //foreach (var jobVerification in dummy_data)
                //{
                //    JobVerificationModel newModel = new JobVerificationModel();
                //    newModel._job = j
                //}
            }

            

            jobData = dummy_list;
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(jobData);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }


        public static DataTable getDataFrom_eWipDBWithMaxString(string cmdText, string JobNo)
        {
            // With...
            var ewipCnn = Global.eWIPCnn;
            DataSet DSJob = new DataSet();

            var DA = new SqlDataAdapter();
            ewipCnn.Open();

            //cmdText = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM job_mst" +
            //" where stat in(\'R\',\'S\')and job=@job";
            string sql = cmdText.Replace("@job", !String.IsNullOrEmpty(JobNo) ? JobNo : "''");
            SqlCommand com = new SqlCommand(sql, ewipCnn);
            //com.Parameters.Add("@job", SqlDbType.NVarChar, -1);
            //com.Parameters["@job"].Value = JobNo;

            DA.SelectCommand = com;
            DA.Fill(DSJob);

            com.Parameters.Clear();
            ewipCnn.Close();

            return DSJob.Tables[0];

        }

        public ActionResult getOperationData(string job)
        {
            var db = Global.ewipDB;
            var JobOperationDetailsData = (from x in db.tbJobRoutes
                                           join y in db.tbJobs on x._job equals y._job
                                           join z in db.tbWcs on x._wc equals z._wc
                                           where x._job == job
                                           orderby x._operationNo
                                           select new
                                           {
                                               _job = x._job,
                                               _oper_num = x._operationNo,
                                               _wc = z._description,
                                           }).ToList();
            var JobTransactionData = db.tbJobTrans.Where(x => x._job.Contains(job))
                                                  .OrderBy(x => x._oper_num)
                                                  .ToList();

            List<OperationDetailModel> ParsedModel = new List<OperationDetailModel>();
            double received = 0;

            foreach (var JobOperation in JobOperationDetailsData)
            {
                double job_date, start_date, end_date, _qty_Rele_qty;
                string item;
                int stopno = 0;
                var data = JobTransactionData.Where(x => x._oper_num == JobOperation._oper_num).ToList();
                tbJobTran pick_data;

                if (data.Count > 0)
                {
                    pick_data = data[(data.Count == 0 ? 0 : data.Count - 1)];
                    item = pick_data._item;
                    job_date = pick_data._jobDate;
                    start_date = pick_data._start_Date;
                    end_date = pick_data._end_Date;
                    _qty_Rele_qty = pick_data._qty_Rele_qty;
                }
                else if (JobTransactionData.Count > 0 && data.Count == 0)
                {
                    pick_data = JobTransactionData[0];
                    item = pick_data._item;
                    job_date = pick_data._jobDate;
                    start_date = pick_data._start_Date;
                    end_date = pick_data._end_Date;
                    _qty_Rele_qty = pick_data._qty_Rele_qty;
                }
                else
                {
                    item = string.Empty;
                    job_date = 0;
                    start_date = 0;
                    end_date = 0;
                    _qty_Rele_qty = 0;
                }

                double RecQTY = 0,
                       ComQTY = 0,
                       RejQTY = 0;

                foreach (var dummy in data)
                {
                    RecQTY += dummy._qty_op_qty;
                    ComQTY += dummy._qty_complete;
                    RejQTY += dummy._qty_scrapped;
                }

                if (stopno < JobOperation._oper_num)
                {
                    stopno = JobOperation._oper_num;
                    received = RecQTY;
                }

                OperationDetailModel newmodel = new OperationDetailModel();
                newmodel.received = received;
                newmodel._qty_Rele_qty = _qty_Rele_qty;
                newmodel._qtyReceived = RecQTY;
                newmodel._qtyCompleted = ComQTY;
                newmodel._qtyScrapped = RejQTY;
                newmodel._job = JobOperation._job;
                newmodel._oper_num = JobOperation._oper_num;
                newmodel._wc = JobOperation._wc;
                newmodel._item = item;

                if (job_date > 0)
                {
                    newmodel._jobDate = DateTime.FromOADate(job_date).ToString("d-MMM-yyyy");
                }
                else
                {
                    newmodel._jobDate = "-";
                }

                if (start_date > 0 && RecQTY != 0)
                {
                    newmodel._start_Date = DateTime.FromOADate(start_date).ToString("d-MMM-yyyy HH:mm");
                }
                else
                {
                    newmodel._start_Date = "-";
                }

                if (end_date > 0 && RecQTY != 0)
                {
                    newmodel._end_Date = DateTime.FromOADate(end_date).ToString("d-MMM-yyyy HH:mm");
                }
                else
                {
                    newmodel._end_Date = "-";
                }

                var color = "text-muted";

                if (RecQTY > ComQTY + RejQTY)
                {
                    color = "text-warning";
                }
                else if (RecQTY == ComQTY + RejQTY && RecQTY != 0)
                {
                    color = "text-success";
                }
                else if (RecQTY < ComQTY + RejQTY)
                {
                    color = "text-danger";
                }

                newmodel.color = color;
                ParsedModel.Add(newmodel);
            }

            var operationJob = ParsedModel.Where(x => x._job == job).ToList() ;

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(operationJob);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getTransactionData(string jobNo, int operNum)
        {
            var db = Global.ewipDB;
            var JobTransactionDetailsData = db.tbJobTrans.Where(x => x._job == jobNo && x._oper_num == operNum).OrderBy(x => x._start_time).ToList();
            List<TransactionDetailModel> ParsedModel = new List<TransactionDetailModel>();
            double start_date, end_date, create_date;

            foreach (var JobTransaction in JobTransactionDetailsData)
            {
                TransactionDetailModel newmodel = new TransactionDetailModel();
                newmodel._emp_num = JobTransaction._emp_num;
                newmodel._machineid = JobTransaction._machineid;
                newmodel._jobsuffix = JobTransaction._jobsuffix;
                newmodel._no_oper = JobTransaction._no_oper;
                newmodel._qty_op_qty = JobTransaction._qty_op_qty;
                newmodel._qty_complete = JobTransaction._qty_complete;
                newmodel._qty_scrapped = JobTransaction._qty_scrapped;

                start_date = JobTransaction._start_Date;

                if (start_date > 0)
                {
                    newmodel._start_Date = DateTime.FromOADate(start_date).ToString("d-MMM-yyyy HH:mm");
                }
                else 
                {
                    newmodel._start_Date = "-";
                }

                end_date = JobTransaction._end_Date;

                if (end_date > 0)
                {
                    newmodel._end_Date = DateTime.FromOADate(end_date).ToString("d-MMM-yyyy HH:mm");
                }
                else
                {
                    newmodel._end_Date = "-";
                }

                create_date = JobTransaction._start_Date;

                if (create_date > 0 && JobTransaction._trans == "YES")
                {
                    newmodel._createDate = DateTime.FromOADate(create_date).ToString("d-MMM-yyyy HH:mm");
                }
                else
                {
                    newmodel._createDate = "-";
                }

                newmodel._oper_num = JobTransaction._oper_num;
                newmodel._item = JobTransaction._item;                
                newmodel._trans = JobTransaction._trans;
                newmodel._job = JobTransaction._job;
                newmodel._jobDate = JobTransaction._jobDate;

                var color = "text-muted"; // Bootstrap color for text.

                if (newmodel._qty_op_qty == newmodel._qty_complete + newmodel._qty_scrapped)
                {
                    color = "text-success";
                }
                else if (newmodel._qty_op_qty > newmodel._qty_complete + newmodel._qty_scrapped)
                {
                    color = "text-warning";
                }
                else if (newmodel._qty_op_qty < newmodel._qty_complete + newmodel._qty_scrapped)
                {
                    color = "text-danger";
                }

                newmodel.color = color;
                ParsedModel.Add(newmodel);
            }

            var transactionJob = ParsedModel.ToList();
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(transactionJob);
            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

    }
}
