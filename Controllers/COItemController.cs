﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class COItemController : Controller
    {
        //
        // GET: /COItem/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetCOItem dt;

        // Print CO Item
        public ActionResult PrintCObyItem(string itemCode, string dateFrom, string dateTo)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetCOItem ds = new DataSetCOItem();
            dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
            dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");
            var getData = CrystalReportClass.getDataCoItem(dateFrom, dateTo, itemCode);

            List<dataCOItem> detailData = new List<dataCOItem>();

            foreach (var detail in getData)
            {
                dataCOItem temp = new dataCOItem();
                temp.Edate = detail.Edate;
                temp.FGCode = detail.FGCode;
                temp.FGDesc = detail.FGDesc;
                temp.Job = detail.Job;
                temp.OP = detail.OP;
                temp.Qty = detail.Qty;
                temp.Rej = detail.Rej;
                temp.Per = detail.Per;
                temp.ReportDate = detail.ReportDate;
                temp.RSCGroup = detail.RSCGroup;
                temp.Sdate = detail.Sdate;
                temp.WCDesc = detail.WCDesc;
                temp.weekd = Convert.ToInt32(detail.weekd);

                detailData.Add(temp);
            }

            DataSetCOItem SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetCOItem();

            dt = (DataSetCOItem)Session["sourcetableCoItem"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRCObyItem.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-COItem" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-COItem" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetCOItem SessionDataTable
        {
            get
            {
                if (Session["sourcetableCoItem"] == null)
                {
                    Session["sourcetableCoItem"] = new DataSetCOItem();
                }

                return (DataSetCOItem)Session["sourcetableCoItem"];
            }
            set { Session["sourcetableCoItem"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataCOItem
        {
            public string RSCGroup { get; set; }

            public string ReportDate { get; set; }

            public string FGCode { get; set; }

            public string FGDesc { get; set; }

            public string Job { get; set; }

            public string Sdate { get; set; }

            public string Edate { get; set; }

            public double OP { get; set; }

            public string WCDesc { get; set; }

            public double Qty { get; set; }

            public double Rej { get; set; }

            public int weekd { get; set; }

            public double Per { get; set; }
        }

    }
}
