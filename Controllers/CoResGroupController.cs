﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class CoResGroupController : Controller
    {
        //
        // GET: /CoResGroup/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetCObyResourceGroup dt;

        // Print CO Resource Group
        [HttpPost]
        public ActionResult PrintCObyResourceGroup(string dateFrom, string dateTo, decimal resorceGroup, string workCenter, string item)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetCObyResourceGroup ds = new DataSetCObyResourceGroup();
            dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
            dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(item);
            var stringItem = "";

            foreach (var itemCode in listItem)
            {
                stringItem += "'" + itemCode + "',";
            }

            try
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                stringItem = "";
            }

            var getData = CrystalReportClass.getDataCOResourceGroup(dateFrom, dateTo, resorceGroup, workCenter, stringItem);

            List<CObyResource_Binding> detailData = new List<CObyResource_Binding>();

            foreach (var detail in getData)
            {
                CObyResource_Binding temp = new CObyResource_Binding();
                temp.Edate = detail.Edate;
                temp.FGCode = detail.FGCode;
                temp.FGDesc = detail.FGDesc;
                temp.Job = detail.Job;
                temp.OP = Convert.ToDouble(detail.OP);
                temp.Per = Convert.ToDouble(detail.Per);
                temp.Qty = Convert.ToDouble(detail.Qty);
                temp.Rej = Convert.ToDouble(detail.Rej);
                temp.ReportDate = detail.ReportDate;
                temp.RSCGroup = detail.RSCGroup;
                temp.Sdate = detail.Sdate;
                temp.weekd = Convert.ToDouble(detail.weekd);
                temp.WCDesc = detail.WCDesc;

                detailData.Add(temp);
            }

            DataSetCObyResourceGroup SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetCObyResourceGroup();

            dt = (DataSetCObyResourceGroup)Session["sourcetableCORG"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRCObyResourceGroup.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-COResourceGroup" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-COResourceGroup" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);
                
                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetCObyResourceGroup SessionDataTable
        {
            get
            {
                if (Session["sourcetableCORG"] == null)
                {
                    Session["sourcetableCORG"] = new DataSetCObyResourceGroup();
                }

                return (DataSetCObyResourceGroup)Session["sourcetableCORG"];
            }
            set { Session["sourcetableCORG"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class CObyResource_Binding
        {
            public string ReportDate { get; set; }

            public string RSCGroup { get; set; }

            public string FGCode { get; set; }

            public string FGDesc { get; set; }

            public string Job { get; set; }

            public string Sdate { get; set; }

            public string Edate { get; set; }

            public double OP { get; set; }

            public double Qty { get; set; }

            public double Rej { get; set; }

            public double weekd { get; set; }

            public double Per { get; set; }

            public string WCDesc { get; set; }
        }

    }
}
