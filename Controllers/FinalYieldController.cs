﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class FinalYieldController : Controller
    {
        //
        // GET: /FinalYield/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetFinalYield dt;

        // Print Final Yield 
        public ActionResult PrintFinalYield(string itemCode, string dateFrom, string dateTo)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetFinalYield ds = new DataSetFinalYield();
            dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
            dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(itemCode);

            var stringItem = "";
            foreach (var item in listItem)
            {
                stringItem += "'" + item + "',";
            }

            if (!stringItem.Equals(""))
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }

            var getData = CrystalReportClass.getDataFinalYield(dateFrom, dateTo, stringItem);

            List<dataFinalYield> detailData = new List<dataFinalYield>();

            foreach (var detail in getData)
            {
                dataFinalYield temp = new dataFinalYield();
                temp.ComQty = (Double)detail.ComQty;
                temp.FGCode = detail.FGCode;
                temp.FGDesc = detail.FGDesc;
                temp.Job = detail.Job;
                temp.Per = (Double)detail.Per;
                temp.RejQty = Convert.ToDouble(detail.RejQty);
                temp.RelQty = (Double)detail.RelQty;
                temp.ReportDate = detail.ReportDate;
                temp.RSCGroup = detail.RSCGroup;
                temp.WCDesc = detail.WCDesc;

                detailData.Add(temp);
            }
            
            DataSetFinalYield SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetFinalYield();

            dt = (DataSetFinalYield)Session["sourcetableFinal"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRFinalYield.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-FinalYield" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-FinalYield" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetFinalYield SessionDataTable
        {
            get
            {
                if (Session["sourcetableFinal"] == null)
                {
                    Session["sourcetableFinal"] = new DataSetFinalYield();
                }

                return (DataSetFinalYield)Session["sourcetableFinal"];
            }
            set { Session["sourcetableFinal"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataFinalYield
        {
            public string RSCGroup { get; set; }

            public string FGCode { get; set; }

            public string FGDesc { get; set; }

            public string Job { get; set; }

            public string WCDesc { get; set; }

            public double ComQty { get; set; }

            public double RelQty { get; set; }

            public double Per { get; set; }

            public double RejQty { get; set; }

            public string ReportDate { get; set; }
        }

    }
}
