﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class ConfigureController : Controller
    {
        //
        // GET: /RejectedDesc/

        [HttpPost]
        public ActionResult ConfigureSLDatabase(Tmp_SL9TableName model, string url)
        {
            var db = Global.db;
            var configSL = db.Tmp_SL9TableNames.FirstOrDefault(item => item.id == "1");
            try
            {
                configSL.job_mst = model.job_mst;
                configSL.jobmatl_mst = model.jobmatl_mst;
                configSL.jobroute_mst = model.jobroute_mst;
                configSL.jobtrans_mst = model.jobtrans_mst;
                configSL.itemloc_mst = model.itemloc_mst;
                configSL.item_mst = model.item_mst;
                configSL.jrt_sch_mst = model.jrt_sch_mst;
                configSL.wc_mst = model.wc_mst;

                db.SubmitChanges();
                Global.setUpSLTable();
                return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckPassword(string password)
        {
            var db = Global.ewipDB;
            var curUser = mpMembership.GetCurrentUser();
            if (curUser._password == password)
            {
                return Json (new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json (new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
