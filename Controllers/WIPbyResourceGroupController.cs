﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class WIPbyResourceGroupController : Controller
    {
        //
        // GET: /WIPbyResourceGroup/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetWIPbyResourceGroup dt;

        // Print Work Center Yield 
        public ActionResult PrintWIPbyResourceGroup(bool ckbReworkOnly, string itemCode, decimal resorceGroup, string workCenter)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetWIPbyResourceGroup ds = new DataSetWIPbyResourceGroup();

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(itemCode);

            var stringItem = "";
            foreach (var list in listItem)
            {
                stringItem += "'" + list + "',";
            }

            if (!stringItem.Equals(""))
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }

            var getData = CrystalReportClass.getdataWIPbyResourceGroup(ckbReworkOnly, stringItem, resorceGroup, workCenter);

            List<dataWIPniResourceGroup> detailData = new List<dataWIPniResourceGroup>();

            foreach (var detail in getData)
            {
                dataWIPniResourceGroup temp = new dataWIPniResourceGroup();
                temp._status = detail._status;
                temp.ChildR = detail.ChildR;
                temp.FGCode = detail.FGCode;
                temp.FGDesc = detail.FGDesc;
                temp.Job = detail.Job;
                temp.OP = Convert.ToDouble(detail.OP);
                temp.Qty = Convert.ToDouble(detail.Qty);
                temp.ReportDate = detail.ReportDate;
                temp.RSCGroup = detail.RSCGroup;
                temp.Sdate = detail.Sdate;
                temp.StockCard = detail.StockCard;
                temp.Tdate = detail.Tdate;
                temp.WC = detail.WC;
                temp.WCDesc = detail.WCDesc;

                detailData.Add(temp);
            }

            DataSetWIPbyResourceGroup SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetWIPbyResourceGroup();

            dt = (DataSetWIPbyResourceGroup)Session["sourcetableWIPRG"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRWIPbyResourceGroup.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-WIPbyResourceGroup" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-WIPbyResourceGroup" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetWIPbyResourceGroup SessionDataTable
        {
            get
            {
                if (Session["sourcetableWIPRG"] == null)
                {
                    Session["sourcetableWIPRG"] = new DataSetWIPbyResourceGroup();
                }

                return (DataSetWIPbyResourceGroup)Session["sourcetableWIPRG"];
            }
            set { Session["sourcetableWIPRG"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataWIPniResourceGroup
        {
            public string ReportDate { get; set; }

            public string StockCard { get; set; }

            public string RSCGroup { get; set; }

            public string FGCode { get; set; }

            public string FGDesc { get; set; }

            public string Job { get; set; }

            public string ChildR { get; set; }

            public string Sdate { get; set; }

            public string Tdate { get; set; }

            public double OP { get; set; }

            public string WCDesc { get; set; }

            public string WC { get; set; }

            public double Qty { get; set; }

            public string _status { get; set; }
        }

    }
}
