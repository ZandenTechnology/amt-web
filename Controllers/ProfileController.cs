﻿using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models.Custom;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace UltimoApps.Controllers
{
    public class ProfileController : Controller
    {
        [HttpPost]
        public ActionResult UpdateProfile(Sc_User model, string url)
        {
            if (ModelState.IsValid)
            {
                if (Sc_RolePermission.IsHasPermission("btn_edit", url))
                {
                    model.ModifDate = DateTime.Now;
                    Sc_User activeUser = Sc_User.getUserbyUserName(mpMembership.GetCurrentUsername());
                    model.ModifUser = activeUser.FirstName + " " + activeUser.LastName;

                    if (Sc_User.Update(model))
                    {
                        return Json(new { success = Global.UpdateMessage }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {

                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { error = Global.FillMessage }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                //var encryptedPassword = mpMembership.HashPassword(model.OldPassword, ConfigurationManager.AppSettings["passwordSalt"].ToString());
                var encryptedPassword = model.OldPassword;
                var loginUser = mpMembership.GetCurrentUser();

                if (loginUser != null && loginUser._password.Equals(encryptedPassword))
                {
                    if (Sc_User.ChangePassword(model))
                    {

                        return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { error = "Wrong old password." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = Global.FillMessage }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ResetPassword(ChangePasswordModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
                {

                    string password = RandomString(6);

                    model.Password = password;

                    var encryptedPassword = mpMembership.HashPassword(model.Password, ConfigurationManager.AppSettings["passwordSalt"].ToString());
                    var loginUser = Sc_User.getUserbyID(model.UserID);

                    if (Sc_User.ChangePassword(model))
                    {

                        return Json(new { success = "Password has been reset. Password : " + password }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
                }
        }

        public static string RandomString(int length)
        {
            var rnd = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rnd.Next(s.Length)]).ToArray());
        }


        //[HttpPost]
        //public ActionResult UpdateMyCompany(Ms_MyComp model, string url)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (Sc_RolePermission.IsHasPermission("btn_edit", url))
        //        {
        //            model.ModifDate = DateTime.Now;
        //            Sc_User activeUser = Sc_User.getUserbyUserName(mpMembership.GetCurrentUsername());
        //            model.ModifUser = activeUser.FirstName + " " + activeUser.LastName;

        //            if (Ms_MyComp.Update(model))
        //            {
        //                return Json(new { success = Global.UpdateMessage }, JsonRequestBehavior.AllowGet);

        //            }
        //            else
        //            {

        //                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
        //            }

        //        }
        //        else
        //        {
        //            return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
        //        }

        //    }
        //    else
        //    {
        //        return Json(new { error = Global.FillMessage }, JsonRequestBehavior.AllowGet);
        //    }
        //}


    }
}
