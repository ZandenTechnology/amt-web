﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using UltimoApps.DAL;
using UltimoApps.Models;
using System.Drawing;
using System.Configuration;
using System.Globalization;
using System.Web.Script.Serialization;
using UltimoApps.DAL.Membership;

namespace UltimoApps.Controllers
{
    public class AutoPostingController : Controller
    {
        //
        // GET: /AutoPosting/
        private int counterUpdate;

        [HttpPost]
        public ActionResult UserExportToSL9(int tansnum, string job, int oper_num, string wc, string action)
        {
            if (action.ToLower().Equals("update"))
            {
                ProcessUpdateJobNumber(tansnum);
                return Json(new { success = "Posting Success" }, JsonRequestBehavior.AllowGet);
            }
            else if (action.ToLower().Equals("rework"))
            {
                var db = Global.db;
                var reworkFlag = db.Tmp_ReworkPostingTables.FirstOrDefault(x => x.job == job && x.oper_num == oper_num && x.needPosting == 1);
                if (reworkFlag != null)
                {
                    DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter(), mst_prev_op = new DB_Parameter();
                    mst_job.name = "@job";
                    mst_job.type_data = SqlDbType.VarChar;
                    mst_job.value = job;

                    mst_oper_num.name = "@oper_num";
                    mst_oper_num.type_data = SqlDbType.Int;
                    mst_oper_num.value = oper_num;

                    List<DB_Parameter> parameters_sl9DB = new List<DB_Parameter>() { mst_job, mst_oper_num };
                    DataTable maxPrevOP = Global.getDataFrom_eWipDB("select max([_operationNo]) as maxOper from dbo.tbJobRoute where [_operationNo] < @oper_num and [_job] = @job", parameters_sl9DB);
                    var maxPrev = int.Parse(maxPrevOP.Select()[0].ItemArray[0].ToString());
                    float targetReverse;

                    try
                    {
                        DB_Parameter rework_opernum = new DB_Parameter();
                        rework_opernum.name = "@oper_num";
                        rework_opernum.type_data = SqlDbType.Int;
                        rework_opernum.value = maxPrev;

                        List<DB_Parameter> parameter_searchRework = new List<DB_Parameter>() { mst_job, rework_opernum };
                        DataTable targetReverseString = Global.getDataFrom_sl9DB("select max(qty_complete) from " + Global.jobtrans_mst + " where job = @job and oper_num = @oper_num ", parameter_searchRework);
                        targetReverse = float.Parse(targetReverseString.Select()[0].ItemArray[0].ToString());
                    }
                    catch
                    {
                        ClearReworkFlagOnASpecificJobOperation(job, oper_num);
                        return Json(new { error = "Unposted Main Trans Line, No need to post rework to SL9!" }, JsonRequestBehavior.AllowGet);
                    }

                    if (targetReverse > 0)
                    {
                        CreateNegateLineForPreviousPostedJob(job, oper_num, true);
                        ClearReworkFlagOnASpecificJobOperation(job, oper_num);

                        return Json(new { success = "Posting Success" }, JsonRequestBehavior.AllowGet);
                    }

                    ClearReworkFlagOnASpecificJobOperation(job, oper_num);
                    return Json(new { error = "Unposted Main Trans Line, No need to post rework to SL9!" }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = "Error Posting Revert Line" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //try
                //{
                //50SI971664
                if (wc.ToUpper() == "STOREP")
                {
                    return Json(new { error = "WC is storep, skipping posting!" }, JsonRequestBehavior.AllowGet);
                }

                SqlConnection ewipdb = Global.eWIPCnn,
                              sl9db = Global.sl9Cnn;

                DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                             parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                             parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                             parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),
                             parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();

                DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter(), mst_wc = new DB_Parameter();
                mst_job.name = "@job";
                mst_job.type_data = SqlDbType.VarChar;
                mst_job.value = job;

                mst_oper_num.name = "@oper_num";
                mst_oper_num.type_data = SqlDbType.Int;
                mst_oper_num.value = oper_num;

                mst_wc.name = "@wc";
                mst_wc.type_data = SqlDbType.VarChar;
                mst_wc.value = wc;
                List<DB_Parameter> parameters_sl9DB = new List<DB_Parameter>() { mst_job, mst_oper_num, mst_wc };

                DataTable data_table = new DataTable(),
                tbJobNonPost = Global.getDataFrom_sl9DB("select top 1 * from " + Global.jobtrans_mst + " where job = @job and oper_num = @oper_num and wc = @wc order by trans_num desc", parameters_sl9DB);
                DataRow[] nonPostRow = tbJobNonPost.Select();

                if (nonPostRow.Length != 0)
                {

                    DataTable tbJobRoute = Global.getDataFrom_eWipDB(
                        " select tm._trans_type, tm._jobDate, tm._job, r._jobSuffix, r._operationNo, tm._item, tm._qty_complete, tm._qty_scrapped, tm._nextoper_num, " +
                        "r._runlbrhrs, r._runmchhrs, tm._start_time, tm._end_time, tm._wc, " +
                        "coalesce((select _qty_Rele_qty from tbJobTransMain where _job = tm._job and _oper_num = (select max(_oper_num) from tbJobTransMain " +
                        "where _job = tm._job and _oper_num < tm._oper_num)), tm.[_qty_op_qty]) as oper_rel_prev, rmst.cntrl_point " +
                        "from dbo.tbJobTransMain tm inner join (select _job, _oper_num, count(*) sumLine, (select count(*) from dbo.tbJobTrans tsInner " +
                        "where tsInner.[_job] = tsOuter.[_job] and[_status] = \'COMPLETED\' and tsInner.[_oper_num] = tsOuter.[_oper_num]) as \'completedLine\' " +
                        "from dbo.tbJobTrans tsOuter where [_job] = @job group by _job, _oper_num ) rs on rs._job = tm._job and rs._oper_num = tm._oper_num \r\nleft join dbo.tbJobRoute r on r.[_job] = tm.[_job] and r.[_operationNo] = tm.[_oper_num] " +
                        "left join " + Global.GetSL9InitialCatalog() + ".dbo." + Global.jobroute_mst + " rmst on r.[_job] = rmst.[job] and r.[_operationNo] = rmst.[oper_num] " +
                        "where tm._job = @job and tm._oper_num = @oper_num" ,
                                parameters_sl9DB);
                    DataRow[] jobRouteData = tbJobRoute.Select();

                    var qtySLComplete = getLineSumCompleteQtyOnSL9(job, oper_num);
                    var qtySLScrap = getLineSumScrapQtyOnSL9(job, oper_num);

                    DataRow[] jobRate = null;
                    try
                    {
                        DataTable mstJobRoute = Global.getDataFrom_sl9DB("select run_rate_lbr, qty_received from " + Global.jobroute_mst + " where job = @job and oper_num = @oper_num ", parameters_sl9DB);
                        jobRate = mstJobRoute.Select();
                    }
                    catch (Exception ex)
                    {
                        return Json(new { error = "Job Route data doesn't exist" }, JsonRequestBehavior.AllowGet);
                    }
                    DataRow[] transType = null;
                    try
                    {
                        DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ", parameters_sl9DB);
                        transType = mstTransType.Select();
                    }
                    catch (Exception ex)
                    {
                        return Json(new { error = "Job Type is not deteced" }, JsonRequestBehavior.AllowGet);
                    }

                    if (int.Parse(jobRouteData[0].ItemArray[15].ToString()) == 0)
                    {
                        return Json(new { error = "Control point is set to 0!" }, JsonRequestBehavior.AllowGet);
                    }
                    else if (int.Parse(nonPostRow[0].ItemArray[24].ToString()) == 0 && int.Parse(nonPostRow[0].ItemArray[28].ToString()) == 0 && double.Parse(jobRouteData[0].ItemArray[6].ToString()) > qtySLComplete && double.Parse(jobRouteData[0].ItemArray[6].ToString()) != 0)
                    {
                        if (jobRouteData.Length != 0)
                        {
                            parameter_3.name = "@jobDate";
                            parameter_3.type_data = SqlDbType.DateTime;
                            double testValue = Convert.ToInt32(jobRouteData[0].ItemArray[1]);
                            DateTime transDate = DateTime.FromOADate(testValue);
                            parameter_3.value = transDate;

                            parameter_4.name = "@job";
                            parameter_4.type_data = SqlDbType.VarChar;
                            parameter_4.value = jobRouteData[0].ItemArray[2];

                            parameter_5.name = "@jobsuffix";
                            parameter_5.type_data = SqlDbType.Float;
                            parameter_5.value = jobRouteData[0].ItemArray[3];

                            parameter_6.name = "@operation";
                            parameter_6.type_data = SqlDbType.Float;
                            parameter_6.value = jobRouteData[0].ItemArray[4];

                            parameter_7.name = "@item";
                            parameter_7.type_data = SqlDbType.VarChar;
                            parameter_7.value = jobRouteData[0].ItemArray[5];

                            parameter_8.name = "@complete";
                            parameter_8.type_data = SqlDbType.Float;
                            parameter_8.value = jobRouteData[0].ItemArray[6];
                            var compQ = float.Parse(jobRouteData[0].ItemArray[6].ToString());

                            parameter_9.name = "@scrap";
                            parameter_9.type_data = SqlDbType.Float;
                            parameter_9.value = jobRouteData[0].ItemArray[7];
                            var scrapQ = float.Parse(jobRouteData[0].ItemArray[7].ToString());

                            parameter_10.name = "@moved";
                            parameter_10.type_data = SqlDbType.Float;
                            parameter_10.value = jobRouteData[0].ItemArray[6];

                            parameter_11.name = "@reason";
                            parameter_11.type_data = SqlDbType.VarChar;
                            parameter_11.value = "";
                            try
                            {
                                float scrapTotal = float.Parse(jobRouteData[0].ItemArray[7].ToString());
                                if (scrapTotal > 0)
                                {
                                    parameter_16.value = "PF";
                                }
                                else
                                {
                                    parameter_16.value = "";
                                }
                            }
                            catch
                            {
                            }

                            parameter_12.name = "@nextOperation";
                            parameter_12.type_data = SqlDbType.Float;
                            parameter_12.value = jobRouteData[0].ItemArray[8];

                            parameter_13.name = "@totalHours";
                            parameter_13.type_data = SqlDbType.Float;
                            if (transType[0].ItemArray[0].ToString().Trim().Equals("L")) 
                            {
                                parameter_13.value = float.Parse(jobRouteData[0].ItemArray[9].ToString()) * float.Parse(jobRouteData[0].ItemArray[6].ToString());
                            }
                            else
                            {
                                parameter_13.value = float.Parse(jobRouteData[0].ItemArray[10].ToString()) * float.Parse(jobRouteData[0].ItemArray[6].ToString());
                            }

                            parameter_14.name = "@operComplete";
                            parameter_14.type_data = SqlDbType.Float;
                            parameter_14.value = 0;

                            parameter_15.name = "@posted";
                            parameter_15.type_data = SqlDbType.Int;
                            parameter_15.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

                            parameter_16.name = "@trans_type";
                            parameter_16.type_data = SqlDbType.Char;
                            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
                            {
                                parameter_16.value = "R";
                            }
                            else
                            {
                                parameter_16.value = "C";
                            }

                            parameter_17.name = "@transnum";
                            parameter_17.type_data = SqlDbType.Float;
                            parameter_17.value = float.Parse(nonPostRow[0].ItemArray[1].ToString());


                            List<DB_Parameter> parameters_ewipDB = new List<DB_Parameter>() { parameter_3, parameter_4, parameter_5, parameter_6,
                                                                                      parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12,
                                                                                      parameter_13, parameter_14, parameter_15, parameter_16, parameter_17};

                            if ((int)jobRouteData[0].ItemArray[8] == 0)
                            {
                                DataTable UpdateJobTrans_MST = Global.getDataFrom_sl9DB("update " + Global.jobtrans_mst + " set trans_date = @jobDate, suffix = @jobsuffix," +
                                                                                        "qty_complete = @complete, qty_scrapped = @scrap, qty_moved = @complete, next_oper= NULL, a_hrs = @totalHours , complete_op = @operComplete, posted=@posted, user_code = 'ESA', trans_type = @trans_type where oper_num = @operation and job = @job and trans_num = @transnum",
                                    parameters_ewipDB);
                            }
                            else
                            {
                                DataTable UpdateJobTrans_MST = Global.getDataFrom_sl9DB("update " + Global.jobtrans_mst + " set trans_date = @jobDate, suffix = @jobsuffix," +
                                                                                        "qty_complete = @complete, qty_scrapped = @scrap, qty_moved = @complete, next_oper= @nextOperation, a_hrs = @totalHours , complete_op = @operComplete, posted=@posted, user_code = 'ESA', trans_type = @trans_type where oper_num = @operation and job = @job and trans_num = @transnum",
                                    parameters_ewipDB);
                            }



                            return Json(new { success = "Posting Success" }, JsonRequestBehavior.AllowGet);
                        }
                        else if ((int)jobRouteData[0].ItemArray[16] == 0)
                        {
                            return Json(new { error = "Work Center is Storer / Mix" }, JsonRequestBehavior.AllowGet);
                        }

                        return Json(new { error = "Job Route data doesn't exist" }, JsonRequestBehavior.AllowGet);

                    }
                    else if (int.Parse(nonPostRow[0].ItemArray[24].ToString()) == 0 && int.Parse(nonPostRow[0].ItemArray[28].ToString()) == 1 && double.Parse(jobRouteData[0].ItemArray[6].ToString()) > qtySLComplete && double.Parse(jobRouteData[0].ItemArray[6].ToString()) != 0)
                    {
                        InsertRemainingSLData(jobRouteData, jobRouteData[0], parameters_sl9DB, qtySLComplete, qtySLScrap);
                    }
                    else if (int.Parse(nonPostRow[0].ItemArray[24].ToString()) == 1 && int.Parse(nonPostRow[0].ItemArray[28].ToString()) == 0 && double.Parse(jobRouteData[0].ItemArray[6].ToString()) == qtySLComplete && Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post == 1 && double.Parse(jobRouteData[0].ItemArray[6].ToString()) != 0)
                    {
                        updateSLPosting(jobRouteData, jobRouteData[0], parameters_sl9DB);
                    }
                    else 
                    {
                        return Json(new { error = "Posting error" }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { success = "Posting Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    DataTable tbJobRoute = Global.getDataFrom_eWipDB("select distinct tm._trans_type, tm._start_Date, tm._job, r._jobSuffix, r._operationNo, tm._item, " +
                                                                     "tm._qty_complete, tm._qty_scrapped, tm._nextoper_num, r._runlbrhrs, r._runmchhrs, tm._start_time, tm._end_time, tm._wc, rmst.cntrl_point from tbJobTransMain tm " +
                                                                     "left join tbJobRoute r on r._job = tm._job and r._operationNo = tm._oper_num " +
                                                                     "left join " + Global.GetSL9InitialCatalog() + ".dbo." + Global.jobroute_mst + " rmst on r._job = rmst.job and r._operationNo = rmst.oper_num " +
                                                                     "where tm._job = @job and tm._oper_num = @oper_num  and tm._wc = @wc",
                        parameters_sl9DB);
                    DataRow[] jobRate = null, transType = null;
                    try
                    {
                        DataTable mstJobRoute = Global.getDataFrom_sl9DB("select run_rate_lbr, qty_received from " + Global.jobroute_mst + " where job = @job and oper_num = @oper_num ", parameters_sl9DB);
                        jobRate = mstJobRoute.Select();
                    }
                    catch (Exception ex)
                    {
                        return Json(new { error = "Job Route data doesn't exist" }, JsonRequestBehavior.AllowGet);
                    }

                    try
                    {
                        DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ", parameters_sl9DB);
                        transType = mstTransType.Select();
                    }
                    catch (Exception ex)
                    {
                        return Json(new { error = "Job Type is not deteced" }, JsonRequestBehavior.AllowGet);
                    }

                    List<DB_Parameter> dummyParam = new List<DB_Parameter>() { };
                    DataTable tbMaxTransNum = Global.getDataFrom_sl9DB("select max(trans_num)+1 from " + Global.jobtrans_mst, dummyParam);


                    DataRow[] MaxTrans = tbMaxTransNum.Select();
                    DataRow[] jobRouteData = tbJobRoute.Select();
                    if (int.Parse(jobRouteData[0].ItemArray[14].ToString()) == 0)
                    {
                        return Json(new { error = "Control point is set to 0!" }, JsonRequestBehavior.AllowGet);
                    }
                    else if (jobRouteData.Length != 0)
                    {
                        parameter_1.name = "@trans_num";
                        parameter_1.type_data = SqlDbType.Decimal;
                        parameter_1.value = MaxTrans[0].ItemArray[0];

                        parameter_2.name = "@job";
                        parameter_2.type_data = SqlDbType.VarChar;
                        parameter_2.value = jobRouteData[0].ItemArray[2];

                        parameter_3.name = "@jobsuffix";
                        parameter_3.type_data = SqlDbType.Float;
                        parameter_3.value = jobRouteData[0].ItemArray[3];

                        parameter_4.name = "@trans_type";
                        parameter_4.type_data = SqlDbType.Char;
                        if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
                        {
                            parameter_4.value = "R";
                        }
                        else
                        {
                            parameter_4.value = "C";
                        }

                        parameter_5.name = "@jobDate";
                        parameter_5.type_data = SqlDbType.DateTime;
                        double testValue = Convert.ToInt32(jobRouteData[0].ItemArray[1]);
                        DateTime transDate = DateTime.FromOADate(testValue);
                        parameter_5.value = transDate;

                        parameter_6.name = "@complete";
                        parameter_6.type_data = SqlDbType.Float;
                        parameter_6.value = jobRouteData[0].ItemArray[6];
                        var compQ = float.Parse(jobRouteData[0].ItemArray[6].ToString());

                        parameter_7.name = "@scrap";
                        parameter_7.type_data = SqlDbType.Float;
                        parameter_7.value = jobRouteData[0].ItemArray[7];
                        var scrapQ = float.Parse(jobRouteData[0].ItemArray[7].ToString());

                        parameter_8.name = "@operation";
                        parameter_8.type_data = SqlDbType.Float;
                        parameter_8.value = jobRouteData[0].ItemArray[4];

                        parameter_9.name = "@totalHours";
                        parameter_9.type_data = SqlDbType.Float;
                        if (transType[0].ItemArray[0].ToString().Trim().Equals("L")) 
                        {
                            parameter_9.value = float.Parse(jobRouteData[0].ItemArray[9].ToString()) * float.Parse(jobRouteData[0].ItemArray[6].ToString());
                        }
                        else
                        {
                            parameter_9.value = float.Parse(jobRouteData[0].ItemArray[10].ToString()) * float.Parse(jobRouteData[0].ItemArray[6].ToString());
                        }

                        parameter_10.name = "@nextOperation";
                        parameter_10.type_data = SqlDbType.Float;
                        parameter_10.value = jobRouteData[0].ItemArray[8];

                        parameter_11.name = "@start_time";
                        parameter_11.type_data = SqlDbType.Int;
                        parameter_11.value = jobRouteData[0].ItemArray[11];

                        parameter_12.name = "@end_time";
                        parameter_12.type_data = SqlDbType.Int;
                        parameter_12.value = jobRouteData[0].ItemArray[12];

                        parameter_13.name = "@qty_released";
                        parameter_13.type_data = SqlDbType.Float;
                        parameter_13.value = jobRouteData[0].ItemArray[6];

                        parameter_14.name = "@operComplete";
                        parameter_14.type_data = SqlDbType.Float;
                        parameter_14.value = 0;

                        parameter_15.name = "@posted";
                        parameter_15.type_data = SqlDbType.Int;
                        parameter_15.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

                        parameter_16.name = "@reason";
                        parameter_16.type_data = SqlDbType.VarChar;
                        parameter_16.value = "";
                        try
                        {
                            float scrapTotal = float.Parse(jobRouteData[0].ItemArray[7].ToString());
                            if (scrapTotal > 0)
                            {
                                parameter_16.value = "PF";
                            }
                            else
                            {
                                parameter_16.value = "";
                            }
                        }
                        catch
                        {
                        }

                        parameter_17.name = "@job_rate";
                        parameter_17.type_data = SqlDbType.Decimal;
                        parameter_17.value = jobRate[0].ItemArray[0];

                        parameter_18.name = "@_wc";
                        parameter_18.type_data = SqlDbType.VarChar;
                        parameter_18.value = jobRouteData[0].ItemArray[13];

                        List<DB_Parameter> parameters_ewipDB = new List<DB_Parameter>() { parameter_1, parameter_2, parameter_3, parameter_4, parameter_5, parameter_6,
                                                                                      parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12,
                                                                                      parameter_13, parameter_14, parameter_15, parameter_16, parameter_17, parameter_18};

                        if (!transType[0].ItemArray[0].ToString().Trim().Equals("") && (int)jobRouteData[0].ItemArray[8] == 0)
                        {
                            DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                                                                    + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                                                                    + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, NULL,'PROD',@start_time, @end_time,'R'"
                                                                                    + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameters_ewipDB);
                        }
                        else if (!transType[0].ItemArray[0].ToString().Trim().Equals(""))
                        {
                            DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                                + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameters_ewipDB);
                        }
                        else
                        {
                            DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                + "values ('AMT', @job, @jobsuffix, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                                + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameters_ewipDB);
                        }

                        return Json(new { success = "Posting Success" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = "Posting Error" }, JsonRequestBehavior.AllowGet);
                    }
                }
                //}
                //catch (Exception ex)
                //{
                //    return Json(new { error = ex.StackTrace.ToString() }, JsonRequestBehavior.AllowGet);
                //}
            }
        }

        private void ProcessUpdateJobNumber(int tansnum)
        {
            var db = Global.ewipDB;
            var updatedJob = db.tbJobTrans.FirstOrDefault(x => x._tansnum == tansnum);

            var sl9CompQty = getLineSumCompleteQtyOnSL9(updatedJob._job, updatedJob._oper_num);
            var sl9ScrapQty = getLineSumScrapQtyOnSL9(updatedJob._job, updatedJob._oper_num);
            var ewipCompQty = getLineSumCompleteQtyOnEwip(updatedJob._job, updatedJob._oper_num);
            var ewipScrapQty = getLineSumScrapQtyOnEwip(updatedJob._job, updatedJob._oper_num);

            if (sl9CompQty != ewipCompQty || sl9ScrapQty != ewipScrapQty)
            {
                ChangeCompleteOPLastLine(updatedJob._job, updatedJob._oper_num);
                insertUpdateSLData(updatedJob._job, updatedJob._oper_num, updatedJob._wc);
            }
        }

        private void insertUpdateSLData(string job, int operNum, string updatedJobWc)
        {
            DB_Parameter parameter_Job = new DB_Parameter();
            DB_Parameter parameter_OperNum = new DB_Parameter();
            DB_Parameter parameter_Wc = new DB_Parameter();

            parameter_Job.name = "@job";
            parameter_Job.type_data = SqlDbType.VarChar;
            parameter_Job.value = job;

            parameter_OperNum.name = "@oper_num";
            parameter_OperNum.type_data = SqlDbType.Int;
            parameter_OperNum.value = operNum;

            parameter_Wc.name = "@wc";
            parameter_Wc.type_data = SqlDbType.VarChar;
            parameter_Wc.value = updatedJobWc;

            List<DB_Parameter> parameters_sl9DB = new List<DB_Parameter>() { parameter_Job, parameter_OperNum, parameter_Wc };

            DataTable tbJobRoute = Global.getDataFrom_eWipDB("select distinct tm._trans_type, tm._start_Date, tm._job, r._jobSuffix, r._operationNo, tm._item, " +
                                                             "tm._qty_complete, tm._qty_scrapped, tm._nextoper_num, r._runlbrhrs, r._runmchhrs, tm._start_time, tm._end_time, tm._wc, tm._qty_op_qty from tbJobTransMain tm " +
                                                             "left join tbJobRoute r on r._job = tm._job and r._operationNo = tm._oper_num " +
                                                             "left join " + Global.GetSL9InitialCatalog() + ".dbo." + Global.jobroute_mst + " rmst  on r._job = rmst.job and r._operationNo = rmst.oper_num" +
                                                             "where rmst.cntrl_point = 1 and tm._job = @job and tm._oper_num = @oper_num  and tm._wc = @wc",
                parameters_sl9DB);

            DataRow[] jobRouteData = tbJobRoute.Select();

            if (jobRouteData.Length != 0)
            {
                var sl9CompQty = getLineSumCompleteQtyOnSL9(job, operNum);
                var sl9ScrapQty = getLineSumScrapQtyOnSL9(job, operNum);
                var ewipCompQty = getLineSumCompleteQtyOnEwip(job, operNum);
                var ewipScrapQty = getLineSumScrapQtyOnEwip(job, operNum);

                InsertNewSLUpdatedData(jobRouteData, job, operNum, ewipCompQty - sl9CompQty, ewipScrapQty - sl9ScrapQty);
            }
        }

        private void InsertNewSLUpdatedData(DataRow[] jobRouteData, string job, int operNum, double compQty, double scrapQty)
        {
            DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),

                parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();


            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
            mst_job.name = "@job";
            mst_job.type_data = SqlDbType.VarChar;
            mst_job.value = job;

            mst_oper_num.name = "@oper_num";
            mst_oper_num.type_data = SqlDbType.Int;
            mst_oper_num.value = operNum;
            List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

            DataRow[] jobRate, transType;

            DataTable mstJobRoute = Global.getDataFrom_sl9DB("select run_rate_lbr, qty_received from " + Global.jobroute_mst + " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
            jobRate = mstJobRoute.Select();

            DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
            transType = mstTransType.Select();

            parameter_2.name = "@job";
            parameter_2.type_data = SqlDbType.VarChar;
            parameter_2.value = jobRouteData[0].ItemArray[2];

            parameter_3.name = "@jobsuffix";
            parameter_3.type_data = SqlDbType.Float;
            parameter_3.value = jobRouteData[0].ItemArray[3];

            parameter_4.name = "@trans_type";
            parameter_4.type_data = SqlDbType.Char;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
            {
                parameter_4.value = "R";
            }
            else
            {
                parameter_4.value = "C";
            }

            parameter_5.name = "@jobDate";
            parameter_5.type_data = SqlDbType.DateTime;
            double testValue = Convert.ToInt32(jobRouteData[0].ItemArray[1]);
            DateTime transDate = DateTime.FromOADate(testValue);
            parameter_5.value = transDate;

            parameter_6.name = "@complete";
            parameter_6.type_data = SqlDbType.Float;
            parameter_6.value = compQty;
            var compQ = compQty;

            parameter_7.name = "@scrap";
            parameter_7.type_data = SqlDbType.Float;
            parameter_7.value = scrapQty;
            var scrapQ = scrapQty;

            parameter_8.name = "@operation";
            parameter_8.type_data = SqlDbType.Float;
            parameter_8.value = jobRouteData[0].ItemArray[4];

            parameter_9.name = "@totalHours";
            parameter_9.type_data = SqlDbType.Float;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
            {
                parameter_9.value = float.Parse(jobRouteData[0].ItemArray[9].ToString()) * float.Parse(compQty.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                parameter_9.value = float.Parse(jobRouteData[0].ItemArray[10].ToString()) * float.Parse(compQty.ToString(CultureInfo.InvariantCulture));
            }

            parameter_10.name = "@nextOperation";
            parameter_10.type_data = SqlDbType.Float;
            parameter_10.value = jobRouteData[0].ItemArray[8];

            parameter_11.name = "@start_time";
            parameter_11.type_data = SqlDbType.Int;
            parameter_11.value = jobRouteData[0].ItemArray[11];

            parameter_12.name = "@end_time";
            parameter_12.type_data = SqlDbType.Int;
            parameter_12.value = jobRouteData[0].ItemArray[12];

            parameter_13.name = "@qty_released";
            parameter_13.type_data = SqlDbType.Float;
            parameter_13.value = jobRouteData[0].ItemArray[6];

            parameter_14.name = "@operComplete";
            parameter_14.type_data = SqlDbType.Float;
            parameter_14.value = 0;

            parameter_15.name = "@posted";
            parameter_15.type_data = SqlDbType.Int;
            parameter_15.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

            parameter_16.name = "@reason";
            parameter_16.type_data = SqlDbType.VarChar;
            parameter_16.value = "";
            try
            {
                float scrapTotal = float.Parse(jobRouteData[0].ItemArray[7].ToString());
                if (scrapTotal > 0)
                {
                    parameter_16.value = "PF";
                }
                else
                {
                    parameter_16.value = "";
                }
            }
            catch
            {
            }

            parameter_17.name = "@job_rate";
            parameter_17.type_data = SqlDbType.Decimal;
            parameter_17.value = jobRate[0].ItemArray[0];

            parameter_18.name = "@_wc";
            parameter_18.type_data = SqlDbType.VarChar;
            parameter_18.value = jobRouteData[0].ItemArray[13];

            List<DB_Parameter> parameters_ewipDB = new List<DB_Parameter>() { parameter_2, parameter_3, parameter_4, parameter_5, parameter_6,
                                                                                      parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12,
                                                                                      parameter_13, parameter_14, parameter_15, parameter_16, parameter_17, parameter_18};
            if (!transType[0].ItemArray[0].ToString().Trim().Equals(""))
            {
                Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                    + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                    + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                    + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameters_ewipDB);
            }
            else
            {
                Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                    + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                    + "values ('AMT', @job, @jobsuffix, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                    + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameters_ewipDB);
            }
        }

        private void ChangeCompleteOPLastLine(string job, int operNum)
        {
            DB_Parameter parameter_Job = new DB_Parameter();
            DB_Parameter parameter_OperNum = new DB_Parameter();

            parameter_Job.name = "@job";
            parameter_Job.type_data = SqlDbType.VarChar;
            parameter_Job.value = job;

            parameter_OperNum.name = "@operNum";
            parameter_OperNum.type_data = SqlDbType.Int;
            parameter_OperNum.value = operNum;

            List<DB_Parameter> parameter_changeCompleteOp = new List<DB_Parameter>() { parameter_Job, parameter_OperNum };

            Global.getDataFrom_sl9DB("update " + Global.jobtrans_mst + " set complete_op = 0 where " +
                                     "trans_num = (select max(trans_num) from " + Global.jobtrans_mst + " where " +
                                     "job = @job and oper_num = @operNum)", parameter_changeCompleteOp);

        }

        private double getLineSumScrapQtyOnEwip(string job, int operNum)
        {
            return Global.ewipDB.tbJobTrans.Where(x => x._job == job && x._oper_num == operNum)
                .Sum(x => x._qty_scrapped);
        }

        private double getLineSumCompleteQtyOnEwip(string job, int operNum)
        {
            return Global.ewipDB.tbJobTrans.Where(x => x._job == job && x._oper_num == operNum)
                .Sum(x => x._qty_complete);
        }

        private void ClearReworkFlagOnASpecificJobOperation(string job, int oper_num)
        {
            var db = Global.db;
            var listOfDeletableFlag =
                db.Tmp_ReworkPostingTables.Where(x => x.job == job && x.oper_num == oper_num).ToList();

            db.Tmp_ReworkPostingTables.DeleteAllOnSubmit(listOfDeletableFlag);
            db.SubmitChanges();
        }

        [HttpPost]
        public ActionResult MassAutoPosting(string submitButton, string fileName, DateTime fromDate, DateTime toDate, string cbNoMatch = null)
        {
            counterUpdate = 0;
            SqlConnection ewipdb = Global.eWIPCnn,
                          sl9db = Global.sl9Cnn;

            DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                         parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                         parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                         parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),
                         parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();

            List<DB_Parameter> parameters_sl9DB = new List<DB_Parameter>();

            //Handle the rework first
            counterUpdate += UpdateAllReworkJob();

            //take the job needed to be exported
            DataTable tbJobNonPost = Global.getDataFrom_sl9DB("select * from " + Global.job_mst + " where stat in('R', 'S')", parameters_sl9DB);
            DataRow[] nonPostRow = tbJobNonPost.Select();

            foreach (var item in nonPostRow)
            {
                // get the tbJobTransMain under Job Number
                DB_Parameter parameter_job = new DB_Parameter();
                parameter_job.name = "@job";
                parameter_job.type_data = SqlDbType.VarChar;
                parameter_job.value = item.ItemArray[2];

                List<DB_Parameter> parameters_jobNonPost = new List<DB_Parameter>() { parameter_job };

                DataTable tbJobRoute = Global.getDataFrom_eWipDB("select tm._trans_type, tm._jobDate, tm._job, r._jobSuffix, r._operationNo, tm._item, "
                        + " tm._qty_complete, tm._qty_scrapped, tm._nextoper_num, r._runlbrhrs, r._runmchhrs, tm._start_time, tm._end_time, tm._wc, "
                        + " coalesce((select _qty_Rele_qty from tbJobTransMain where _job = tm._job and _oper_num = "
                        + " (select max(_oper_num) from tbJobTransMain where _job = tm._job and _oper_num < tm._oper_num)), tm.[_qty_op_qty]) as oper_rel_prev "
                        + " from dbo.tbJobTransMain tm inner join (select _job, _oper_num, count(*) sumLine, (select count(*) from dbo.tbJobTrans tsInner "
                        + " where tsInner.[_job] = tsOuter.[_job] and[_status] = 'COMPLETED' and tsInner.[_oper_num] = tsOuter.[_oper_num]) as 'completedLine' "
                        + " from dbo.tbJobTrans tsOuter where [_job] = @job group by _job, _oper_num ) rs "
                        + " on rs._job = tm._job and rs._oper_num = tm._oper_num "
                        + " left join dbo.tbJobRoute r on r.[_job] = tm.[_job] and r.[_operationNo] = tm.[_oper_num] "
                        + "left join " + Global.GetSL9InitialCatalog() + ".dbo." + Global.jobroute_mst + " rmst on r.[_job] = rmst.[job] and r.[_operationNo] = rmst.[oper_num] "
                        + " where rmst.cntrl_point = 1 "
                , parameters_jobNonPost);

                DataRow[] jobRouteData = tbJobRoute.Select();

                if (jobRouteData.Length != 0)
                {
                    foreach (var routeLine in jobRouteData)
                    {
                        if (routeLine.ItemArray[13].ToString().ToUpper() == "STOREP")
                        {
                            continue;
                        }

                        DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
                        mst_job.name = "@job";
                        mst_job.type_data = SqlDbType.VarChar;
                        mst_job.value = routeLine.ItemArray[2];

                        mst_oper_num.name = "@oper_num";
                        mst_oper_num.type_data = SqlDbType.Int;
                        mst_oper_num.value = routeLine.ItemArray[4];
                        List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

                        DataTable checkLine = Global.getDataFrom_sl9DB("select top 1 * from " + Global.jobtrans_mst + " where job = @job and oper_num = @oper_num order by job, oper_num, trans_num desc", parameterSpecificLine);
                        DataRow[] isLineThere = checkLine.Select();

                        if (isLineThere.Length != 0)
                        {
                            var qtySLComplete = getLineSumCompleteQtyOnSL9(routeLine.ItemArray[2].ToString(), int.Parse(routeLine.ItemArray[4].ToString()));
                            var qtySLScrap = getLineSumScrapQtyOnSL9(routeLine.ItemArray[2].ToString(), int.Parse(routeLine.ItemArray[4].ToString()));
                            //ULTIMOD - 06-18-2018 - THIS IS TO SEE IF THE LINE COMPETED OP = 1 OR 0
                            if (int.Parse(isLineThere[0].ItemArray[24].ToString()) == 0 && int.Parse(isLineThere[0].ItemArray[28].ToString()) == 0 && double.Parse(routeLine.ItemArray[6].ToString()) > qtySLComplete && double.Parse(jobRouteData[0].ItemArray[6].ToString()) != 0)
                            {
                                UpdateJobTransPosting(parameterSpecificLine, routeLine);
                            }
                            else if (int.Parse(isLineThere[0].ItemArray[24].ToString()) == 0 && int.Parse(isLineThere[0].ItemArray[28].ToString()) == 1 && double.Parse(routeLine.ItemArray[6].ToString()) > qtySLComplete && double.Parse(jobRouteData[0].ItemArray[6].ToString()) != 0)
                            {
                                InsertRemainingSLData(jobRouteData, routeLine, parameterSpecificLine, qtySLComplete, qtySLScrap);

                            }
                            else if (int.Parse(isLineThere[0].ItemArray[24].ToString()) == 1 &&
                                     int.Parse(isLineThere[0].ItemArray[28].ToString()) == 0 &&
                                     double.Parse(routeLine.ItemArray[6].ToString()) != qtySLComplete &&
                                     double.Parse(jobRouteData[0].ItemArray[6].ToString()) != 0 &&
                                     qtySLComplete == 0) //REWORK
                            {
                                InsertNewSLData(jobRouteData, routeLine, parameterSpecificLine);
                            }
                            else if (int.Parse(isLineThere[0].ItemArray[24].ToString()) == 1 && int.Parse(isLineThere[0].ItemArray[28].ToString()) == 0 && double.Parse(routeLine.ItemArray[6].ToString()) == qtySLComplete && Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post == 1 && double.Parse(jobRouteData[0].ItemArray[6].ToString()) != 0)
                            {
                                updateSLPosting(jobRouteData, routeLine, parameterSpecificLine);
                                counterUpdate++;
                            }
                        }
                        else
                        {
                            InsertNewSLData(jobRouteData, routeLine, parameterSpecificLine);
                        }
                    }
                }
            }
            return Json(new { success = counterUpdate + " data successfully posted " }, JsonRequestBehavior.AllowGet);
        }

        private int UpdateAllReworkJob()
        {
            var ultDb = Global.db;
            var counter = 0;
            var allReworkNeedsPosting = ultDb.Tmp_ReworkPostingTables.ToList();

            foreach (var singleRework in allReworkNeedsPosting)
            {
                if (CheckIfReworkJobIsPosted(singleRework.job, singleRework.oper_num))
                {
                    CreateNegateLineForMassPostedJob(singleRework.job, singleRework.oper_num);
                    counter++;
                }
            }

            ultDb.Tmp_ReworkPostingTables.DeleteAllOnSubmit(allReworkNeedsPosting);
            ultDb.SubmitChanges();

            return counter;
        }

        private void CreateNegateLineForMassPostedJob(string job, int operationNum, bool flagUserPost = false)
        {
            var targetRework =
                Global.db.Tmp_ReworkPostingTables.FirstOrDefault(x => x.job == job && x.oper_num == operationNum);
            var qtySLComplete = targetRework.QtyCompBeforeRework;
            var qtySLScrap = targetRework.QtyScrapBeforeRework;

            DB_Parameter parameter_job = new DB_Parameter();
            parameter_job.name = "@job";
            parameter_job.type_data = SqlDbType.VarChar;
            parameter_job.value = job;
            DB_Parameter parameter_operNum = new DB_Parameter();
            parameter_operNum.name = "@oper_num";
            parameter_operNum.type_data = SqlDbType.Int;
            parameter_operNum.value = operationNum;
            List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { parameter_job, parameter_operNum };

            DataTable tbJobRoute = Global.getDataFrom_sl9DB("select trans_num, site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                     + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, "
                                     + "shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow from " + Global.jobtrans_mst
                                     + " where job = @job and oper_num = (select top 1 oper_num from " + Global.jobtrans_mst + " where job = @job and oper_num < @oper_num order by oper_num desc) order by trans_num desc",
            parameterSpecificLine);
            DataRow routeLine = tbJobRoute.Select()[0];

            var prevOpNum = Global.ewipDB.tbJobTrans.Where(x => x._oper_num < operationNum && x._job == job)
                .Max(x => x._oper_num);
            var qtyEwipComplete = Global.ewipDB.tbJobTrans.Where(x => x._oper_num == prevOpNum && x._job == job)
                .Sum(x => x._qty_complete);
            var qtyEwipScrap = Global.ewipDB.tbJobTrans.Where(x => x._oper_num == prevOpNum && x._job == job)
                .Sum(x => x._qty_scrapped);

            if (int.Parse(routeLine.ItemArray[24].ToString()) == 0)
            {
                UpdateReworkSLData(routeLine, (float)qtyEwipComplete, (float)qtyEwipScrap, job, operationNum);
            }
            else
            {
                InsertReworkSLData(routeLine, parameterSpecificLine, (float)(qtySLComplete * -1), (float)(qtySLScrap * -1), flagUserPost);
            }
        }

        private void CreateNegateLineForPreviousPostedJob(string job, int operationNum, bool flagUserPost = false)
        {
            var qtySLComplete = getLinePreviousSumCompleteQty(job, operationNum);
            var qtySLScrap = getLinePreviousSumScrapQty(job, operationNum);


            DB_Parameter parameter_job = new DB_Parameter();
            parameter_job.name = "@job";
            parameter_job.type_data = SqlDbType.VarChar;
            parameter_job.value = job;
            DB_Parameter parameter_operNum = new DB_Parameter();
            parameter_operNum.name = "@oper_num";
            parameter_operNum.type_data = SqlDbType.Int;
            parameter_operNum.value = operationNum;
            List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { parameter_job, parameter_operNum };

            DataTable tbJobRoute = Global.getDataFrom_sl9DB("select trans_num, site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                     + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, "
                                     + "shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow from " + Global.jobtrans_mst
                                     + " where job = @job and oper_num = (select top 1 oper_num from " + Global.jobtrans_mst + " where job = @job and oper_num < @oper_num order by oper_num desc) order by trans_num desc",
            parameterSpecificLine);
            DataRow routeLine = tbJobRoute.Select()[0];

            var prevOpNum = Global.ewipDB.tbJobTrans.Where(x => x._oper_num < operationNum && x._job == job)
                .Max(x => x._oper_num);
            var qtyEwipComplete = Global.ewipDB.tbJobTrans.Where(x => x._oper_num == prevOpNum && x._job == job)
                .Sum(x => x._qty_complete);
            var qtyEwipScrap = Global.ewipDB.tbJobTrans.Where(x => x._oper_num == prevOpNum && x._job == job)
                .Sum(x => x._qty_scrapped);

            if (int.Parse(routeLine.ItemArray[24].ToString()) == 0)
            {
                UpdateReworkSLData(routeLine, (float)qtyEwipComplete, (float)qtyEwipScrap, job, operationNum);
            }
            else
            {
                InsertReworkSLData(routeLine, parameterSpecificLine, (float)(qtySLComplete * -1) + (float)qtyEwipComplete, (float)(qtySLScrap * -1) + (float)qtyEwipScrap, flagUserPost);
            }
        }

        private void UpdateReworkSLData(DataRow routeLine, float qtyEwipComplete,
            float qtyEwipScrap, string job, int opernum)
        {
            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter(), mst_prev_op = new DB_Parameter();
            mst_job.name = "@job";
            mst_job.type_data = SqlDbType.VarChar;
            mst_job.value = job;

            mst_oper_num.name = "@oper_num";
            mst_oper_num.type_data = SqlDbType.Int;
            mst_oper_num.value = opernum;

            List<DB_Parameter> parameters_sl9DB = new List<DB_Parameter>() { mst_job, mst_oper_num };

            DataTable tbJobRoute = Global.getDataFrom_eWipDB(
                " select r._runlbrhrs, r._runmchhrs " +
                "from dbo.tbJobTransMain tm inner join (select _job, _oper_num, count(*) sumLine, (select count(*) from dbo.tbJobTrans tsInner " +
                "where tsInner.[_job] = tsOuter.[_job] and[_status] = \'COMPLETED\' and tsInner.[_oper_num] = tsOuter.[_oper_num]) as \'completedLine\' " +
                "from dbo.tbJobTrans tsOuter where [_job] = @job group by _job, _oper_num ) rs on rs._job = tm._job and rs._oper_num = tm._oper_num \r\nleft join dbo.tbJobRoute r on r.[_job] = tm.[_job] and r.[_operationNo] = tm.[_oper_num] " +
                "left join " + Global.GetSL9InitialCatalog() + ".dbo." + Global.jobroute_mst + " rmst on r.[_job] = rmst.[job] and r.[_operationNo] = rmst.[oper_num] " +
                "where rmst.cntrl_point = 1",
                parameters_sl9DB);

            var labourMachineRate = (from x in Global.ewipDB.tbJobRoutes 
                                    where x._job == job && x._operationNo < opernum
                                          select x).OrderByDescending(x => x._operationNo).FirstOrDefault();
                                    

            DataRow[] transType = null;


            try
            {
                DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = (select top 1 oper_num from  "
                                                                  + Global.jrt_sch_mst + " where job = @job and oper_num < @oper_num order by oper_num desc)", parameters_sl9DB);
                transType = mstTransType.Select();
            }
            catch (Exception ex)
            {
                return;
            }

            DataRow[] jobRouteData = tbJobRoute.Select();

            DB_Parameter parameter_1 = new DB_Parameter(),
                parameter_2 = new DB_Parameter(),
                parameter_3 = new DB_Parameter(),
                parameter_4 = new DB_Parameter(),
                parameter_5 = new DB_Parameter();

            parameter_1.name = "@complete";
            parameter_1.type_data = SqlDbType.Float;
            parameter_1.value = qtyEwipComplete;

            parameter_2.name = "@scrap";
            parameter_2.type_data = SqlDbType.Float;
            parameter_2.value = qtyEwipScrap;

            parameter_3.name = "@posted";
            parameter_3.type_data = SqlDbType.Int;
            parameter_3.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

            parameter_4.name = "@totalHours";
            parameter_4.type_data = SqlDbType.Float;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
            {
                parameter_4.value = float.Parse(labourMachineRate._runlbrhrs.ToString()) * qtyEwipComplete;
            }
            else
            {
                parameter_4.value = float.Parse(labourMachineRate._runmchhrs.ToString()) * qtyEwipComplete;
            }

            parameter_5.name = "@trans_num";
            parameter_5.type_data = SqlDbType.Int;
            parameter_5.value = routeLine.ItemArray[0];

            List<DB_Parameter> parameterUpdateRework = new List<DB_Parameter>() { parameter_1, parameter_2, parameter_3, parameter_4, parameter_5 };

            Global.getDataFrom_sl9DB("update " + Global.jobtrans_mst + " set qty_moved = @complete, qty_complete = @complete, qty_scrapped = @scrap, a_hrs = @totalHours, posted = @posted, complete_op = 0 " +
                                     "where trans_num = @trans_num",
                parameterUpdateRework);
        }

        private bool InsertReworkSLData(DataRow routeLine, List<DB_Parameter> parameterSpecificLine, float qtyComplete,
            float qtyScrap, bool flagUserPost = false)
        {

            DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),
                parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();
            DataRow[] jobRate, transType;

            try
            {
                DataTable mstJobRoute = Global.getDataFrom_sl9DB("select run_rate_lbr, qty_received from " + Global.jobroute_mst + " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
                jobRate = mstJobRoute.Select();

                DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = (select top 1 oper_num from  "
                                                                  + Global.jrt_sch_mst + " where job = @job and oper_num < @oper_num order by oper_num desc)", parameterSpecificLine);
                transType = mstTransType.Select();
            }
            catch (Exception ex)
            {
                return false;
            }

            DataTable tbMaxTransNum = Global.getDataFrom_sl9DB("select max(trans_num)+1 from " + Global.jobtrans_mst, new List<DB_Parameter>());

            DataRow[] MaxTrans = tbMaxTransNum.Select();

            parameter_1.name = "@trans_num";
            parameter_1.type_data = SqlDbType.Decimal;
            parameter_1.value = 0;

            parameter_2.name = "@job";
            parameter_2.type_data = SqlDbType.VarChar;
            parameter_2.value = routeLine.ItemArray[2];

            parameter_3.name = "@jobsuffix";
            parameter_3.type_data = SqlDbType.Float;
            parameter_3.value = routeLine.ItemArray[3];

            parameter_4.name = "@trans_type";
            parameter_4.type_data = SqlDbType.Char;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
            {
                parameter_4.value = "R";
            }
            else
            {
                parameter_4.value = "C";
            }

            parameter_5.name = "@jobDate";
            parameter_5.type_data = SqlDbType.DateTime;
            parameter_5.value = routeLine.ItemArray[5];

            parameter_6.name = "@complete";
            parameter_6.type_data = SqlDbType.Float;
            parameter_6.value = qtyComplete;

            parameter_7.name = "@scrap";
            parameter_7.type_data = SqlDbType.Float;
            parameter_7.value = qtyScrap;

            parameter_8.name = "@operation";
            parameter_8.type_data = SqlDbType.Float;
            parameter_8.value = routeLine.ItemArray[8];

            parameter_9.name = "@totalHours";
            parameter_9.type_data = SqlDbType.Float;
            parameter_9.value = float.Parse(routeLine.ItemArray[9].ToString()) * -1;

            parameter_10.name = "@nextOperation";
            parameter_10.type_data = SqlDbType.Float;
            parameter_10.value = routeLine.ItemArray[10];

            parameter_11.name = "@start_time";
            parameter_11.type_data = SqlDbType.Int;
            parameter_11.value = routeLine.ItemArray[12];

            parameter_12.name = "@end_time";
            parameter_12.type_data = SqlDbType.Int;
            parameter_12.value = routeLine.ItemArray[13];

            parameter_14.name = "@operComplete";
            parameter_14.type_data = SqlDbType.Float;
            parameter_14.value = 0;

            parameter_15.name = "@posted";
            parameter_15.type_data = SqlDbType.Int;
            parameter_15.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

            parameter_16.name = "@reason";
            parameter_16.type_data = SqlDbType.VarChar;
            parameter_16.value = "";
            try
            {
                float scrapTotal = float.Parse(routeLine.ItemArray[7].ToString());
                if (scrapTotal != 0)
                {
                    parameter_16.value = "PF";
                }
                else
                {
                    parameter_16.value = "";
                }
            }
            catch
            {
                parameter_16.value = "";
            }

            parameter_17.name = "@job_rate";
            parameter_17.type_data = SqlDbType.Decimal;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
            {
                parameter_17.value = jobRate[0].ItemArray[0];
            }
            else
            {
                parameter_17.value = jobRate[0].ItemArray[1];
            }

            parameter_18.name = "@_wc";
            parameter_18.type_data = SqlDbType.VarChar;
            parameter_18.value = routeLine.ItemArray[29];

            List<DB_Parameter> parameterNewLine = new List<DB_Parameter>()
                {
                    parameter_1, parameter_2, parameter_3, parameter_4, parameter_5, parameter_6,
                    parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12, parameter_14, parameter_15, parameter_16, parameter_17, parameter_18
                };
            if (!transType[0].ItemArray[0].ToString().Trim().Equals(""))
            {
                Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst +
                                         " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                         + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                         + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                                         + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" +
                                         Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )",
                    parameterNewLine);
                counterUpdate++;
            }
            else
            {
                Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst +
                                         " (site_ref, job, suffix, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                         + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                         + "values ('AMT', @job, @jobsuffix, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                                         + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" +
                                         Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )",
                    parameterNewLine);
                counterUpdate++;
            }
            return true;
        }

        private bool CheckIfReworkJobIsPosted(string jobNo, int operationNumber)
        {
            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
            mst_job.name = "@job";
            mst_job.type_data = SqlDbType.VarChar;
            mst_job.value = jobNo;

            mst_oper_num.name = "@oper_num";
            mst_oper_num.type_data = SqlDbType.Int;
            mst_oper_num.value = operationNumber;
            List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

            DataTable checkLine = Global.getDataFrom_sl9DB("select top 1 * from " + Global.jobtrans_mst + " where job = @job and oper_num < @oper_num order by job, oper_num, trans_num desc", parameterSpecificLine);
            DataRow[] isLineThere = checkLine.Select();
            return isLineThere.Length > 0;
        }

        private void UpdateJobTransPosting(List<DB_Parameter> parameterSpecificLine, DataRow routeLine)
        {
            DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),
                parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();

            DataRow[] jobRate = null;
            try
            {
                DataTable mstJobRoute = Global.getDataFrom_sl9DB(
                    "select run_rate_lbr, qty_received from " + Global.jobroute_mst +
                    " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
                jobRate = mstJobRoute.Select();
            }
            catch (Exception ex)
            {

            }

            DataRow[] transType = null;
            try
            {
                DataTable mstTransType = Global.getDataFrom_sl9DB(
                    "select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ",
                    parameterSpecificLine);
                transType = mstTransType.Select();
            }
            catch (Exception ex)
            {

            }

            parameter_3.name = "@jobDate";
            parameter_3.type_data = SqlDbType.DateTime;
            double testValue = Convert.ToInt32(routeLine.ItemArray[1]);
            DateTime transDate = DateTime.FromOADate(testValue);
            parameter_3.value = DateTime.Now;

            parameter_4.name = "@job";
            parameter_4.type_data = SqlDbType.VarChar;
            parameter_4.value = routeLine.ItemArray[2];

            parameter_5.name = "@jobsuffix";
            parameter_5.type_data = SqlDbType.Float;
            parameter_5.value = routeLine.ItemArray[3];

            parameter_6.name = "@operation";
            parameter_6.type_data = SqlDbType.Float;
            parameter_6.value = routeLine.ItemArray[4];

            parameter_7.name = "@item";
            parameter_7.type_data = SqlDbType.VarChar;
            parameter_7.value = routeLine.ItemArray[5];

            parameter_8.name = "@complete";
            parameter_8.type_data = SqlDbType.Float;
            parameter_8.value = routeLine.ItemArray[6];

            parameter_9.name = "@scrap";
            parameter_9.type_data = SqlDbType.Float;
            parameter_9.value = routeLine.ItemArray[7];

            parameter_10.name = "@moved";
            parameter_10.type_data = SqlDbType.Float;
            parameter_10.value = routeLine.ItemArray[6];

            parameter_11.name = "@reason";
            parameter_11.type_data = SqlDbType.VarChar;
            parameter_11.value = "";
            try
            {
                float scrapTotal = float.Parse(routeLine.ItemArray[7].ToString());
                if (scrapTotal > 0)
                {
                    parameter_11.value = "PF";
                }
                else
                {
                    parameter_11.value = "";
                }
            }
            catch
            {
                parameter_11.value = "";
            }

            parameter_12.name = "@nextOperation";
            parameter_12.type_data = SqlDbType.Float;
            parameter_12.value = routeLine.ItemArray[8];

            parameter_13.name = "@totalHours";
            parameter_13.type_data = SqlDbType.Float;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L")) 
            {
                parameter_13.value = float.Parse(routeLine.ItemArray[9].ToString()) * float.Parse(routeLine.ItemArray[6].ToString());
            }
            else
            {
                parameter_13.value = float.Parse(routeLine.ItemArray[10].ToString()) * float.Parse(routeLine.ItemArray[6].ToString());
            }

            var compQ = float.Parse(routeLine.ItemArray[6].ToString());
            var scrapQ = float.Parse(routeLine.ItemArray[7].ToString());
            parameter_14.name = "@operComplete";
            parameter_14.type_data = SqlDbType.Float;
            parameter_14.value = 0;

            parameter_15.name = "@posted";
            parameter_15.type_data = SqlDbType.Int;
            parameter_15.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

            parameter_16.name = "@trans_type";
            parameter_16.type_data = SqlDbType.Char;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
            {
                parameter_16.value = "R";
            }
            else
            {
                parameter_16.value = "C";
            }

            List<DB_Parameter> parameters_ewipDB = new List<DB_Parameter>()
            {
                parameter_3, parameter_4, parameter_5, parameter_6,
                parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12,
                parameter_13, parameter_14, parameter_15, parameter_16
            };

            DataTable UpdateJobTrans_MST = Global.getDataFrom_sl9DB("update " + Global.jobtrans_mst +
                                                                    " set trans_date = @jobDate, suffix = @jobsuffix," +
                                                                    "qty_complete = @complete, qty_scrapped = @scrap, qty_moved = @complete, next_oper= @nextOperation, a_hrs = @totalHours , complete_op = @operComplete, posted=@posted, user_code = 'ESA', trans_type = @trans_type where oper_num = @operation and job = @job",
                parameters_ewipDB);

            counterUpdate++;
        }

        private bool updateSLPosting(DataRow[] jobRouteData, DataRow routeLine, List<DB_Parameter> parameterSpecificLine)
        {
            try
            {
                DataTable UpdateJobTrans_MST = Global.getDataFrom_sl9DB("update " + Global.jobtrans_mst + " set posted = 1 where oper_num = @oper_num and job = @job",
                                     parameterSpecificLine);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool InsertNewSLData(DataRow[] jobRouteData, DataRow routeLine, List<DB_Parameter> parameterSpecificLine, float qtyComplete = 0, float qtyScrap = 0)
        {

            DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),
                parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();
            DataRow[] jobRate, transType;

            try
            {
                DataTable mstJobRoute = Global.getDataFrom_sl9DB("select run_rate_lbr, qty_received from " + Global.jobroute_mst + " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
                jobRate = mstJobRoute.Select();

                DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
                transType = mstTransType.Select();
            }
            catch (Exception ex)
            {
                return false;
            }

            List<DB_Parameter> dummyParam = new List<DB_Parameter>() { };
            DataTable tbMaxTransNum = Global.getDataFrom_sl9DB("select max(trans_num)+1 from " + Global.jobtrans_mst, dummyParam);


            DataRow[] MaxTrans = tbMaxTransNum.Select();
            if (jobRouteData.Length != 0)
            {
                parameter_1.name = "@trans_num";
                parameter_1.type_data = SqlDbType.Decimal;
                parameter_1.value = MaxTrans[0].ItemArray[0];

                parameter_2.name = "@job";
                parameter_2.type_data = SqlDbType.VarChar;
                parameter_2.value = routeLine.ItemArray[2];

                parameter_3.name = "@jobsuffix";
                parameter_3.type_data = SqlDbType.Float;
                parameter_3.value = routeLine.ItemArray[3];

                parameter_4.name = "@trans_type";
                parameter_4.type_data = SqlDbType.Char;
                if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
                {
                    parameter_4.value = "R";
                }
                else
                {
                    parameter_4.value = "C";
                }

                parameter_5.name = "@jobDate";
                parameter_5.type_data = SqlDbType.DateTime;
                double testValue = Convert.ToInt32(routeLine.ItemArray[1]);
                DateTime transDate = DateTime.FromOADate(testValue);
                parameter_5.value = DateTime.Now;

                parameter_6.name = "@complete";
                parameter_6.type_data = SqlDbType.Float;
                parameter_6.value = routeLine.ItemArray[6];
                float compQ = qtyComplete != 0 ? qtyComplete : float.Parse(routeLine.ItemArray[6].ToString());

                parameter_7.name = "@scrap";
                parameter_7.type_data = SqlDbType.Float;
                parameter_7.value = routeLine.ItemArray[7];
                float scrapQ = qtyScrap != 0 ? qtyScrap : float.Parse(routeLine.ItemArray[7].ToString());

                parameter_8.name = "@operation";
                parameter_8.type_data = SqlDbType.Float;
                parameter_8.value = routeLine.ItemArray[4];

                parameter_9.name = "@totalHours";
                parameter_9.type_data = SqlDbType.Float;
                if (transType[0].ItemArray[0].ToString().Trim().Equals("L")) //INI SALAH
                {
                    parameter_9.value = float.Parse(routeLine.ItemArray[9].ToString()) * float.Parse(routeLine.ItemArray[6].ToString());
                }
                else
                {
                    parameter_9.value = float.Parse(routeLine.ItemArray[10].ToString()) * float.Parse(routeLine.ItemArray[6].ToString());
                }

                parameter_10.name = "@nextOperation";
                parameter_10.type_data = SqlDbType.Float;
                parameter_10.value = routeLine.ItemArray[8];

                parameter_11.name = "@start_time";
                parameter_11.type_data = SqlDbType.Int;
                parameter_11.value = routeLine.ItemArray[11];

                parameter_12.name = "@end_time";
                parameter_12.type_data = SqlDbType.Int;
                parameter_12.value = routeLine.ItemArray[12];

                parameter_13.name = "@qty_released";
                parameter_13.type_data = SqlDbType.Float;
                parameter_13.value = routeLine.ItemArray[6];

                parameter_14.name = "@operComplete";
                parameter_14.type_data = SqlDbType.Float;
                parameter_14.value = 0;

                parameter_15.name = "@posted";
                parameter_15.type_data = SqlDbType.Int;
                parameter_15.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

                parameter_16.name = "@reason";
                parameter_16.type_data = SqlDbType.VarChar;
                parameter_16.value = "";
                try
                {
                    float scrapTotal = float.Parse(routeLine.ItemArray[7].ToString());
                    if (scrapTotal != 0)
                    {
                        parameter_16.value = "PF";
                    }
                    else
                    {
                        parameter_16.value = "";
                    }
                }
                catch
                {
                    parameter_16.value = "";
                }

                parameter_17.name = "@job_rate";
                parameter_17.type_data = SqlDbType.Decimal;
                parameter_17.value = jobRate[0].ItemArray[0];

                parameter_18.name = "@_wc";
                parameter_18.type_data = SqlDbType.VarChar;
                parameter_18.value = routeLine.ItemArray[13];

                List<DB_Parameter> parameterNewLine = new List<DB_Parameter>()
                {
                    parameter_1, parameter_2, parameter_3, parameter_4, parameter_5, parameter_6,
                    parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12,
                    parameter_13, parameter_14, parameter_15, parameter_16, parameter_17, parameter_18
                };
                if (!transType[0].ItemArray[0].ToString().Trim().Equals("") && compQ != 0 && (int)routeLine.ItemArray[8] == 0)
                {
                    Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst +
                                             " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                             + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                             + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, NULL,'PROD',@start_time, @end_time,'R'"
                                             + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" +
                                             Guid.NewGuid() + "', 'AMT Web','AMT Web', getdate(), 0 )",
                        parameterNewLine);
                    counterUpdate++;
                }
                else if (!transType[0].ItemArray[0].ToString().Trim().Equals("") && compQ != 0)
                {
                    Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst +
                                             " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                             + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                             + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                                             + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" +
                                             Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )",
                        parameterNewLine);
                    counterUpdate++;
                }
                else if (compQ != 0)
                {
                    Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst +
                                             " (site_ref, job, suffix, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                             + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                             + "values ('AMT', @job, @jobsuffix, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                                             + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" +
                                             Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )",
                        parameterNewLine);
                    counterUpdate++;
                }

                return true;
            }

            return false;
        }

        private void InsertRemainingSLData(DataRow[] jobRouteData, DataRow routeLine, List<DB_Parameter> parameterSpecificLine, double qtySLComplete, double qtySLScrap)
        {
            DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                        parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                        parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                        parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),
                        parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();

            DataRow[] jobRate = null, transType = null;
            try
            {
                DataTable mstJobRoute = Global.getDataFrom_sl9DB("select run_rate_lbr, qty_received from " + Global.jobroute_mst + " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
                jobRate = mstJobRoute.Select();

                DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ", parameterSpecificLine);
                transType = mstTransType.Select();
            }
            catch (Exception ex)
            {
            }

            List<DB_Parameter> dummyParam = new List<DB_Parameter>() { };
            DataTable tbMaxTransNum = Global.getDataFrom_sl9DB("select max(trans_num)+1 from " + Global.jobtrans_mst, dummyParam);

            DataRow[] MaxTrans = tbMaxTransNum.Select();
            if (jobRouteData.Length != 0)
            {
                parameter_1.name = "@trans_num";
                parameter_1.type_data = SqlDbType.Decimal;
                parameter_1.value = MaxTrans[0].ItemArray[0];

                parameter_2.name = "@job";
                parameter_2.type_data = SqlDbType.VarChar;
                parameter_2.value = routeLine.ItemArray[2];

                parameter_3.name = "@jobsuffix";
                parameter_3.type_data = SqlDbType.Float;
                parameter_3.value = routeLine.ItemArray[3];

                parameter_4.name = "@trans_type";
                parameter_4.type_data = SqlDbType.Char;
                if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
                {
                    parameter_4.value = "R";
                }
                else
                {
                    parameter_4.value = "C";
                }

                parameter_5.name = "@jobDate";
                parameter_5.type_data = SqlDbType.DateTime;
                double testValue = Convert.ToInt32(routeLine.ItemArray[1]);
                DateTime transDate = DateTime.FromOADate(testValue);
                parameter_5.value = DateTime.Now;

                parameter_6.name = "@complete";
                parameter_6.type_data = SqlDbType.Float;
                parameter_6.value = Double.Parse(routeLine.ItemArray[6].ToString()) - qtySLComplete;
                float compQ = float.Parse(routeLine.ItemArray[6].ToString());

                parameter_7.name = "@scrap";
                parameter_7.type_data = SqlDbType.Float;
                parameter_7.value = Double.Parse(routeLine.ItemArray[7].ToString()) - qtySLScrap;
                float scrapQ = float.Parse(routeLine.ItemArray[7].ToString());

                parameter_8.name = "@operation";
                parameter_8.type_data = SqlDbType.Float;
                parameter_8.value = routeLine.ItemArray[4];

                parameter_9.name = "@totalHours";
                parameter_9.type_data = SqlDbType.Float;
                if (transType[0].ItemArray[0].ToString().Trim().Equals("L")) 
                {
                    parameter_9.value = float.Parse(routeLine.ItemArray[9].ToString()) * float.Parse(routeLine.ItemArray[6].ToString());
                }
                else
                {
                    parameter_9.value = float.Parse(routeLine.ItemArray[10].ToString()) * float.Parse(routeLine.ItemArray[6].ToString());
                }

                parameter_10.name = "@nextOperation";
                parameter_10.type_data = SqlDbType.Float;
                parameter_10.value = routeLine.ItemArray[8];

                parameter_11.name = "@start_time";
                parameter_11.type_data = SqlDbType.Int;
                parameter_11.value = routeLine.ItemArray[11];

                parameter_12.name = "@end_time";
                parameter_12.type_data = SqlDbType.Int;
                parameter_12.value = routeLine.ItemArray[12];

                parameter_13.name = "@qty_released";
                parameter_13.type_data = SqlDbType.Float;
                parameter_13.value = routeLine.ItemArray[6];

                parameter_14.name = "@operComplete";
                parameter_14.type_data = SqlDbType.Float;
                parameter_14.value = 0;

                parameter_15.name = "@posted";
                parameter_15.type_data = SqlDbType.Int;
                parameter_15.value = Global.db.Tmp_SL9TableNames.FirstOrDefault().freeze_post;

                parameter_16.name = "@reason";
                parameter_16.type_data = SqlDbType.VarChar;
                parameter_16.value = "";
                try
                {
                    float scrapTotal = float.Parse(routeLine.ItemArray[7].ToString());
                    if (scrapTotal != 0)
                    {
                        parameter_16.value = "PF";
                    }
                    else
                    {
                        parameter_16.value = "";
                    }
                }
                catch
                {
                    parameter_16.value = "";
                }

                parameter_17.name = "@job_rate";
                parameter_17.type_data = SqlDbType.Decimal;
                parameter_17.value = jobRate[0].ItemArray[0];

                parameter_18.name = "@_wc";
                parameter_18.type_data = SqlDbType.VarChar;
                parameter_18.value = routeLine.ItemArray[13];

                List<DB_Parameter> parameterNewLine = new List<DB_Parameter>() { parameter_1, parameter_2, parameter_3, parameter_4, parameter_5, parameter_6,
                                                                                      parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12,
                                                                                      parameter_13, parameter_14, parameter_15, parameter_16, parameter_17, parameter_18};

                if (!transType[0].ItemArray[0].ToString().Trim().Equals("") && (int)routeLine.ItemArray[8] == 0)
                {
                    DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                                                                            + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                                                                            + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, NULL,'PROD',@start_time, @end_time,'R'"
                                                                            + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameterNewLine);

                }
                else if (!transType[0].ItemArray[0].ToString().Trim().Equals(""))
                {
                    DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                        + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                        + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                        + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameterNewLine);

                }
                else
                {
                    DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                        + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                        + "values ('AMT', @job, @jobsuffix, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                        + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameterNewLine);
                }
                counterUpdate++;
            }
        }

        private bool createNewReverseLine(List<DB_Parameter> parameters_sl9DB, string jobNo, int operNum, int maxPrev)
        {
            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter(), mst_prev_op = new DB_Parameter();
            mst_job.name = "@job";
            mst_job.type_data = SqlDbType.VarChar;
            mst_job.value = jobNo;

            mst_oper_num.name = "@oper_num";
            mst_oper_num.type_data = SqlDbType.Int;
            mst_oper_num.value = operNum;

            DataTable nextOPString = Global.getDataFrom_eWipDB("select min([_operationNo]) as maxOper from dbo.tbJobRoute where [_operationNo] > @oper_num and [_job] = @job", parameters_sl9DB);
            var nextOP = int.Parse(nextOPString.Select()[0].ItemArray[0].ToString());

            DataTable targetWcString = Global.getDataFrom_eWipDB("select [_wc] from dbo.tbJobRoute where [_operationNo] = @oper_num and [_job] = @job", parameters_sl9DB);
            var targetWc = targetWcString.Select()[0].ItemArray[0].ToString();

            mst_prev_op.name = "@oper_num";
            mst_prev_op.type_data = SqlDbType.Int;
            mst_prev_op.value = maxPrev;

            List<DB_Parameter> parameters_Job = new List<DB_Parameter>() { mst_job, mst_prev_op };

            DataTable tbRevert = Global.getDataFrom_eWipDB("select [_job], [_oper_num], sum([_qty_complete]) sumLineComp, sum([_qty_scrapped]) sumScrap, sum([_qty_op_qty]) sumReceivedQty from dbo.tbJobTrans jt "
            + " where jt._job = @job and[_oper_num] in "
            + " (select DISTINCT[_oper_num] from dbo.tbJobTrans jt "
            + " where [_Reworkst] = 'R' and [_job] = @job and [_oper_num] = @oper_num "
            + " group by[_job], [_oper_num]) "
            + " group by[_job], [_oper_num] "
            , parameters_Job);

            DataRow[] revertLine = tbRevert.Select();
            var negativeQty = float.Parse(revertLine[0].ItemArray[4].ToString()) - float.Parse(revertLine[0].ItemArray[2].ToString());

            DB_Parameter parameter_1 = new DB_Parameter(), parameter_2 = new DB_Parameter(), parameter_3 = new DB_Parameter(), parameter_4 = new DB_Parameter(),
                        parameter_5 = new DB_Parameter(), parameter_6 = new DB_Parameter(), parameter_7 = new DB_Parameter(), parameter_8 = new DB_Parameter(),
                        parameter_9 = new DB_Parameter(), parameter_10 = new DB_Parameter(), parameter_11 = new DB_Parameter(), parameter_12 = new DB_Parameter(),
                        parameter_13 = new DB_Parameter(), parameter_14 = new DB_Parameter(), parameter_15 = new DB_Parameter(), parameter_16 = new DB_Parameter(),
                        parameter_17 = new DB_Parameter(), parameter_18 = new DB_Parameter();

            DataRow[] jobRate = null, transType = null;
            try
            {
                DataTable mstJobRoute = Global.getDataFrom_sl9DB("select run_rate_lbr, qty_received from " + Global.jobroute_mst + " where job = @job and oper_num = @oper_num ", parameters_sl9DB);
                jobRate = mstJobRoute.Select();

                DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ", parameters_sl9DB);
                transType = mstTransType.Select();
            }
            catch (Exception ex)
            {
                return false;
            }

            List<DB_Parameter> dummyParam = new List<DB_Parameter>() { };
            DataTable tbMaxTransNum = Global.getDataFrom_sl9DB("select max(trans_num)+1 from " + Global.jobtrans_mst, dummyParam);

            DataRow[] MaxTrans = tbMaxTransNum.Select();

            parameter_1.name = "@trans_num";
            parameter_1.type_data = SqlDbType.Decimal;
            parameter_1.value = MaxTrans[0].ItemArray[0];

            parameter_2.name = "@job";
            parameter_2.type_data = SqlDbType.VarChar;
            parameter_2.value = jobNo;

            parameter_3.name = "@jobsuffix";
            parameter_3.type_data = SqlDbType.Float;
            parameter_3.value = 0;

            parameter_4.name = "@trans_type";
            parameter_4.type_data = SqlDbType.Char;
            if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
            {
                parameter_4.value = "R";
            }
            else
            {
                parameter_4.value = "C";
            }

            parameter_5.name = "@jobDate";
            parameter_5.type_data = SqlDbType.DateTime;
            parameter_5.value = DateTime.Now;

            parameter_6.name = "@complete";
            parameter_6.type_data = SqlDbType.Float;
            parameter_6.value = negativeQty * -1;

            parameter_7.name = "@scrap";
            parameter_7.type_data = SqlDbType.Float;
            parameter_7.value = 0;

            parameter_8.name = "@operation";
            parameter_8.type_data = SqlDbType.Float;
            parameter_8.value = operNum;

            parameter_9.name = "@totalHours";
            parameter_9.type_data = SqlDbType.Float;
            parameter_9.value = 0;

            parameter_10.name = "@nextOperation";
            parameter_10.type_data = SqlDbType.Float;
            parameter_10.value = maxPrev;

            parameter_11.name = "@start_time";
            parameter_11.type_data = SqlDbType.Int;
            parameter_11.value = 0;

            parameter_12.name = "@end_time";
            parameter_12.type_data = SqlDbType.Int;
            parameter_12.value = 0;

            parameter_13.name = "@qty_released";
            parameter_13.type_data = SqlDbType.Float;
            parameter_13.value = 0;

            parameter_14.name = "@operComplete";
            parameter_14.type_data = SqlDbType.Float;
            parameter_14.value = 0;

            parameter_15.name = "@posted";
            parameter_15.type_data = SqlDbType.Int;
            parameter_15.value = 1;

            parameter_16.name = "@reason";
            parameter_16.type_data = SqlDbType.VarChar;
            parameter_16.value = "";

            parameter_17.name = "@job_rate";
            parameter_17.type_data = SqlDbType.Decimal;
            parameter_17.value = jobRate[0].ItemArray[0];

            parameter_18.name = "@_wc";
            parameter_18.type_data = SqlDbType.VarChar;
            parameter_18.value = targetWc;

            List<DB_Parameter> parameterNewLine = new List<DB_Parameter>() { parameter_1, parameter_2, parameter_3, parameter_4, parameter_5, parameter_6,
                                                                                      parameter_7, parameter_8, parameter_9, parameter_10, parameter_11, parameter_12,
                                                                                      parameter_13, parameter_14, parameter_15, parameter_16, parameter_17, parameter_18};
            if (!transType[0].ItemArray[0].ToString().Trim().Equals(""))
            {
                DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_type, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                    + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                    + "values ('AMT', @job, @jobsuffix, @trans_type, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                    + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameterNewLine);

                return true;
            }
            else
            {
                DataTable InsertJobTrans_MST = Global.getDataFrom_sl9DB("insert into " + Global.jobtrans_mst + " (site_ref, job, suffix, trans_date, qty_complete, qty_scrapped, oper_num, a_hrs, next_oper, emp_num, start_time,end_time"
                    + ",pay_rate, qty_moved, whse, user_code, close_job, issue_parent, complete_op, pr_rate, job_rate, shift, posted, low_level, backflush, reason_code, trans_class, wc, awaiting_eop, CreateDate, RowPointer, CreatedBy, UpdatedBy, RecordDate, InWorkflow ) "
                    + "values ('AMT', @job, @jobsuffix, @jobDate, @complete, @scrap, @operation,@totalHours, @nextOperation,'PROD',@start_time, @end_time,'R'"
                    + ", @complete, 'MAIN', 'ESA', 0, 0, @operComplete,0,@job_rate,1,@posted,0,0,@reason,'J',@_wc,0,getdate(),'" + Guid.NewGuid().ToString() + "', 'AMT Web','AMT Web', getdate(), 0 )", parameterNewLine);
                return true;
            }
        }

        //private double getReworkState( string job, int oper_num )
        //{
        //    DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
        //    mst_job.name = "@job";
        //    mst_job.type_data = SqlDbType.VarChar;
        //    mst_job.value = job;

        //    mst_oper_num.name = "@oper_num";
        //    mst_oper_num.type_data = SqlDbType.Int;
        //    mst_oper_num.value = oper_num;
        //    List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

        //    DataTable getReworkLine = Global.getDataFrom_eWipDB("", parameterSpecificLine);
        //}

        private double getLinePreviousSumCompleteQty(string job, int oper_num)
        {
            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
            mst_job.name = "@job";
            mst_job.type_data = SqlDbType.VarChar;
            mst_job.value = job;

            mst_oper_num.name = "@oper_num";
            mst_oper_num.type_data = SqlDbType.Int;
            mst_oper_num.value = oper_num;
            List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

            DataTable checkLine = Global.getDataFrom_sl9DB("select sum(qty_complete) from " + Global.jobtrans_mst + " where job = @job and oper_num = (select top 1 oper_num from " + Global.jobtrans_mst + " where job = @job and oper_num < @oper_num order by oper_num desc)  group by job, oper_num", parameterSpecificLine);
            DataRow[] isLineThere = checkLine.Select();

            return double.Parse(isLineThere[0].ItemArray[0].ToString());
        }

        private double getLineSumCompleteQtyOnSL9(string job, int oper_num)
        {
            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
            mst_job.name = "@job";
            mst_job.type_data = SqlDbType.VarChar;
            mst_job.value = job;

            mst_oper_num.name = "@oper_num";
            mst_oper_num.type_data = SqlDbType.Int;
            mst_oper_num.value = oper_num;
            List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

            DataTable checkLine = Global.getDataFrom_sl9DB("select sum(qty_complete) from " + Global.jobtrans_mst + " where job = @job and oper_num = @oper_num group by job, oper_num", parameterSpecificLine);
            DataRow[] isLineThere = checkLine.Select();

            return double.Parse(isLineThere[0].ItemArray[0].ToString());
        }

        private double getLinePreviousSumScrapQty(string job, int oper_num)
        {
            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
            mst_job.name = "@job";
            mst_job.type_data = SqlDbType.VarChar;
            mst_job.value = job;

            mst_oper_num.name = "@oper_num";
            mst_oper_num.type_data = SqlDbType.Int;
            mst_oper_num.value = oper_num;
            List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

            DataTable checkLine = Global.getDataFrom_sl9DB("select sum(qty_scrapped) from " + Global.jobtrans_mst + " where job = @job and oper_num = (select top 1 oper_num from " + Global.jobtrans_mst + " where job = @job and oper_num < @oper_num order by oper_num desc )  group by job, oper_num", parameterSpecificLine);
            DataRow[] isLineThere = checkLine.Select();

            return double.Parse(isLineThere[0].ItemArray[0].ToString());
        }

        private double getLineSumScrapQtyOnSL9(string job, int oper_num)
        {
            try
            {
                DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter();
                mst_job.name = "@job";
                mst_job.type_data = SqlDbType.VarChar;
                mst_job.value = job;

                mst_oper_num.name = "@oper_num";
                mst_oper_num.type_data = SqlDbType.Int;
                mst_oper_num.value = oper_num;
                List<DB_Parameter> parameterSpecificLine = new List<DB_Parameter>() { mst_job, mst_oper_num };

                DataTable checkLine = Global.getDataFrom_sl9DB("select sum(qty_scrapped) from " + Global.jobtrans_mst + " where job = @job and oper_num = @oper_num group by job, oper_num", parameterSpecificLine);
                DataRow[] isLineThere = checkLine.Select();

                return double.Parse(isLineThere[0].ItemArray[0].ToString());
            }
            catch (Exception)
            {
                return 0;
            }
        }

        [HttpPost]
        public ActionResult FreezePost(int freeze_post)
        {
            var db = Global.db;
            var configSL = db.Tmp_SL9TableNames.FirstOrDefault(item => item.id == "1");
            try
            {
                configSL.freeze_post = freeze_post;

                db.SubmitChanges();
                Global.setUpSLTable();
                return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckPassword(string password, string freeze_post)
        {
            var db = Global.ewipDB;
            var curUser = mpMembership.GetCurrentUser();
            if (curUser._password == password)
            {
                var ultdb = Global.db;
                var freeze = ultdb.Tmp_SL9TableNames.FirstOrDefault(item => item.id == "1");
                if (freeze.freeze_post == 1)
                {
                    freeze.freeze_post = 0;
                }
                else
                {
                    freeze.freeze_post = 1;
                }
                ultdb.SubmitChanges();
                return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public int getDataFreeze()
        {
            var db = Global.db;
            var configSL = (from x in db.Tmp_SL9TableNames
                            select x.freeze_post).FirstOrDefault();
            return configSL;
        }
    }
}
