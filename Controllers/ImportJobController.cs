﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class ImportJobController : Controller
    {
        //
        // GET: /ImportJob/

        internal int lblTot = 0;
        internal static List<String> unsufficientJob = new List<String>();
        internal int TotalJob = 0;
        internal string successString = "";
        internal static Dictionary<String, double> mtlOnHandStatic = new Dictionary<String, double>();
        internal static Dictionary<String, double> mtlStartStatic = new Dictionary<String, double>();

        public ActionResult prosesImport(ImportJobModel model)
        {
            try
            {
                successString = "";
                var result = GetWorkOrderdata(model.From, model.To, model.Type, model.JobNo);
                UpdateDownloadLog(model.From, model.To);
                successString = TotalJob.ToString() + " Data download finished!";
                if (unsufficientJob.Count() > 0)
                {
                    var resultPrint = PrintInsufficientMaterial();
                    var arrayJson = resultPrint.Split(';');
                    return Json(new { success = arrayJson[0], excel = arrayJson[1], redirect = arrayJson[2] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    successString += " No Insufficient Job detected!";
                }

                string sql = null;
                if (result)
                {
                    return Json(new { success = successString }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private SqlConnection sqlCnn = Global.sl9Cnn,
                              ewipCnn = Global.eWIPCnn;
        SqlCommand com = new SqlCommand();

        bool GetWorkOrderdata(string txtFrom, string txtTo, string cmbType, string txtJobNo)
        {
            // Dim DSType As New DataSet
            DataSet DSJob = new DataSet();
            DataSet DSRoute = new DataSet();
            DataSet DSJobAC = new DataSet();
            DataSet DSRouteAC = new DataSet();
            DataSet DSTransAC = new DataSet();
            DataSet DSWS = new DataSet();
            DataSet DSJRT = new DataSet();
            DataSet DSwc = new DataSet();
            // By Yeo 20081110
            DataSet DSItem = new DataSet();
            DataSet DSMaterial = new DataSet();
            int tot = 0;
            TotalJob = 0;

            DateTime SD = new DateTime();
            DateTime ED = new DateTime();
            Global.setUpSLTable();
            if (!String.IsNullOrEmpty(txtFrom) && !String.IsNullOrEmpty(txtTo))
            {
                string[] s = txtFrom.Split('/');
                SD = new DateTime(int.Parse(s[2]), int.Parse(s[1]), int.Parse(s[0]));
                string[] s1 = txtTo.Split('/');
                ED = new DateTime(int.Parse(s1[2]), int.Parse(s1[1]), int.Parse(s1[0]));
            }

            int at = 0;
            //PB1.Value = 0;
            //PB1.Minimum = 0;
            //PB1.Maximum = tot;
            int ct = 0;
            if (cmbType != "Work Center" && cmbType != "Item")
            {

                SqlCommand com = new SqlCommand();

                string sql = null;

                try
                {

                    // With...
                    var DA = new SqlDataAdapter();
                    sqlCnn.Open();

                    if ((cmbType == "Job-Between Date"))
                    {
                        sql = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM " + Global.job_mst + " mst where stat in(\'R\',\'S\') and job_date between @Sd and DATEADD(day,1,@Ed) ";

                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@Sd", SqlDbType.DateTime);
                        com.Parameters["@Sd"].Value = SD;

                        com.Parameters.Add("@Ed", SqlDbType.DateTime);
                        com.Parameters["@Ed"].Value = ED;


                    }
                    else
                    {
                        sql = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM " + Global.job_mst + " mst where stat in(\'R\',\'S\')and job=@job ";
                        
                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@Job", SqlDbType.NVarChar, 20);
                        com.Parameters["@Job"].Value = txtJobNo;

                    }

                    DA.SelectCommand = com;
                    DA.Fill(DSJob, "tbsch");

                    
                    tot = (tot + DSJob.Tables[0].Rows.Count);

                    // With...
                    // job,suffix,opernum,wc
                    if ((cmbType == "Job-Between Date"))
                    {
                        sql = "select j.job as job, j.suffix as suffix, j.oper_num as opernum, j.wc as wc, j.RecordDate as start_Date, j.qty_received as qty_op_qty, j.qty_complete as qty_complete, j.qty_scrapped as qty_scrapped, j.qty_moved as qty_Rele_qty "
                                   + "from " + Global.jobroute_mst + " j inner join " + Global.job_mst + " mst on j.job=mst.job and j.suffix=mst.suffix where mst.stat in(\'R\',\'S\') and mst.job_date between @Sd and DATEADD(day,1,@Ed) ";

                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@Sd", SqlDbType.DateTime);
                        com.Parameters["@Sd"].Value = SD;

                        com.Parameters.Add("@Ed", SqlDbType.DateTime);
                        com.Parameters["@Ed"].Value = ED;

                    }
                    else
                    {
                        sql = "select j.job as job, j.suffix as suffix, j.oper_num as opernum, j.wc as wc, j.RecordDate as start_Date, j.qty_received as qty_op_qty, j.qty_complete as qty_complete, j.qty_scrapped as qty_scrapped, j.qty_moved as qty_Rele_qty from " + Global.jobroute_mst + " j "
                                    + "inner join " + Global.job_mst + " mst on j.job=mst.job and j.suffix=mst.suffix where mst.stat in(\'R\',\'S\') and  mst.job=@job ";

                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@job", SqlDbType.NVarChar, 20);
                        com.Parameters["@job"].Value = txtJobNo;
                    }

                    DA.SelectCommand = com;
                    DA.Fill(DSRoute, "tbsch");

                    // JRT SCH Table
                    // With...

                    // job,suffix,opernum,wc
                    if ((cmbType == "Job-Between Date"))
                    {
                        sql = "select j.job,j.suffix,j.oper_num,j.run_lbr_hrs,j.run_mch_hrs from " + Global.jrt_sch_mst + " j left join dbo." + Global.job_mst + " mst on mst.job = j.job where j.job in(select job from " + Global.job_mst + " where stat in(\'R\',\'S\') and job_date between @Sd and DATEADD(day,1,@Ed)) ";

                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@Sd", SqlDbType.DateTime);
                        com.Parameters["@Sd"].Value = SD;

                        com.Parameters.Add("@Ed", SqlDbType.DateTime);
                        com.Parameters["@Ed"].Value = ED;
                    }
                    else
                    {
                        sql = "select j.job,j.suffix,j.oper_num,j.run_lbr_hrs,j.run_mch_hrs from " + Global.jrt_sch_mst + " j left join dbo." + Global.job_mst + " mst on mst.job = j.job where j.job in(select job from " + Global.job_mst + " where stat in(\'R\',\'S\') and job=@job) ";

                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@job", SqlDbType.NVarChar, 20);
                        com.Parameters["@job"].Value = txtJobNo;
                    }


                    DA.SelectCommand = com;
                    DA.Fill(DSJRT, "tbJRT");

                    // With...
                    if ((cmbType == "Job-Between Date"))
                    {
                        sql = "select j.Job,j.item from " + Global.jobmatl_mst + " j left join dbo." + Global.job_mst + " mst on mst.job = j.job where j.Job in (select job from " + Global.job_mst + " where stat in(\'R\',\'S\') and Job_date between @Sd and DATEADD(day,1,@Ed)) ";

                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@Sd", SqlDbType.DateTime);
                        com.Parameters["@Sd"].Value = SD;

                        com.Parameters.Add("@Ed", SqlDbType.DateTime);
                        com.Parameters["@Ed"].Value = ED;
                    }
                    else
                    {
                        sql = "select j.Job,j.item from " + Global.jobmatl_mst + " j left join dbo." + Global.job_mst + " mst on mst.job = j.job where j.Job in (select job from " + Global.job_mst + " where stat in(\'R\',\'S\') and  job=@job) ";
 
                        com = new SqlCommand(sql, sqlCnn);
                        com.Parameters.Add("@job", SqlDbType.NVarChar, 20);
                        com.Parameters["@job"].Value = txtJobNo;
                    }

                    DA.SelectCommand = com;
                    DA.Fill(DSMaterial, "tbmat");

                    tot = tot + DSRoute.Tables[0].Rows.Count;
                    lblTot = tot;
                    //PB1.Maximum = tot;
                    //PB1.Minimum = 0;
                    //Application.DoEvents();
                    // ********************************************************************
                }
                catch (Exception ex)
                {
                    //MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP");
                    //lblLoading.Visible = false;
                    return false;
                }

                try
                {


                    // With...
                    var DA = new SqlDataAdapter();

                    if ((cmbType == "Job-Between Date"))
                    {

                        sql = " SELECT * FROM tbJob where _jobDate between @Sd and DATEADD(day,1,@Ed)";

                        com = new SqlCommand(sql, ewipCnn);
                        com.Parameters.Add("@Sd", SqlDbType.Float);
                        com.Parameters["@Sd"].Value = SD.ToOADate();

                        com.Parameters.Add("@Ed", SqlDbType.Float);
                        com.Parameters["@Ed"].Value = ED.ToOADate();

                    }
                    else
                    {
                        sql = " SELECT * FROM tbJob  where _job=@job";

                        com = new SqlCommand(sql, ewipCnn);
                        com.Parameters.Add("@job", SqlDbType.NVarChar, 20);
                        com.Parameters["@job"].Value = txtJobNo;
                    }

                    DA.SelectCommand = com;
                    DA.Fill(DSJobAC);
                }
                catch (Exception ex)
                {
                    //MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP");
                    //lblLoading.Visible = false;
                    return false;
                }




                try
                {

                    // With...
                    var DA = new SqlDataAdapter();

                    if ((cmbType == "Job-Between Date"))
                    {
                        sql = "select tbJobRoute.* from tbJobRoute  inner join tbJob on tbJobRoute._job=tbjob._job and tbJobRoute._jobSuffix=tbjob._jobSuffix where tbjob._jobDate between @Sd  and DATEADD(day,1,@Ed)";

                        com = new SqlCommand(sql, ewipCnn);
                        com.Parameters.Add("@Sd", SqlDbType.Float);
                        com.Parameters["@Sd"].Value = SD.ToOADate();

                        com.Parameters.Add("@Ed", SqlDbType.Float);
                        com.Parameters["@Ed"].Value = ED.ToOADate();
                    }
                    else
                    {
                        sql = "select tbJobRoute.* from tbJobRoute  inner join tbJob on tbJobRoute._job=tbjob._job and tbJobRoute._jobSuffix=tbjob._jobSuffix where tbjob._job=@job";

                        com = new SqlCommand(sql, ewipCnn);
                        com.Parameters.Add("@job", SqlDbType.NVarChar, 20);
                        com.Parameters["@job"].Value = txtJobNo;
                    }

                    DA.SelectCommand = com;
                    DA.Fill(DSRouteAC, "tbsch");
                }
                catch (Exception ex)
                {
                    //MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP");
                    //lblLoading.Visible = false;
                    return false;
                }
                finally
                {
                    sqlCnn.Close();
                    com.Parameters.Clear();
                }

                // Add codes at 26 Feb 2018
                // Delete Duplicate rows
                //DataSet DSTransACDuplicate = new DataSet();
                //try
                //{
                //    // With...
                //    var DA = new SqlDataAdapter();

                //    sql = "select _job, _jobsuffix, _jobsuffixParent, _item, _oper_num, _wc, count(*) as duplicate_count from tbjobtrans group by _job, _jobsuffix, _jobsuffixParent, _item, _oper_num, _wc having count(*) > 1";

                //    com = new SqlCommand(sql, ewipCnn);

                //    DA.SelectCommand = com;
                //    DA.Fill(DSTransACDuplicate, "tbsch");
                //}
                //catch (Exception ex)
                //{
                //    return false;
                //}
                //finally
                //{
                //    ewipCnn.Close();
                //    com.Parameters.Clear();
                //}

                //DataRow[] Duplicate = DSTransACDuplicate.Tables[0].Select();
                //if ((Duplicate.Length > 0))
                //{
                //    for (int m = 0; (m <= Duplicate.Length - 1); m++)
                //    {
                //        var item = Duplicate[m];
                //        // With...
                //        try
                //        {


                //            sql = "with T as (select top " + (int.Parse(item["duplicate_count"].ToString()) - 1).ToString() + " * from tbJobTrans where _job=@job and _jobsuffix=@jobsuffix and _jobsuffixParent=@jobsuffixParent and _item=@item and _oper_num=@oper_num and _wc=@wc order by _tansnum desc) delete from T";

                //            ewipCnn.Open();
                //            com = new SqlCommand(sql, ewipCnn);
                //            com.Parameters.Add("@job", SqlDbType.NVarChar, 20);
                //            com.Parameters["@job"].Value = item["_job"];

                //            com.Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10);
                //            com.Parameters["@jobsuffix"].Value = item["_jobsuffix"];

                //            com.Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10);
                //            com.Parameters["@jobsuffixParent"].Value = item["_jobsuffixParent"];

                //            com.Parameters.Add("@item", SqlDbType.VarChar, 50);
                //            com.Parameters["@item"].Value = item["_item"];

                //            com.Parameters.Add("@oper_num", SqlDbType.Int);
                //            com.Parameters["@oper_num"].Value = item["_oper_num"];

                //            com.Parameters.Add("@wc", SqlDbType.VarChar, 50);
                //            com.Parameters["@wc"].Value = item["_wc"];

                //            com.ExecuteNonQuery();
                //        }
                //        catch (Exception ex)
                //        {
                //        }
                //        finally
                //        {
                //            com.Parameters.Clear();
                //            ewipCnn.Close();
                //        }

                //    }

                //}

                //DataSet DSTransACDuplicateToo = new DataSet();
                //try
                //{
                //    // With...
                //    var DA = new SqlDataAdapter();

                //    ////ewipCnn.Open();

                //    sql = "declare @row int declare @job varchar(50) declare @jobsuffix varchar(50) declare @jobsuffixParent varchar(50) declare @item varchar(50) declare @oper_num int declare @wc varchar(50) declare @table_duplicate table(_job varchar(50), _jobsuffix varchar(50), _jobsuffixParent varchar(50), _item varchar(50), _oper_num int, _wc varchar(50)) (select row_number() over (order by _job, _oper_num desc) as _row, _job, _jobsuffixParent, _item, _oper_num, _wc into table_sampah from tbJobtrans group by _job, _jobsuffixParent, _item, _oper_num, _wc having count(*) > 1) while exists (select * from table_sampah) begin select top 1 @row=_row, @job=_job, @jobsuffixParent=_jobsuffixParent, @item=_item, @oper_num=_oper_num, @wc=_wc from table_sampah if (select sum(_qty_op_qty) from tbjobtrans where _job=@job and _jobsuffixParent=@jobsuffixParent and _item=@item and _oper_num=@oper_num and _wc=@wc) > (select sum(_qty_Complete) from tbjobtrans where _job=@job and _jobsuffixParent=@jobsuffixParent and _item=@item and _oper_num=(select max(_operationNo) from tbjobroute where _job=@job and _operationNo<@oper_num)) begin insert into @table_duplicate(_job, _jobsuffix, _jobsuffixParent, _item, _oper_num, _wc)values(@job, @jobsuffixParent, @jobsuffixParent, @item, @oper_num, @wc) end delete table_sampah where _row=@row end drop table table_sampah select * from @table_duplicate";

                //    com = new SqlCommand(sql, ewipCnn);

                //    DA.SelectCommand = com;
                //    DA.Fill(DSTransACDuplicateToo, "tbsch");
                //}
                //catch (Exception ex)
                //{
                //    return false;
                //}
                //finally
                //{
                //    ewipCnn.Close();
                //    com.Parameters.Clear();
                //}

                //DataRow[] DuplicateToo = DSTransACDuplicateToo.Tables[0].Select();
                //if ((DuplicateToo.Length > 0))
                //{
                //    //string tes = DuplicateToo[0]["_job"].ToString();
                //    for (int m = 0; (m <= (DuplicateToo.Length - 1)); m++)
                //    {
                //        var item = DuplicateToo[m];
                //        // With...
                //        try
                //        {

                //            //ewipCnn.Open();

                //            sql = "with T as (select * from tbJobTrans where _job=@job and _jobsuffix=@jobsuffix and _jobsuffixParent=@jobsuffixParent " +
                //            "and _item=@item and _oper_num=@oper_num and _wc=@wc) delete from T";

                //            com = new SqlCommand(sql, ewipCnn);

                //            com.Parameters.Add("@job", SqlDbType.VarChar, 50);
                //            com.Parameters["@job"].Value = item["_job"];

                //            com.Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10);
                //            com.Parameters["@jobsuffix"].Value = item["_jobsuffix"];

                //            com.Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10);
                //            com.Parameters["@jobsuffixParent"].Value = item["_jobsuffixParent"];

                //            com.Parameters.Add("@item", SqlDbType.VarChar, 50);
                //            com.Parameters["@item"].Value = item["_item"];

                //            com.Parameters.Add("@oper_num", SqlDbType.VarChar, 50);
                //            com.Parameters["@item"].Value = item["_item"];

                //            com.Parameters.Add("@item", SqlDbType.Int);
                //            com.Parameters["@item"].Value = item["_oper_num"];

                //            com.Parameters.Add("@wc", SqlDbType.VarChar, 50);
                //            com.Parameters["@wc"].Value = item["_wc"];

                //            com.ExecuteNonQuery();
                //        }
                //        catch (Exception ex)
                //        {
                //        }
                //        finally
                //        {
                //            com.Parameters.Clear();
                //            ewipCnn.Close();
                //        }

                //    }

                //}

                // End Add codes at 26 Feb 2018
                // Add codes at 22 Feb 2018
                try
                {
                    // With...
                    var DA = new SqlDataAdapter();

                    //ewipCnn.Open();

                    if ((cmbType == "Job-Between Date"))
                    {

                        sql = "select tbJobTrans.* from tbJobTrans  inner join tbJob on tbJobTrans._job=tbJob._job and tbJobTrans._i" +
                              "tem=tbJob._item where tbJob._jobDate between @Sd  and DATEADD(day,1,@Ed)";
                        com = new SqlCommand(sql, ewipCnn);
                        com.Parameters.Add("@Sd", SqlDbType.Float);
                        com.Parameters["@Sd"].Value = SD.ToOADate();

                        com.Parameters.Add("@Ed", SqlDbType.Float);
                        com.Parameters["@Ed"].Value = ED.ToOADate();
                    }
                    else
                    {

                        sql = "select tbJobTrans.* from tbJobTrans  inner join tbJob on tbJobTrans._job=tbJob._job and tbJobTrans._i" +
                        "tem=tbJob._item where tbJob._job=@job";
                        com = new SqlCommand(sql, ewipCnn);
                        com.Parameters.Add("@Job", SqlDbType.NVarChar, 20);
                        com.Parameters["@Job"].Value = txtJobNo.Trim();
                    }


                    DA.SelectCommand = com;
                    DA.Fill(DSTransAC, "tbsch");
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    ewipCnn.Close();
                    com.Parameters.Clear();
                }

                DataSet DSQtyOnHand = new DataSet();
                var DAITEM = new SqlDataAdapter();
                Dictionary<String, double> mtlOnHand = new Dictionary<String, double>();
                // End Add codes at 22 Feb 2018
                if ((cmbType == "Job-Between Date"))
                {
                    var AllItem = "SELECT DISTINCT  jm.item, loc.qty_on_hand "
                                + "FROM dbo." + Global.job_mst + " mst LEFT JOIN dbo.item_mst it on it.item = mst.item "
                                + "LEFT JOIN dbo." + Global.jobmatl_mst + " jm on jm.job = it.job  "
                                + "LEFT JOIN dbo." + Global.itemloc_mst + " loc on loc.item = jm.item "
                                + "where mst.stat in('R','S') and jm.matl_type='M' and jm.suffix = '1' and loc.loc = 'TL' and mst.job_date between convert(DATE,@Sd) and convert(DATE, DATEADD(day,1,@Ed)) ";
                    com = new SqlCommand(AllItem, sqlCnn);
                    com.Parameters.Add("@Sd", SqlDbType.DateTime);
                    com.Parameters["@Sd"].Value = SD;

                    com.Parameters.Add("@Ed", SqlDbType.DateTime);
                    com.Parameters["@Ed"].Value = ED;
                }
                else
                {
                    var AllItem = "SELECT DISTINCT  jm.item, loc.qty_on_hand "
                               + "FROM dbo." + Global.job_mst + " mst LEFT JOIN dbo.item_mst it on it.item = mst.item "
                               + "LEFT JOIN dbo." + Global.jobmatl_mst + " jm on jm.job = it.job "
                               + "LEFT JOIN dbo." + Global.itemloc_mst + " loc on loc.item = jm.item "
                               + "where mst.stat in('R','S') and jm.matl_type='M' and jm.suffix = '1' and loc.loc = 'TL' and mst.job = @Job ";
                    com = new SqlCommand(AllItem, sqlCnn);
                    com.Parameters.Add("@Job", SqlDbType.NVarChar, 20);
                    com.Parameters["@Job"].Value = txtJobNo.Trim();
                }
                try
                {
                    DAITEM.SelectCommand = com;
                    DAITEM.Fill(DSQtyOnHand, "dtItem");
                    foreach (DataRow item in DSQtyOnHand.Tables[0].Rows)
                    {
                        mtlOnHand[item[0].ToString()] = double.Parse(item[1].ToString());
                        mtlStartStatic[item[0].ToString()] = double.Parse(item[1].ToString());
                    }

                    if ((DSJob.Tables[0].Rows.Count > 0))
                    {
                        int ji;
                        for (ji = 0; (ji
                                    <= (DSJob.Tables[0].Rows.Count - 1)); ji++)
                        {
                            var flagSufficient = true;
                            var flagAdded = false;
                            var flagSubtract = false;
                            var currentJob = DSJob.Tables[0].Rows[ji]["job"].ToString().Trim();
                            var dicMaterialUsed = new Dictionary<String, Double>();
                            var StartMaterialQty = new Dictionary<String, Double>();

                            //CHECK IF JOB IS ALREADY IN THE DATABASE SO NO FURTHER MATERIAL CHECKING IS NEEDED 
                            DataSet DSCheckJob = new DataSet();
                            var DACheckJob = new SqlDataAdapter();
                            var getJobSQL = "select * from dbo.tbJob where _job = @Job";
                            com = new SqlCommand(getJobSQL, ewipCnn);
                            com.Parameters.Add("@Job", SqlDbType.NVarChar, 20);
                            com.Parameters["@Job"].Value = currentJob.Trim();
                            try
                            {
                                ewipCnn.Open();
                                DACheckJob.SelectCommand = com;
                                DACheckJob.Fill(DSCheckJob, "dtMaterial");
                            }
                            catch (Exception ex)
                            {
                                return false;
                            }
                            finally
                            {
                                ewipCnn.Close();
                                com.Parameters.Clear();
                            }

                            if (DSCheckJob.Tables[0].Rows.Count == 0)
                            {
                                DataSet DSMaterialNeeded = new DataSet();
                                var DAMaterial = new SqlDataAdapter();
                                var SQL = "select distinct rs.item as BOM, CONVERT(float,rs.matl_qty) * CONVERT(float,item_qty.qty_released) as totalNeeded "
                                                    + "from (SELECT DISTINCT  mst.job, it.item as jobItem,jm.item , jm.matl_qty "
                                                    + "FROM dbo.job_mst mst LEFT JOIN dbo.item_mst it on it.item = mst.item "
                                                    + "LEFT JOIN dbo.jobmatl_mst jm on jm.job = it.job "
                                                    + "where mst.stat in('R','S') and jm.matl_type='M') rs "
                                                    + "left join dbo.job_mst item_Qty on item_qty.job = rs.job where rs.job = @Job";
                                com = new SqlCommand(SQL, sqlCnn);
                                com.Parameters.Add("@Job", SqlDbType.NVarChar, 20);
                                com.Parameters["@Job"].Value = currentJob.Trim();
                                try
                                {
                                    sqlCnn.Open();
                                    DAMaterial.SelectCommand = com;
                                    DAMaterial.Fill(DSMaterialNeeded, "dtMaterial");
                                }
                                catch (Exception ex)
                                {
                                    return false;
                                }
                                finally
                                {
                                    sqlCnn.Close();
                                    com.Parameters.Clear();
                                }

                                foreach (DataRow item in DSMaterialNeeded.Tables[0].Rows)
                                {
                                    try
                                    {
                                        StartMaterialQty[item[0].ToString()] = mtlOnHand[item[0].ToString()];
                                        mtlOnHand[item[0].ToString()] -= Double.Parse(item[1].ToString());
                                        if (mtlOnHand[item[0].ToString()] < 0)
                                        {
                                            flagSufficient = false;
                                            mtlOnHand[item[0].ToString()] = StartMaterialQty[item[0].ToString()];
                                            unsufficientJob.Add(currentJob);
                                            if (flagAdded && !flagSubtract)
                                            {
                                                TotalJob--;
                                                flagSubtract = true;
                                            }
                                        }
                                        else
                                        {
                                            if (!flagAdded && flagSufficient)
                                            {
                                                TotalJob++;
                                            }
                                            flagAdded = true;
                                        }
                                    }
                                    catch (KeyNotFoundException ex)
                                    {
                                        flagSufficient = false;
                                        unsufficientJob.Add(currentJob);
                                        if (flagAdded && !flagSubtract && !flagSufficient)
                                        {
                                            TotalJob--;
                                            flagSubtract = true;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        return false;
                                    }
                                }
                            }

                            if (flagSufficient)
                            {
                                DataRow[] DRMat = DSMaterial.Tables[0].Select(("Job=\'"
                                                + (DSJob.Tables[0].Rows[ji]["job"].ToString().Trim() + "\'")));
                                string strmat = "";
                                if ((DRMat.Length > 0))
                                {
                                    Hashtable HAsMat = new Hashtable();

                                    for (int j2 = 0; (j2
                                                <= (DRMat.Length - 1)); j2++)
                                    {
                                        if ((strmat == ""))
                                        {
                                            strmat = DRMat[j2]["item"].ToString();
                                        }
                                        else if ((HAsMat.Contains(DRMat[j2]["item"].ToString()) == false))
                                        {
                                            strmat = (strmat + ("||" + DRMat[j2]["item"]));
                                            HAsMat.Add(DRMat[j2]["item"], DRMat[j2]["item"]);
                                        }

                                    }

                                }

                                bool Stup = false;
                                // With...
                                try
                                {
                                    ewipCnn.Open();

                                    sql = "sp_tbJob_Download_auto";

                                    com = new SqlCommand(sql, ewipCnn);
                                    com.CommandType = CommandType.StoredProcedure;
                                    com.Parameters.Add("@job_" + 1, SqlDbType.VarChar, 50);
                                    com.Parameters["@job_" + 1].Value = DSJob.Tables[0].Rows[ji]["job"].ToString().Trim();


                                    com.Parameters.Add("@jobsuffix_" + 1, SqlDbType.Int);
                                    com.Parameters["@jobsuffix_" + 1].Value = DSJob.Tables[0].Rows[ji]["suffix"];

                                    com.Parameters.Add("@jobDate_" + 1, SqlDbType.Float);
                                    com.Parameters["@jobDate_" + 1].Value = ((DateTime)DSJob.Tables[0].Rows[ji]["job_date"]).ToOADate();

                                    com.Parameters.Add("@jobtype_" + 1, SqlDbType.VarChar, 50);
                                    com.Parameters["@jobtype_" + 1].Value = DSJob.Tables[0].Rows[ji]["type"].ToString().Trim();

                                    com.Parameters.Add("@item_" + 1, SqlDbType.VarChar, 50);
                                    com.Parameters["@item_" + 1].Value = DSJob.Tables[0].Rows[ji]["item"].ToString().Trim();

                                    com.Parameters.Add("@qtyReleased_" + 1, SqlDbType.Float);
                                    com.Parameters["@qtyReleased_" + 1].Value = DSJob.Tables[0].Rows[ji]["qty_released"];

                                    com.Parameters.Add("@qtyCompleted_" + 1, SqlDbType.Float);
                                    com.Parameters["@qtyCompleted_" + 1].Value = DSJob.Tables[0].Rows[ji]["qty_complete"];

                                    com.Parameters.Add("@qtyScrapped_" + 1, SqlDbType.Float);
                                    com.Parameters["@qtyScrapped_" + 1].Value = DSJob.Tables[0].Rows[ji]["qty_scrapped"];

                                    com.Parameters.Add("@stas_" + 1, SqlDbType.Char, 1);
                                    com.Parameters["@stas_" + 1].Value = DSJob.Tables[0].Rows[ji]["stat"].ToString().Trim();

                                    com.Parameters.Add("@material_" + 1, SqlDbType.VarChar, 1000);
                                    com.Parameters["@material_" + 1].Value = strmat;



                                    DataRow[] NR = DSJobAC.Tables[0].Select(("_job=\'"
                                                    + (DSJob.Tables[0].Rows[ji]["job"].ToString().Trim() + ("\' and _jobsuffix=" + DSJob.Tables[0].Rows[ji]["suffix"]))));

                                    com.Parameters.Add("@status_" + 1, SqlDbType.VarChar, 50);


                                    if ((NR.Length > 0))
                                    {
                                        // If NR(0).Item("_qtyReleased") = DSJob.Tables(0).Rows(i).Item("qty_released") And NR(0).Item("_material") = strmat Then

                                        if ((NR[0]["_qtyReleased"] == DSJob.Tables[0].Rows[ji]["qty_released"]))
                                        {
                                            com.Parameters["@status_" + 1].Value = "No";
                                        }
                                        else
                                        {
                                            com.Parameters["@status_" + 1].Value = "Edit";
                                        }

                                    }
                                    else
                                    {
                                        com.Parameters["@status_" + 1].Value = "New";
                                    }

                                    var result = com.ExecuteNonQuery();
                                    ewipCnn.Close();
                                    com.Parameters.Clear();
                                    Stup = true;
                                }
                                catch (Exception ex)
                                {
                                    Stup = false;
                                    //System.Diagnostics.EventLogEntryType errtype;
                                    // s = EventLogEntryType.Error
                                    //errtype = EventLogEntryType.Error;
                                    // MsgBox(("Download error-" & DSJob.Tables(0).Rows(i).Item("job") & ": " & ex.Message), MsgBoxStyle.Critical, "eWIP")
                                }
                                finally
                                {
                                    ewipCnn.Close();
                                    com.Parameters.Clear();
                                }

                                string strAllJobroute = "";
                                if ((Stup == true))
                                {
                                    DataRow[] NEWRoute = DSRoute.Tables[0].Select(("job=\'"
                                                    + (DSJob.Tables[0].Rows[ji]["job"].ToString().Trim() + "\'")));
                                    if ((NEWRoute.Length > 0))
                                    {
                                        // Add codes at 26 Feb 2018

                                        for (int n = 0; (n<= (NEWRoute.Length - 1)); n++)
                                        {

                                            var specificCntrlPoint = GetCntrlPointSpecificJobAndLine(currentJob.Trim(), NEWRoute, n);

                                            if (int.Parse(specificCntrlPoint.Tables[0].Rows[0][0].ToString()) == 0
                                                && float.Parse(NEWRoute[n]["qty_complete"].ToString()) == 0)
                                            {
                                                NEWRoute[n]["qty_complete"] = NEWRoute[n]["qty_op_qty"];
                                                NEWRoute[n]["qty_Rele_qty"] = NEWRoute[n]["qty_op_qty"];
                                                NEWRoute[(n + 1)]["qty_Rele_qty"] = NEWRoute[n]["qty_op_qty"];
                                                NEWRoute[(n + 1)]["qty_op_qty"] = NEWRoute[n]["qty_op_qty"];

                                                var nextCntrlPoint = GetCntrlPointSpecificJobAndLine(currentJob.Trim(), NEWRoute, n + 1);
                                                if (int.Parse(nextCntrlPoint.Tables[0].Rows[0][0].ToString()) == 1)
                                                {
                                                    break;
                                                }
                                            }
                                        }

                                        // End Add codes at 26 Feb 2018

                                        for (int j = 0; (j
                                                    <= (NEWRoute.Length - 1)); j++)
                                        {
                                            // With...
                                            try
                                            {

                                                ewipCnn.Open();

                                                sql = "sp_tbjobRoute_Download";

                                                com = new SqlCommand(sql, ewipCnn);
                                                com.CommandType = CommandType.StoredProcedure;


                                                for (int J1 = 0; (J1 <= 14); J1++)
                                                {
                                                    if ((j < NEWRoute.Length))
                                                    {
                                                        com.Parameters.Add("@job_" + (J1 + 1), SqlDbType.VarChar, 50);
                                                        com.Parameters["@job_" + (J1 + 1)].Value = NEWRoute[j]["job"].ToString().Trim();

                                                        com.Parameters.Add("@jobsuffix_" + (J1 + 1), SqlDbType.Int);
                                                        com.Parameters["@jobsuffix_" + (J1 + 1)].Value = NEWRoute[j]["suffix"];

                                                        com.Parameters.Add("@operationNo_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@operationNo_" + (J1 + 1)].Value = NEWRoute[j]["opernum"];

                                                        com.Parameters.Add("@wc_" + (J1 + 1), SqlDbType.VarChar, 50);
                                                        com.Parameters["@wc_" + (J1 + 1)].Value = NEWRoute[j]["wc"];

                                                        // Get Labour or Machine
                                                        DataRow[] RWLBH = DSJRT.Tables[0].Select(("job=\'"
                                                                        + (NEWRoute[j]["job"].ToString().Trim() + ("\' and suffix=\'"
                                                                        + (NEWRoute[j]["suffix"] + ("\' and oper_num=" + NEWRoute[j]["opernum"]))))));
                                                        double lbrHur = 0;
                                                        double MacHur = 0;
                                                        if ((RWLBH.Length > 0))
                                                        {
                                                            // run_lbr_hrs,run_mch_hrs
                                                            double.TryParse(RWLBH[0]["run_lbr_hrs"].ToString(), out lbrHur);
                                                            double.TryParse(RWLBH[0]["run_mch_hrs"].ToString(), out MacHur);
                                                        }

                                                        com.Parameters.Add("@runlbrhrs_" + (J1 + 1), SqlDbType.Decimal, 9);
                                                        com.Parameters["@runlbrhrs_" + (J1 + 1)].Value = lbrHur;

                                                        com.Parameters.Add("@runmchhrs_" + (J1 + 1), SqlDbType.Decimal, 9);
                                                        com.Parameters["@runmchhrs_" + (J1 + 1)].Value = MacHur;

                                                        if ((strAllJobroute == ""))
                                                        {
                                                            strAllJobroute = NEWRoute[j]["opernum"].ToString();
                                                        }
                                                        else
                                                        {
                                                            strAllJobroute = (strAllJobroute + ("," + NEWRoute[j]["opernum"]));
                                                        }

                                                        DataRow[] NR = DSRouteAC.Tables[0].Select(("_job=\'"
                                                                        + (NEWRoute[j]["job"].ToString().Trim() + ("\' and _jobsuffix="
                                                                        + (NEWRoute[j]["suffix"] + (" and  _operationNo=" + NEWRoute[j]["opernum"]))))));


                                                        com.Parameters.Add("@status_" + (J1 + 1), SqlDbType.VarChar, 255);


                                                        if ((NR.Length > 0))
                                                        {
                                                            com.Parameters["@status_" + (J1 + 1)].Value = "Edit";
                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@status_" + (J1 + 1)].Value = "New";
                                                        }

                                                    }
                                                    else
                                                    {
                                                        com.Parameters.Add("@job_" + (J1 + 1), SqlDbType.VarChar, 50);
                                                        com.Parameters["@job_" + (J1 + 1)].Value = "";

                                                        com.Parameters.Add("@jobsuffix_" + (J1 + 1), SqlDbType.Int);
                                                        com.Parameters["@jobsuffix_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@operationNo_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@operationNo_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@wc_" + (J1 + 1), SqlDbType.VarChar, 50);
                                                        com.Parameters["@wc_" + (J1 + 1)].Value = "";

                                                        com.Parameters.Add("@runlbrhrs_" + (J1 + 1), SqlDbType.Decimal);
                                                        com.Parameters["@runlbrhrs_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@runmchhrs_" + (J1 + 1), SqlDbType.Decimal);
                                                        com.Parameters["@runmchhrs_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@status_" + (J1 + 1), SqlDbType.VarChar, 255);
                                                        com.Parameters["@status_" + (J1 + 1)].Value = "";

                                                    }

                                                    j = (j + 1);
                                                }

                                                j = (j - 1);
                                                com.ExecuteNonQuery();
                                            }
                                            catch (Exception ex)
                                            {
                                                //System.Diagnostics.EventLogEntryType errtype;
                                                //errtype = EventLogEntryType.Error;
                                                // MsgBox(("Download error-" & DSRoute.Tables(0).Rows(i).Item("job") & ": " & ex.Message), MsgBoxStyle.Critical, "eWIP")
                                            }
                                            finally
                                            {
                                                com.Parameters.Clear();
                                                ewipCnn.Close();
                                            }




                                            //Application.DoEvents();
                                            // If Trim(strAllJobroute) <> "" Then
                                            //     With com
                                            //         .Connection = cnser
                                            //         .CommandType = CommandType.Text
                                            //         .CommandText = "delete from jobroute_mst where job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and oper_num not in(" & strAllJobroute & ")"
                                            //         cnser.Open()
                                            //         .ExecuteNonQuery()
                                            //         cnser.Close()
                                            //     End With
                                            // End If
                                            at = (at + 1);
                                            //lblRunning.Text = at;
                                            //if ((PB1.Value + (1 <= PB1.Maximum)))
                                            //{
                                            //    PB1.Value = at;
                                            //}

                                            //Application.DoEvents();
                                        }

                                        // Add codes at 5 Feb 2018
                                        bool is_k_last = false;
                                        for (int k = 0; k <= NEWRoute.Length - 1; k++)
                                        {
                                            // With...
                                            try
                                            {

                                                sql = "sp_tbjobTrans_Download_new";

                                                com = new SqlCommand(sql, ewipCnn);
                                                com.CommandType = CommandType.StoredProcedure;

                                                for (int J1 = 0; (J1 <= 14); J1++)
                                                {
                                                    com.Parameters.Add("@Nextstatus_" + (J1 + 1), SqlDbType.Int);
                                                    com.Parameters["@Nextstatus_" + (J1 + 1)].Value = 0;


                                                    if ((k < (NEWRoute.Length - 1)))
                                                    {
                                                        float qty_completeVar = 0, qty_completeVar2 = 0, qty_op_qtyVar = 0;

                                                        float.TryParse(NEWRoute[k]["qty_complete"].ToString(), out qty_completeVar);
                                                        float.TryParse(NEWRoute[(k + 1)]["qty_complete"].ToString(), out qty_completeVar2);
                                                        float.TryParse(NEWRoute[(k + 1)]["qty_op_qty"].ToString(), out qty_op_qtyVar);

                                                        if ((qty_completeVar != 0)
                                                                    && ((qty_completeVar2 == 0)
                                                                    && qty_op_qtyVar == 0))
                                                        {
                                                            com.Parameters["@Nextstatus_" + (J1 + 1)].Value = 0;
                                                        }

                                                    }

                                                    if (((k < NEWRoute.Length)
                                                                && (is_k_last == false)))
                                                    {
                                                        DataRow[] NR = DSTransAC.Tables[0].Select(("_job=\'"
                                                                        + (NEWRoute[k]["job"].ToString().Trim() + ("\' and _oper_num="
                                                                        + (NEWRoute[k]["opernum"] + (" and _wc=\'"
                                                                        + (NEWRoute[k]["wc"] + ("\' and _qty_op_qty=" + NEWRoute[k]["qty_op_qty"]))))))));
                                                        DataRow[] NRs = DSTransAC.Tables[0].Select(("_job=\'"
                                                                        + (NEWRoute[k]["job"].ToString().Trim() + ("\' and _oper_num="
                                                                        + (NEWRoute[k]["opernum"] + (" and _wc=\'"
                                                                        + (NEWRoute[k]["wc"] + "\'")))))));
                                                        double sum_qty_op_qty = 0;
                                                        for (int a = 0; (a
                                                                    <= (NRs.Length - 1)); a++)
                                                        {
                                                            sum_qty_op_qty = (sum_qty_op_qty + float.Parse(NRs[a]["_qty_op_qty"].ToString()));
                                                        }


                                                        float qty_completeVar = 0, qty_scrapped = 0, qty_op_qtyVar = 0;

                                                        float.TryParse(NEWRoute[k]["qty_complete"].ToString(), out qty_completeVar);
                                                        float.TryParse(NEWRoute[k]["qty_scrapped"].ToString(), out qty_scrapped);
                                                        float.TryParse(NEWRoute[k]["qty_op_qty"].ToString(), out qty_op_qtyVar);

                                                        com.Parameters.Add("@job_" + (J1 + 1), SqlDbType.VarChar, 50);
                                                        if ((qty_completeVar == 0 && qty_scrapped == 0 && qty_op_qtyVar == 0) || NR.Length > 0 || sum_qty_op_qty >= qty_op_qtyVar)
                                                        //if (qty_completeVar == 0
                                                        //            && qty_scrapped == 0
                                                        //            && qty_op_qtyVar == 0
                                                        //            || NR.Length > 0
                                                        //            || sum_qty_op_qty >= qty_op_qtyVar)
                                                        {
                                                            com.Parameters["@job_" + (J1 + 1)].Value = "";
                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@job_" + (J1 + 1)].Value = NEWRoute[k]["job"].ToString().Trim();
                                                        }

                                                        com.Parameters.Add("@jobsuffix_" + (J1 + 1), SqlDbType.VarChar, 10);
                                                        com.Parameters["@jobsuffix_" + (J1 + 1)].Value = 0;

                                                        if ((int.Parse(NEWRoute[k]["suffix"].ToString()) == 0))
                                                        {
                                                            com.Parameters["@jobsuffix_" + (J1 + 1)].Value = "M";
                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@jobsuffix_" + (J1 + 1)].Value = "A";
                                                        }

                                                        com.Parameters.Add("@jobsuffixParent_" + (J1 + 1), SqlDbType.VarChar, 10);
                                                        com.Parameters["@jobsuffixParent_" + (J1 + 1)].Value = "M";

                                                        com.Parameters.Add("@operationNo_" + (J1 + 1), SqlDbType.Int);
                                                        com.Parameters["@operationNo_" + (J1 + 1)].Value = int.Parse(NEWRoute[k]["opernum"].ToString());

                                                        com.Parameters.Add("@wc_" + (J1 + 1), SqlDbType.VarChar, 50);
                                                        com.Parameters["@wc_" + (J1 + 1)].Value = NEWRoute[k]["wc"];

                                                        com.Parameters.Add("@start_Date_" + (J1 + 1), SqlDbType.Float);


                                                        if (qty_op_qtyVar != 0
                                                                    && qty_completeVar != 0)
                                                        {
                                                            com.Parameters["@start_Date_" + (J1 + 1)].Value = DateTime.Parse(NEWRoute[k]["start_Date"].ToString()).ToOADate();

                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@start_Date_" + (J1 + 1)].Value = 0;
                                                        }

                                                        com.Parameters.Add("@start_time_" + (J1 + 1), SqlDbType.Int);

                                                        if (qty_op_qtyVar != 0
                                                                    && qty_completeVar != 0)
                                                        {
                                                            com.Parameters["@start_time_" + (J1 + 1)].Value = Convert.ToInt32(((DateTime)NEWRoute[k]["start_Date"]).TimeOfDay.TotalSeconds);
                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@start_time_" + (J1 + 1)].Value = 0;
                                                        }

                                                        com.Parameters.Add("@qty_scrapped_" + (J1 + 1), SqlDbType.Float);

                                                        if (qty_op_qtyVar != 0
                                                                    && qty_completeVar != 0)
                                                        {
                                                            com.Parameters["@qty_scrapped_" + (J1 + 1)].Value = float.Parse(NEWRoute[k]["qty_scrapped"].ToString());
                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@qty_scrapped_" + (J1 + 1)].Value = 0;
                                                        }

                                                        com.Parameters.Add("@qty_Rele_qty_" + (J1 + 1), SqlDbType.Float);


                                                        float qty_Rele_qty = 0;

                                                        float.TryParse(NEWRoute[k]["qty_Rele_qty"].ToString(), out qty_Rele_qty);

                                                        if (qty_op_qtyVar != 0
                                                                    || qty_Rele_qty != 0
                                                                    && qty_completeVar != 0)
                                                        {

                                                            com.Parameters["@qty_Rele_qty_" + (J1 + 1)].Value = NEWRoute[k]["qty_Rele_qty"];

                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@qty_Rele_qty_" + (J1 + 1)].Value = 0;
                                                        }

                                                        com.Parameters.Add("@qty_complete_" + (J1 + 1), SqlDbType.Float);

                                                        if (((qty_op_qtyVar != 0)
                                                                    || (qty_Rele_qty != 0)))
                                                        {
                                                            com.Parameters["@qty_complete_" + (J1 + 1)].Value = NEWRoute[k]["qty_complete"];
                                                        }
                                                        else
                                                        {
                                                            com.Parameters["@qty_complete_" + (J1 + 1)].Value = 0;
                                                        }


                                                        com.Parameters.Add("@qty_op_qty_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@qty_op_qty_" + (J1 + 1)].Value = NEWRoute[k]["qty_op_qty"];

                                                        if ((k < (NEWRoute.Length - 1)))
                                                        {
                                                            k = (k + 1);
                                                        }
                                                        else
                                                        {
                                                            is_k_last = true;
                                                        }

                                                    }
                                                    else
                                                    {

                                                        com.Parameters.Add("@job_" + (J1 + 1), SqlDbType.VarChar, 55);
                                                        com.Parameters["@job_" + (J1 + 1)].Value = "";

                                                        com.Parameters.Add("@jobsuffix_" + (J1 + 1), SqlDbType.VarChar, 10);
                                                        com.Parameters["@jobsuffix_" + (J1 + 1)].Value = "-";

                                                        com.Parameters.Add("@jobsuffixParent_" + (J1 + 1), SqlDbType.VarChar, 10);
                                                        com.Parameters["@jobsuffixParent_" + (J1 + 1)].Value = "-";

                                                        com.Parameters.Add("@operationNo_" + (J1 + 1), SqlDbType.Int);
                                                        com.Parameters["@operationNo_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@wc_" + (J1 + 1), SqlDbType.VarChar, 50);
                                                        com.Parameters["@wc_" + (J1 + 1)].Value = "";

                                                        com.Parameters.Add("@start_Date_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@start_Date_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@start_time_" + (J1 + 1), SqlDbType.Int);
                                                        com.Parameters["@start_time_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@qty_scrapped_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@qty_scrapped_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@qty_Rele_qty_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@qty_Rele_qty_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@qty_complete_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@qty_complete_" + (J1 + 1)].Value = 0;

                                                        com.Parameters.Add("@qty_op_qty_" + (J1 + 1), SqlDbType.Float);
                                                        com.Parameters["@qty_op_qty_" + (J1 + 1)].Value = 0;

                                                    }

                                                }

                                                if ((k % 14) == 1 && is_k_last == false)
                                                {
                                                    k = (k - 1);
                                                }

                                                ewipCnn.Open();

                                                var result = com.ExecuteNonQuery();

                                                com.Parameters.Clear();
                                                ewipCnn.Close();
                                            }
                                            catch (Exception ex)
                                            {
                                                //System.Diagnostics.EventLogEntryType errtype;
                                                //errtype = EventLogEntryType.Error;
                                                // MsgBox(("Download error-" & DSRoute.Tables(0).Rows(i).Item("job") & ": " & ex.Message), MsgBoxStyle.Critical, "eWIP")
                                            }
                                            finally
                                            {
                                                com.Parameters.Clear();
                                                ewipCnn.Close();
                                            }
                                        }

                                        // End add codes at 5 Feb 2018

                                    }

                                    ewipCnn.Close();
                                    com.Parameters.Clear();
                                }
                            }
                            else
                            {
                                List<String> keyList = new List<String>(StartMaterialQty.Keys);
                                foreach (var key in keyList)
                                {
                                    mtlOnHand[key] = StartMaterialQty[key];
                                }
                            }
                        }
                        mtlOnHandStatic = mtlOnHand;
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    ewipCnn.Close();
                }

            }
            else if (cmbType == "Work Center")
            {
                // ''''''''''''''''''''''''''Work Center Download
                // ''''''''''''''''''''''''''Work Center Download
                // ''''''''''''''''''''''''''Work Center Download
                // ''''''''''''''''''''''''''Work Center Download
                // ''''''''''''''''''''''''''Work Center Download
                // With...
                var DA = new SqlDataAdapter();

                //ewipCnn.Open();

                string sql = "select wc,description,efficiency,dept,Run_Rate_Lbr,sched_drv from " + Global.wc_mst + "";

                com = new SqlCommand(sql, sqlCnn);

                DA.SelectCommand = com;
                DA.Fill(DSwc, "tbsch");
                tot = (tot + DSwc.Tables[0].Rows.Count);
                lblTot = tot;
                //PB1.Value = 0;
                //PB1.Minimum = 0;
                //PB1.Maximum = tot;
                //Application.DoEvents();
                ct = 0;
                // By Yeo 20081110
                try
                {
                    DataSet DSAC = new DataSet();
                    if ((DSwc.Tables[0].Rows.Count > 0))
                    {
                        clsMain clsM = new clsMain();
                        DSAC = clsM.GetDataset("select * from tbWC", "tbWC");


                        for (int i = 0; (i
                                    <= (DSwc.Tables[0].Rows.Count - 1)); i++)
                        {
                            DataRow[] DRAC;
                            bool stAC = false;
                            if ((DSAC.Tables[0].Rows.Count > 0))
                            {
                                DRAC = DSAC.Tables[0].Select(("_wc=\'"
                                                + (DSwc.Tables[0].Rows[i]["wc"] + "\'")));
                                if ((DRAC.Length > 0))
                                {

                                    if (((DRAC[0]["_description"] != DSwc.Tables[0].Rows[i]["description"])
                                                || ((DRAC[0]["_labour"] != DSwc.Tables[0].Rows[i]["sched_drv"])
                                                || (DRAC[0]["_Run_Rate"] != DSwc.Tables[0].Rows[i]["Run_Rate_Lbr"]))))
                                    {
                                        stAC = false;
                                    }
                                    else
                                    {
                                        stAC = true;
                                    }

                                }

                            }

                            if ((stAC == false))
                            {
                                sql = "sp_tbwc_Download";
                                ewipCnn.Open();

                                com = new SqlCommand(sql, ewipCnn);
                                com.CommandType = CommandType.StoredProcedure;

                                com.Parameters.Add("@_wc", SqlDbType.NVarChar, 6);
                                com.Parameters["@_wc"].Value = DSwc.Tables[0].Rows[i]["wc"];

                                com.Parameters.Add("@_description", SqlDbType.NVarChar, 256);
                                com.Parameters["@_description"].Value = DSwc.Tables[0].Rows[i]["description"];

                                com.Parameters.Add("@_efficiency", SqlDbType.NVarChar, 40);
                                com.Parameters["@_efficiency"].Value = DSwc.Tables[0].Rows[i]["efficiency"];

                                com.Parameters.Add("@_dept", SqlDbType.NVarChar, 40);
                                com.Parameters["@_dept"].Value = DSwc.Tables[0].Rows[i]["dept"];

                                com.Parameters.Add("@_labour", SqlDbType.Char, 1);
                                com.Parameters["@_labour"].Value = DSwc.Tables[0].Rows[i]["sched_drv"];

                                com.Parameters.Add("@_Run_Rate", SqlDbType.Float);
                                com.Parameters["@_Run_Rate"].Value = DSwc.Tables[0].Rows[i]["Run_Rate_Lbr"];

                                com.ExecuteNonQuery();

                                com.Parameters.Clear();
                                ewipCnn.Close();
                            }

                            at = (at + 1);

                            //if ((PB1.Value
                            //            + (ct <= PB1.Maximum)))
                            //{
                            //    PB1.Value = at;
                            //}

                            //Application.DoEvents();
                        }

                    }

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    ewipCnn.Close();
                }

            }
            else if (cmbType == "Item")
            {

                // ''''''''''''''''''''''' Item
                // ''''''''''''''''''''''' Item
                // ''''''''''''''''''''''' Item
                // ''''''''''''''''''''''' Item
                // ''''''''''''''''''''''' Item

                // With...

                //ewipCnn.Open();

                var DA = new SqlDataAdapter();
                string sql = "select item,description,Stat,Drawing_Nbr,Revision from " + Global.item_mst + "";

                com = new SqlCommand(sql, sqlCnn);
                DA.SelectCommand = com;
                DA.Fill(DSItem, "tbItem");
                tot = (tot + DSItem.Tables[0].Rows.Count);
                lblTot = tot;

                ct = 0;
                // By Yeo 20081110
                try
                {
                    DataSet DSAC = new DataSet();
                    if ((DSItem.Tables[0].Rows.Count > 0))
                    {
                        clsMain clsM = new clsMain();
                        DSAC = clsM.GetDataset("select * from tbItem", "tbItem");

                        for (int i = 0; (i
                                    <= (DSItem.Tables[0].Rows.Count - 1)); i++)
                        {
                            DataRow[] DRAC;
                            bool stAC = false;
                            if ((DSAC.Tables[0].Rows.Count > 0))
                            {
                                DRAC = DSAC.Tables[0].Select(("_itemCode=\'"
                                                + (DSItem.Tables[0].Rows[i]["item"] + "\'")));
                                if ((DRAC.Length > 0))
                                {
                                    string strdrawing = "";
                                    string strRevision = "";
                                    if ((DSItem.Tables[0].Rows[i]["Drawing_Nbr"] != DBNull.Value))
                                    {
                                        strdrawing = DSItem.Tables[0].Rows[i]["Drawing_Nbr"].ToString();
                                    }

                                    if ((DSItem.Tables[0].Rows[i]["Revision"]) != DBNull.Value)
                                    {
                                        strRevision = DSItem.Tables[0].Rows[i]["Revision"].ToString();
                                    }

                                    if ((DSItem.Tables[0].Rows[i]["description"] != DBNull.Value))
                                    {
                                        if (((DRAC[0]["_itemdescription"] != DSItem.Tables[0].Rows[i]["description"])
                                                    || ((DRAC[0]["_type"] != DSItem.Tables[0].Rows[i]["Stat"])
                                                    || ((DRAC[0]["_Drawing_Nbr"] != strdrawing)
                                                    || (DRAC[0]["_Revision"] != strRevision)))))
                                        {
                                            stAC = false;
                                        }
                                        else
                                        {
                                            stAC = true;
                                        }

                                    }

                                }

                            }

                            if ((stAC == false))
                            {
                                // With...
                                sql = "sp_tbItem_Download";
                                ewipCnn.Open();

                                com = new SqlCommand(sql, ewipCnn);
                                com.CommandType = CommandType.StoredProcedure;

                                com.Parameters.Add("@itemCode", SqlDbType.NVarChar, 50);
                                com.Parameters["@itemCode"].Value = DSItem.Tables[0].Rows[i]["item"];

                                com.Parameters.Add("@itemdescription", SqlDbType.NVarChar, 256);

                                if ((DSItem.Tables[0].Rows[i]["description"] != DBNull.Value))
                                {
                                    com.Parameters["@itemdescription"].Value = DSItem.Tables[0].Rows[i]["description"];
                                }
                                else
                                {
                                    com.Parameters["@itemdescription"].Value = "";
                                }

                                com.Parameters.Add("@type", SqlDbType.NVarChar, 5);
                                if ((DSItem.Tables[0].Rows[i]["Stat"] != DBNull.Value))
                                {
                                    com.Parameters["@type"].Value = DSItem.Tables[0].Rows[i]["Stat"];
                                }
                                else
                                {
                                    com.Parameters["@type"].Value = "";
                                }

                                com.Parameters.Add("@Drawing_Nbr", SqlDbType.NVarChar, 256);
                                if ((DSItem.Tables[0].Rows[i]["Drawing_Nbr"]) != DBNull.Value)
                                {
                                    com.Parameters["@Drawing_Nbr"].Value = DSItem.Tables[0].Rows[i]["Drawing_Nbr"];
                                }
                                else
                                {
                                    com.Parameters["@Drawing_Nbr"].Value = "";
                                }

                                com.Parameters.Add("@Revision", SqlDbType.NVarChar, 256);
                                if ((DSItem.Tables[0].Rows[i]["Revision"] != DBNull.Value))
                                {
                                    com.Parameters["@Revision"].Value = DSItem.Tables[0].Rows[i]["Revision"];
                                }
                                else
                                {
                                    com.Parameters["@Revision"].Value = "";
                                }

                                com.ExecuteNonQuery();
                                com.Parameters.Clear();
                                ewipCnn.Close();
                            }

                            at = (at + 1);

                        }

                    }

                }
                catch (Exception ex)
                {

                }
                finally
                {
                    ewipCnn.Close();
                }

            }
            return true;
        }

        private DataSet GetCntrlPointSpecificJobAndLine(string currentJob, DataRow[] NEWRoute, int targetIndex)
        {
            var DACntrlPoint = new SqlDataAdapter();
            DataSet DSCntrlPoint = new DataSet();

            string sql = "select cntrl_point from " + Global.jobroute_mst +
                  " where job = @job and oper_num = @oper_num";
            com = new SqlCommand(sql, sqlCnn);
            com.Parameters.Add("@Job", SqlDbType.NVarChar, 10);
            com.Parameters["@Job"].Value = currentJob.Trim();

            com.Parameters.Add("@oper_num", SqlDbType.Int);
            com.Parameters["@oper_num"].Value = NEWRoute[targetIndex][2];

            try
            {
                sqlCnn.Open();
                DACntrlPoint.SelectCommand = com;
                DACntrlPoint.Fill(DSCntrlPoint, "jobroute_mst");
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlCnn.Close();
                com.Parameters.Clear();
            }

            return DSCntrlPoint;
        }

        void UpdateDownloadLog(string txtFrom, string txtTo)
        {
            DateTime SD = new DateTime();
            DateTime ED = new DateTime();
            if (!String.IsNullOrEmpty(txtFrom) && !String.IsNullOrEmpty(txtTo))
            {
                string[] s = txtFrom.Split('/');
                SD = new DateTime(int.Parse(s[2]), int.Parse(s[1]), int.Parse(s[0]));
                string[] s1 = txtTo.Split('/');
                ED = new DateTime(int.Parse(s1[2]), int.Parse(s1[1]), int.Parse(s1[0]));
            }

            try
            {

                // With...

                var DA = new SqlDataAdapter();
                ewipCnn.Open();

                string sql = "sp_tbdownloadLog_add";

                com = new SqlCommand(sql, ewipCnn);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.Add("@Sd", SqlDbType.Float, 8);
                com.Parameters["@Sd"].Value = SD.ToOADate();

                com.Parameters.Add("@Ed", SqlDbType.Float, 8);
                com.Parameters["@Ed"].Value = ED.ToOADate();

                com.Parameters.Add("@logby", SqlDbType.VarChar, 50);
                com.Parameters["@logby"].Value = mpMembership.GetCurrentUsername();

                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                ewipCnn.Close();
                com.Parameters.Clear();
            }
        }

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetInsufficientMaterialbyDate dt;

        public string PrintInsufficientMaterial()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            var Report1 = "";

            SqlConnection sl9db = Global.sl9Cnn,
                                  ewipdb = Global.eWIPCnn;


            // Insufficient Material By Date
            DataSetInsufficientMaterialbyDate ds = new DataSetInsufficientMaterialbyDate();
            StringBuilder whereJob = new StringBuilder();
            Dictionary<string, int> jobList = new Dictionary<string, int>();
            foreach (var item in unsufficientJob)
            {
                try
                {
                    if (jobList[item] != 1)
                    {
                        continue;
                    }
                }
                catch (KeyNotFoundException ex)
                {
                    whereJob.Append("'" + item + "',");
                    jobList[item] = 1;
                }
            }

            whereJob.Remove(whereJob.Length - 1, 1);
            DB_Parameter mst_jobfrom = new DB_Parameter(), mst_jobto = new DB_Parameter();
            List<DB_Parameter> parameters_sl9DB = new List<DB_Parameter>() { };

            DataTable data_table = new DataTable(),
            GetData = Global.getDataFrom_sl9DB("select * from (SELECT DISTINCT mst.job, rs.jobItem, rs.item, (rs.matl_qty * mst.qty_released) as totalNeeded FROM " + Global.job_mst + " mst "
                    + "left join (select  b.item as jobItem,a.item , a.matl_qty from " + Global.jobmatl_mst + " a inner join " + Global.item_mst + " b on a.job = b.job where a.matl_type=\'M\' and a.suffix = \'1\') rs on rs.jobItem = mst.item "
                    + "left join (select * from " + Global.itemloc_mst + " where loc = 'TL') rs2 on rs2.item = rs.item where stat in('R','S') and mst.job in (" + whereJob + ") )lrs  ", parameters_sl9DB);
            DataRow[] detailData = GetData.Select();

            List<dataInsufficientMaterial> table_data = new List<dataInsufficientMaterial>();

            foreach (var detail in detailData)
            {
                dataInsufficientMaterial temp = new dataInsufficientMaterial();
                temp.job = detail.ItemArray[0].ToString();
                temp.jobitem = detail.ItemArray[1].ToString();
                temp.item = detail.ItemArray[2].ToString();
                temp.totalNeeded = Convert.ToDouble(detail.ItemArray[3].ToString());
                try
                {
                    temp.qty_on_hand = mtlOnHandStatic[detail.ItemArray[2].ToString()];
                }
                catch (Exception ex)
                {
                    temp.qty_on_hand = 0;
                }

                try
                {
                    temp.qty_start = mtlStartStatic[detail.ItemArray[2].ToString()];
                }
                catch (Exception ex)
                {
                    temp.qty_start = 0;
                }

                table_data.Add(temp);
            }

            DataSetInsufficientMaterialbyDate SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetInsufficientMaterialbyDate();

            dt = (DataSetInsufficientMaterialbyDate)Session["sourcetableInsufficientMaterial"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRInsufficientMaterialbyJob.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(table_data);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-InsufficientMaterial" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-InsufficientMaterial" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);
            }

            unsufficientJob = new List<String>();
            return successString + ";" + "/Uploads/" + fileNameExcel + ";" + "/Uploads/" + fileName;
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetInsufficientMaterialbyDate SessionDataTable
        {
            get
            {
                if (Session["sourcetableInsufficientMaterial"] == null)
                {
                    Session["sourcetableInsufficientMaterial"] = new DataSetInsufficientMaterialbyDate();
                }

                return (DataSetInsufficientMaterialbyDate)Session["sourcetableInsufficientMaterial"];
            }
            set { Session["sourcetableInsufficientMaterial"] = value; }
        }

        public static Dictionary<string, double> copyMatl()
        {
            return mtlStartStatic;
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataInsufficientMaterial
        {
            public string job { get; set; }

            public string jobitem { get; set; }

            public string item { get; set; }

            public double totalNeeded { get; set; }

            public double qty_on_hand { get; set; }

            public double qty_start { get; set; }
        }

    }
}
