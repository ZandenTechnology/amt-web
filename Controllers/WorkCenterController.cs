﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class WorkCenterController : Controller
    {
        //
        // GET: /WorkCenter/

        [HttpPost]
        public ActionResult prosesWorkCenter(tbWc model, string command, string url)
        {
            var db = Global.ewipDB;
            if (command.Equals("saveWC"))
            {
                try
                {
                    if (tbWc.insertWorkCenter(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (command.Equals("updateWC"))
            {

                try
                {
                    if (tbWc.updateWorkCenter(model))
                    {

                        return Json(new { success = Global.UpdateMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }

            else if (command.Equals("deleteWC"))
            {

                try
                {
                    if (tbWc.deleteWorkCenter(model))
                    {
                        return Json(new { success = Global.DeleteMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }

            else return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllWorkCenterData()
        {
            var db = Global.ewipDB;
            var WCData = (from tempWs in db.tbWorkStations
                          join tempWc in db.tbWcs on tempWs._wc equals tempWc._wc
                          join tempRG in db.tbResourceGroups on tempWs._rid equals tempRG._rid

                          select new
                         {
                             _wcid = tempWc._wcid,
                             _wc = tempWc._wc,
                             _description = tempWc._description,
                             _efficiency = tempWc._efficiency,
                             _dept = tempWc._dept,
                             _transfer = tempWc._transfer,
                             _labour = tempWc._labour,
                             _Run_Rate = tempWc._Run_Rate,
                             _rGroupID = tempRG._rGroupID,
                         }).ToList();


            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(WCData);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

   [HttpPost]
        public ActionResult SetWorkCenter(string WorkCenterStr, string data)
        {
            var WCID = WorkCenterStr.ToString();
            List<tbMachineGroup> DataWC = new JavaScriptSerializer().Deserialize<List<tbMachineGroup>>(data);

            var serializer = new JavaScriptSerializer();
            try
            {
               
                //var serializedList = serializer.Deserialize<List<string>>(model.listWorkCenter);


                var db = Global.ewipDB;
                //var getWorkStation = db.tbWorkStations.Where(x => x.rid == model.rid).ToList(); 
                var getMachineGroup = db.tbMachineGroups.Where(x => x._wc == WCID).ToList();
                //var getMaxWS = db.tbWorkStations.OrderByDescending(z => z.sIDno).FirstOrDefault().sIDno + 1;
                var getMaxMG = db.tbMachineGroups.OrderByDescending(z => z._mgid).FirstOrDefault()._mgid + 1;
                var list = new List<tbMachineGroup>();
                foreach (var work in DataWC)
                {
                    var checkWork = getMachineGroup.Where(y => y._Mid == work._Mid).FirstOrDefault();
                    //var checkWork = getWorkStation.Where(y => y.wc == work).FirstOrDefault();

                    if (checkWork == null)
                    {
                        //var newWork = new tbWorkStation();
                        //newWork.sIDno = getMaxWS;
                        //newWork.rid = model.rid;
                        //newWork._wc = work;

                        var newWork = new tbMachineGroup();
                        newWork._mgid = getMaxMG;
                        newWork._Mid = work._Mid;
                        newWork._wc = WorkCenterStr;
                        getMaxMG += 1;

                        list.Add(newWork);
                        db.tbMachineGroups.InsertOnSubmit(newWork);
                    }
                    else
                    {
                        getMachineGroup.Remove(checkWork);
                    }
                }

                foreach (var deselected in getMachineGroup)
                {
                    db.tbMachineGroups.DeleteOnSubmit(deselected);
                }

                db.SubmitChanges();
                return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }


           

            
            //var db = Global.ewipDB;

            //ewip_tbMachineGroup.setMachineGroup(DataWC, WorkCenterStr);



            //var serializer = new JavaScriptSerializer();
            //var serializedResult = serializer.Serialize("done");
           
            //return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

      
        //public ActionResult GetAllocatedMachine(int MIGid)
        //{
        //    var db = Global.ewipDB;
        //    var WCData = (from MachineGroup in db.tbMachineGroups
        //                  join WorkCenter in db.tbWcs on MachineGroup._wc equals WorkCenter._wc
        //                  join Machines in db.tbMachines on MachineGroup._Mid equals Machines._mid
        //                  where MachineGroup._mgid == MIGid
        //                  select new
        //                  {
        //                      _mid = Machines._mid,
        //                      _machineID = Machines._machineID,
        //                      _MachineDesc = Machines._MachineDesc
        //                  }).ToList();


        //    var serializer = new JavaScriptSerializer();
        //    var serializedResult = serializer.Serialize(WCData);

        //    return Json(serializedResult, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetAllocatedMachine(string WC)
        {
            var db = Global.ewipDB;
            var WCData = (from MachineGroup in db.tbMachineGroups
                          join WorkCenter in db.tbWcs on MachineGroup._wc equals WorkCenter._wc
                          join Machines in db.tbMachines on MachineGroup._Mid equals Machines._mid
                          where MachineGroup._wc == WC
                          select new
                          {
                              _mid = Machines._mid,
                              _machineID = Machines._machineID,
                              _MachineDesc = Machines._MachineDesc
                          }).ToList();


            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(WCData);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

       public ActionResult updateWorkCenterGrouping(Grouping_WorkCenterModel model, string url)
        {
            try
            {
                var serializer = new JavaScriptSerializer();
                var serializedList = serializer.Deserialize<List<string>>(model.listWorkCenter);

                var db = Global.ewipDB;
                var getWorkStation = db.tbWorkStations.Where(x => x._rid == model._rid).ToList(); ;
                var getMaxWS = db.tbWorkStations.OrderByDescending(z => z._sIDno).FirstOrDefault()._sIDno + 1;
                var list = new List<tbWorkStation>();
                foreach (var work in serializedList)
                {
                    var checkWork = getWorkStation.Where(y => y._wc == work).FirstOrDefault();

                    if (checkWork == null)
                    {
                        var newWork = new tbWorkStation();
                        newWork._sIDno = getMaxWS;
                        newWork._rid = model._rid;
                        newWork._wc = work;
                        getMaxWS += 1;

                        list.Add(newWork);
                        db.tbWorkStations.InsertOnSubmit(newWork);
                    }
                    else
                    {
                        getWorkStation.Remove(checkWork);
                    }
                }

                foreach (var deselected in getWorkStation)
                {
                    db.tbWorkStations.DeleteOnSubmit(deselected);
                }

                db.SubmitChanges();
                return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllAllocatedWorkCenter(decimal _rid)
        {
            try
            {
                var db = Global.ewipDB;
                var listActive = (from x in db.tbWorkStations
                                  where x._rid == _rid
                                  select x).ToList();

                var joinedActive = (from y in listActive
                                    join z in db.tbWcs on y._wc equals z._wc
                                    select new
                                    {
                                        _wcid = z._wcid,
                                        _wc = z._wc,
                                        _description = z._description
                                    }).ToList();

                var serializer = new JavaScriptSerializer();
                var serializeResult = serializer.Serialize(joinedActive);

                return Json(serializeResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllAvailableWorkCenter(decimal _rid)
        {
            try
            {
                var db = Global.ewipDB;
                var listActive = (from x in db.tbWorkStations
                                  where x._rid == _rid
                                  select x).ToList();

                var AvailableWC = (from y in db.tbWcs
                                   where !(from z in listActive select z._wc).Contains(y._wc)
                                   select y).ToList();

                var serializer = new JavaScriptSerializer();
                var serializeResult = serializer.Serialize(AvailableWC);

                return Json(serializeResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

    }

    internal class modelWS
    {
        private string _wc;

        public string wc
        {
            get { return wc; }
            set { wc = value; }
        }
    }

}
