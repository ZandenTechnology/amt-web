﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class WIPbyItemController : Controller
    {
        //
        // GET: /WIPbyItem/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetWIPbyItem dt;

        // Print Failure Analysis 
        public ActionResult PrintWIPbyItem(string itemCode, bool ckbRework, bool ckbTransfer)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetWIPbyItem ds = new DataSetWIPbyItem();

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(itemCode);

            var stringItem = "";
            foreach (var list in listItem)
            {
                stringItem += "'" + list + "',";
            }

            if (!stringItem.Equals(""))
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }

            var getData = CrystalReportClass.getdataWIPbyItem(ckbRework, ckbTransfer, stringItem);

            List<dataWIPbyItem> detailData = new List<dataWIPbyItem>();

            foreach (var detail in getData)
            {
                dataWIPbyItem temp = new dataWIPbyItem();
                temp._status = detail._status;
                temp.ChildR = detail.ChildR;
                temp.Job = detail.Job;
                temp.OP = Convert.ToDouble(detail.OP);
                temp.Qty = Convert.ToDouble(detail.Qty);
                temp.ReportDate = detail.ReportDate;
                temp.StockCard = detail.StockCard;
                temp.Tdate = detail.Tdate;
                temp.WCDesc = detail.WCDesc;

                detailData.Add(temp);
            }

            DataSetWIPbyItem SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetWIPbyItem();

            dt = (DataSetWIPbyItem)Session["sourcetableWIPbyItem"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRWIPbyItem.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-WIPbyItem" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-WIPbyItem" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetWIPbyItem SessionDataTable
        {
            get
            {
                if (Session["sourcetableWIPbyItem"] == null)
                {
                    Session["sourcetableWIPbyItem"] = new DataSetWIPbyItem();
                }

                return (DataSetWIPbyItem)Session["sourcetableWIPbyItem"];
            }
            set { Session["sourcetableWIPbyItem"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataWIPbyItem
        {
            public string ReportDate { get; set; }

            public string StockCard { get; set; }

            public string Job { get; set; }

            public string ChildR { get; set; }

            public string Tdate { get; set; }

            public double OP { get; set; }

            public string WCDesc { get; set; }

            public string _status { get; set; }

            public double Qty { get; set; }
        }

    }
}
