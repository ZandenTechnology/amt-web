﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class RejectedDescController : Controller
    {
        //
        // GET: /RejectedDesc/

        [HttpPost]
        public ActionResult prosesRejectedDesc(tbRejected model, string command, string url)
        {
            var db = Global.ewipDB;
            if (command.Equals("saveRD"))
            {
                try
                {
                    if (tbRejected.insertRejectDesc(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else if (command.Equals("updateRD"))
            {

                try
                {
                    if (tbRejected.updateRejectDesc(model))
                    {

                        return Json(new { success = Global.UpdateMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (command.Equals("deleteRD"))
            {

                try
                {
                    if (tbRejected.deleteRejectDesc(model))
                    {
                        return Json(new { success = Global.DeleteMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }

            else return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllRejectedDescData(string _wc, string _itemCode)
        {
            var db = Global.ewipDB;
            List<tbRejected> rejectId = new List<tbRejected>();

            if (_itemCode != "")
            {
                rejectId = (from x in db.tbRejecteds
                            where !(from y in db.tbRejectedGroups
                                    where y._wc == _wc && y._itemCode == _itemCode
                                    select y._RejID.ToString()).Contains(x._RejID.ToString())
                            select x).ToList();
            }
            else if (_wc != "")
            {
                rejectId = (from x in db.tbRejecteds
                            where !(from y in db.tbRejectedGroups
                                    where y._wc == _wc
                                    select y._RejID.ToString()).Contains(x._RejID.ToString())
                            select x).ToList();
            }

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(rejectId);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            //ewipDBDataContext ewipDB = new ewipDBDataContext();
            //List<tbResourceGroup> ResGroup = ewipDB.tbResourceGroups.ToList();

            WorkCenterModel model = new WorkCenterModel();

            return View(model);
        }

    }
}
