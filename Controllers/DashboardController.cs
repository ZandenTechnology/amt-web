﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UltimoApps.DAL;
using System.Web.Security;
using UltimoApps.DAL.Membership;

namespace UltimoApps.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        ultimoDBDataContext db = new ultimoDBDataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Controller(string Id, string searchID)
        {
            tbUser curUser = mpMembership.GetCurrentUser();
            if (curUser == null)
            {
                return View("~/Views/Frm_Login.cshtml");
            }
            if (Id.Equals("error-not-found"))
            {
                return HttpNotFound("403");
            }
            if (Id.Equals("Frm_Profile_ChangePassword") || Id.Equals("Frm_Profile_Edit"))
            {
                tbUser model = tbUser.GetUserbyUserName(mpMembership.GetCurrentUsername());
                return View("~/Views/" + Id + ".cshtml", model);      
            }
            if (Id.Equals("Frm_ConfigureDB"))
            {
                return View("~/Views/" + Id + ".cshtml", Tmp_SL9TableName.getDefaultName());
            }
            return View("~/Views/" + Id + ".cshtml");
        }
    }
}
