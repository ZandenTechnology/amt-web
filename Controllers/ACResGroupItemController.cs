﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class ACResGroupItemController : Controller
    {
        //
        // GET: /ACResGroupItem/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetACResGroupItem dt;

        // Print Work Center Yield 
        public ActionResult PrintACResGroupItem(string itemCode, string dateFrom, string dateTo, string rid, string wc)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetACResGroupItem ds = new DataSetACResGroupItem();
            dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
            dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(itemCode);

            var stringItem = "";
            foreach (var list in listItem)
            {
                stringItem += "'" + list + "',";
            }

            if (!stringItem.Equals(""))
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }

            var getData = CrystalReportClass.getdataACbyResGroupItem(dateFrom, dateTo, stringItem, rid, wc);

            List<dataACbyResGroupItem> detailData = new List<dataACbyResGroupItem>();

            foreach (var detail in getData)
            {
                dataACbyResGroupItem temp = new dataACbyResGroupItem();
                temp._item = detail._item;
                temp._itemdescription = detail._itemdescription;
                temp._rGroupID = detail._rGroupID;
                temp._rGroupName = detail._rGroupName;
                temp.sumHour = Convert.ToDouble(detail.sumHour);
                temp.sumTotal = Convert.ToDouble(detail.sumTotal);
                temp.sumMinute = Convert.ToDouble(detail.sumMinute);
                temp.dateFrom = Convert.ToDateTime(detail.dateFrom);
                temp.dateTo = Convert.ToDateTime(detail.dateTo);

                detailData.Add(temp);
            }

            DataSetACResGroupItem SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetACResGroupItem();

            dt = (DataSetACResGroupItem)Session["sourcetableACRGItem"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRACbyRGSummary.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-ACResGroupItem" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-ACResGroupItem" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count() != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);


                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetACResGroupItem SessionDataTable
        {
            get
            {
                if (Session["sourcetableACRGItem"] == null)
                {
                    Session["sourcetableACRGItem"] = new DataSetACResGroupItem();
                }

                return (DataSetACResGroupItem)Session["sourcetableACRGItem"];
            }
            set { Session["sourcetableACRGItem"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataACbyResGroupItem
        {
            public string _rGroupID { get; set; }

            public string _rGroupName { get; set; }

            public string _item { get; set; }

            public string _itemdescription { get; set; }

            public double sumTotal { get; set; }

            public double sumHour { get; set; }

            public double sumMinute { get; set; }

            public DateTime dateFrom { get; set; }

            public DateTime dateTo { get; set; }
        }

    }
}
