﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class WCYieldController : Controller
    {
        //
        // GET: /WCYield/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetWCYield dt;
        string tempHolder;

        // Print Work Center Yield 
        public ActionResult PrintWCYield(string wc, string itemCode, string dateFrom, string dateTo)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
                var connection = connectionString;
                var Report = "";
                DataSetWCYield ds = new DataSetWCYield();
                dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
                dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");

                var serializer = new JavaScriptSerializer();
                var listItem = serializer.Deserialize<List<string>>(itemCode);

                var stringItem = "";
                foreach (var item in listItem)
                {
                    stringItem += "'" + item + "',";
                }

                if (!stringItem.Equals(""))
                {
                    tempHolder = stringItem;
                    stringItem = stringItem.Substring(0, stringItem.Length - 1);
                }

                var getData = CrystalReportClass.getDataWorkCenterYield(dateFrom, dateTo, stringItem, wc);

                List<dataWorkCenter> detailData = new List<dataWorkCenter>();

                foreach (var detail in getData)
                {
                    dataWorkCenter temp = new dataWorkCenter();
                    temp._description = detail._description;
                    temp._end_date = detail._end_Date;
                    temp._item = detail._item;
                    temp._job = detail._job;
                    temp._oper_num = detail._oper_num;
                    temp._qty_complete = detail._qty_complete;
                    temp._qty_scrapped = detail._qty_scrapped;
                    temp.dateFrom = Convert.ToDateTime(detail.dateFrom);
                    temp.dateTo = Convert.ToDateTime(detail.dateTo);
                    temp.percentage = detail.percentage;
                    temp.percentageNumber = (decimal)detail.percentageNumber;

                    var tempSplit = detail._itemdescription.Split(',');
                    string itemDesc = "";
                    foreach (var itemSplit in tempSplit)
                    {
                        if (itemSplit != "")
                        {
                            itemDesc += itemSplit;
                        }
                    }

                    temp._itemdescription = itemDesc;

                    detailData.Add(temp);
                }

                DataSetWCYield SOPDS = ds;

                parameters = new Hashtable();
                rptDoc = new ReportDocument();
                dt = new DataSetWCYield();

                dt = (DataSetWCYield)Session["sourcetableWCYield"];
                parameters = (Hashtable)Session["parameters"];
                SessionDataTable = SOPDS;
                rptName = (string)Session["rptName"];
                Report = "~/CRWorkCenterYield.rpt";

                rptDoc.Load(Server.MapPath(Report));
                rptDoc.SetDataSource(detailData);

                SetParameter();
                string fileDir = Server.MapPath("/Uploads/");
                string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-WorkCenterYield" + ".pdf";
                string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-WorkCenterYield" + ".xls";
                string filePath = fileDir + fileName;
                string filePathExcel = fileDir + fileNameExcel;

                if (!System.IO.Directory.Exists(fileDir))
                {
                    System.IO.Directory.CreateDirectory(fileDir);
                }

                if (detailData.Count() != 0)
                {
                    rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                    rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                    return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return Json(new { keyword = tempHolder, error = ex.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetWCYield SessionDataTable
        {
            get
            {
                if (Session["sourcetableWCYield"] == null)
                {
                    Session["sourcetableWCYield"] = new DataSetWCYield();
                }

                return (DataSetWCYield)Session["sourcetableWCYield"];
            }
            set { Session["sourcetableWCYield"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataWorkCenter
        {
            public string _description { get; set; }

            public string _item { get; set; }

            public string _itemdescription { get; set; }

            public string _job { get; set; }

            public string _end_date { get; set; }

            public double _oper_num { get; set; }

            public double _qty_complete { get; set; }

            public double _qty_scrapped { get; set; }

            public string percentage { get; set; }

            public decimal percentageNumber { get; set; }

            public DateTime dateFrom { get; set; }

            public DateTime dateTo { get; set; }

        }

    }
}
