﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using UltimoApps.DAL;
using UltimoApps.Models;
using System.Drawing;

namespace UltimoApps.Controllers
{
    public class ExportCompletedOperationsController : Controller
    {
        [HttpPost]
        public ActionResult ExportToExcel(string submitButton, string fileName, DateTime fromDate, DateTime toDate, string cbNoMatch = null)
        {
            switch (submitButton)
            {
                case "Export":
                    SqlConnection sl9db = Global.sl9Cnn,
                                  ewipdb = Global.eWIPCnn;

                    DB_Parameter parameter_1 = new DB_Parameter(),
                                 parameter_2 = new DB_Parameter();
                    parameter_1.name = "@fromDate";
                    parameter_1.type_data = SqlDbType.Float;
                    parameter_1.value = fromDate.ToOADate();
                    parameter_2.name = "@toDate";
                    parameter_2.type_data = SqlDbType.Float;
                    parameter_2.value = toDate.AddDays(1).ToOADate();

                    List<DB_Parameter> parameters_ewipDB = new List<DB_Parameter>() { parameter_1, parameter_2 },
                                       parameters_sl9DB = new List<DB_Parameter>(parameters_ewipDB);
                    parameters_sl9DB[0].type_data = SqlDbType.DateTime;
                    parameters_sl9DB[0].value = fromDate;
                    parameters_sl9DB[1].type_data = SqlDbType.DateTime;
                    parameters_sl9DB[1].value = toDate.AddDays(1);

                    DataTable data_table = new DataTable(),
                              job_mst = Global.getDataFrom_sl9DB("select * from " + Global.job_mst + " where job_date between @fromDate and @toDate and stat in('R', 'S')",
                                                                 parameters_sl9DB),
                              jobroute_mst = Global.getDataFrom_sl9DB("select * from " + Global.jobroute_mst + " where job in (select job from " + Global.job_mst + " where job_date between @fromDate and @toDate and stat in('R', 'S'))",
                                                                      parameters_sl9DB),
                              tbJobTransMains = Global.getDataFrom_eWipDB("select * from tbJobTransMain where _wc not in('STORER', 'MIX') and _job in(select _job from tbjob where _jobDate between @fromDate and @toDate)",
                                                                          parameters_ewipDB),
                              tbJobRoute = Global.getDataFrom_eWipDB("select * from tbJobRoute where _job in(select _job from tbjob where _jobDate between @fromDate and @toDate)",
                                                                     parameters_ewipDB);

                    DataRowCollection job_mst_row = job_mst.Rows;
                    int data_need_to_check_max = job_mst_row.Count;

                    //data_table.Columns.AddRange(new DataColumn[43]
                    data_table.Columns.AddRange(new DataColumn[40]
                    {
                        new DataColumn("Transaction", typeof(string)),
                        new DataColumn("Transaction Type", typeof(string)),
                        new DataColumn("Transaction Date", typeof(string)),
                        new DataColumn("Job", typeof(string)),
                        new DataColumn("Job Suffix", typeof(string)),
                        new DataColumn("Operation", typeof(int)),
                        new DataColumn("Item", typeof(string)),
                        new DataColumn("Item Description", typeof(string)),
                        new DataColumn("WC", typeof(string)),
                        new DataColumn("WC Description", typeof(string)),

                        new DataColumn("User Initials", typeof(string)),
                        new DataColumn("Employee", typeof(string)),
                        new DataColumn("Employee Name", typeof(string)),
                        new DataColumn("Shift", typeof(int)),
                        new DataColumn("Pay Type", typeof(string)),
                        new DataColumn("Pay Rate", typeof(decimal)),
                        new DataColumn("Job Cost Rate", typeof(decimal)),
                        new DataColumn("Indirect Code", typeof(int)),
                        new DataColumn("Description", typeof(string)),
                        new DataColumn("Completed", typeof(double)),
                        
                        new DataColumn("Scrapped", typeof(double)),
                        new DataColumn("Moved", typeof(double)),
                        new DataColumn("U/M", typeof(string)),
                        new DataColumn("Warehouse", typeof(string)),
                        new DataColumn("Reason", typeof(string)),
                        new DataColumn("Reason Description", typeof(string)),
                        new DataColumn("Next Operation", typeof(string)),
                        new DataColumn("Work Center", typeof(string)),
                        new DataColumn("Work Center Description", typeof(string)),
                        new DataColumn("Move To Location", typeof(string)),

                        new DataColumn("Location Description", typeof(string)),
                        new DataColumn("Lot", typeof(string)),
                        new DataColumn("Start Time", typeof(string)),
                        new DataColumn("End Time", typeof(string)),
                        new DataColumn("Total Hours", typeof(decimal)),
                        new DataColumn("Cost Code", typeof(string)),
                        new DataColumn("Control Point", typeof(string)),
                        new DataColumn("Oper Complete", typeof(int)),
                        new DataColumn("Close Job", typeof(string)),
                        new DataColumn("Issue to Parent", typeof(string))
                        
                        //, new DataColumn("Remark 1", typeof(string)),
                        //new DataColumn("Remark 2", typeof(string)),
                        //new DataColumn("Remark 3", typeof(string))
                    });

                    if (data_need_to_check_max > 0)
                    {
                        var eWIPdb = Global.ewipDB;
                        var tbWC = from data in eWIPdb.tbWcs select data;

                        int data_found_count = 0;

                        for (int data_need_to_check_progress = 0; data_need_to_check_progress <= data_need_to_check_max - 1; data_need_to_check_progress++)
                        {
                            var data = job_mst_row[data_need_to_check_progress];
                            string job = data["job"].ToString();
                            DataRow[] row_jobroute_mst = jobroute_mst.Select("job='" + job + "'", "oper_num");

                            foreach (var datum in row_jobroute_mst)
                            {
                                int oper_num = int.Parse(datum["oper_num"].ToString());
                                DataRow[] row_tbJobTransMains = tbJobTransMains.Select("_job='" + job + "' and _oper_num='" + oper_num + "'");

                                if (row_tbJobTransMains.Count() > 0)
                                {
                                    DataRow row = row_tbJobTransMains[0];
                                    double sl9db_complete = double.Parse(datum["qty_complete"].ToString()),
                                           sl9db_reject = double.Parse(datum["qty_scrapped"].ToString()),
                                           ewipdb_complete = double.Parse(row["_qty_complete"].ToString()),
                                           ewipdb_reject = double.Parse(row["_qty_scrapped"].ToString());

                                    if ((ewipdb_complete != sl9db_complete) || (ewipdb_reject != sl9db_reject))
                                    {
                                        string remarks1 = "N";
                                        double finale_complete = 0,
                                               finale_reject = 0;

                                        if (sl9db_complete != ewipdb_complete)
                                        {
                                            finale_complete = ewipdb_complete - sl9db_complete;

                                            if (sl9db_complete > ewipdb_complete)
                                            {
                                                remarks1 = "D";
                                            }
                                        }

                                        if (sl9db_reject != ewipdb_reject)
                                        {
                                            finale_complete = ewipdb_reject - sl9db_reject;

                                            if (sl9db_reject > ewipdb_reject)
                                            {
                                                remarks1 = "D";
                                            }
                                        }

                                        if (sl9db_reject <= ewipdb_reject)
                                        {
                                            remarks1 = "N";
                                        }

                                        if (((cbNoMatch == "on") && (finale_complete < 0 || finale_reject < 0)) ||
                                            (cbNoMatch == null && finale_complete >= 0 && finale_reject >= 0))
                                        {
                                            data_found_count++;
                                            string transactionType = "Run",
                                                   employee = string.Empty,
                                                   reason = string.Empty,
                                                   moveToLocation = string.Empty,
                                                   lot = "LOT",
                                                   wc = row["_wc"].ToString(),
                                                   item = row["_item"].ToString();
                                            decimal payRate = 0,
                                                    totalHours = 0;

                                            if (finale_reject != 0)
                                            {
                                                reason = "PF";
                                            }

                                            var wc_data_row = (from dummy in tbWC
                                                               where dummy._wc == wc
                                                               select dummy).ToList();

                                            DataRow[] present_jobroute_data_row = tbJobRoute.Select("_job='" + job + "' and _operationNo='" + oper_num + "'");
                                            DataRow[] transType = null;
                                            DB_Parameter mst_job = new DB_Parameter(), mst_oper_num = new DB_Parameter(), mst_prev_op = new DB_Parameter();
                                            mst_job.name = "@job";
                                            mst_job.type_data = SqlDbType.VarChar;
                                            mst_job.value = present_jobroute_data_row[0].ItemArray[1];

                                            mst_oper_num.name = "@oper_num";
                                            mst_oper_num.type_data = SqlDbType.Int;
                                            mst_oper_num.value = present_jobroute_data_row[0].ItemArray[3];

                                            List<DB_Parameter> parameters_TransType = new List<DB_Parameter>() { mst_job, mst_oper_num };

                                            try
                                            {
                                                DataTable mstTransType = Global.getDataFrom_sl9DB("select sched_drv from " + Global.jrt_sch_mst + " where job = @job and oper_num = @oper_num ", parameters_TransType);
                                                transType = mstTransType.Select();
                                            }
                                            catch (Exception ex)
                                            {
                                                return Json(new { error = "Job Type is not deteced" }, JsonRequestBehavior.AllowGet);
                                            }

                                            if (wc_data_row.Count > 0)
                                            {
                                                tbWc wc_row = wc_data_row[0];
                                                payRate = (decimal)wc_row._Run_Rate;
                                                payRate = Math.Round(payRate, 2);

                                               
                                                if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
                                                {
                                                    employee = "PROD";
                                                }
                                                else
                                                {
                                                    transactionType = "Machine";
                                                }
                                            }


                                            if (present_jobroute_data_row.Count() > 0)
                                            {


                                                DataRow jobroute_row = present_jobroute_data_row[0];

                                                if (transType[0].ItemArray[0].ToString().Trim().Equals("L"))
                                                {
                                                    totalHours = decimal.Parse(jobroute_row["_runlbrhrs"].ToString());
                                                }
                                                else
                                                {
                                                    totalHours = decimal.Parse(jobroute_row["_runmchhrs"].ToString());
                                                }

                                                totalHours *= (decimal)finale_complete;
                                            }

                                            int next_nearest_oper_num = int.Parse(row["_nextoper_num"].ToString());
                                            DataRow[] next_jobroute_data_row = tbJobRoute.Select("_job='" + job + "' and _operationNo>'" + oper_num + "'", "_operationNo");

                                            if (next_jobroute_data_row.Count() > 0)
                                            {
                                                foreach (var dummy in next_jobroute_data_row)
                                                {
                                                    int dummy_oper_num = int.Parse(dummy["_operationNo"].ToString());

                                                    if (dummy_oper_num < next_nearest_oper_num)
                                                    {
                                                        next_nearest_oper_num = dummy_oper_num;
                                                    }
                                                }

                                                lot = string.Empty;
                                            }
                                            else
                                            {
                                                next_nearest_oper_num = 0;
                                            }

                                            if (lot == "LOT" && item.Length >= 2)
                                            {
                                                string dummy_string = item.Substring(0, 2);

                                                if (dummy_string == "FG" || dummy_string == "AC")
                                                {
                                                    moveToLocation = "TL";
                                                }
                                                else
                                                {
                                                    moveToLocation = "FS";
                                                }
                                            }

                                            data_table.Rows.Add(string.Empty,
                                                                transactionType,
                                                                DateTime.FromOADate(double.Parse(row["_end_Date"].ToString())).ToString("dd/MM/yyyy"),
                                                                job,
                                                                "-",
                                                                oper_num,
                                                                item,
                                                                string.Empty,
                                                                wc,
                                                                wc,

                                                                "",
                                                                employee,
                                                                string.Empty,
                                                                1,
                                                                "Regular",
                                                                payRate,
                                                                payRate,
                                                                0,
                                                                string.Empty,
                                                                finale_complete,

                                                                finale_reject,
                                                                finale_complete,
                                                                string.Empty,
                                                                "MAIN",
                                                                reason,
                                                                string.Empty,
                                                                next_nearest_oper_num == 0 ? string.Empty : next_nearest_oper_num.ToString(),
                                                                string.Empty,
                                                                string.Empty,
                                                                moveToLocation,

                                                                string.Empty,
                                                                lot,
                                                                TimeSpan.FromSeconds(double.Parse(row["_start_time"].ToString())).ToString(),
                                                                TimeSpan.FromSeconds(double.Parse(row["_end_time"].ToString())).ToString(),
                                                                totalHours,
                                                                string.Empty,
                                                                string.Empty,
                                                                1,
                                                                string.Empty,
                                                                string.Empty

                                                                //, remarks1,
                                                //string.Empty,
                                                //string.Empty
                                                                );
                                        }
                                    }
                                }
                            }
                        }
                    }

                    using (XLWorkbook workBook = new XLWorkbook())
                    {
                        workBook.Worksheets.Add(data_table, "Export Data from eWIP");
                        MemoryStream memoryStream = new MemoryStream();
                        workBook.SaveAs(memoryStream);
                        memoryStream.Position = 0;
                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment; filename=" + fileName.ToString() + ".xls");
                        Response.ContentType = "application/ms-excel";
                        Response.BinaryWrite(memoryStream.ToArray());
                        Response.End();
                    }

                    break;
                default:
                    break;
            }

            return View();
        }
    }
}
