﻿using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace UltimoApps.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/

        //public ActionResult Index(string Id)
        //{
        //    var objDataAccess = new DAL.Repository();
        //    var Result = objDataAccess.GetMenuById(Id);
        //    return View(Result);
        //}
        //public ActionResult Error(string Id)
        //{
        //    return HttpNotFound("403");
        //}

        public ActionResult GetAllMenuForm(string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_Menu.getAllMenuFormData("", "", 0, 1000));

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllMenuFormByRoleID(string url, string roleId)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_Menu.GetAllMenuFormDataByRoleID(roleId));

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetAllMenuFormData(string filter, string searchKey, int skip, int take, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_search", url))
            {
                try
                {
                    MenuBindingModel result = new MenuBindingModel { Data = Sc_Menu.getAllMenuFormData(filter, searchKey, skip, take), Total = Sc_Menu.GetAllMenuFormDataTotal(filter, searchKey) };
                    var serializer = new JavaScriptSerializer();
                    var serializedResult = serializer.Serialize(result);

                    return Json(serializedResult, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getAllParentMenu()
        {
            try
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_Menu.getAllParentMenu());

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult updateMenuForm(Compact_MenuFormModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                try
                {
                    if (Sc_Menu.UpdateFromCompactModel(model))
                    {
                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { error = "Update Error : " + Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { error = ex.Message + " : " + ex.InnerException }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = "Update Error : " + Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult newMenuForm(Compact_MenuFormModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_create", url))
            {
                if (Sc_Menu.insertCompactModel(model))
                {
                    return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "Update Error : Inserted data not valid" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = "Update Error : " + Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult deleteMenuForm(string id, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_delete", url))
            {
                try
                {
                    if (Sc_MenuForm.delete(id))
                    {
                        return Json(new { success = "success" }, JsonRequestBehavior.AllowGet);
                    }
                    else {
                        return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch
                {
                    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = "Update Error : " + Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
