﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;


namespace UltimoApps.Controllers
{
    public class JobRouteController : Controller
    {

        private double RlQty;
        private double RlRej;
        private double RlOP;
        //
        // GET: /JobRoute/

        public ActionResult processDataEntry(Compact_DataEntryModel model, string url)
        {
            if (model.actionType != null)
            {
                var db = Global.ewipDB;

                bool flagStart = false; ;
                DateTime tempStart = DateTime.Now;
                Double doubleStart = 0;
                bool flagEnd = false;
                DateTime tempEnd = DateTime.Now;
                Double doubleEnd = 0;

                try
                {
                    tempStart = DateTime.Parse(model.start_Date.ToString());
                    doubleStart = tempStart.ToOADate();
                }
                catch
                {
                    flagStart = true;
                }
                try
                {
                    tempEnd = DateTime.Parse(model.end_Date.ToString());
                    doubleEnd = tempEnd.ToOADate();
                }
                catch
                {
                    flagEnd = true;
                }

                var getEmp = db.tbOperators.FirstOrDefault(x => x._operatorID == model.emp_num);
                var startTime = Convert.ToInt32(tempStart.TimeOfDay.TotalSeconds);
                var endTime = Convert.ToInt32(tempEnd.TimeOfDay.TotalSeconds);
                var curUser = mpMembership.GetCurrentUser();

                if (model.machineId == null)
                {
                    model.machineId = "-";
                }

                if (model.remarks == null)
                {
                    model.remarks = "";
                }

                if (model.actionType.ToUpper().Equals("UPDATE"))
                {
                    var temp = UpdateData(model, doubleStart, doubleEnd, startTime, endTime, flagStart, flagEnd);
                    if (temp != "")
                    {
                        return Json(new { error = temp }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
                }

                if (model.actionType.ToUpper().Equals("PAUSE") || model.actionType.ToUpper().Equals("RESUME"))
                {
                    var temp = PAUSE_SAVE(model, doubleStart, doubleEnd, startTime, endTime);
                    if (temp != "")
                    {
                        return Json(new { error = temp }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
                }

                if (model.jobNo == "" || model.jobNo == null)
                {
                    return Json(new { error = "Please enter the job no.!" }, JsonRequestBehavior.AllowGet);
                }

                if (model.oper_num == 0)
                {
                    return Json(new { error = "Please select operation no.!" }, JsonRequestBehavior.AllowGet);
                }

                if (model.tansnum == 0)
                {
                    return Json(new { error = "Please select the job no.!" }, JsonRequestBehavior.AllowGet);
                }

                bool chkbool = false;
                if (model.ARelQty == 0 && model.qty_op_qty == 0)
                {
                    chkbool = true;
                }

                if (chkbool == false)
                {
                    if (model.qty_op_qty == 0)
                    {
                        return Json(new { error = "Please enter received quantity!" }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (model.actionType.ToLower().Equals("rework"))
                {
                    doubleStart = DateTime.Now.ToOADate();
                }

                if (!model.actionType.ToLower().Equals("skip"))
                {
                    if (model.start_Date == null || flagStart == true)
                    {
                        return Json(new { error = "Please enter start date!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    doubleStart = DateTime.Now.ToOADate();
                }

                if (model.actionType.ToLower().Equals("completed"))
                {
                    if (model.number_of_operator == 0)
                    {
                        return Json(new { error = "Please enter the no. of operator!" }, JsonRequestBehavior.AllowGet);
                    }
                    if (model.end_Date == null || flagEnd == true)
                    {
                        return Json(new { error = "Please enter end date!" }, JsonRequestBehavior.AllowGet);
                    }
                    if (model.qty_complete == 0 && model.qty_scrapped == 0)
                    {
                        return Json(new { error = "Please check completed and rejected quantity!" }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (model.actionType.ToLower().Equals("completed") || model.actionType.ToLower().Equals("split"))
                {
                    var temp = SAVE_SPLIT(model, doubleStart, doubleEnd, startTime, endTime, flagStart, flagEnd);

                    if (temp != "")
                    {
                        return Json(new { error = temp }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
                }
                else if (model.actionType.ToLower().Equals("start") || model.actionType.ToLower().Equals("skip") || model.actionType.ToLower().Equals("rework"))
                {
                    var temp = START_SAVE(model, doubleStart, doubleEnd, startTime, endTime, flagStart, flagEnd);

                    if (temp != "")
                    {
                        return Json(new { error = temp }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "Please select an action" }, JsonRequestBehavior.AllowGet);
            }
        }

        private string SAVE_SPLIT(Compact_DataEntryModel model, double startDate, double endDate, int startTime, int endTime, bool flagStart, bool flagEnd)
        {
            if (model.jobNo == "")
            {
                return "Please enter the job no.!";
            }

            if (model.tansnum == 0)
            {
                return "Please select operation no.!";
            }

            if (model.oper_num == 0)
            {
                return "Please select operation no.!";
            }

            if (model.number_of_operator == 0)
            {
                return "Please enter the no. of operator!";
            }

            if (model.qty_op_qty == 0)
            {
                return "Please enter received quantity!";
            }

            if (model.start_Date == null || flagStart == true)
            {
                return "Please enter start date!";
            }

            if (model.actionType.ToLower().Equals("completed") || model.actionType.ToLower().Equals("split"))
            {
                if (model.end_Date == null || flagEnd == true)
                {
                    return "Please enter end date!";
                }
                if (model.qty_complete == 0 && model.qty_scrapped == 0)
                {
                    return "Please check completed and rejected quantity!";
                }

                if (model.actionType.ToLower().Equals("completed"))
                {
                    if (model.qty_op_qty != model.qty_complete + model.qty_scrapped)
                    {
                        return "Please check completed and rejected quantity!";
                    }
                }
                else
                {
                    if (model.qty_complete == 0 && model.qty_scrapped == 0)
                    {
                        return "Please check completed and rejected quantity!";
                    }
                    if (model.qty_op_qty == model.qty_complete + model.qty_scrapped)
                    {
                        return "Please check completed and rejected quantity!";
                    }
                }
                if (model.qty_scrapped != model.total_rejected)
                {
                    return "Please check rejected quantity!";
                }
            }

            string strspt = GetMaxSufID(model.jobNo, model.jobSuffixParent, model.jobSuffix, model.oper_num);
            if (model.actionType.ToLower().Equals("split"))
            {
                if (strspt == "")
                {
                    return "Split only three level only,Please check your admin!";
                }
                else
                {
                    model.jobSuffixParent = model.jobSuffix;
                }
            }
            bool splitst = false;
            if (!model.actionType.ToLower().Equals("split"))
            {
                if (CheckSplitCon(model.tansnum, model.jobNo, model.oper_num, model.jobSuffixParent, model.jobSuffix))
                {
                    if (strspt == "")
                    {
                        return "Split only three level only,Please check your admin!";
                    }
                    else
                    {
                        splitst = true;
                        model.jobSuffixParent = model.jobSuffix;
                        model.jobSuffix = strspt;
                    }
                }
            }

            var db = Global.ewipDB;
            var getEmp = db.tbOperators.FirstOrDefault(x => x._operatorID == model.emp_num);
            var curUser = mpMembership.GetCurrentUser();

            string strSupNew = "";
            string tempVal = "";
            if (!model.actionType.ToLower().Equals("split"))
            {
                tempVal = model.jobSuffix;
                strSupNew = model.jobSuffix;
            }
            else
            {
                strSupNew = strspt;
                tempVal = strspt;
            }

            int tempValOP = 0;
            if (model.number_of_operator == 0)
            {
                tempValOP = 1;
            }
            else
            {
                tempValOP = model.number_of_operator;
            }

            double tempARelQty = 0;
            var tempSplitAction = 0;
            if (splitst == false)
            {
                tempSplitAction = 0;
                tempARelQty = model.ARelQty;
            }
            else
            {
                tempSplitAction = 1;
                tempARelQty = model.qty_complete + model.qty_scrapped;
            }

            if (!model.actionType.ToLower().Equals("split"))
            {
                db.sp_JOB_COMPLETED_UPDATED_new(model.tansnum, model.jobSuffix, model.jobSuffixParent, getEmp._operatorID, getEmp._operatorName,
                    endDate, endTime, tempValOP, model.qty_complete, model.qty_scrapped, model.actionType, model.remarks, tempSplitAction, tempARelQty);
            }
            else
            {
                db.sp_JOB_SPLIT_UPDATED_new(model.tansnum, tempVal, model.jobSuffixParent, getEmp._operatorID, getEmp._operatorName,
                    endDate, endTime, tempValOP, model.qty_complete, model.qty_scrapped, model.actionType, model.remarks, tempSplitAction, tempARelQty);
            }

            Delete_rejected(model.tansnum);

            var serealizer = new JavaScriptSerializer();
            var getReject = new List<modelJobTrans>();

            if (model.listRejected != null)
            {
                getReject = serealizer.Deserialize<List<modelJobTrans>>(model.listRejected);
            }

            foreach (var itemRej in getReject)
            {
                db.sp_JOB_REJECTED_UPDATED((int)itemRej._refID, itemRej._RejectedCode, itemRej._RejectedDesc, (int)itemRej._RejectedQty);
            }

            return "";
        }

        private string START_SAVE(Compact_DataEntryModel model, double startDate, double endDate, int startTime, int endTime, bool flagStart, bool flagEnd)
        {
            try
            {
                var db = Global.ewipDB;
                var getEmp = db.tbOperators.FirstOrDefault(x => x._operatorID == model.emp_num);
                var curUser = mpMembership.GetCurrentUser();

                if (model.start_Date == null || flagStart == true)
                {
                    return "Please check start date!";
                }

                if (model.actionType == "START")
                {

                    db.sp_JOB_START_ADD_new(model.tansnum, model.jobNo, model.jobSuffix, model.jobSuffixParent, model.item,
                        model.wc, startDate, model.ARelQty, model.qty_op_qty, model.oper_num, getEmp._operatorID, getEmp._operatorName,
                        startTime, curUser._userID, curUser._userName, model.actionType, model.machineId, model.remarks);
                    return "";

                }
                else if (model.actionType == "REWORK")
                {
                    createReworkPost(model.jobNo, model.oper_num);
                    var minOP = (from x in db.tbJobRoutes
                                 where x._job == model.jobNo
                                 select x).Min(min => min._operationNo);

                    db.sp_JOB_Rework_ADD_new(model.tansnum, model.jobNo, model.jobSuffix, model.jobSuffixParent, model.item,
                        model.wc, startDate, model.qty_op_qty, model.qty_op_qty, model.oper_num, getEmp._operatorID, getEmp._operatorName,
                        startTime, curUser._userID, curUser._userName, model.actionType, model.machineId, minOP, curUser._userName, model.remarks);
                    return "";

                }
                else
                {
                    db.sp_JOB_SKIP_ADD_new(model.tansnum, model.jobNo, model.jobSuffix, model.jobSuffixParent, model.item, model.wc
                        , startDate, model.ARelQty, model.qty_op_qty, model.oper_num, model.emp_num, getEmp._operatorName, startTime,
                        curUser._userID, curUser._userName, model.actionType, model.machineId, model.remarks);
                    return "";
                }
            }
            catch (Exception ex)
            {
                return ex.StackTrace;
            }

        }

        private void createReworkPost(string jobNo, int operationNum)
        {
            var ultDB = Global.db;
            var ewipDB = Global.ewipDB;
            var previousOPNumber = getOperNumBeforeOperation(jobNo, operationNum);
            var oldJobTranValue = ewipDB.tbJobTrans.FirstOrDefault(x => x._job == jobNo && x._oper_num == previousOPNumber);
            var checkIfAlreadyRework = ultDB.Tmp_ReworkPostingTables.FirstOrDefault(x => x.job == jobNo && x.oper_num == operationNum);

            if (checkIfAlreadyRework != null)
            {
                checkIfAlreadyRework.needPosting = 1;
                checkIfAlreadyRework.QtyCompBeforeRework = oldJobTranValue._qty_complete;
                checkIfAlreadyRework.QtyScrapBeforeRework = oldJobTranValue._qty_scrapped;
                checkIfAlreadyRework.CreatedDate = DateTime.Now;
            }
            else
            {
                var newRework = new Tmp_ReworkPostingTable();
                newRework.job = jobNo;
                newRework.needPosting = 1;
                newRework.oper_num = operationNum;
                newRework.QtyCompBeforeRework = oldJobTranValue._qty_complete;
                newRework.QtyScrapBeforeRework = oldJobTranValue._qty_scrapped;
                newRework.CreatedDate = DateTime.Now;
                ultDB.Tmp_ReworkPostingTables.InsertOnSubmit(newRework);
            }

            ultDB.SubmitChanges();
        }

        private int getOperNumBeforeOperation(string jobNo, int operationNum)
        {
            return Global.ewipDB.tbJobTrans.OrderByDescending(x => x._oper_num)
                .FirstOrDefault(x => x._oper_num < operationNum && x._job == jobNo)._oper_num;
        }

        private string PAUSE_SAVE(Compact_DataEntryModel model, double startDate, double endDate, int startTime, int endTime)
        {
            try
            {
                var db = Global.ewipDB;
                var parm = (model.actionType == "PAUSE") ? 0 : 1;
                var get_ServerDate = DateTime.Now.ToOADate();

                db.sp_JOB_PAUSE_UPDATED(model.tansnum, parm, get_ServerDate, get_ServerDate, model.remarks);
                return "";
            }
            catch
            {
                return Global.ErrorMessage;
            }
        }

        private bool Delete_rejected(int idno)
        {
            try
            {
                var db = Global.ewipDB;
                var delCand = db.tbRejectedTrans.Where(x => x._refID == idno).FirstOrDefault();
                db.tbRejectedTrans.DeleteOnSubmit(delCand);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private string UpdateData(Compact_DataEntryModel model, double startDate, double endDate, int startTime, int endTime, bool flagStart, bool flagEnd)
        {
            var db = Global.ewipDB;
            try
            {
                if (model.start_Date == null || flagStart == true)
                {
                    return "Please check start date!";
                }

                if (model.end_Date == null || flagEnd == true)
                {
                    return "Please check end date!";
                }

                if (startDate > endDate)
                {
                    return "please check start date!";
                }

                var getEmp = db.tbOperators.FirstOrDefault(x => x._operatorID == model.emp_num);
                var curUser = mpMembership.GetCurrentUser();

                db.sp_JOB_JOB_UPDATED_new(model.ckbRemarks, model.tansnum, model.machineId, getEmp._operatorID, getEmp._operatorName,
                    model.oper_num, model.ARelQty, model.qty_op_qty, model.qty_complete, model.qty_scrapped, startDate, startTime, endDate, endTime
                    , model.remarks);

                Delete_rejected(model.tansnum);

                var newHash = (from x in db.tbRejectedTrans where x._refID == model.tansnum select x).ToList();

                foreach (var item in newHash)
                {
                    db.sp_JOB_REJECTED_UPDATED(model.tansnum, item._RejectedCode, item._RejectedDesc, item._RejectedQty);
                }

                AllTotalHisData(model.jobNo, model.oper_num);
                return "";
            }
            catch
            {
                return Global.ErrorMessage;
            }
        }

        private bool CheckSplitCon(decimal tansnum, string jobNo, float operNum, string suffixParent, string suffix)
        {
            var db = Global.ewipDB;
            var getDataSet = (from x in db.tbJobTrans
                              where x._tansnum != tansnum
                              && x._job == jobNo
                              && x._oper_num == operNum
                              && x._jobsuffixParent == suffix
                              select x).ToList();

            if (getDataSet.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string GetMaxSufID(string jobNumber, string parent, string suffix, double operNum)
        {
            var db = Global.ewipDB;
            String textSubVal = "''";

            DB_Parameter parameterJob = new DB_Parameter(), parameterParent = new DB_Parameter(), parameterOper = new DB_Parameter();
            parameterJob.name = "@job";
            parameterJob.type_data = SqlDbType.VarChar;
            parameterJob.value = jobNumber;

            parameterParent.name = "@parent";
            parameterParent.type_data = SqlDbType.VarChar;
            parameterParent.value = suffix;

            parameterOper.name = "@operationNum";
            parameterOper.type_data = SqlDbType.Int;
            parameterOper.value = operNum;

            List<DB_Parameter> parameters_ewipDB = new List<DB_Parameter>() { parameterJob, parameterParent, parameterOper };

            DataTable DTCheckChiled = Global.getDataFrom_eWipDB("select * from tbJobTrans where _job = @job and _jobsuffixParent = @parent and _oper_num = @operationNum", parameters_ewipDB);
            DataRow[] DRCheckChiled = DTCheckChiled.Select();
            if (DRCheckChiled.Length > 0)
            {
                foreach (var item in DRCheckChiled)
                {
                    if (textSubVal.Equals("''"))
                    {
                        textSubVal = "'" + item.ItemArray[2] + "'";
                    }
                    else
                    {
                        textSubVal += ",'" + item.ItemArray[2] + "'";
                    }
                }
            }

            List<DB_Parameter> parameters_MinName = new List<DB_Parameter>() { parameterParent };
            DataTable DTminName = Global.getDataFrom_eWipDB("select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent= @parent and _lel_1_Name not in (" + textSubVal + ")", parameters_MinName);
            DataRow[] DRminName = DTminName.Select();

            return DRminName[0].ItemArray[0].ToString();
        }

        private void AllTotalHisData(string HissrtJobNo, int HIsstrop)
        {

            Double TotalC_qtry = 0;
            Double TotalR_qtry = 0;
            Double TotalP_qtry = 0;
            HashSet<object> HashT = new HashSet<object>();

            var db = Global.ewipDB;

            var DSMH = (from x in db.tbJobTransHis
                        where x._job == HissrtJobNo
                        && x._oper_num == HIsstrop
                        select x).ToList().OrderBy(order => order._reworkCount);
            if (DSMH.Count() > 0)
            {
                foreach (var itemDSMH in DSMH)
                {
                    if (HashT.Contains(itemDSMH._jobsuffix) == false)
                    {
                        TotalP_qtry = TotalP_qtry + itemDSMH._qty_op_qty;
                        TotalC_qtry = TotalC_qtry + itemDSMH._qty_complete;
                        TotalR_qtry = TotalR_qtry + itemDSMH._qty_scrapped;

                        var maxOperNum = (from maxJT in db.tbJobTrans
                                          where maxJT._job == HissrtJobNo && maxJT._jobsuffix == itemDSMH._jobsuffix
                                          && maxJT._oper_num <= itemDSMH._oper_num
                                          select maxJT).ToList().Max(max => max._oper_num);

                        var resultSetJob = (from x in db.tbJobTrans
                                            where x._job == HissrtJobNo && x._jobsuffix == itemDSMH._jobsuffix
                                            && x._oper_num == maxOperNum
                                            select x).ToList();

                        if (resultSetJob.Count() > 0)
                        {
                            if (resultSetJob[0]._oper_num == HIsstrop)
                            {
                                if (resultSetJob[0]._status != "COMPLETED")
                                {
                                    TotalP_qtry = TotalP_qtry - resultSetJob[0]._qty_op_qty;
                                    TotalC_qtry = TotalC_qtry - resultSetJob[0]._qty_op_qty;
                                }
                                else
                                {
                                    TotalP_qtry = TotalP_qtry + resultSetJob[0]._qty_op_qty;
                                    TotalC_qtry = TotalC_qtry - resultSetJob[0]._qty_op_qty;
                                    TotalR_qtry = TotalR_qtry + resultSetJob[0]._qty_op_qty;
                                }
                            }
                            else
                            {
                                TotalP_qtry = TotalP_qtry - resultSetJob[0]._qty_op_qty;
                                TotalC_qtry = TotalC_qtry - resultSetJob[0]._qty_op_qty;
                            }
                        }

                        var DSRej1 = (from sumQty in db.tbJobTrans
                                      where sumQty._job == HissrtJobNo
                                      && sumQty._oper_num <= HIsstrop
                                      && sumQty._jobsuffix == itemDSMH._jobsuffix
                                      && sumQty._reworkCount > itemDSMH._reworkCount
                                      select sumQty).ToList().Sum(sum => sum._qty_scrapped);

                        if (DSRej1 != 0)
                        {
                            if (DSRej1 != 0)
                            {
                                TotalP_qtry = TotalP_qtry - DSRej1;
                                TotalC_qtry = TotalC_qtry - DSRej1;
                            }

                            var DSRej2 = (from sumQty in db.tbJobTransHis
                                          where sumQty._job == HissrtJobNo && sumQty._oper_num < HIsstrop
                                          && sumQty._jobsuffix == itemDSMH._jobsuffix
                                          && sumQty._reworkCount > itemDSMH._reworkCount
                                          select sumQty).ToList().Sum(sum => sum._qty_scrapped);

                            if (DSRej2 != 0)
                            {
                                if (DSRej2 != 0)
                                {
                                    TotalP_qtry = TotalP_qtry - DSRej2;
                                    TotalC_qtry = TotalC_qtry - DSRej2;
                                }
                            }
                        }
                        HashT.Add(itemDSMH._jobsuffix);
                    }
                }

                var DSMHJT = (from x in db.tbJobTrans
                              where x._job == HissrtJobNo && x._oper_num == HIsstrop
                              && x._status == "COMPLETED"
                              select x).ToList();

                foreach (var itemDSMHJT in DSMHJT)
                {
                    if (HashT.Contains(itemDSMHJT._jobsuffix) == false)
                    {
                        TotalC_qtry = TotalC_qtry + itemDSMHJT._qty_complete;
                        TotalR_qtry = TotalR_qtry + itemDSMHJT._qty_scrapped;
                        TotalP_qtry = TotalP_qtry + itemDSMHJT._qty_op_qty;
                        HashT.Add(itemDSMHJT._jobsuffix);
                    }
                }
            }
            else
            {
                var DSMHOuter = (from x in db.tbJobTrans
                                 where x._job == HissrtJobNo && x._oper_num == HIsstrop
                                 && x._status == "COMPLETED"
                                 select x).ToList().OrderBy(order => order._reworkCount);

                if (DSMHOuter.Count() > 0)
                {
                    foreach (var itemOuter in DSMHOuter)
                    {
                        TotalC_qtry = TotalC_qtry + itemOuter._qty_complete;
                        TotalR_qtry = TotalR_qtry + itemOuter._qty_scrapped;
                        TotalP_qtry = TotalP_qtry + itemOuter._qty_op_qty;
                    }
                }
            }

            RlQty = TotalC_qtry;
            RlRej = TotalR_qtry;
            RlOP = TotalP_qtry;

            if (RlQty != 0 && RlRej != 0 || RlOP != 0)
            {
                var getMain = db.tbJobTransMains.Where(x => x._job == HissrtJobNo && x._oper_num == HIsstrop).FirstOrDefault();
                getMain._qty_op_qty = RlOP;
                getMain._qty_complete = RlQty;
                getMain._qty_scrapped = RlRej;

                db.SubmitChanges();
            }

            RlQty = 0;
            RlRej = 0;
            RlOP = 0;
        }

        public ActionResult getCurrentRole()
        {
            return Json(new { role = mpMembership.GetCurrentRole(), name = Json(mpMembership.GetCurrentUsername(), JsonRequestBehavior.AllowGet) }, JsonRequestBehavior.AllowGet);
        }

        private bool CloseAllCase(string strJobNo, string strSuf, string strSufP, int strOPNum)
        {
            //try
            //{
            var db = Global.ewipDB;
            var resultSet = (from x in db.tbJobRoutes
                             where x._job == strJobNo && x._operationNo == strOPNum
                             select x).ToList().OrderBy(y => y._operationNo);

            if (resultSet.Count() > 0)
            {
                var i = 0;
                foreach (var item in resultSet)
                {
                    var opNo = 0;
                    if (resultSet.Count() > i + 1)
                    {
                        opNo = resultSet
                            .OrderBy(x => x._operationNo)
                            .Where(y => y._operationNo > item._operationNo).FirstOrDefault()._operationNo;
                    }
                    else
                    {
                        opNo = 0;
                    }
                    db.sp_JOB_Close_Com_is_Zero(strJobNo, strSuf, strSufP, strOPNum, item._operationNo, item._wc, opNo);

                    i++;
                }
            }

            return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }

        public ActionResult getRejectedCodeAll(string wc, string item)
        {
            var db = Global.ewipDB;
            var rejectedCode = db.getAllRejectedCodeByWCandItem(wc, item).ToList();

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(rejectedCode);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getRejectedTransData(int tansnum)
        {
            try
            {
                var db = Global.ewipDB;
                var rejectedTrans = (from x in db.tbRejectedTrans
                                     where x._refID == tansnum
                                     select x).ToList();

                var sumRT = rejectedTrans.Sum(sum => sum._RejectedQty);

                var serealizer = new JavaScriptSerializer();
                var serealizedResult = serealizer.Serialize(rejectedTrans);

                return Json(new { List = new { Data = serealizedResult, Total = sumRT } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult processRejectData(tbRejectedTran model, string url, string command)
        {
            return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getJobInformation(string jobNo)
        {
            try
            {
                var db = Global.ewipDB;
                var result = (from x in db.tbJobs
                              join y in db.tbItems on x._item equals y._itemCode
                              where x._job == jobNo
                              select new
                              {
                                  _itemCode = y._itemCode,
                                  _itemName = y._itemdescription,
                                  _qtyReleased = x._qtyReleased,
                                  _jobDate = x._jobDate
                              }
                              ).FirstOrDefault();

                if (result != null)
                {
                    modelParsedJobDetails resultParsed = new modelParsedJobDetails();
                    resultParsed._description = result._itemName;
                    resultParsed._itemCode = result._itemCode;
                    resultParsed._qty_Rele_qty = result._qtyReleased;
                    var holder = Convert.ToInt32(result._jobDate);
                    resultParsed._jobDateParse = (DateTime.FromOADate(holder).ToString()).Substring(0, 9);

                    var serializer = new JavaScriptSerializer();
                    var serializedResult = serializer.Serialize(resultParsed);

                    return Json(serializedResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getAllJobLineNumber(string jobNo)
        {
            var db = Global.ewipDB;
            var result = (from x in db.tbJobRoutes where x._job == jobNo select x).ToList();

            var serializer = new JavaScriptSerializer();
            var serializedItem = serializer.Serialize(result);

            return Json(serializedItem, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllJobLineDetail(string jobNo, int lineNo)
        {
            var db = Global.ewipDB;
            var result = (from x in db.tbJobTrans
                          join y in db.tbItems on x._item equals y._itemCode
                          join z in db.tbJobs on x._job equals z._job
                          where x._job == jobNo && x._oper_num == lineNo
                          select new
                          {
                              _tansnum = x._tansnum,
                              _job = x._job,
                              _jobsuffix = x._jobsuffix,
                              _jobsuffixParent = x._jobsuffixParent,
                              _operationNo = x._oper_num,
                              _wc = x._wc,
                              _qty_Rele_qty = x._qty_Rele_qty,
                              _qty_op_qty = x._qty_op_qty,
                              _qty_scrapped = x._qty_scrapped,
                              _qty_complete = x._qty_complete,
                              _jobDate = x._jobDate,
                              _itemCode = y._itemCode,
                              _description = y._itemdescription,
                              _status = x._status,
                              _reworkst = x._Reworkst,
                              _noOperator = x._no_oper,
                              _startDate = x._start_Date,
                              _startTime = x._start_time,
                              _endDate = x._end_Date,
                              _endTIme = x._end_time
                          }
                          ).ToList();

            List<modelParsedJobDetails> listItem = new List<modelParsedJobDetails>();
            foreach (var item in result)
            {
                modelParsedJobDetails getItem = new modelParsedJobDetails();
                getItem._tansnum = item._tansnum;
                getItem._job = item._job;
                getItem._jobsuffix = item._jobsuffix;
                getItem._jobsuffixParent = item._jobsuffixParent;
                getItem._wc = item._wc;
                getItem._qty_Rele_qty = item._qty_Rele_qty;
                getItem._qty_op_qty = item._qty_op_qty;
                getItem._qty_complete = item._qty_complete;
                getItem._qty_scrapped = item._qty_scrapped;
                getItem._itemCode = item._itemCode;
                getItem._description = item._description;
                getItem._operationNo = item._operationNo;
                getItem._status = item._status;
                getItem._numberOp = item._noOperator;

                var tempStart = TimeSpan.FromSeconds(double.Parse(item._startTime)).ToString();
                var tempEnd = TimeSpan.FromSeconds(double.Parse(item._endTIme.ToString())).ToString();
                if (item._startDate != 0)
                {
                    //this method cuts decimal
                    //var holder = Convert.ToInt32(item._startDate);
                    var temp1 = DateTime.FromOADate(item._startDate).ToString("dd/MM/yyyy");
                    string Date = temp1 + " " + tempStart.ToString();
                    getItem.startDate = DateTime.ParseExact(Date, "dd/MM/yyyy HH:mm:ss", null);
                }
                else
                {
                    getItem.startDate = null;
                }

                if (item._endDate != 0)
                {
                    //this method cuts decimal
                    //var holder = Convert.ToInt32(item._endDate);
                    var temp1 = DateTime.FromOADate(item._endDate).ToString("dd/MM/yyyy");
                    var Date = temp1 + " " + tempEnd.ToString();
                    getItem.endDate = DateTime.ParseExact(Date, "dd/MM/yyyy HH:mm:ss", null);
                }
                else
                {
                    getItem.endDate = null;
                }

                if (item._reworkst != "")
                {
                    getItem._reworkst = " - " + item._reworkst;
                }
                else
                {
                    getItem._reworkst = item._reworkst;
                }

                var tempHolder = Convert.ToInt32(item._jobDate);
                getItem._jobDateParse = (DateTime.FromOADate(tempHolder).ToString()).Substring(0, 9);

                listItem.Add(getItem);
            }

            var serializer = new JavaScriptSerializer();
            var serializedItem = serializer.Serialize(listItem);

            return Json(serializedItem, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobNoPrint(string JobNo)
        {


            //var prn = new ClsAMTprint.clsmain();
            //string restr = "";
            //string[] s = JobNo.Split('-');

            //if (s.Length > 2)
            //{
            //    restr = "-R";
            //}

            //if (prn.printAMT((s[0].Trim() + ('-' + s[1].Trim())), Global.eWipConnString, "PDFCreator", true))
            //{
            //    return Json(new { success = "Please Collect your print copy in level-1" }, JsonRequestBehavior.AllowGet);
            //}
            //else if (prn.printAMT((s[0].Trim() + ('-' + (s[1] + restr).Trim())), Global.eWipConnString, "PDFCreator", false))
            //{
            //    return Json(new { success = "Please Collect your print copy in level-1" }, JsonRequestBehavior.AllowGet);
            //}

            return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);

        }

    }

    internal class modelJobTrans
    {
        public decimal _rejid { get; set; }

        public decimal _refID { get; set; }

        public string _RejectedCode { get; set; }

        public string _RejectedDesc { get; set; }

        public float _RejectedQty { get; set; }
    }

    internal class modelParsedJobDetails
    {

        public decimal _tansnum { get; set; }

        public string _job { get; set; }

        public string _jobsuffix { get; set; }

        public string _jobsuffixParent { get; set; }

        public string _wc { get; set; }

        public double _operationNo { get; set; }

        public double _qty_Rele_qty { get; set; }

        public double _qty_op_qty { get; set; }

        public double _qty_scrapped { get; set; }

        public double _qty_complete { get; set; }

        public string _jobDateParse { get; set; }

        public string _description { get; set; }

        public string _itemCode { get; set; }

        public string _status { get; set; }

        public string _reworkst { get; set; }

        public int _numberOp { get; set; }

        public DateTime? startDate { get; set; }

        public DateTime? endDate { get; set; }
    }
}
