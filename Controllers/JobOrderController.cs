﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class JobOrderController : Controller
    {
        //
        // GET: /JobOrder/
                
        [HttpPost]
        public ActionResult GetAllJobOrderData(string jobNo, string fromDate, string toDate, int skip, int take)
        {
            var db = Global.ewipDB;
            var JobData = (from x in db.tbJobs select x).ToList();

            if (jobNo != "")
            {
                JobData = JobData.Where(m => m._job.Contains(jobNo)).ToList();
            }
            else
            {
                if (fromDate != "")
                {
                    var dateFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    var doubleFrom = dateFrom.ToOADate();
                    JobData = JobData.Where(m => m._jobDate >= doubleFrom).ToList();
                }

                if (toDate != "")
                {
                    var dateTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    dateTo = dateTo.AddDays(1);
                    var doubleTo = dateTo.ToOADate();
                    JobData = JobData.Where(m => m._jobDate <= doubleTo).ToList();
                }
            }            
            
            List<JobOrderModel> ParsedModel = new List<JobOrderModel>();

            foreach( var Job in JobData)
            {
                JobOrderModel newModel = new JobOrderModel();
                newModel._job = Job._job;
                newModel._item = Job._item;
                newModel._jobDate = Job._jobDate;
                newModel._jobsuffix = Job._jobsuffix;
                newModel._jobtype = Job._jobtype;
                newModel._qtyCompleted = Job._qtyCompleted;
                newModel._qtyReleased = Job._qtyReleased;
                newModel._qtyScrapped = Job._qtyScrapped;
                newModel._status = Job._status;
                var holder = Convert.ToInt32(Job._jobDate);
                newModel.jobDateParsed = DateTime.FromOADate(holder);

                ParsedModel.Add(newModel);
            }

            var serializer = new JavaScriptSerializer();

            model_JobOrder Holder = new model_JobOrder { Data = ParsedModel.Skip(skip).Take(take).ToList(), Total = ParsedModel.Count() };
            var serializedResult = serializer.Serialize(Holder);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllJobDetailsData(string job, int skip, int take)
        {
            var db = Global.ewipDB;
            var JobDetailsData = (from x in db.tbJobRoutes
                                  join y in db.tbJobs on x._job equals y._job
                                  join z in db.tbWcs on x._wc equals z._wc
                                  select new
                                  {
                                      _job = y._job,
                                      _item = y._item,
                                      _jobDate = y._jobDate,
                                      _jobsuffix = y._jobsuffix,
                                      _jobtype = y._jobtype,
                                      _qtyCompleted = y._qtyCompleted,
                                      _qtyReleased = y._qtyReleased,
                                      _qtyScrapped = y._qtyScrapped,
                                      _status = y._status,
                                      _wc = z._wc,
                                      _description = z._description,
                                      _dept = z._dept
                                  }).ToList();

            List<JobDetailModel> ParsedModel = new List<JobDetailModel>();

            foreach (var JobDetails in JobDetailsData)
            {
                JobDetailModel newModel = new JobDetailModel();
                newModel._job = JobDetails._job;
                newModel._item = JobDetails._item;
                newModel._jobDate = JobDetails._jobDate;
                newModel._jobsuffix = JobDetails._jobsuffix;
                newModel._jobtype = JobDetails._jobtype;
                newModel._qtyCompleted = JobDetails._qtyCompleted;
                newModel._qtyReleased = JobDetails._qtyReleased;
                newModel._qtyScrapped = JobDetails._qtyScrapped;
                newModel._status = JobDetails._status;
                newModel._wc = JobDetails._wc;
                newModel._description = JobDetails._description;
                newModel._dept = JobDetails._dept;
                var holder = Convert.ToInt32(JobDetails._jobDate);
                newModel.jobDateParsed = DateTime.FromOADate(holder);

                ParsedModel.Add(newModel);
            }

            var detailJob = ParsedModel.Where(x => x._job == job).ToList();

            var serializer = new JavaScriptSerializer();

            model_JobDetailsOrder Holder = new model_JobDetailsOrder { Data = detailJob.Skip(skip).Take(take).ToList(), Total = detailJob.Count() };
            var serializedResult = serializer.Serialize(Holder);

            return Json(serializedResult, JsonRequestBehavior.AllowGet);
        }
                
    }
}
