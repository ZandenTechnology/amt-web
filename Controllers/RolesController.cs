﻿using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using UltimoApps.Models.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace UltimoApps.Controllers
{
    public class RolesController : Controller
    {
        //
        // GET: /Roles/


        public ActionResult GetAllRolesData(string url, string filter, string searchKey, int skip, int take)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                RolesBindingModel result = new RolesBindingModel { Data = Sc_Role.GetAllRolesData(filter, searchKey, skip, take), Total = Sc_Role.GetAllRolesDataTotal(filter, searchKey) };

                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(result);

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult InsertRole(RoleModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_create", url))         
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var user = mpMembership.GetCurrentUser();

                        var db = Global.db;

                        Sc_Role newRole = new Sc_Role
                        {
                            RoleID = model.RoleID,
                            Role = model.Role,
                            RoleDesc = model.Description,
                            InputDate = DateTime.Now,
                            InputUser = user._name
                        };

                        db.Sc_Roles.InsertOnSubmit(newRole);

                        //Role Menu
                        var serializer = new JavaScriptSerializer();
                        var serializedMenu = serializer.Deserialize<List<RoleMenu>>(model.ListMenuID);
                        var serializedCtrl = serializer.Deserialize<List<RolePermission>>(model.ListControlID);

                        var m = mpMembership.GetCurrentUser();

                        foreach (var menu in serializedMenu)
                        {
                            Sc_RoleMenu newItem = new Sc_RoleMenu
                            {
                                RolesID = model.RoleID,
                                MenuID = menu.MenuID,
                                IsActive = true,
                                InputDate = DateTime.Now,
                                InputUser = m._name
                            };

                            db.Sc_RoleMenus.InsertOnSubmit(newItem);
                        }

                        foreach (var ctrl in serializedCtrl)
                        {
                            Sc_RolePermission newItem = new Sc_RolePermission
                            {
                                RoleID = model.RoleID,
                                CtrlID = ctrl.CtrlID,
                                IsActive = true,
                                InputDate = DateTime.Now,
                                InputUser = m._name
                            };

                            db.Sc_RolePermissions.InsertOnSubmit(newItem);
                        }

                        db.SubmitChanges();

                        return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);


                    }
                    catch
                    {
                        //return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                        return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    //["Status"] = Global.FillMessage;
                    return Json(new { error = Global.FillMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult UpdateRole(RoleModel model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_edit", url))
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var user = mpMembership.GetCurrentUser();
                        var db = Global.db;

                        var getRole = db.Sc_Roles.FirstOrDefault(item => item.RoleID == model.RoleID);

                        getRole.Role = model.Role;
                        getRole.RoleDesc = model.Description;
                        getRole.RoleType = model.RoleType;
                        getRole.ModifDate = DateTime.Now;
                        getRole.ModifUser = user._name;
                        getRole.IsActive = model.IsActive;

                        //List<Sc_RolePermission> listHasCtrl = (from x in db.Sc_RolePermissions
                        //                             where x.RoleID == model.RoleID
                        //                             select x).ToList();

                        //List<Sc_RoleMenu> listHasMenu = (from x in db.Sc_RoleMenus
                        //                                       where x.RolesID == model.RoleID
                        //                                       select x).ToList();
                        
                        //Role Menu
                        List<RoleMenu> serializedMenu = null;
                        List<RolePermission> serializedCtrl = null;

                        var serializer = new JavaScriptSerializer();

                        if (model.ListMenuID != null)
                        {
                            serializedMenu = serializer.Deserialize<List<RoleMenu>>(model.ListMenuID);
                        }

                        if (model.ListControlID != null)
                        {
                            serializedCtrl = serializer.Deserialize<List<RolePermission>>(model.ListControlID);
                        }

                        var m = mpMembership.GetCurrentUser();

                        foreach (var menu in serializedMenu)
                        {
                            var getMenu = db.Sc_RoleMenus.FirstOrDefault(item => item.RolesID == menu.RoleID && item.MenuID == menu.MenuID);

                            //try
                            //{
                            //    var itemToRemove = listHasMenu.FirstOrDefault(r => r.RolesID == menu.RoleID && r.MenuID == menu.MenuID);
                        
                            //    if (itemToRemove != null)
                            //    {
                            //        listHasMenu.Remove(itemToRemove);
                            //    }
                            //}
                            //catch
                            //{
                            //    ;
                            //}

                            if (getMenu == null)
                            {
                                Sc_RoleMenu newItem = new Sc_RoleMenu
                                {
                                    RolesID = model.RoleID,
                                    MenuID = menu.MenuID,
                                    IsActive = menu.IsMenuActive,
                                    InputDate = DateTime.Now,
                                    InputUser = m._name
                                };

                                db.Sc_RoleMenus.InsertOnSubmit(newItem);
                            }
                            else
                            {
                                getMenu.IsActive = menu.IsMenuActive;
                                getMenu.ModifDate = DateTime.Now;
                                getMenu.ModifUser = m._userName;
                            }

                        }

                        foreach (var ctrl in serializedCtrl)
                        {
                            var getRolePermission = db.Sc_RolePermissions.FirstOrDefault(item => item.RoleID == ctrl.RoleID && item.CtrlID == ctrl.CtrlID);

                            //try
                            //{
                            //    var itemToRemove = listHasCtrl.FirstOrDefault(r => r.RoleID == ctrl.RoleID && r.CtrlID == ctrl.CtrlID);
                            //    if (itemToRemove != null)
                            //    {
                            //        listHasCtrl.Remove(itemToRemove);
                            //    }
                            //}
                            //catch
                            //{
                            //    ;
                            //}

                            if (getRolePermission == null)
                            {
                                Sc_RolePermission newItem = new Sc_RolePermission
                                {
                                    RoleID = model.RoleID,
                                    CtrlID = ctrl.CtrlID,
                                    IsActive = ctrl.IsActive,
                                    InputDate = DateTime.Now,
                                    InputUser = m._name
                                };

                                db.Sc_RolePermissions.InsertOnSubmit(newItem);
                            }
                            else
                            {
                                getRolePermission.IsActive = ctrl.IsActive;
                                getRolePermission.ModifDate = DateTime.Now;
                                getRolePermission.ModifUser = m._userName;
                            }
                        }

                        //if (listHasCtrl != null)
                        //{
                        //    foreach (var setToFalse in listHasCtrl)
                        //    {
                        //        setToFalse.IsActive = false;
                        //        setToFalse.ModifDate = DateTime.Now;
                        //        setToFalse.ModifUser = m._userName;
                        //    }
                        //}

                        //if (listHasMenu != null)
                        //{
                        //    foreach (var setToFalse in listHasMenu)
                        //    {
                        //        setToFalse.IsActive = false;
                        //        setToFalse.ModifDate = DateTime.Now;
                        //        setToFalse.ModifUser = m._userName;
                        //    }
                        //}                        

                        db.SubmitChanges();

                        return Json(new { success = Global.UpdateMessage }, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        //TempData["Status"] = ex.Message + " : " + ex.InnerException;
                        return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    //["Status"] = Global.FillMessage;
                    return Json(new { error = Global.FillMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]
        public ActionResult DeleteRole(string RoleID, string url)
        {
            try
            {
                if (Sc_RolePermission.IsHasPermission("btn_delete", url))
                {
                    var db = Global.db;

                    var getRole = db.Sc_Roles.FirstOrDefault(item => item.RoleID == RoleID);

                    db.Sc_Roles.DeleteOnSubmit(getRole);
                    db.SubmitChanges();

                    return Json(new { success = Global.DeleteMessage }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch 
            {
                return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }


    }

    internal class RoleMenu
    {
        private string _RoleID;

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        private string _MenuID;

        public string MenuID
        {
            get { return _MenuID; }
            set { _MenuID = value; }
        }
        private bool _IsMenuActive;

        public bool IsMenuActive
        {
            get { return _IsMenuActive; }
            set { _IsMenuActive = value; }
        }
    }

    internal class RolePermission
    {
        private string _RoleID;

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        private string _CtrlID;

        public string CtrlID
        {
            get { return _CtrlID; }
            set { _CtrlID = value; }
        }

        private bool _IsActive;

        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
    }
}
