﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using System.Web.Script.Serialization;

namespace UltimoApps.Controllers
{
    public class RegistrationController : Controller
    {
        //
        // GET: /Registration/

        public ActionResult accRegistration(Sc_Register model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_accept", url))
            {
                var db = Global.db;
                Sc_Register get = db.Sc_Registers.FirstOrDefault(item => item.RegistID == model.RegistID);
                bool userSuccess = false;
                bool flagSuccess = false;

                if (get.Status.Equals("PENDING") || get.Status.Equals("REJECTED"))
                {
                    flagSuccess = Sc_Register.updateAcceptedRegist(get);
                    userSuccess = Sc_User.addFromRegist(get);
                }

                if (flagSuccess && userSuccess)
                {
                    return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult decRegistration(Sc_Register model, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_decline", url))
            {
                var db = Global.db;
                Sc_Register get = db.Sc_Registers.FirstOrDefault(item => item.RegistID == model.RegistID);

                bool flagSuccess = false;

                if (get.Status.Equals("PENDING"))
                {
                    flagSuccess = Sc_Register.updateRejectedRegist(get);
                }

                if (flagSuccess)
                {
                    return Json(new { success = Global.InsertMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllRegistrationData(string company, string status, string filter, string searchKey, int skip, int take, string url)
        {
            if (Sc_RolePermission.IsHasPermission("btn_search", url))
            {
                UserRegistrationBindingModel result = new UserRegistrationBindingModel { Data = Sc_Register.GetAllRegistrationData(company, status, filter, searchKey, skip, take), Total = Sc_Register.GetAllRegistrationDataTotal(company, status, filter, searchKey) };
                
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(result);

                return Json(serializedResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = Global.AccessMessage }, JsonRequestBehavior.AllowGet);
            }

        }

       
    }
}
