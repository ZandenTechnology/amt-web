﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;

namespace UltimoApps.Controllers
{
    public class FailureAnalysisController : Controller
    {
        //
        // GET: /FailureAnalysis/

        Hashtable parameters;
        ReportDocument rptDoc;
        string rptName;
        DataSetFailureAnalysis dt;

        // Print Failure Analysis 
        public ActionResult PrintFailureAnalysis(string itemCode, string dateFrom, string dateTo)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;
            var connection = connectionString;
            var Report = "";
            DataSetFailureAnalysis ds = new DataSetFailureAnalysis();
            dateFrom = DateTime.Parse(dateFrom).ToString("yyyy-MM-dd");
            dateTo = DateTime.Parse(dateTo).ToString("yyyy-MM-dd");

            var serializer = new JavaScriptSerializer();
            var listItem = serializer.Deserialize<List<string>>(itemCode);

            var stringItem = "";
            foreach (var list in listItem)
            {
                stringItem += "'" + list + "',";
            }

            if (!stringItem.Equals(""))
            {
                stringItem = stringItem.Substring(0, stringItem.Length - 1);
            }

            var getData = CrystalReportClass.getDataFailureAnalysis(dateFrom, dateTo, stringItem);

            List<dataFailureAnalysis> detailData = new List<dataFailureAnalysis>();

            foreach (var detail in getData)
            {
                dataFailureAnalysis temp = new dataFailureAnalysis();
                temp.FGCode = detail.FGCode;
                temp.FGDesc = detail.FGDesc;
                temp.Job = detail.Job;
                temp.OP = Convert.ToDouble(detail.OP);
                temp.OpQty = Convert.ToDouble(detail.OpQty);
                temp.RejCode = detail.RejCode;
                temp.RejDesc = detail.RejDesc;
                temp.RejPer = Convert.ToDouble(detail.RejPer);
                temp.RejQty = Convert.ToDouble(detail.RejQty);
                temp.ReportDate = detail.ReportDate;
                temp.WC = detail.WC;
                temp.WCDesc = detail.WCDesc;
                temp.SumValue = (double)detail.sumValue;

                detailData.Add(temp);
            }

            DataSetFailureAnalysis SOPDS = ds;

            parameters = new Hashtable();
            rptDoc = new ReportDocument();
            dt = new DataSetFailureAnalysis();

            dt = (DataSetFailureAnalysis)Session["sourcetableFailure"];
            parameters = (Hashtable)Session["parameters"];
            SessionDataTable = SOPDS;
            rptName = (string)Session["rptName"];
            Report = "~/CRFailureAnalysis.rpt";

            rptDoc.Load(Server.MapPath(Report));
            rptDoc.SetDataSource(detailData);

            SetParameter();
            string fileDir = Server.MapPath("/Uploads/");
            string fileName = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-FailureAnalysis" + ".pdf";
            string fileNameExcel = DateTime.Now.ToString("ddMMyyyy-HHmm") + "-FailureAnalysis" + ".xls";
            string filePath = fileDir + fileName;
            string filePathExcel = fileDir + fileNameExcel;

            if (!System.IO.Directory.Exists(fileDir))
            {
                System.IO.Directory.CreateDirectory(fileDir);
            }

            if (detailData.Count != 0)
            {
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filePath);
                rptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filePathExcel);

                return Json(new { excel = "/Uploads/" + fileNameExcel, redirect = "/Uploads/" + fileName, success = "Print export process success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = "No data to Display" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SetParameter()
        {
            if (parameters != null)
            {
                foreach (string param in parameters.Keys)
                {
                    rptDoc.SetParameterValue(param, parameters[param]);
                }
            }
        }

        protected DataSetFailureAnalysis SessionDataTable
        {
            get
            {
                if (Session["sourcetableFailure"] == null)
                {
                    Session["sourcetableFailure"] = new DataSetFailureAnalysis();
                }

                return (DataSetFailureAnalysis)Session["sourcetableFailure"];
            }
            set { Session["sourcetableFailure"] = value; }
        }

        protected string Report
        {
            get
            {
                return (string)Session["rptName"];
            }
            set
            {
                Session["rptName"] = value;
            }
        }

        internal class dataFailureAnalysis
        {
            public string ReportDate { get; set; }

            public string FGCode { get; set; }

            public string FGDesc { get; set; }

            public string Job { get; set; }

            public double OP { get; set; }

            public string WC { get; set; }

            public string WCDesc { get; set; }

            public string RejCode { get; set; }

            public string RejDesc { get; set; }

            public double RejQty { get; set; }

            public double OpQty { get; set; }

            public double RejPer { get; set; }

            public double SumValue { get; set; }
        }

    }
}
