﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.Controllers
{
    public class JobProcessController : Controller
    {
        //
        // GET: /JocProcess/

        [HttpPost]
        public ActionResult GetAllJobProcessData(string jobNo, string fromDate, string toDate, string WC, int skip, int take)
        {
            var db = Global.ewipDB;
            var JobProcessData = (from x in db.tbJobTrans select x).ToList();

            if (jobNo != "")
            {
                JobProcessData = JobProcessData.Where(m => m._job.Contains(jobNo)).ToList();
            }

            if (fromDate != "")
            {
                var dateFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var doubleFrom = dateFrom.ToOADate();
                JobProcessData = JobProcessData.Where(m => m._jobDate >= doubleFrom).ToList();
            }

            if (toDate != "")
            {
                var dateTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                dateTo = dateTo.AddDays(1);
                var doubleTo = dateTo.ToOADate();
                JobProcessData = JobProcessData.Where(m => m._jobDate <= doubleTo).ToList();
            }

            if (WC != "")
            {
                JobProcessData = JobProcessData.Where(m => m._wc.Contains(WC)).ToList();
            }

            List<JobProcessModel> ParsedModel = new List<JobProcessModel>();

            foreach (var JobProcess in JobProcessData)
            {
                JobProcessModel newModel = new JobProcessModel();
                newModel._job = JobProcess._job;
                newModel._item = JobProcess._item;
                newModel._wc = JobProcess._wc;
                newModel._oper_num = JobProcess._oper_num;
                newModel._jobsuffixParent = JobProcess._jobsuffixParent;
                newModel._jobsuffix = JobProcess._jobsuffix;
                newModel._qty_Rele_qty = JobProcess._qty_Rele_qty;
                newModel._qty_op_qty = JobProcess._qty_op_qty;
                newModel._qty_complete = JobProcess._qty_complete;
                newModel._qty_scrapped = JobProcess._qty_scrapped;

                var jobDate = Convert.ToInt32(JobProcess._jobDate);
                newModel.jobDateParsed = DateTime.FromOADate(jobDate);

                var startDate = Convert.ToInt32(JobProcess._start_Date);
                newModel.jobStartParsed = DateTime.FromOADate(startDate);

                var endDate = Convert.ToInt32(JobProcess._end_Date);
                newModel.jobEndParsed = DateTime.FromOADate(endDate);

                ParsedModel.Add(newModel);
            }

            var serializer = new JavaScriptSerializer();   

            model_JobProcess holder = new model_JobProcess{ Data = ParsedModel.Skip(skip).Take(take).ToList(), Total = ParsedModel.Count()};
            var serializedResult = serializer.Serialize(holder);

            return Json( serializedResult , JsonRequestBehavior.AllowGet);
        }
        

    }
}
