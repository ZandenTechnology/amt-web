﻿using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace UltimoApps.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View("~/Views/Frm_Login.cshtml");
        }

        //public ActionResult Login()
        //{

        //}

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var loginUser = tbUser.GetUserbyUserName(model.Username);
                //var encryptedPassword = mpMembership.HashPassword(model.Password, ConfigurationManager.AppSettings["passwordSalt"].ToString());

                if (loginUser != null && loginUser._password.Equals(model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);

                    return View("~/Views/Frm_Dashboard.cshtml");
                }
                else
                {
                    TempData["StatusLogin"] = "Invalid username or password";
                }
            }
            return View("~/Views/Frm_Login.cshtml", model);
        }

        [HttpPost]
        public ActionResult Registration(Sc_Register model)
        {
            //if (ModelState.IsValid)
            //{
            var db = Global.db;
            var maxRegID = db.Sc_Registers.OrderByDescending(u => u.RegistID).FirstOrDefault();
            model.RegistID = maxRegID.RegistID + 1;
            model.ApplyDate = DateTime.Now;
            model.Status = "PENDING";
            model.ApvBy = null;
            model.ApvDate = null;
            model.ApvRemarks = null;
            model.InputDate = DateTime.Now;
            model.InputUser = model.FirstName;
            model.ModifDate = DateTime.Now;
            model.ModifUser = model.FirstName;

            db.Sc_Registers.InsertOnSubmit(model);            
            try
            {
    
                db.SubmitChanges();
                return View("~/Views/Frm_Login.cshtml");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return View("~/Views/Frm_Registration.cshtml", model);
            }

        }

        [HttpPost]
        public void Logout()
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/");
        }
    }
}
