﻿using UltimoApps.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace UltimoApps.Handler.Menu
{
    /// <summary>
    /// Summary description for GetAllMenuForm
    /// </summary>
    public class GetAllMenuForm : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var url = context.Request.QueryString["url"];

            if (Sc_RolePermission.IsHasPermission("btn_edit",url))
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(Sc_Menu.getAllMenuFormData("", "", 0, 1000));

                context.Response.ContentType = "application/json";
                context.Response.Write(serializedResult);
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}