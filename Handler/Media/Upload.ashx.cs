﻿using UltimoApps.DAL;
using UltimoApps.DAL.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace UltimoApps.Handler.Media
{
    /// <summary>
    /// Summary description for Upload
    /// </summary>
    public class Upload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            UploadStatus upload = new UploadStatus();


            var hfUploadID = context.Request.QueryString["id"];
            var container = context.Request.QueryString["container"];

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                HttpPostedFile file = files[0];

                if (file.ContentLength <= Global.MaxFileSize)
                {
                    try
                    {
                        // Find filename
                        string _text = file.FileName;
                        string filename;
                        string _fullFilePath;

                        _text = _text.Replace("+", "plus");

                        filename = _text.Substring(_text.LastIndexOf("\\") + 1, _text.Length - _text.LastIndexOf("\\") - 1).ToLower();

                        //remove invalid char / * & # ?
                        filename = filename.Replace("/", "").Replace("*", "").Replace("&", "").Replace("?", "");


                        string path = "/Media/" + filename;

                        _fullFilePath = System.Web.HttpContext.Current.Server.MapPath(path);

                        file.SaveAs(_fullFilePath);

                        upload.UploadedURL = path;

                        path = string.Format(UIFormat.MediaFormat, path);

                        upload.SuccessMessage = "File has been uploaded.";
                        upload.UploadedContainerContent = path;
                        upload.UploadedContainer = "#" + container;
                        upload.UploadedID = "#" + hfUploadID;
                        //serializedResult = "{\"success\":\"" + path + "\" }"; 
                        //serializedResult = "{\"success\":\"" + path + "\", \"hfUploadID\" : \"#" + hfUploadID + "\", \"container\" : \"#"+container+"\" }"; 

                    }
                    catch (Exception ex)
                    {
                        upload.ErrorMessage = ex.Message.ToString();
                    }

                }
                else
                {
                    upload.ErrorMessage = "Max file size 100kb";
                }
            }
            else
            {
                upload.ErrorMessage = Global.ErrorMessage;
            }

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(upload);

            //var result = newConverter.Serialize(upload, new System.Web.Script.Serialization.JavaScriptSerializer());
            context.Response.ContentType = "application/json";
            context.Response.Write(serializedResult);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}