﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL.Helper
{
    public class UploadStatus
    {
        public string UploadedID { get; set; }
        public string UploadedURL { get; set; }
        public string UploadedContainerContent { get; set; }
        public string UploadedContainer { get; set; }
        public string ErrorMessage { get; set; }
        public string SuccessMessage { get; set; }
    }
}