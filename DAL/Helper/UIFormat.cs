﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL.Helper
{
    public class UIFormat
    {
        public static string Datepicker = "MM/dd/yyyy";
        public static string MediaFormat = "<li ><img class=\"img-responsive\" src=\"{0}\"/></li>";

    }
}