﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;

namespace UltimoApps.DAL.Helper
{
    public static class FontHelper
    {
        public static Font LoadFont(Assembly Asm , String Name, int Size , FontStyle Style , GraphicsUnit Unit ) {
            PrivateFontCollection Collection = new PrivateFontCollection();
            byte[] Bytes = FontData(Asm, Name);
            IntPtr Ptr = Marshal.AllocCoTaskMem(Bytes.Length);
            Marshal.Copy(Bytes, 0, Ptr, Bytes.Length);
            Collection.AddMemoryFont(Ptr, Bytes.Length);
            Marshal.FreeCoTaskMem(Ptr);
            return new Font(Collection.Families[0], Size, Style);
        }

            
        private static Byte[] FontData(Assembly Asm, String Name)
        {
            //Stream Stream = Asm.GetManifestResourceStream(Name);
            var Stream = new FileStream( HttpContext.Current.Server.MapPath(Name), FileMode.Open);
            if ((Stream == null)) {
                throw new Exception(string.Format("Unable to load font \'{0}\'", Name));
            }

            Byte[] Buffer = new Byte[Stream.Length];
            Stream.Read(Buffer, 0, int.Parse(Stream.Length.ToString()));
            return Buffer;
        }

    }
}