﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    public class clsMain
    {

        public DataSet GetDataset(string SQLs, string TableName)
        {
            DataSet GetDataset;

            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                // If cn.State = ConnectionState.Closed Then
                //     GetDataset = Nothing
                //     Exit Function
                // ElseIf Not cn.State = ConnectionState.Open Then
                //     GetDataset = Nothing
                //     Exit Function
                // End If
                DataSet ds = new DataSet();
                SqlConnection ewipCnn = Global.eWIPCnn;

                da = new SqlDataAdapter(SQLs, ewipCnn);
                da.Fill(ds, TableName);
                da.Dispose();
                GetDataset = ds;
                ds.Dispose();

            }
            catch (Exception ex)
            {
                GetDataset = null;
            }

            return GetDataset;

        }

    }
}