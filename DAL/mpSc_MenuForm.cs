﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class Sc_MenuForm
    {
        public static List<GetSpecificFormbyMenuIDResult> GetSpecificFormbyMenuID (string MenuID, bool hasForm)
        {
            return Global.db.GetSpecificFormbyMenuID(MenuID, hasForm).ToList();
        }

        public static List<GetAllSpecificFormbyMenuIDResult> GetAllSpecificFormbyMenuID(string MenuID, string RoleID)
        {
            return Global.db.GetAllSpecificFormbyMenuID(MenuID, RoleID).ToList();
        }

        public static Boolean delete(string ID)
        {
            try
            {
                var db = Global.db;
                db.DeleteMenuFormData(ID);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}