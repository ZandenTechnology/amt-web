﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.DAL;

namespace UltimoApps.DAL
{
    partial class tbResourceGroup
    {
        public static tbResourceGroup GetNameByGroupName(string _rGroupName)
        {
            var db = Global.ewipDB;
            return db.tbResourceGroups.FirstOrDefault(x => x._rGroupName == _rGroupName);
        }

        public static bool insertResourceGroup(tbResourceGroup model)
        {
            try
            {
                var db = Global.ewipDB;
                tbResourceGroup newResGroup = new tbResourceGroup();
                tbResourceGroup maxUser = db.tbResourceGroups.OrderBy(x => x._rid).FirstOrDefault();
                newResGroup._rid = maxUser._rid + 1;
                newResGroup._rGroupID = model._rGroupID;
                newResGroup._rGroupName = model._rGroupName;
                newResGroup._cc = model._cc;
                newResGroup._to = model._to;
                newResGroup._transfer = model._transfer;
                newResGroup._yieldper = model._yieldper;
                db.tbResourceGroups.InsertOnSubmit(newResGroup);

                db.SubmitChanges();
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool updateResourceGroup(tbResourceGroup model)
        {
            try
            {
                var db = Global.ewipDB;
                tbResourceGroup get = db.tbResourceGroups.FirstOrDefault(y => y._rid == model._rid);
                get._rid = model._rid;
                //get._rGroupID = model._rGroupID;
                get._rGroupName = model._rGroupName;
                get._cc = model._cc;
                get._to = model._to;
                get._transfer = model._transfer;
                get._yieldper = model._yieldper;

                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool deleteResourceGroup(tbResourceGroup model)
        {
            try
            {
                var db = Global.ewipDB;
                tbResourceGroup get = db.tbResourceGroups.FirstOrDefault(y => y._rid == model._rid);
                db.tbResourceGroups.DeleteOnSubmit(get);
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}