﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL {
    partial class tbMachine {
        public static bool saveMachine(tbMachine model)
        {
            try
            {
                var db = Global.ewipDB;
                tbMachine newMachine = new tbMachine();
                newMachine._machineID = model._machineID;
                newMachine._MachineDesc = model._MachineDesc;
                db.tbMachines.InsertOnSubmit(newMachine);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool updateMachine(tbMachine model)
        {
            try
            {
                var db = Global.ewipDB;
                tbMachine get = db.tbMachines.FirstOrDefault(y => y._mid == model._mid);
                get._MachineDesc = model._MachineDesc;
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool deleteMachine(tbMachine model)
        {
            try
            {
                var db = Global.ewipDB;
                tbMachine get = db.tbMachines.FirstOrDefault(y => y._mid == model._mid);
                db.tbMachines.DeleteOnSubmit(get);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}