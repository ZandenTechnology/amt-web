﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.DAL;

namespace UltimoApps.DAL
{
    partial class tbRejected
    {
        public static tbRejected GetNameByRejectDesc(string _RejectedDesc)
        {
            var db = Global.ewipDB;
            return db.tbRejecteds.FirstOrDefault(x => x._RejectedDesc == _RejectedDesc);
        }

        public static bool insertRejectDesc(tbRejected model)
        {
            try
            {
                var db = Global.ewipDB;
                tbRejected newRejectDesc = new tbRejected();
                tbRejected maxUser = db.tbRejecteds.OrderBy(x => x._RejID).FirstOrDefault();
                newRejectDesc._RejID = maxUser._RejID + 1;
                newRejectDesc._RejectedCode = model._RejectedCode;
                newRejectDesc._RejectedDesc = model._RejectedDesc;

                db.tbRejecteds.InsertOnSubmit(newRejectDesc);

                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool updateRejectDesc(tbRejected model)
        {
            try
            {
                var db = Global.ewipDB;
                tbRejected get = db.tbRejecteds.FirstOrDefault(y => y._RejID == model._RejID);
                get._RejID = model._RejID;
                //get._RejectedCode = model._RejectedCode;
                get._RejectedDesc = model._RejectedDesc;

                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool deleteRejectDesc(tbRejected model)
        {
            try
            {
                var db = Global.ewipDB;
                tbRejected get = db.tbRejecteds.FirstOrDefault(y => y._RejID == model._RejID);
                db.tbRejecteds.DeleteOnSubmit(get);
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}