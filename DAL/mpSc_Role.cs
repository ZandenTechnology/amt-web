﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class Sc_Role
    {
        public static List<GetAllRolesDataResult> GetAllRolesData(string filter, string searchKey, int step, int take)
        {
            return Global.db.GetAllRolesData(filter, searchKey, step, take).ToList();
        }

        public static int GetAllRolesDataTotal(string filter, string searchKey)
        {
            return (int)Global.db.GetAllRolesDataTotal(filter, searchKey).FirstOrDefault().totalData;
        }

        public static bool insert(Sc_Role model)
        {
            try
            {
                var db = Global.db;
                
                db.Sc_Roles.InsertOnSubmit(model);
                Global.SubmitChanges(db);

                return true;
            }
            catch
            {
                return false;
            }
        }
          

        public static bool update(Sc_Role model)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Roles.FirstOrDefault(item => item.RoleID == model.RoleID);

                get.Role = model.Role;
                get.RoleDesc = model.RoleDesc;
                get.RoleType = model.RoleType;
                get.IsActive = model.IsActive;

                get.ModifDate = model.ModifDate;
                get.ModifUser = model.ModifUser;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}