﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using UltimoApps.Models;
using UltimoApps.DAL;
using System.Data.SqlClient;

namespace UltimoApps.DAL
{
    public class Global
    {
        public static string AccessMessage = "Sorry, you don't have access.";
        public static string FillMessage = "Please fill all fields.";
        public static string InsertMessage = "Data has been successfully saved.";
        public static string UpdateMessage = "Data has been successfully updated.";
        public static string ErrorMessage = "Process has failed to do what it intended to do, please contact administrator";
        public static string DeleteMessage = "Data has been successfully deleted.";
        public static string SuccessMessage = "Process finished successfully";
        public static string SearchMessage = "Sorry, You don't have data to search";

        internal static string Sl9ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["sl9"].ConnectionString;
        internal static string eWipConnString = System.Configuration.ConfigurationManager.ConnectionStrings["ewip"].ConnectionString;

        public static Tmp_SL9TableName init_tbl = db.Tmp_SL9TableNames.FirstOrDefault();
        public static string job_mst = init_tbl.job_mst;
        public static string jobmatl_mst = init_tbl.jobmatl_mst;
        public static string jobroute_mst = init_tbl.jobroute_mst;
        public static string jobtrans_mst = init_tbl.jobtrans_mst;
        public static string itemloc_mst = init_tbl.itemloc_mst;
        public static string item_mst = init_tbl.item_mst;
        public static string jrt_sch_mst = init_tbl.jrt_sch_mst;
        public static string wc_mst = init_tbl.wc_mst;
        public static int freeze_post = init_tbl.freeze_post;

        public static int MaxFileSize = 300000;

        public static bool setUpSLTable()
        {
            try
            {
                var newConfig = db.Tmp_SL9TableNames.FirstOrDefault();
                job_mst = newConfig.job_mst;
                jobmatl_mst = newConfig.jobmatl_mst;
                jobroute_mst = newConfig.jobroute_mst;
                jobtrans_mst = newConfig.jobtrans_mst;
                itemloc_mst = newConfig.itemloc_mst;
                item_mst = newConfig.item_mst;
                jrt_sch_mst = newConfig.jrt_sch_mst;
                wc_mst = newConfig.wc_mst;
                freeze_post = newConfig.freeze_post;
                    
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static string GetSL9InitialCatalog()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["sl9"].ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
            return builder.InitialCatalog;
        }

        public static ultimoDBDataContext db
        {
            get { return new ultimoDBDataContext(); }
        }

        public static void SubmitChanges(ultimoDBDataContext db)
        {
            db.SubmitChanges();
        }

        //public static sl9DBDataContext sl9DB
        //{
        //    get { return new sl9DBDataContext(); }
        //}

        //public static void SubmitChanges(sl9DBDataContext sl9DB)
        //{
        //    sl9DB.SubmitChanges();
        //}

        public static ewipDBDataContext ewipDB
        {
            get { return new ewipDBDataContext(); }
        }

        public static void SubmitChanges(ewipDBDataContext ewipDB)
        {
            ewipDB.SubmitChanges();
        }

        public static SqlConnection sl9Cnn
        {
            get { return new SqlConnection(Sl9ConnString); }
        }

        public static SqlConnection eWIPCnn
        {
            get { return new SqlConnection(eWipConnString); }
        }

        SqlCommand commands = new SqlCommand("", eWIPCnn);

        public static DataTable getDataFromDb(SqlConnection connection, string cmdText, List<DB_Parameter> parameters = null)
        {
            SqlCommand command = new SqlCommand(cmdText, connection);

            foreach (DB_Parameter parameter in parameters)
            {
                if (parameter.type_data == SqlDbType.NVarChar)
                {
                    command.Parameters.Add(parameter.name, parameter.type_data, 20);
                }
                else
                {
                    command.Parameters.Add(parameter.name, parameter.type_data);
                }

                command.Parameters[parameter.name].Value = parameter.value;
            }

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            command.Parameters.Clear();
            return dataTable;
        }

        public static DataTable getDataFrom_eWipDB(string cmdText, List<DB_Parameter> parameters = null)
        {
            return getDataFromDb(eWIPCnn, cmdText, parameters);
        }
        public static DataTable getDataFrom_sl9DB(string cmdText, List<DB_Parameter> parameters = null)
        {
            return getDataFromDb(sl9Cnn, cmdText, parameters);
        }

        public static int getFreezeData()
        {
            var result = db.Tmp_SL9TableNames.FirstOrDefault();
            return result.freeze_post;
        }
    }
}
