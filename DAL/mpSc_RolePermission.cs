﻿using UltimoApps.DAL.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class Sc_RolePermission
    {
        public static List<spGetAllUserAccessControllerResult> GetAllUserAccessController(string menuURL, string roleID)
        {
            var temp = Global.db.spGetAllUserAccessController(menuURL, roleID).ToList();
            return temp;
        }

        public static bool IsHasPermission(string Ctrl, string MenuURL)
        {
            List<string> ctrlList = new List<string>();
            tbUser curUser = mpMembership.GetCurrentUser();
            string curRole = null;
            switch(curUser._privilege){
                case "Admin":
                    curRole="R0001";
                    break;
                    case "Supervisor":
                    curRole="R0002";
                    break;
                    case "Rework":
                    curRole="R0003";
                    break;
            }
            
            var grantedAccess = Sc_RolePermission.GetAllUserAccessController(MenuURL, curRole);

            foreach (var controller in grantedAccess)
            {
                if (controller.ctrl.Contains(Ctrl))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool insert(Sc_RolePermission model)
        {
            var db = Global.db;
            //var get = new Sc_RoleMenu();
            //get.RolesID = model.RolesID;
            //get.IsActive = model.IsActive;
            model.InputDate = DateTime.Now;
            model.InputUser = Sc_User.getNamebyEmail(mpMembership.GetCurrentUsername());

            db.Sc_RolePermissions.InsertOnSubmit(model);

            if (Sc_RolePermission.validate(model))
            {
                try
                {
                    db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool Update(Sc_RolePermission model)
        {
            var db = Global.db;
            var get = db.Sc_RolePermissions.FirstOrDefault(item => item.RoleID == model.RoleID && item.CtrlID == model.CtrlID);

            try
            {
                get.IsActive = model.IsActive;
                get.ModifDate = model.ModifDate;
                get.ModifUser = model.ModifUser;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool validate(Sc_RolePermission model)
        {
            if ((model.RoleID == null) || (model.RoleID == ""))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}