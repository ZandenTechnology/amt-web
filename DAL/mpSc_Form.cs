﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.DAL;
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using System.Web.Script.Serialization;

namespace UltimoApps.DAL
{
    partial class Sc_Form
    {
        public static List<GetAllFormDataResult> GetAllFormData(string filter, string searchKey, int step, int take)
        {
            return Global.db.GetAllFormData(filter, searchKey, step, take).ToList();
        }

        public static int GetAllFormDataTotal(string filter, string searchKey)
        {
            return (int)Global.db.GetAllFormDataTotal(filter, searchKey).FirstOrDefault().totalData;
        }


        public static bool insert(Sc_Form model)
        {
            var db = Global.db;
            var get = new Sc_Form();
            get.FormID = model.FormID;
            get.WebForm = model.WebForm;
            get.FormType = model.FormType;
            get.Caption = model.Caption;
            get.FileName = "/Forms";
            get.IsActive = true;
            get.InputDate = DateTime.Now;
            get.InputUser = Sc_User.getNamebyEmail(mpMembership.GetCurrentUsername());
            get.ModifDate = DateTime.Now;
            get.ModifUser = Sc_User.getNamebyEmail(mpMembership.GetCurrentUsername());
            db.Sc_Forms.InsertOnSubmit(get);

            if (Sc_Form.validate(get))
            {
                try
                {
                    //db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool update(Sc_Form model)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Forms.FirstOrDefault(item => item.FormID == model.FormID);
                //get.FirstName = data.FirstName;
                get.FormID = model.FormID;
                get.WebForm = model.WebForm;
                get.FormType = model.FormType;
                get.Caption = model.Caption;
                get.IsActive = model.IsActive;
                get.ModifDate = model.ModifDate;
                get.ModifUser = model.ModifUser;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool validateCompactModel(Compact_FormControlModel model)
        {
            if ((model.FormID == null) || (model.FormID == ""))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool validate(Sc_Form model)
        {
            if ((model.FormID == null) || (model.FormID == ""))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        internal static bool DeleteFormData(string ID)
        {
            try
            {
                Global.db.DeleteFormData(ID);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static bool updateFromCompactModel(Compact_FormControlModel model)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Forms.FirstOrDefault(item => item.FormID == model.FormID);
                //get.FirstName = data.FirstName;
                get.FormID = model.FormID;
                get.WebForm = model.WebForm;
                get.FormType = model.FormType;
                get.Caption = model.Caption;
                get.IsActive = model.IsActive;
                get.ModifDate = model.ModifDate;
                get.ModifUser = model.ModifUser;

                //insert role
                if (model.ListControl != null)
                {
                    var serializer = new JavaScriptSerializer();
                    var serializedForm = serializer.Deserialize<List<ControlFormintModel>>(model.ListControl);

                    List<Sc_FormCtrl> listHas = (from x in db.Sc_FormCtrls
                                                 where x.FormID == model.FormID
                                                 select x).ToList();

                    var maxCtrlID = listHas.OrderByDescending(u => u.CtrlID).FirstOrDefault();
                    int maxID;
                    if (maxCtrlID != null)
                    {
                        maxID = int.Parse(maxCtrlID.CtrlID.Substring(maxCtrlID.CtrlID.Length - 2, 2)) + 1;
                    }
                    else
                    {
                        maxID = 1;
                    }

                    var m = mpMembership.GetCurrentUser();

                    foreach (var form in serializedForm)
                    {
                        Sc_FormCtrl checkItem = db.Sc_FormCtrls.FirstOrDefault(x => x.FormID == model.FormID && x.Ctrl == form.Ctrl);

                        try
                        {
                            var itemToRemove = listHas.Single(r => r.FormID == model.FormID && r.Ctrl == form.Ctrl);
                            listHas.Remove(itemToRemove);
                        }
                        catch
                        {
                            ;
                        }

                        if (checkItem == null)
                        {
                            Sc_FormCtrl item = new Sc_FormCtrl();
                            item.CtrlID = model.FormID + "-" + maxID.ToString("D2");
                            item.FormID = model.FormID;
                            item.Ctrl = form.Ctrl;
                            item.CtrlType = form.CtrlType;
                            item.CtrlDesc = form.Label + " " + model.Caption;
                            item.Label = form.Label;
                            item.IsBlast = false;
                            item.IsActive = true;
                            item.InputDate = DateTime.Now;
                            item.InputUser = m._name;

                            db.Sc_FormCtrls.InsertOnSubmit(item);
                            maxID++;
                        }
                        else
                        {
                            checkItem.IsActive = true;
                            checkItem.ModifDate = DateTime.Now;
                            checkItem.ModifUser = m._userName;
                        }
                    }

                    if (listHas != null)
                    {
                        foreach (var setToFalse in listHas)
                        {
                            setToFalse.IsActive = false;
                            setToFalse.ModifDate = DateTime.Now;
                            setToFalse.ModifUser = m._userName;
                        }
                    }

                }

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static bool insertCompactModel(Compact_FormControlModel model)
        {
            var db = Global.db;
            var get = new Sc_Form();
            get.FormID = model.FormID;
            get.WebForm = model.WebForm;
            get.FormType = model.FormType;
            get.Caption = model.Caption;
            get.FileName = "/Forms";
            get.IsActive = true;
            get.InputDate = DateTime.Now;
            get.InputUser = Sc_User.getNamebyEmail(mpMembership.GetCurrentUsername());
            get.ModifDate = DateTime.Now;
            get.ModifUser = Sc_User.getNamebyEmail(mpMembership.GetCurrentUsername());
            db.Sc_Forms.InsertOnSubmit(get);

            if (model.ListControl != null)
            {
                var serializer = new JavaScriptSerializer();
                var serializedForm = serializer.Deserialize<List<ControlFormintModel>>(model.ListControl);

                int maxID = 1;
                var m = mpMembership.GetCurrentUser();

                foreach (var form in serializedForm)
                {
                    Sc_FormCtrl checkItem = db.Sc_FormCtrls.FirstOrDefault(x => x.FormID == model.FormID && x.Ctrl == form.Ctrl);

                    if (checkItem == null)
                    {
                        Sc_FormCtrl item = new Sc_FormCtrl();
                        item.CtrlID = model.FormID + "-" + maxID.ToString("D2");
                        item.FormID = model.FormID;
                        item.Ctrl = form.Ctrl;
                        item.CtrlType = form.CtrlType;
                        item.CtrlDesc = form.Label + " " + model.Caption;
                        item.Label = form.Label;
                        item.IsBlast = false;
                        item.IsActive = true;
                        item.InputDate = DateTime.Now;
                        item.InputUser = m._name;

                        db.Sc_FormCtrls.InsertOnSubmit(item);
                        maxID++;

                        if (!Sc_FormControl.validate(item))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        checkItem.IsActive = true;
                        checkItem.ModifDate = DateTime.Now;
                        checkItem.ModifUser = m._userName;
                    }
                }
            }
            if (Sc_Form.validate(get))
            {
                Global.SubmitChanges(db);
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    internal class ControlFormintModel
    {
        private string _Ctrl;

        public string Ctrl
        {
            get { return _Ctrl; }
            set { _Ctrl = value; }
        }
        private string _CtrlType;

        public string CtrlType
        {
            get { return _CtrlType; }
            set { _CtrlType = value; }
        }

        private string _Label;

        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }
    }
}