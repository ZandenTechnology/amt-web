﻿using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace UltimoApps.DAL
{
    partial class Sc_Menu
    {
        public static string getParentNameByMenuURL(string keyURL)
        {
            return Global.db.fnGetParentNameByChildMenuURL(keyURL);
        }


        public static List<string> getParentMenuID(string keyWord)
        {
            var db = Global.db;
            List<string> Parent = new List<string>();
            var Menu = db.Sc_Menus.FirstOrDefault(item => item.MenuURL == keyWord);
            try
            {
                Parent.Add(Menu.MenuID);
                Parent.Add(Menu.ParentID);
                if (Menu.ParentID.Substring(4, 1).Equals("0") == false)
                {
                    var tempParent = db.Sc_Menus.FirstOrDefault(item => item.MenuID == Menu.ParentID);
                    Parent.Add(tempParent.ParentID);
                }
            }
            catch
            {
                ;
            }
            return Parent;
        }

        public static List<sp_getMenubyLevelResult> getMenubyLevel(int Level, string RoleID)
        {
            return Global.db.sp_getMenubyLevel(Level, RoleID).ToList();
        }

        public static List<sp_getMenubyParentResult> getMenubyParent(string parentID, string roleID)
        {
            return Global.db.sp_getMenubyParent(parentID, roleID).ToList();
        }

        public static List<GetAllMenuFormDataResult> getAllMenuFormData(string filter, string searchKey, int step, int take)
        {
            return Global.db.GetAllMenuFormData(filter, searchKey, step, take).ToList();
        }

        public static int GetAllMenuFormDataTotal(string filter, string searchKey)
        {
            return (int)Global.db.GetAllMenuFormDataTotal(filter, searchKey).FirstOrDefault().totalData;
        }

        public static List<GetAllMenuFormDataByRoleIDResult> GetAllMenuFormDataByRoleID(string roleID)
        {
            return Global.db.GetAllMenuFormDataByRoleID(roleID).ToList();
        }


        public static List<sp_getAllParentMenuResult> getAllParentMenu()
        {
            return Global.db.sp_getAllParentMenu().ToList();
        }

        internal static bool insert(Sc_Menu model)
        {
            var db = Global.db;
            var get = new Sc_Menu();
            get.MenuID = model.MenuID;
            get.ParentID = model.ParentID;
            get.MenuLvl = model.MenuLvl;
            get.Menu = model.Menu;
            get.MenuDesc = model.MenuDesc;
            get.MenuURL = model.MenuURL;
            get.IsActive = true;
            get.IsHide = model.IsHide;
            get.InputDate = DateTime.Now;
            get.InputUser = Sc_User.getNamebyEmail(mpMembership.GetCurrentUsername());

            db.Sc_Menus.InsertOnSubmit(get);

            if (Sc_Menu.validate(model))
            {
                try
                {
                    db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool Update(Sc_Menu model)
        {
            var db = Global.db;
            var get = db.Sc_Menus.FirstOrDefault(item => item.MenuID == model.MenuID);
            get.ParentID = model.ParentID;
            get.Menu = model.Menu;
            get.MenuDesc = model.MenuDesc;
            get.IsActive = model.IsActive;
            get.IsHide = model.IsHide;
            get.ModifDate = model.ModifDate;
            get.ModifUser = model.ModifUser;
            try
            {
                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool UpdateFromCompactModel(Compact_MenuFormModel model)
        {
            var db = Global.db;
            var get = db.Sc_Menus.FirstOrDefault(item => item.MenuID == model.MenuID);
            tbUser currentUser = mpMembership.GetCurrentUser();

            get.ParentID = model.ParentID;
            get.Menu = model.Menu;
            get.MenuDesc = model.MenuDesc;
            get.IsActive = model.IsActive;
            get.IsHide = model.IsHide;
            get.ModifDate = DateTime.Now;
            get.ModifUser = currentUser._userName;

            if (model.ListForm != null)
            {
                var serializer = new JavaScriptSerializer();
                var serializedForm = serializer.Deserialize<List<MenuFromIntModel>>(model.ListForm);

                List<Sc_MenuForm> listHas = (from x in db.Sc_MenuForms
                                             where x.MenuID == model.MenuID
                                             select x).ToList();

                var m = mpMembership.GetCurrentUser();

                foreach (var form in serializedForm)
                {
                    Sc_MenuForm checkItem = db.Sc_MenuForms.FirstOrDefault(x => x.MenuID == form.MenuID && x.FormID == form.FormID);

                    try
                    {
                        var itemToRemove = listHas.Single(r => r.MenuID == form.MenuID && r.FormID == form.FormID);
                        listHas.Remove(itemToRemove);
                    }
                    catch
                    {
                        ;
                    }

                    if (checkItem == null)
                    {
                        Sc_MenuForm item = new Sc_MenuForm();
                        item.MenuID = form.MenuID;
                        item.FormID = form.FormID;
                        item.IsActive = true;
                        item.InputDate = DateTime.Now;
                        item.InputUser = m._name;

                        db.Sc_MenuForms.InsertOnSubmit(item);
                    }
                    else
                    {
                        checkItem.IsActive = true;
                        checkItem.ModifDate = DateTime.Now;
                        checkItem.ModifUser = m._userName;
                    }
                }

                if (listHas != null)
                {
                    foreach (var setToFalse in listHas)
                    {
                        setToFalse.IsActive = false;
                        setToFalse.ModifDate = DateTime.Now;
                        setToFalse.ModifUser = m._userName;
                    }
                }
            }

            try
            {
                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool validate(Sc_Menu model)
        {
            if ((model.MenuID == null) || (model.MenuID == ""))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool insertCompactModel(Compact_MenuFormModel model)
        {
            var db = Global.db;
            var get = new Sc_Menu();
            var currentUser = mpMembership.GetCurrentUser();

            get.MenuID = model.MenuID;
            get.ParentID = model.ParentID;
            get.MenuLvl = byte.Parse(model.MenuLvl);
            get.Menu = model.Menu;
            get.MenuDesc = model.MenuDesc;
            get.MenuURL = model.MenuURL;
            get.IsActive = true;
            get.IsHide = model.IsHide;
            get.InputDate = DateTime.Now;
            get.InputUser = currentUser._userName;

            db.Sc_Menus.InsertOnSubmit(get);

            if (model.ListForm != null)
            {
                var serializer = new JavaScriptSerializer();
                var serializedForm = serializer.Deserialize<List<MenuFromIntModel>>(model.ListForm);

                var m = mpMembership.GetCurrentUser();

                foreach (var form in serializedForm)
                {
                    Sc_MenuForm checkItem = db.Sc_MenuForms.FirstOrDefault(x => x.MenuID == form.MenuID && x.FormID == form.FormID);

                    if (checkItem == null)
                    {
                        Sc_MenuForm item = new Sc_MenuForm();
                        item.MenuID = form.MenuID;
                        item.FormID = form.FormID;
                        item.IsActive = true;
                        item.InputDate = DateTime.Now;
                        item.InputUser = m._name;

                        db.Sc_MenuForms.InsertOnSubmit(item);
                    }
                    else
                    {
                        checkItem.IsActive = true;
                        checkItem.ModifDate = DateTime.Now;
                        checkItem.ModifUser = m._userName;
                    }
                }            
            }
            try
            {
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    internal class MenuFromIntModel
    {
        private string _MenuID;

        public string MenuID
        {
            get { return _MenuID; }
            set { _MenuID = value; }
        }

        private string _FormID;

        public string FormID
        {
            get { return _FormID; }
            set { _FormID = value; }
        }
    }
}
