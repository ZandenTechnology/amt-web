﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using UltimoApps.DAL;
using UltimoApps.Models;

namespace UltimoApps.DAL
{
    public class ewip_tbMachineGroup
    {
        public static bool setMachineGroup(List<tbMachineGroup> model, string wcID)
        {

            //try
            //{
            //    var serializer = new JavaScriptSerializer();
            //    //var serializedList = serializer.Deserialize<List<string>>(model.listWorkCenter);


            //    var db = Global.ewipDB;
            //    //var getWorkStation = db.tbWorkStations.Where(x => x.rid == model.rid).ToList(); 
            //    var getMachineGroup = db.tbMachineGroups.Where(x => x._wc == wcID).ToList(); 
            //    //var getMaxWS = db.tbWorkStations.OrderByDescending(z => z.sIDno).FirstOrDefault().sIDno + 1;
            //    var getMaxMG = db.tbMachineGroups.OrderByDescending(z => z._mgid).FirstOrDefault()._mgid + 1;
            //    var list = new List<tbMachineGroup>();
            //    foreach (var work in model)
            //    {
            //        var checkWork = getMachineGroup.Where(y => y._Mid == work._Mid).FirstOrDefault();
            //        //var checkWork = getWorkStation.Where(y => y.wc == work).FirstOrDefault();

            //        if (checkWork == null)
            //        {
            //            //var newWork = new tbWorkStation();
            //            //newWork.sIDno = getMaxWS;
            //            //newWork.rid = model.rid;
            //            //newWork._wc = work;

            //            var newWork = new tbMachineGroup();
            //            newWork._mgid = getMaxMG;
            //            newWork._Mid = work._Mid;
            //            newWork._wc = work._wc;
            //            getMaxMG += 1;

            //            list.Add(newWork);
            //            db.tbMachineGroups.InsertOnSubmit(newWork);
            //        }
            //        else
            //        {
            //            getMachineGroup.Remove(checkWork);
            //        }
            //    }

            //    foreach (var deselected in getMachineGroup)
            //    {
            //        db.tbMachineGroups.DeleteOnSubmit(deselected);
            //    }

            //    db.SubmitChanges();
            //    return Json(new { success = Global.SuccessMessage }, JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception)
            //{
            //    return Json(new { error = Global.ErrorMessage }, JsonRequestBehavior.AllowGet);
            //}


            //try
            //{
            //    var db = Global.ewipDB;


            //    foreach (var data in model)
            //    {

            //        var WCData = (from MachineGroup in db.tbMachineGroups
            //                      join WorkCenter in db.tbWcs on MachineGroup._wc equals WorkCenter._wc
            //                      join Machines in db.tbMachines on MachineGroup._Mid equals Machines._mid
            //                      where MachineGroup._wc == data._wc && MachineGroup._Mid == data._Mid
            //                      select new
            //                      {
            //                          _mid = Machines._mid,
            //                          _machineID = Machines._machineID,
            //                          _MachineDesc = Machines._MachineDesc
            //                      }).FirstOrDefault();

            //        if (WCData == null)
            //        {

            //            var WCDataMax = (from MachineGroup in db.tbMachineGroups
            //                          join WorkCenter in db.tbWcs on MachineGroup._wc equals WorkCenter._wc
            //                          join Machines in db.tbMachines on MachineGroup._Mid equals Machines._mid
            //                          where MachineGroup._wc == data._wc && MachineGroup._Mid == data._Mid
                                       
            //                       ;

            //            tbMachineGroup newMachineGroup = new tbMachineGroup();
            //            newMachineGroup._wc = data._wc;
            //            newMachineGroup._mgid = data._mgid;
            //            newMachineGroup._Mid = data._Mid;
            //            db.tbMachineGroups.InsertOnSubmit(newMachineGroup);
            //            db.SubmitChanges();
            //        }
            //        else
            //        {

            //        }
            //    }

            //    foreach (var data in model)
            //    {

            //        tbMachineGroup newMachineGroup = new tbMachineGroup();
            //        newMachineGroup._wc = data._wc;
            //        newMachineGroup._mgid = data._mgid;
            //        newMachineGroup._Mid = data._Mid;
            //        db.tbMachineGroups.InsertOnSubmit(newMachineGroup);
            //        db.SubmitChanges();
            //    }
            //    return true;
            //}
            //catch (Exception)
            //{
            //    return false;
            //}

                return true;
        }


        public static bool DeleteMachineGroup(List<tbMachineGroup> model)
        {
            try
            {

                foreach (var data in model)
                {
                    var db = Global.ewipDB;
                    tbMachineGroup newMachineGroup = new tbMachineGroup();
                    newMachineGroup._wc = data._wc;
                    newMachineGroup._mgid = data._mgid;
                    newMachineGroup._Mid = data._Mid;
                    db.tbMachineGroups.DeleteOnSubmit(newMachineGroup);
                    db.SubmitChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}