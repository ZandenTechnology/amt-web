﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class tbOperationStatus
    {
        public static bool updateActionControl(tbOperationStatus model)
        {
            try
            {
                var db = Global.ewipDB;
                tbOperationStatus get = db.tbOperationStatus.FirstOrDefault(y => y._idno == model._idno);
                get._skip = model._skip;
                get._Split = model._Split;
                get._transfer = model._transfer;
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}