﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    
    public class CrystalReportClass
    {
        static ewipDBDataContext db = UltimoApps.DAL.Global.ewipDB;

        public static List<tbJobTran> GetAllDataEntry()
        {
            return db.tbJobTrans.ToList();
        }

        public static List<Report_CObyResourceGroupResult> getDataCOResourceGroup(string dateFrom, string dateTo, decimal resorceGroup, string workCenter, string item)
        {
            return db.Report_CObyResourceGroup(dateFrom, dateTo, resorceGroup, workCenter, item).ToList();
        }

        public static List<Report_CObyItemReportResult> getDataCoItem(string dateFrom, string dateTo, string itemCode)
        {
            return db.Report_CObyItemReport(dateFrom, dateTo, itemCode).ToList();
        }

        public static List<Report_FinalYieldReportResult> getDataFinalYield(string dateFrom, string dateTo, string itemCode)
        {
            return db.Report_FinalYieldReport(dateFrom, dateTo, itemCode).ToList();
        }

        public static List<Report_WorkCenterYeildResult> getDataWorkCenterYield(string dateFrom, string dateTo, string itemCode, string wc)
        {
            return db.Report_WorkCenterYeild(dateFrom, dateTo, itemCode, wc).ToList();
        }

        public static List<Report_FailureAnalysisResult> getDataFailureAnalysis(string dateFrom, string dateTo, string itemCode)
        {
            return db.Report_FailureAnalysis(dateFrom, dateTo, itemCode).ToList();
        }

        public static List<Report_WIPbyItemReportResult> getdataWIPbyItem(bool ckbRework, bool ckbTransfer, string itemCode)
        {
            return db.Report_WIPbyItemReport(ckbRework, ckbTransfer, itemCode).ToList();
        }

        public static List<Report_WIPbyResourceGroupResult> getdataWIPbyResourceGroup(bool ckbRework, string itemCode, decimal resorceGroup, string workCenter)
        {
            return db.Report_WIPbyResourceGroup(ckbRework, itemCode, resorceGroup, workCenter).ToList();
        }

        public static List<Report_ActualCycle_ItemByDetailsResult> getdataACbyItemDetail(string dateFrom, string dateTo, string itemCode, string rid, string wc)
        {
            return db.Report_ActualCycle_ItemByDetails(dateFrom, dateTo, itemCode, rid, wc).ToList();
        }

        public static List<Report_ActualCycle_OperatorResult> getdataACbyOperator(string dateFrom, string dateTo, string Operator, string rid, string wc)
        {
            return db.Report_ActualCycle_Operator(dateFrom, dateTo, Operator, rid, wc).ToList();
        }

        public static List<Report_ActualCycle_ResourceGroup_ItemsResult> getdataACbyResGroupItem(string dateFrom, string dateTo, string itemCode, string rid, string wc)
        {
            return db.Report_ActualCycle_ResourceGroup_Items(dateFrom, dateTo, itemCode, rid, wc).ToList();
        }

        public static List<Report_ActualCycle_ResourceGroup_WCMachinesResult> getdataACbyResGroupWCMachine(string dateFrom, string dateTo, string itemCode, string rid, string wc)
        {
            return db.Report_ActualCycle_ResourceGroup_WCMachines(dateFrom, dateTo, itemCode, rid, wc).ToList();
        }

        public static List<Report_InsufficientMaterialbyDateResult> getdataInsufficientMaterialbyDate(DateTime Sd, DateTime Ed)
        {
            return db.Report_InsufficientMaterialbyDate(Sd, Ed).ToList();
        }

        public static List<Report_InsufficientMaterialbyJobResult> getdataInsufficientMaterialbyJob(string job)
        {
            return db.Report_InsufficientMaterialbyJob(job).ToList();
        }

    }
}