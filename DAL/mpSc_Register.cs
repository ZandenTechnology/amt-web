﻿using UltimoApps.DAL.Membership;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class Sc_Register
    {
        public static List<GetAllRegistrationDataResult> GetAllRegistrationData(string company, string status, string filter, string searchKey, int skip, int take)
        {
            return Global.db.GetAllRegistrationData(company, status, filter, searchKey, skip, take).ToList();
        }

        public static List<GetAllRegistrationDataTotalResult> GetAllRegistrationDataTotal(string company, string status, string filter, string searchKey)
        {
            return Global.db.GetAllRegistrationDataTotal(company, status, filter, searchKey).ToList();
        }

        internal static bool updateAcceptedRegist(Sc_Register model)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Registers.FirstOrDefault(item => item.RegistID == model.RegistID);
                Sc_User activeUser = Sc_User.getUserbyUserName(mpMembership.GetCurrentUsername());
                get.Status = "ACCEPTED";
                get.ApvRemarks = model.ApvRemarks;
                get.ModifUser = activeUser.FirstName + " " + activeUser.LastName;
                get.ModifDate = DateTime.Now;
                get.ApvBy = activeUser.FirstName + " " + activeUser.LastName;
                get.ApvDate = DateTime.Now;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static bool updateRejectedRegist(Sc_Register model)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Registers.FirstOrDefault(item => item.RegistID == model.RegistID);
                Sc_User activeUser = Sc_User.getUserbyUserName(mpMembership.GetCurrentUsername());
                get.Status = "REJECTED";
                get.ApvRemarks = model.ApvRemarks;
                get.ModifUser = activeUser.FirstName + " " + activeUser.LastName;
                get.ModifDate = DateTime.Now;
                get.ApvBy = activeUser.FirstName + " " + activeUser.LastName;
                get.ApvDate = DateTime.Now;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}