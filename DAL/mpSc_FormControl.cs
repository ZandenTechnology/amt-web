﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class Sc_FormControl
    {
        public static List<GetSpecificFormCtrlResult> GetSpecificFormCtrl(String FormID)
        {
            return Global.db.GetSpecificFormCtrl(FormID).ToList();
        }

        public static List<GetSpecificFormCtrlByRolePermissionResult> GetSpecificFormCtrlByRolePermission(string roleID, string formID)
        {
            var temp = Global.db.GetSpecificFormCtrlByRolePermission(roleID, formID).ToList();
            return temp;
        }

        public static List<GetAllControlTypeEnumResult> GetAllControlTypeEnum(string FormID)
        {
            return Global.db.GetAllControlTypeEnum(FormID).ToList();
        }

        public static bool validate(Sc_FormCtrl model)
        {
            if ((model.CtrlID == null) || (model.FormID == null) || (model.CtrlType == null) || (model.CtrlID == "") || (model.FormID == "") || (model.CtrlType == ""))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}