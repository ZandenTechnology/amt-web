﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltimoApps.DAL
{
    public interface IRepository
    {
        Sc_Form GetMenuDetailBySeoUrl(string SeoString);
        Sc_Form GetMenuById(string id);
    }

}