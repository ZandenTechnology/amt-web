﻿
using UltimoApps.DAL.Membership;
using UltimoApps.Models;
using UltimoApps.Models.Custom;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace UltimoApps.DAL
{
    partial class Sc_User
    {

        public static bool Update(Sc_User data)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Users.FirstOrDefault(item => item.UserID == data.UserID);
                //get.FirstName = data.FirstName;
                get.UserName = data.UserName;
                get.FirstName = data.FirstName;
                get.LastName = data.LastName;
                get.Gender = data.Gender;
                get.BirthDate = data.BirthDate;
                get.MobileNo = data.MobileNo;
                get.SecMobileNo = data.SecMobileNo;
                get.PersonalEmail = data.PersonalEmail;
                get.ModifDate = data.ModifDate;
                get.ModifUser = data.ModifUser;
                get.Photo = data.Photo;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static bool ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Users.FirstOrDefault(item => item.UserID == model.UserID);

                var encryptedPassword = mpMembership.HashPassword(model.Password, ConfigurationManager.AppSettings["passwordSalt"].ToString());

                get.Password = encryptedPassword;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Sc_User getUserbyID(long id)
        {
            var db = Global.db;
            Sc_User temp = db.Sc_Users.FirstOrDefault(item => item.UserID.Equals(id));
            return temp;
        }

        public static Sc_User getUserbyUserName(string id)
        {
            var db = Global.db;
            Sc_User temp = db.Sc_Users.FirstOrDefault(item => item.UserName.Equals(id));
            return temp;
        }

        public static string getNamebyEmail(string id)
        {
            var db = Global.db;
            Sc_User temp = db.Sc_Users.FirstOrDefault(item => item.UserName.Equals(id));
            return temp.FirstName;
        }

        public static List<GetUserListDataResult> GetUserListData( string filter, string searchKey, int skip, int take)
        {
            return Global.db.GetUserListData( filter, searchKey, skip, take).ToList();
        }

        public static int GetUserListDataTotal(  string filter, string searchKey)
        {
            return (int)Global.db.GetUserListDataTotal( filter, searchKey).FirstOrDefault().totalData;
        }

        internal static bool addFromRegist(Sc_Register model)
        {
            var db = Global.db;
            var maxUserID = db.Sc_Users.OrderByDescending(u => u.UserID).FirstOrDefault();
            Sc_User activeUser = Sc_User.getUserbyUserName(mpMembership.GetCurrentUsername());
            Sc_User user = new Sc_User();
            Sc_Register get = db.Sc_Registers.FirstOrDefault(item => item.RegistID == model.RegistID);
            user.UserID = maxUserID.UserID + 1;
            user.RegistID = model.RegistID;
            user.UserName = model.CorpEmail;
            user.Password = "mf4CS3n7Yykic1Lqv2wCkoAMxljycR4SLxjepoJuaj0=";
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Gender = model.Gender;
            user.BirthDate = null;
            user.MobileNo = "000";
            user.SecMobileNo = "000";
            user.PersonalEmail = null;
            user.IsLockedOut = false;
            user.IsActive = true;
            user.IsChangePass = false;
            user.IsLockedOut = false;
            user.InputDate = DateTime.Now;
            user.InputUser = activeUser.FirstName + " " + activeUser.LastName;
            user.ModifDate = DateTime.Now;
            user.ModifUser = activeUser.FirstName + " " + activeUser.LastName;
            db.Sc_Users.InsertOnSubmit(user);

            if (Sc_User.validate(user))
            {
                try
                {
                    db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        private static bool validate(Sc_User model)
        {
            if (model.UserID < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
