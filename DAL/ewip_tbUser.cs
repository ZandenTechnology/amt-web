﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class tbUser
    {
        public static tbUser GetUserbyUserName(string username)
        {
            var db = Global.ewipDB;
            return db.tbUsers.FirstOrDefault(x => x._userName == username);
        }

        public static bool saveUserAMT(tbUser model)
        {
            try
            {
                var db = Global.ewipDB;
                tbUser newUser = new tbUser();
                newUser._userID = model._userID;
                newUser._name = model._name;
                newUser._userName = model._userName;
                newUser._password = model._password;
                newUser._privilege = model._privilege;
                newUser._createdDate = DateTime.Now;
                db.tbUsers.InsertOnSubmit(newUser);

                db.SubmitChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool updateUserAMT(tbUser model)
        {
            try
            {
                var db = Global.ewipDB;
                tbUser get = db.tbUsers.FirstOrDefault(y => y._idno == model._idno);
                get._idno = model._idno;
                get._privilege = model._privilege;
                get._password = model._password;
                get._userName = model._userName;
                get._name = model._name;
                get._createdDate = get._createdDate;

                db.SubmitChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool deleteUserAMT(tbUser model)
        {
            try
            {
                var db = Global.ewipDB;
                tbUser get = db.tbUsers.FirstOrDefault(y => y._idno == model._idno);
                db.tbUsers.DeleteOnSubmit(get);

                db.SubmitChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}