﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltimoApps.DAL
{
    public class Repository : IRepository
    {
        ultimoDBDataContext objContext = new ultimoDBDataContext();
        public Sc_Form GetMenuDetailBySeoUrl(string SeoString)
        {
            try
            {
                string frmName = objContext.fnGetWebFormByMenuURL(SeoString);
                return objContext.Sc_Forms.Where(o => o.WebForm.ToLower().Trim() == frmName.Trim()).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }
        public Sc_Form GetMenuById(string id)
        {
            return objContext.Sc_Forms.Where(o => o.FormID == id).FirstOrDefault();
        }
    }
}