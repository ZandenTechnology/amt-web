﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.Models;

namespace UltimoApps.DAL
{
    partial class tbRejectedGroup
    {
        public static bool saveRejectedGrouping(Grouping_RejectedModel model)
        {
            try
            {
                var db = Global.ewipDB;
                tbRejectedGroup newRejectedGrouping = new tbRejectedGroup();
                newRejectedGrouping._wc= model._wc;

                if (model._itemCode != null)
                {
                    newRejectedGrouping.__itemCode = model._itemCode;
                }
                else if (model._itemCode == null)
                {
                    newRejectedGrouping.__itemCode = "";
                }

                if (model._RejID != null)
                {
                    newRejectedGrouping._RejID = model._RejID;
                }
                else if (model._RejID == null)
                {
                    newRejectedGrouping.__RejID = 0;
                }

                db.tbRejectedGroups.InsertOnSubmit(newRejectedGrouping);
                db.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool updateRejectedGrouping(Grouping_RejectedModel model)
        {
            try
            {
                var status = saveRejectedGrouping(model);
                status = deleteRejectedGrouping(model);
                //var db = Global.ewipDB;
                //tbRejectedGroup get = db.tbRejectedGroups.FirstOrDefault(y => y._rgid == model._rgid);
                //get._wc = model._wc;
                //get._itemCode = model._itemCode;
                //get._RejID = model._RejID;
                //db.SubmitChanges();
                return status;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool deleteRejectedGrouping(Grouping_RejectedModel model)
        {
            try
            {
                var db = Global.ewipDB;
                tbRejectedGroup get = db.tbRejectedGroups.FirstOrDefault(y => y._rgid == model._rgid);
                db.tbRejectedGroups.DeleteOnSubmit(get);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}