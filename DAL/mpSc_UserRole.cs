﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.Models;
using UltimoApps.DAL.Membership;
using System.Web.Script.Serialization;

namespace UltimoApps.DAL
{
    partial class Sc_UserRole
    {
        public static bool UpdateFromUserRole(UserRoleModel model)
        {
            try
            {
                var db = Global.db;
                var get = db.Sc_Users.FirstOrDefault(item => item.UserID == model.UserID);
                var currentUser = tbUser.GetUserbyUserName(mpMembership.GetCurrentUsername());
                get.UserName = model.UserName;
                get.FirstName = model.FirstName;
                get.LastName = model.LastName;
                get.CompID = model.CompID;
                get.Gender = model.Gender;
                get.BirthDate = model.BirthDate;
                get.MobileNo = model.MobileNo;
                get.SecMobileNo = model.SecMobileNo;
                get.PersonalEmail = model.PersonalEmail;
                get.ModifDate = DateTime.Now;
                get.ModifUser = currentUser._userName;

                //insert role
                if (model.ListRolesID != null)
                {
                    var serializer = new JavaScriptSerializer();
                    var serializedMenu = serializer.Deserialize<List<UserRoleIntModel>>(model.ListRolesID);

                    //List<Sc_UserRole> listHas = (from x in db.Sc_UserRoles
                    //                             where x.UserID == model.UserID
                    //                             select x).ToList();

                    var m = mpMembership.GetCurrentUser();

                    foreach (var menu in serializedMenu)
                    {
                        Sc_UserRole checkItem = db.Sc_UserRoles.FirstOrDefault(x => x.UserID == long.Parse(menu.UserID) && x.RoleID == menu.RoleID);

                        //try
                        //{
                        //    var itemToRemove = listHas.Single(r => r.UserID == long.Parse(menu.UserID) && r.RoleID == menu.RoleID);
                        //    listHas.Remove(itemToRemove);
                        //}
                        //catch
                        //{
                        //    ;
                        //}

                        if (checkItem == null)
                        {
                            Sc_UserRole item = new Sc_UserRole();
                            item.UserID = long.Parse(menu.UserID);
                            item.RoleID = menu.RoleID;
                            item.UserName = m._userName;
                            item.IsActive = menu.IsHasRole;
                            item.InputDate = DateTime.Now;
                            item.InputUser = m._name;

                            db.Sc_UserRoles.InsertOnSubmit(item);
                        }
                        else
                        {
                            checkItem.IsActive = menu.IsHasRole;
                            checkItem.ModifDate = DateTime.Now;
                            checkItem.ModifUser = m._userName;
                        }
                    }

                }

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static bool insertUserList(UserRoleModel model)
        {
            try
            {
                //edit user
                var db = Global.db;
                var currentUser = tbUser.GetUserbyUserName(mpMembership.GetCurrentUsername());
                var maxUserID = db.Sc_Users.OrderByDescending(u => u.UserID).FirstOrDefault();
                Sc_User newUser = new Sc_User();
                newUser.UserID = maxUserID.UserID + 1;
                newUser.CompID = model.CompID;
                newUser.RegistID = 0;
                newUser.UserName = model.UserName;
                newUser.CompID = model.CompID;
                newUser.Password = "mf4CS3n7Yykic1Lqv2wCkoAMxljycR4SLxjepoJuaj0=";
                newUser.FirstName = model.FirstName;
                newUser.LastName = model.LastName;
                newUser.Gender = model.Gender;
                newUser.BirthDate = model.BirthDate;
                newUser.MobileNo = model.MobileNo;
                newUser.SecMobileNo = model.SecMobileNo;
                newUser.PersonalEmail = model.PersonalEmail;
                newUser.IsLockedOut = false;
                newUser.IsChangePass = false;
                newUser.IsUpdateProfile = false;
                newUser.IsActive = true;
                newUser.InputDate = DateTime.Now;
                newUser.InputUser = currentUser._name;
                newUser.ModifDate = DateTime.Now;
                newUser.ModifUser = currentUser._userName;
                db.Sc_Users.InsertOnSubmit(newUser);

                //insert role
                //check if user assigned role to ID
                if (model.ListRolesID != null)
                {
                    var serializer = new JavaScriptSerializer();
                    var serializedMenu = serializer.Deserialize<List<UserRoleIntModel>>(model.ListRolesID);

                    var m = mpMembership.GetCurrentUser();

                    foreach (var menu in serializedMenu)
                    {
                        Sc_UserRole checkItem = db.Sc_UserRoles.FirstOrDefault(x => x.UserID == long.Parse(menu.UserID) && x.RoleID == menu.RoleID);

                        if (checkItem == null)
                        {
                            Sc_UserRole item = new Sc_UserRole();
                            item.UserID = maxUserID.UserID + 1;
                            item.RoleID = menu.RoleID;
                            item.UserName = m._userName;
                            item.IsActive = true;
                            item.InputDate = DateTime.Now;
                            item.InputUser = m._name;

                            db.Sc_UserRoles.InsertOnSubmit(item);
                        }
                        else
                        {
                            checkItem.IsActive = true;
                            checkItem.ModifDate = DateTime.Now;
                            checkItem.ModifUser = m._userName;
                        }
                    }
                }

                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static List<GetAllUserRoleDataByUserIDResult> GetAllUserRoleDataByUserID(long UserID)
        {
            return Global.db.GetAllUserRoleDataByUserID(UserID).ToList();
        }

        public static bool deleteUserRole(long userID)
        {
            try
            {
                Global.db.DeleteUserRole(userID.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }
    }


    internal class UserRoleIntModel
    {
        private string _RoleID;

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        private string _UserID;

        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private bool _IsHasRole;

        public bool IsHasRole
        {
            get { return _IsHasRole; }
            set { _IsHasRole = value; }
        }
    }
}