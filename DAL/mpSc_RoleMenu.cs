﻿using UltimoApps.DAL.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimoApps.DAL
{
    partial class Sc_RoleMenu
    {
        public static int getUserHasMasterClearance(string RoleID, string MenuURL)
        {
            var get = Global.db.sp_getUserHasMasterClearance(RoleID, MenuURL).FirstOrDefault();
            return (int)get.totalData;
        }

        public static bool insert(Sc_RoleMenu model)
        {
            var db = Global.db;
            //var get = new Sc_RoleMenu();
            //get.RolesID = model.RolesID;
            //get.IsActive = model.IsActive;
            model.InputDate = DateTime.Now;
            model.InputUser = Sc_User.getNamebyEmail(mpMembership.GetCurrentUsername());

            db.Sc_RoleMenus.InsertOnSubmit(model);

            if (Sc_RoleMenu.validate(model))
            {
                try
                {
                    db.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool Update(Sc_RoleMenu model)
        {
            var db = Global.db;
            var get = db.Sc_RoleMenus.FirstOrDefault(item => item.RolesID == model.RolesID && item.MenuID == model.MenuID);
       
            try
            {
                get.IsActive = model.IsActive;
                get.ModifDate = model.ModifDate;
                get.ModifUser = model.ModifUser;

                Global.SubmitChanges(db);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool validate(Sc_RoleMenu model)
        {
            if ((model.RolesID == null) || (model.RolesID == ""))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}