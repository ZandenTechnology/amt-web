﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UltimoApps.DAL;

namespace UltimoApps.DAL
{
    partial class tbWc
    {
        public static tbWc GetNameByWorkName(string _wc)
        {
            var db = Global.ewipDB;
            return db.tbWcs.FirstOrDefault(x => x._wc == _wc);
        }

        public static bool insertWorkCenter(tbWc model)
        {
            try
            {
                var db = Global.ewipDB;
                tbWc newWorkCenter = new tbWc();
                tbWc maxUser = db.tbWcs.OrderBy(x => x._wcid).FirstOrDefault();
                newWorkCenter._wcid = maxUser._wcid + 1;
                newWorkCenter._wc = model._wc;
                newWorkCenter._description = model._description;
                newWorkCenter._labour = model._labour;
                newWorkCenter._Run_Rate = model._Run_Rate;

                db.tbWcs.InsertOnSubmit(newWorkCenter);

                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool updateWorkCenter(tbWc model)
        {
            try
            {
                var db = Global.ewipDB;
                tbWc get = db.tbWcs.FirstOrDefault(y => y._wcid == model._wcid);
                get._wcid = model._wcid;
                //get._wc = model._wc;
                get._description = model._description;
                get._transfer = model._transfer;
                get._Run_Rate = model._Run_Rate;
                    
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool deleteWorkCenter(tbWc model)
        {
            try
            {
                var db = Global.ewipDB;
                tbWc get = db.tbWcs.FirstOrDefault(y => y._wcid == model._wcid);
                db.tbWcs.DeleteOnSubmit(get);
                db.SubmitChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}