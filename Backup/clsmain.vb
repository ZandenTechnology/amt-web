Imports System.Data.SqlClient

Public Class clsmain
   
    'Public sqls As String

    Public WONo As String
    'Dim DB As New clsmain

    Public refprint As String = "False"
    Public cs As String '= System.Configuration.ConfigurationManager.AppSettings("AMTconn")
    'Dim config As System.Collections.Specialized.NameValueCollection = System.Configuration.ConfigurationManager.AppSettings()
    'Public SqlString As String = config.Item("AMTconn")
    Public conn As SqlClient.SqlConnection
    Public com As New SqlClient.SqlCommand
    Public rs As SqlClient.SqlDataReader

    'Public Function AMT() As Boolean
    '    Dim obj As New frmAMTPrint
    '    obj.Show()
    '    obj.Close()

    'End Function

    'Dim com As New SqlCommand
    Dim da As New SqlDataAdapter
    'Dim conn As New SqlConnection

    Public Function Connect(ByVal CS As String) As String
        Try
            If Not conn.State = ConnectionState.Closed Then
                Connect = "Error : Connection is still open. Unable to connect"
                Exit Function
            End If
            conn = New SqlConnection(CS)
            conn.Open()

            Connect = "Success"
        Catch ex As Exception
            Connect = "Error : " & ex.Message
        End Try
    End Function

    Public Function Disconnect() As String
        Try
            If conn.State = ConnectionState.Closed Then
                Disconnect = "Error : Connection is already closed"
                Exit Function
            End If
            conn.Close()
            Disconnect = "Success"
        Catch ex As Exception
            Disconnect = "Error : " & ex.Message
        End Try
    End Function

    Public Function GetConnectionStatus() As Boolean
        Try
            Select Case conn.State
                Case ConnectionState.Broken
                    GetConnectionStatus = "Connection is broken"
                Case ConnectionState.Closed
                    GetConnectionStatus = "Connection is closed"
                Case ConnectionState.Connecting
                    GetConnectionStatus = "Connection is connecting"
                Case ConnectionState.Executing
                    GetConnectionStatus = "Connection is executing"
                Case ConnectionState.Fetching
                    GetConnectionStatus = "Connection is fetching"
                Case ConnectionState.Open
                    GetConnectionStatus = "Connection is open"
            End Select
        Catch ex As Exception
            GetConnectionStatus = False
        End Try
    End Function

    Public Function GetDataset(ByVal CS As String, ByVal SQLs As String, ByVal TableName As String) As DataSet
        Try
            Dim ds As New DataSet

            da = New SqlDataAdapter(SQLs, CS)
            da.Fill(ds, TableName)

            da.Dispose()

            GetDataset = ds

            ds.Dispose()
        Catch ex As Exception
            GetDataset = Nothing
        End Try
    End Function

    Public Function GetDatasetConnected(ByVal SQLs As String, ByVal TableName As String) As DataSet
        Try
            If conn.State = ConnectionState.Closed Then
                GetDatasetConnected = Nothing
                Exit Function
            ElseIf Not conn.State = ConnectionState.Open Then
                GetDatasetConnected = Nothing
                Exit Function
            End If
            Dim ds As New DataSet

            da = New SqlDataAdapter(SQLs, conn)
            da.Fill(ds, TableName)

            da.Dispose()

            GetDatasetConnected = ds

            ds.Dispose()
        Catch ex As Exception
            GetDatasetConnected = Nothing
        End Try
    End Function

    Public Function UpdateRecord(ByVal SQLs As String) As Boolean
        Try
            If conn.State = ConnectionState.Closed Then
                UpdateRecord = "Connection is closed. Unable to update"
                Exit Function
            ElseIf Not conn.State = ConnectionState.Open Then
                UpdateRecord = "Connection is busy. Unable to update"
                Exit Function
            End If

            com = New SqlCommand(SQLs, conn)
            com.ExecuteNonQuery()
            com.Dispose()

            UpdateRecord = True

        Catch ex As Exception
            UpdateRecord = False
        End Try
    End Function

    Public Function ExecStoredProcedure(ByVal CS As String, ByVal SPName As String, ByVal TableName As String) As DataSet
        Try
            Dim ds As New DataSet
            da = New SqlDataAdapter(SPName, CS)
            da.Fill(ds, TableName)

            da.Dispose()

            ExecStoredProcedure = ds

            ds.Dispose()
        Catch ex As Exception
            ExecStoredProcedure = Nothing
        End Try
    End Function

    Public Function deleteData(ByVal SQLs As String) As Boolean
        deleteData = False
        Dim com As New SqlCommand
        Try
            With com
                conn.Close()
                .Connection = conn
                .CommandType = CommandType.Text
                .CommandText = SQLs
                conn.Open()
                .ExecuteNonQuery()
                ' conn.Close()
                deleteData = True
            End With
        Catch ex As Exception
        Finally
            ' conn.Close()
        End Try
    End Function

    Public Function ExecProcedureInsert(ByVal CS As String, ByVal procedurename As String, ByVal parm() As SqlParameter) As String
        Dim cn As New SqlConnection(CS)
        Try
            Dim com As New SqlCommand
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = procedurename
                AssignParameters(com, parm)
                cn.Open()
                ExecProcedureInsert = .ExecuteNonQuery
            End With

        Catch ex As Exception
            ExecProcedureInsert = ex.Message
        Finally
            cn.Close()
        End Try
    End Function

    Private Sub AssignParameters(ByVal cmd As SqlCommand, ByVal cmdParameters() As SqlParameter)
        If (cmdParameters Is Nothing) Then Exit Sub
        For Each p As SqlParameter In cmdParameters
            cmd.Parameters.Add(p)
        Next
    End Sub

    Public Function ExecProcedureUpdate(ByVal CS As String, ByVal procedurename As String, ByVal parm() As SqlParameter) As String
        Dim cn As New SqlConnection(CS)
        Try
            Dim com As New SqlCommand
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = procedurename
                AssignParameters(com, parm)
                cn.Open()
                ExecProcedureUpdate = .ExecuteNonQuery
            End With
        Catch ex As Exception
            ExecProcedureUpdate = Nothing
        Finally
            cn.Close()
        End Try
    End Function

    Public Function ExecProcedureSelctTable(ByVal CS As String, ByVal procedurename As String, ByVal parm() As SqlParameter) As DataSet
        Dim cn As New SqlConnection(CS)
        Dim ds As DataSet
        ds = New DataSet
        Try
            Dim com As New SqlCommand
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = procedurename
                AssignParameters(com, parm)
                Dim sqlad As New SqlDataAdapter(com)
                da.Fill(ds, "tbname")
                ExecProcedureSelctTable = ds
            End With
        Catch ex As Exception
            ExecProcedureSelctTable = Nothing
        Finally
            cn.Close()
        End Try
    End Function

    'Public Function printAMT(ByVal WONo As String, ByVal str As String, ByVal PName As String, ByVal st As Boolean) As Boolean
    '    Dim wrknoR As String
    '    RetPrt = False
    '    SetConn(str)
    '    WONo = UCase(WONo)
    '    Dim RowVal(11) As String
    '    Dim recCnt As Double = 0
    '    cs = str

    '    Try
    '        Dim conn As New SqlConnection(cs)
    '        Dim SQLs As String
    '        conn.Open()

    '        Dim pf As New frmAMTprintpreview
    '        pf.PName = PName
    '        pf.ds.Tables.Add("WO")
    '        pf.ds.Tables(0).Clear()
    '        pf.ds.Tables(0).Columns.Add("OP", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("Work Centre Name", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("Trans Date", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("Start Time", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("End Time", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("Total Hrs", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("CP", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("RJ", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("Emp ID", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("OP Completed", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("Remark", System.Type.GetType("System.String"))
    '        pf.ds.Tables(0).Columns.Add("CtlPt", System.Type.GetType("System.String")) 'use as word description

    '        Dim da As New SqlDataAdapter

    '        'for rework data format 
    '        'WONo = "50SI981117-B-R"
    '        'WONo = "BA00082225-M"
    '        'WONo = "ETH0981130-M"
    '        'WONo = "FN00081743-A-R"
    '        'WONo = "BA00082224-M-R"

    '        WONo = Trim(WONo)
    '        Dim dsroute As DataSet
    '        dsroute = New DataSet

    '        Dim dsjobtrans As DataSet
    '        dsjobtrans = New DataSet

    '        Dim dswkDsc As DataSet
    '        dswkDsc = New DataSet
    '        SQLs = "select * from tbwc order by _wc"
    '        dswkDsc = GetDataset(cs, SQLs, "dsc")
    '        SQLs = ""

    '        Dim dsItm As DataSet
    '        dsItm = New DataSet
    '        SQLs = "select * from tbitem order by _itemcode"
    '        dsItm = GetDataset(cs, SQLs, "itm")
    '        SQLs = ""

    '        Dim dsJob As DataSet
    '        dsJob = New DataSet
    '        SQLs = "select * from tbjob order by _job"
    '        dsJob = GetDataset(cs, SQLs, "job")
    '        SQLs = ""

    '        Dim tempjobno, tempsuffix As String
    '        If InStr(WONo, "-") > 0 Then

    '            tempjobno = (Split(WONo, "-"))(0)
    '            If (Split(WONo, "-"))(1) = "" Then
    '                ''tempsuffix = "*"
    '                tempsuffix = "M"
    '            Else
    '                tempsuffix = (Split(WONo, "-"))(1)
    '            End If

    '            'retrieve job route
    '            SQLs = "Select * from tbjobroute where _job='" & tempjobno & "' order by _operationNo"
    '            dsroute = GetDataset(cs, SQLs, "jobroute")
    '            If dsroute.Tables("jobroute").Rows.Count = 0 Then
    '                MsgBox("No records to Print")
    '                Exit Function
    '            End If

    '            'retrieve job suffix
    '            SQLs = "select * from tbjobtrans where _job='" & tempjobno & "' order by _oper_num"
    '            If st = False Then
    '                SQLs = "select * from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' order by _oper_num"
    '            Else
    '                SQLs = "select * from tbjobtransHis where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' order by _oper_num"
    '            End If
    '            dsjobtrans = GetDataset(cs, SQLs, "suffix")
    '            Dim DSPause As New DataSet
    '            SQLs = "select * from tbPause where _refID in(select _tansnum from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "')"
    '            DSPause = GetDataset(cs, SQLs, "suffix")


    '            If InStr(WONo, "-R") > 0 Then
    '                If st = False Then
    '                    SQLs = "select * from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' and _Reworkst='R' order by _oper_num"
    '                Else
    '                    SQLs = "select * from tbjobtransHis where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' and _Reworkst='R' order by _oper_num"
    '                End If
    '                dsjobtrans = New DataSet
    '                dsjobtrans = GetDataset(cs, SQLs, "suffix")

    '                DSPause = New DataSet
    '                SQLs = "select * from tbPause where _refID in(select _tansnum from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "')"
    '                DSPause = GetDataset(cs, SQLs, "suffix")

    '                SQLs = "select * from tbjob a,tbjobroute b where " & _
    '                       "a._job = b._job " & _
    '                       "and a._job='" & Split(WONo, "-")(0) & "' order by _operationNO"

    '                'Dim com As New SqlCommand(SQLs, conn)
    '                'Dim dr As SqlDataReader = com.ExecuteReader()

    '                '20090701
    '                Dim dsMasterSTG As DataSet
    '                dsMasterSTG = New DataSet
    '                dsMasterSTG = GetDataset(cs, SQLs, "mst")

    '                Dim dvstg As New DataView
    '                dvstg.Table = dsMasterSTG.Tables(0)
    '                dvstg.RowFilter = "_operationNo >= " & dsjobtrans.Tables("suffix").Rows(0).Item("_oper_num")
    '                For Each i As DataRowView In dvstg
    '                    recCnt += 1

    '                    If recCnt = 1 Then
    '                        Dim drjob() As DataRow
    '                        drjob = dsJob.Tables("job").Select("_job='" & tempjobno & "'")
    '                        If drjob.Length > 0 Then

    '                            '20090528 if reccnt>1, the means the work qty retrieve already, the release qty get 1st record
    '                            If recCnt > 1 Then
    '                            Else
    '                                wkoqty = drjob(0).Item("_qtyreleased")
    '                                If dsjobtrans.Tables("suffix").Rows.Count > 0 Then
    '                                    wkoqty = dsjobtrans.Tables("suffix").Rows(0).Item("_qty_rele_qty")
    '                                End If
    '                            End If

    '                            If drjob(0).Item("_stas") = "R" Then
    '                                wkono = WONo '& "-R"
    '                                wrknoR = wkono
    '                            Else
    '                                wrknoR = wkono
    '                                wkono = WONo
    '                            End If
    '                            wkomtr = "" 'v.Item("_item")
    '                            wkofgcode = drjob(0).Item("_item")
    '                            Dim drprt() As DataRow
    '                            drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(drjob(0).Item("_item")) & "'")
    '                            If drprt.Length = 0 Then
    '                                wkoptno = ""
    '                                wkorevno = ""
    '                                wkoprname = ""
    '                            Else
    '                                If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
    '                                    Dim temp() As String
    '                                    temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
    '                                    wkoprname = temp(0)

    '                                    If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
    '                                        temp = Split(temp(1), ")")
    '                                        wkoptno = temp(0)
    '                                        wkorevno = ""
    '                                    Else
    '                                        wkoptno = temp(1)
    '                                        wkorevno = ""
    '                                    End If
    '                                Else
    '                                    wkoptno = ""
    '                                    wkorevno = ""
    '                                    wkoprname = drprt(0).Item("_itemdescription")
    '                                End If
    '                            End If
    '                        End If
    '                        wkoeftdate = Format(Date.Now, "dd/MM/yyyy")
    '                    End If

    '                    'this part not sure need to add or not
    '                    Dim drstg() As DataRow
    '                    drstg = dsjobtrans.Tables("suffix").Select("_oper_num=" & i.Item("_operationno") & " ")

    '                    If drstg.Length = 0 Then
    '                        RowVal(0) = UCase(Trim(i.Item("_operationno")))
    '                        RowVal(1) = UCase(Trim(i.Item("_wc")))
    '                        RowVal(2) = Format(Date.FromOADate(i.Item("_jobdate")), "dd/MM/yyyy")
    '                        RowVal(3) = ""
    '                        RowVal(4) = ""
    '                        RowVal(5) = ""
    '                        RowVal(6) = "0"
    '                        RowVal(7) = "0"
    '                        RowVal(8) = ""
    '                        RowVal(9) = ""
    '                        RowVal(10) = ""

    '                        If dswkDsc.Tables(0).Rows.Count = 0 Then
    '                            RowVal(11) = ""
    '                        Else
    '                            Dim drn() As DataRow
    '                            drn = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(i.Item("_wc"))) & "'")
    '                            If drn.Length = 0 Then
    '                                RowVal(11) = ""
    '                            Else
    '                                RowVal(11) = drn(0).Item("_description")
    '                            End If
    '                        End If
    '                    Else
    '                        RowVal(0) = UCase(Trim(drstg(0).Item("_oper_num")))
    '                        RowVal(1) = UCase(Trim(drstg(0).Item("_wc")))
    '                        RowVal(2) = Format(Date.FromOADate(drstg(0).Item("_start_date")), "dd/MM/yyyy")
    '                        RowVal(3) = Format(Date.FromOADate(drstg(0).Item("_start_date")), "dd/MM/yyyy HH:mm")
    '                        If RowVal(3) = "30/12/1899 00:00" Then
    '                            RowVal(3) = ""
    '                        End If

    '                        RowVal(4) = Format(Date.FromOADate(drstg(0).Item("_end_date")), "dd/MM/yyyy HH:mm")
    '                        If RowVal(4) = "30/12/1899 00:00" Then
    '                            RowVal(4) = ""
    '                        End If
    '                        Dim DrPause As DataRow() = DSPause.Tables(0).Select("_refID=" & drstg(0).Item("_tansnum"))
    '                        Dim topausehr As Double
    '                        Dim intI As Integer = 0
    '                        If DrPause.Length > 0 Then

    '                            For intI = 0 To DrPause.Length - 1
    '                                topausehr = topausehr + gettothrs(DrPause(intI).Item("_pstart"), DrPause(intI).Item("_pend"))
    '                            Next
    '                        End If

    '                        Dim tothrs As Double = gettothrs(drstg(0).Item("_start_date"), drstg(0).Item("_end_date"))
    '                        RowVal(5) = tothrs - topausehr

    '                        RowVal(6) = UCase(Trim(drstg(0).Item("_qty_complete")))
    '                        RowVal(7) = UCase(Trim(drstg(0).Item("_qty_scrapped")))
    '                        ' RowVal(8) = UCase(Trim(drstg(0).Item("_emp_num")))

    '                        If UCase(Trim(drstg(0).Item("_emp_num"))) <> "" Then
    '                            If UCase(Trim(drstg(0).Item("_emp_num"))).Length > 4 Then
    '                                RowVal(8) = Mid(UCase(Trim(drstg(0).Item("_emp_num"))), 1, 4)
    '                            Else
    '                                RowVal(8) = UCase(Trim(drstg(0).Item("_emp_num")))
    '                            End If

    '                        Else
    '                            RowVal(8) = UCase(Trim(drstg(0).Item("_emp_num")))
    '                        End If

    '                        RowVal(9) = ""

    '                        If drstg(0).Item("_Reworkst") = "R" Then
    '                            RowVal(10) = "Rework"
    '                        Else
    '                            RowVal(10) = UCase(Trim(drstg(0).Item("_remarks")))
    '                        End If

    '                        If dswkDsc.Tables(0).Rows.Count = 0 Then
    '                            RowVal(11) = ""
    '                        Else
    '                            Dim drnew() As DataRow
    '                            drnew = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(drstg(0).Item("_wc"))) & "'")
    '                            If drnew.Length = 0 Then
    '                                RowVal(11) = ""
    '                            Else
    '                                RowVal(11) = drnew(0).Item("_description")
    '                            End If
    '                        End If
    '                    End If

    '                    pf.ds.Tables(0).Rows.Add(RowVal)
    '                Next

    '                ''Do While dr.Read()
    '                ''    recCnt += 1

    '                ''    If recCnt = 1 Then
    '                ''        Dim drjob() As DataRow
    '                ''        drjob = dsJob.Tables("job").Select("_job='" & tempjobno & "'")
    '                ''        If drjob.Length > 0 Then

    '                ''            '20090528 if reccnt>1, the means the work qty retrieve already, the release qty get 1st record
    '                ''            If recCnt > 1 Then
    '                ''            Else
    '                ''                wkoqty = drjob(0).Item("_qtyreleased")
    '                ''                If dsjobtrans.Tables("suffix").Rows.Count > 0 Then
    '                ''                    wkoqty = dsjobtrans.Tables("suffix").Rows(0).Item("_qty_rele_qty")
    '                ''                End If
    '                ''            End If

    '                ''            If drjob(0).Item("_stas") = "R" Then
    '                ''                wkono = WONo '& "-R"
    '                ''                wrknoR = wkono
    '                ''            Else
    '                ''                wrknoR = wkono
    '                ''                wkono = WONo
    '                ''            End If
    '                ''            wkomtr = "" 'v.Item("_item")
    '                ''            wkofgcode = drjob(0).Item("_item")
    '                ''            Dim drprt() As DataRow
    '                ''            drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(drjob(0).Item("_item")) & "'")
    '                ''            If drprt.Length = 0 Then
    '                ''                wkoptno = ""
    '                ''                wkorevno = ""
    '                ''                wkoprname = ""
    '                ''            Else
    '                ''                If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
    '                ''                    Dim temp() As String
    '                ''                    temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
    '                ''                    wkoprname = temp(0)

    '                ''                    If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
    '                ''                        temp = Split(temp(1), ")")
    '                ''                        wkoptno = temp(0)
    '                ''                        wkorevno = ""
    '                ''                    Else
    '                ''                        wkoptno = temp(1)
    '                ''                        wkorevno = ""
    '                ''                    End If
    '                ''                Else
    '                ''                    wkoptno = ""
    '                ''                    wkorevno = ""
    '                ''                    wkoprname = drprt(0).Item("_itemdescription")
    '                ''                End If
    '                ''            End If
    '                ''        End If
    '                ''        wkoeftdate = Format(Date.Now, "dd/MM/yyyy")
    '                ''    End If

    '                ''    'this part not sure need to add or not
    '                ''    Dim drstg() As DataRow
    '                ''    drstg = dsjobtrans.Tables("suffix").Select("_oper_num=" & dr.Item("_operationno") & " ")

    '                ''    If drstg.Length = 0 Then
    '                ''        RowVal(0) = UCase(Trim(dr.Item("_operationno")))
    '                ''        RowVal(1) = UCase(Trim(dr.Item("_wc")))
    '                ''        RowVal(2) = Format(Date.FromOADate(dr.Item("_jobdate")), "dd/MM/yyyy")
    '                ''        RowVal(3) = ""
    '                ''        RowVal(4) = ""
    '                ''        RowVal(5) = ""
    '                ''        RowVal(6) = "0"
    '                ''        RowVal(7) = "0"
    '                ''        RowVal(8) = ""
    '                ''        RowVal(9) = ""
    '                ''        RowVal(10) = ""

    '                ''        If dswkDsc.Tables(0).Rows.Count = 0 Then
    '                ''            RowVal(11) = ""
    '                ''        Else
    '                ''            Dim drn() As DataRow
    '                ''            drn = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(dr.Item("_wc"))) & "'")
    '                ''            If drn.Length = 0 Then
    '                ''                RowVal(11) = ""
    '                ''            Else
    '                ''                RowVal(11) = drn(0).Item("_description")
    '                ''            End If
    '                ''        End If
    '                ''    Else
    '                ''        RowVal(0) = UCase(Trim(drstg(0).Item("_oper_num")))
    '                ''        RowVal(1) = UCase(Trim(drstg(0).Item("_wc")))
    '                ''        RowVal(2) = Format(Date.FromOADate(drstg(0).Item("_start_date")), "dd/MM/yyyy")
    '                ''        RowVal(3) = Format(Date.FromOADate(drstg(0).Item("_start_date")), "dd/MM/yyyy HH:mm")
    '                ''        If RowVal(3) = "30/12/1899 00:00" Then
    '                ''            RowVal(3) = ""
    '                ''        End If

    '                ''        RowVal(4) = Format(Date.FromOADate(drstg(0).Item("_end_date")), "dd/MM/yyyy HH:mm")
    '                ''        If RowVal(4) = "30/12/1899 00:00" Then
    '                ''            RowVal(4) = ""
    '                ''        End If
    '                ''        Dim DrPause As DataRow() = DSPause.Tables(0).Select("_refID=" & drstg(0).Item("_tansnum"))
    '                ''        Dim topausehr As Double
    '                ''        Dim intI As Integer = 0
    '                ''        If DrPause.Length > 0 Then

    '                ''            For intI = 0 To DrPause.Length - 1
    '                ''                topausehr = topausehr + gettothrs(DrPause(intI).Item("_pstart"), DrPause(intI).Item("_pend"))
    '                ''            Next
    '                ''        End If

    '                ''        Dim tothrs As Double = gettothrs(drstg(0).Item("_start_date"), drstg(0).Item("_end_date"))
    '                ''        RowVal(5) = tothrs - topausehr

    '                ''        RowVal(6) = UCase(Trim(drstg(0).Item("_qty_complete")))
    '                ''        RowVal(7) = UCase(Trim(drstg(0).Item("_qty_scrapped")))
    '                ''        RowVal(8) = UCase(Trim(drstg(0).Item("_emp_num")))
    '                ''        RowVal(9) = ""

    '                ''        If drstg(0).Item("_Reworkst") = "R" Then
    '                ''            RowVal(10) = "Rework"
    '                ''        Else
    '                ''            RowVal(10) = UCase(Trim(drstg(0).Item("_remarks")))
    '                ''        End If

    '                ''        If dswkDsc.Tables(0).Rows.Count = 0 Then
    '                ''            RowVal(11) = ""
    '                ''        Else
    '                ''            Dim drnew() As DataRow
    '                ''            drnew = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(drstg(0).Item("_wc"))) & "'")
    '                ''            If drnew.Length = 0 Then
    '                ''                RowVal(11) = ""
    '                ''            Else
    '                ''                RowVal(11) = drnew(0).Item("_description")
    '                ''            End If
    '                ''        End If
    '                ''    End If

    '                ''    pf.ds.Tables(0).Rows.Add(RowVal)
    '                ''Loop
    '                conn.Close()

    '                If pf.ds.Tables(0).Rows.Count > 0 Then
    '                    RetPrt = True
    '                    pf.Target = pf.PDamt
    '                    pf.ShowDialog()
    '                Else
    '                    MsgBox("There are No Records Found")
    '                End If
    '                Return RetPrt
    '                Exit Function    'for rework --------------------end
    '            End If


    '            '20090701
    '            Dim refR As Boolean = False  'for check the stage have any rework or not

    '            Dim stgno As Integer
    '            For Each v As DataRow In dsjobtrans.Tables("suffix").Rows
    '                recCnt += 1
    '                If recCnt = 1 Then
    '                    wkoqty = v.Item("_qty_rele_qty")
    '                End If

    '                '20090701
    '                If v.Item("_reworkst") = "R" Then
    '                    refR = True
    '                    Exit For
    '                End If

    '                If v.Item("_Reworkst") = "R" Then
    '                    wkono = WONo & "-R"
    '                    wkono = WONo '& "-R"
    '                    wrknoR = wkono
    '                Else
    '                    wrknoR = wkono
    '                    wkono = WONo
    '                End If
    '                wkomtr = "" 'v.Item("_item")
    '                wkofgcode = v.Item("_item")

    '                Dim drprt() As DataRow
    '                drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(v.Item("_item")) & "'")
    '                If drprt.Length = 0 Then
    '                    wkoptno = ""
    '                    wkorevno = ""
    '                    wkoprname = ""
    '                Else
    '                    If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
    '                        Dim temp() As String
    '                        temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
    '                        wkoprname = temp(0)
    '                        'If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), "REV") Then
    '                        '    temp = Split(temp(1), "rev")
    '                        '    wkoptno = temp(0)
    '                        '    If temp.Length > 0 Then
    '                        '        wkorevno = "Rev " & Mid(temp(1), 1, temp(1).Length - 1)
    '                        '    Else
    '                        '        wkorevno = ""
    '                        '    End If
    '                        'Else
    '                        '    wkoptno = Mid(temp(1), 1, temp(1).Length - 1)
    '                        '    wkorevno = ""
    '                        'End If
    '                        If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
    '                            temp = Split(temp(1), ")")
    '                            wkoptno = temp(0)
    '                            'If temp.Length > 0 Then
    '                            '    wkorevno = "Rev " & Mid(temp(1), 1, temp(1).Length - 1)
    '                            'Else
    '                            '    wkorevno = ""
    '                            'End If
    '                            wkorevno = ""
    '                        Else
    '                            wkoptno = temp(1)
    '                            wkorevno = ""
    '                        End If
    '                    Else
    '                        wkoptno = ""
    '                        wkorevno = ""
    '                        wkoprname = drprt(0).Item("_itemdescription")
    '                    End If
    '                End If

    '                wkoeftdate = Format(Date.Now, "dd/MM/yyyy")

    '                RowVal(0) = UCase(Trim(v.Item("_oper_num")))
    '                RowVal(1) = UCase(Trim(v.Item("_wc")))
    '                RowVal(2) = Format(Date.FromOADate(v.Item("_start_date")), "dd/MM/yyyy")
    '                RowVal(3) = Format(Date.FromOADate(v.Item("_start_date")), "dd/MM/yyyy HH:mm")
    '                If RowVal(3) = "30/12/1899 00:00" Then
    '                    RowVal(3) = ""
    '                End If

    '                RowVal(4) = Format(Date.FromOADate(v.Item("_end_date")), "dd/MM/yyyy HH:mm")
    '                If RowVal(4) = "30/12/1899 00:00" Then
    '                    RowVal(4) = ""
    '                End If
    '                Dim DrPause As DataRow() = DSPause.Tables(0).Select("_refID=" & v.Item("_tansnum"))
    '                Dim topausehr As Double
    '                Dim intI As Integer = 0
    '                If DrPause.Length > 0 Then

    '                    For intI = 0 To DrPause.Length - 1
    '                        topausehr = topausehr + gettothrs(DrPause(intI).Item("_pstart"), DrPause(intI).Item("_pend"))
    '                    Next
    '                End If

    '                Dim tothrs As Double = gettothrs(v.Item("_start_date"), v.Item("_end_date"))
    '                RowVal(5) = tothrs - topausehr

    '                RowVal(6) = UCase(Trim(v.Item("_qty_complete")))
    '                RowVal(7) = UCase(Trim(v.Item("_qty_scrapped")))
    '                If UCase(Trim(v.Item("_emp_num"))) <> "" Then
    '                    If UCase(Trim(v.Item("_emp_num"))).Length > 4 Then
    '                        RowVal(8) = Mid(UCase(Trim(v.Item("_emp_num"))), 1, 4)
    '                    Else
    '                        RowVal(8) = UCase(Trim(v.Item("_emp_num")))
    '                    End If

    '                Else
    '                RowVal(8) = UCase(Trim(v.Item("_emp_num")))
    '                End If

    '                RowVal(9) = ""
    '                If dsjobtrans.Tables("suffix").Rows.Count = recCnt Then
    '                    If st = False Then
    '                        RowVal(10) = ""
    '                    Else
    '                        RowVal(10) = "Rework"
    '                    End If
    '                    '20090528 if the rwk sts is R
    '                    If v.Item("_Reworkst") = "R" Then
    '                        RowVal(10) = "Rework"
    '                    End If
    '                Else
    '                    RowVal(10) = UCase(Trim(v.Item("_remarks")))
    '                End If

    '                If dswkDsc.Tables(0).Rows.Count = 0 Then
    '                    RowVal(11) = ""
    '                Else
    '                    Dim dr() As DataRow
    '                    dr = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(v.Item("_wc"))) & "'")
    '                    If dr.Length = 0 Then
    '                        RowVal(11) = ""
    '                    Else
    '                        RowVal(11) = dr(0).Item("_description")
    '                    End If
    '                End If

    '                pf.ds.Tables(0).Rows.Add(RowVal)
    '                stgno = v.Item("_oper_num")
    '            Next

    '            '20090701
    '            If refR = True Then

    '            Else
    '                Dim dsmax As DataSet
    '                dsmax = New DataSet
    '                If st = False Then
    '                    SQLs = "select * from tbjobtrans where _jobsuffixparent='" & tempsuffix & "' and _jobsuffix<>'" & tempsuffix & "' and _job='" & tempjobno & "'"
    '                Else
    '                    SQLs = "select * from tbjobtransHis where _jobsuffixparent='" & tempsuffix & "' and _jobsuffix<>'" & tempsuffix & "' and _job='" & tempjobno & "'"
    '                End If
    '                dsmax = GetDataset(cs, SQLs, "max")

    '                If dsmax.Tables("max").Rows.Count = 0 Then
    '                    Dim dvopno As New DataView
    '                    dvopno.Table = dsroute.Tables("jobroute")
    '                    dvopno.RowFilter = "_operationno> " & stgno & ""
    '                    For Each v As DataRowView In dvopno
    '                        recCnt += 1
    '                        If dsJob.Tables("job").Rows.Count = 0 Then
    '                            wkoqty = wkoqty 'v.Item("_qtyReleased")
    '                            If wrknoR <> "" Then
    '                                wrknoR = wkono
    '                            Else
    '                                wkono = WONo
    '                            End If
    '                            wkomtr = ""
    '                            wkofgcode = ""
    '                            wkoptno = ""
    '                            wkorevno = ""
    '                            wkoprname = ""
    '                            wkoeftdate = Format(Date.Now, "dd/MM/yyyy")
    '                            wkoqty = wkoqty
    '                        Else
    '                            Dim drjob() As DataRow
    '                            drjob = dsJob.Tables("job").Select("_job='" & tempjobno & "'")
    '                            If drjob.Length > 0 Then
    '                                '20090528 if reccnt>1 the means before have trans record
    '                                If recCnt > 1 Then
    '                                Else
    '                                    wkoqty = drjob(0).Item("_qtyreleased")
    '                                End If

    '                                If drjob(0).Item("_stas") = "R" Then
    '                                    wkono = WONo '& "-R"
    '                                    'wrknoR = wkono
    '                                Else
    '                                    'wrknoR = wkono
    '                                    wkono = WONo
    '                                End If
    '                                wkomtr = "" 'v.Item("_item")
    '                                wkofgcode = drjob(0).Item("_item")
    '                                Dim drprt() As DataRow
    '                                drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(drjob(0).Item("_item")) & "'")
    '                                If drprt.Length = 0 Then
    '                                    wkoptno = ""
    '                                    wkorevno = ""
    '                                    wkoprname = ""
    '                                Else
    '                                    If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
    '                                        Dim temp() As String
    '                                        temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
    '                                        wkoprname = temp(0)
    '                                        'If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), "REV") Then
    '                                        '    temp = Split(temp(1), "rev")
    '                                        '    wkoptno = temp(0)
    '                                        '    If temp.Length > 0 Then
    '                                        '        wkorevno = "Rev " & Mid(temp(1), 1, temp(1).Length - 1)
    '                                        '    Else
    '                                        '        wkorevno = ""
    '                                        '    End If
    '                                        'Else
    '                                        '    wkoptno = Mid(temp(1), 1, temp(1).Length - 1)
    '                                        '    wkorevno = ""
    '                                        'End If
    '                                        If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
    '                                            temp = Split(temp(1), ")")
    '                                            wkoptno = temp(0)
    '                                            'If temp.Length > 0 Then
    '                                            '    wkorevno = "Rev " & Mid(temp(1), 1, temp(1).Length - 1)
    '                                            'Else
    '                                            '    wkorevno = ""
    '                                            'End If
    '                                            wkorevno = ""
    '                                        Else
    '                                            wkoptno = temp(1)
    '                                            wkorevno = ""
    '                                        End If
    '                                    Else
    '                                        wkoptno = ""
    '                                        wkorevno = ""
    '                                        wkoprname = drprt(0).Item("_itemdescription")
    '                                    End If
    '                                End If
    '                            End If
    '                        End If
    '                        wkoeftdate = Format(Date.Now, "dd/MM/yyyy")

    '                        RowVal(0) = UCase(Trim(v.Item("_operationno")))
    '                        RowVal(1) = UCase(Trim(v.Item("_wc")))
    '                        RowVal(2) = ""
    '                        RowVal(3) = ""
    '                        RowVal(4) = ""
    '                        RowVal(5) = ""
    '                        RowVal(6) = "0"
    '                        RowVal(7) = "0"
    '                        RowVal(8) = ""
    '                        RowVal(9) = ""
    '                        RowVal(10) = "" 'UCase(Trim(v.Item("_remarks")))

    '                        If dswkDsc.Tables(0).Rows.Count = 0 Then
    '                            RowVal(11) = ""
    '                        Else
    '                            Dim dr() As DataRow
    '                            dr = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(v.Item("_wc"))) & "'")
    '                            If dr.Length = 0 Then
    '                                RowVal(11) = ""
    '                            Else
    '                                RowVal(11) = dr(0).Item("_description")
    '                            End If
    '                        End If

    '                        pf.ds.Tables(0).Rows.Add(RowVal)
    '                    Next
    '                End If

    '            End If
    '        Else
    '            tempjobno = WONo
    '            tempsuffix = ""

    '            SQLs = "select * from tbjob a,tbjobroute b where " & _
    '                   "a._job = b._job " & _
    '                   "and a._job='" & WONo & "'"

    '            Dim com As New SqlCommand(SQLs, conn)
    '            Dim dr As SqlDataReader = com.ExecuteReader()

    '            Do While dr.Read()
    '                recCnt += 1
    '                If dsJob.Tables("job").Rows.Count = 0 Then
    '                    wkoqty = dr.Item("_qtyReleased")
    '                    wkono = WONo
    '                    wkomtr = ""
    '                    wkofgcode = ""
    '                    wkoptno = ""
    '                    wkorevno = ""
    '                    wkoprname = ""
    '                    wkoeftdate = Format(Date.Now, "dd/MM/yyyy")
    '                Else
    '                    Dim drjob() As DataRow
    '                    drjob = dsJob.Tables("job").Select("_job='" & tempjobno & "'")
    '                    If drjob.Length > 0 Then

    '                        '20090528 if reccnt>1, the means the work qty retrieve already, the release qty get 1st record
    '                        If recCnt > 1 Then
    '                        Else
    '                            wkoqty = drjob(0).Item("_qtyreleased")
    '                        End If

    '                        If drjob(0).Item("_stas") = "R" Then
    '                            wkono = WONo '& "-R"
    '                            wrknoR = wkono
    '                        Else
    '                            wrknoR = wkono
    '                            wkono = WONo
    '                        End If
    '                        wkomtr = "" 'v.Item("_item")
    '                        wkofgcode = drjob(0).Item("_item")
    '                        Dim drprt() As DataRow
    '                        drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(drjob(0).Item("_item")) & "'")
    '                        If drprt.Length = 0 Then
    '                            wkoptno = ""
    '                            wkorevno = ""
    '                            wkoprname = ""
    '                        Else
    '                            If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
    '                                Dim temp() As String
    '                                temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
    '                                wkoprname = temp(0)
    '                                'If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), "REV") Then
    '                                '    temp = Split(temp(1), "rev")
    '                                '    wkoptno = temp(0)
    '                                '    If temp.Length > 0 Then
    '                                '        wkorevno = "Rev " & Mid(temp(1), 1, temp(1).Length - 1)
    '                                '    Else
    '                                '        wkorevno = ""
    '                                '    End If
    '                                'Else
    '                                '    wkoptno = Mid(temp(1), 1, temp(1).Length - 1)
    '                                '    wkorevno = ""
    '                                'End If
    '                                If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
    '                                    temp = Split(temp(1), ")")
    '                                    wkoptno = temp(0)
    '                                    'If temp.Length > 0 Then
    '                                    '    wkorevno = "Rev " & Mid(temp(1), 1, temp(1).Length - 1)
    '                                    'Else
    '                                    '    wkorevno = ""
    '                                    'End If
    '                                    wkorevno = ""
    '                                Else
    '                                    wkoptno = temp(1)
    '                                    wkorevno = ""
    '                                End If
    '                            Else
    '                                wkoptno = ""
    '                                wkorevno = ""
    '                                wkoprname = drprt(0).Item("_itemdescription")
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '                wkoeftdate = Format(Date.Now, "dd/MM/yyyy")

    '                RowVal(0) = UCase(Trim(dr.Item("_operationno")))
    '                RowVal(1) = UCase(Trim(dr.Item("_wc")))
    '                RowVal(2) = Format(Date.FromOADate(dr.Item("_jobdate")), "dd/MM/yyyy")
    '                RowVal(3) = ""
    '                RowVal(4) = ""
    '                RowVal(5) = ""
    '                RowVal(6) = "0"
    '                RowVal(7) = "0"
    '                RowVal(8) = ""
    '                RowVal(9) = ""
    '                RowVal(10) = ""

    '                If dswkDsc.Tables(0).Rows.Count = 0 Then
    '                    RowVal(11) = ""
    '                Else
    '                    Dim drn() As DataRow
    '                    drn = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(dr.Item("_wc"))) & "'")
    '                    If drn.Length = 0 Then
    '                        RowVal(11) = ""
    '                    Else
    '                        RowVal(11) = drn(0).Item("_description")
    '                    End If
    '                End If

    '                pf.ds.Tables(0).Rows.Add(RowVal)
    '            Loop
    '            conn.Close()
    '        End If

    '            If pf.ds.Tables(0).Rows.Count > 0 Then
    '                RetPrt = True
    '                pf.Target = pf.PDamt
    '                pf.ShowDialog()
    '            Else
    '                MsgBox("There are No Records Found")
    '            End If

    '    Catch ex As Exception
    '        MsgBox("There is an error : " & ex.Message)
    '    Finally
    '        conn.Close()
    '    End Try
    '    Return RetPrt
    'End Function

    Public Function gettothrs(ByVal st As Double, ByVal ed As Double) As Double
        Return 0.0
    End Function

    Public Sub SetConn(ByVal strc As String)
        conn = New SqlClient.SqlConnection(strc)
    End Sub

    Public Function printAMT(ByVal WONo As String, ByVal str As String, ByVal PName As String, ByVal st As Boolean) As Boolean
        Dim wrknoR As String
        RetPrt = False
        SetConn(str)
        WONo = UCase(WONo)
        Dim RowVal(11) As String
        Dim recCnt As Double = 0
        cs = str

        Try
            Dim conn As New SqlConnection(cs)
            Dim SQLs As String
            conn.Open()

            Dim pf As New frmAMTprintpreview
            pf.PName = PName
            pf.ds.Tables.Add("WO")
            pf.ds.Tables(0).Clear()
            pf.ds.Tables(0).Columns.Add("OP", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("Work Centre Name", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("Trans Date", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("Start Time", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("End Time", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("Total Hrs", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("CP", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("RJ", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("Emp ID", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("OP Completed", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("Remark", System.Type.GetType("System.String"))
            pf.ds.Tables(0).Columns.Add("CtlPt", System.Type.GetType("System.String")) 'use as word description

            Dim da As New SqlDataAdapter

            'for rework data format 
            'WONo = "sd33990848"

            WONo = Trim(WONo)
            Dim dsroute As DataSet
            dsroute = New DataSet

            Dim dsjobtrans As DataSet
            dsjobtrans = New DataSet

            Dim dswkDsc As DataSet
            dswkDsc = New DataSet
            SQLs = "select * from tbwc order by _wc"
            dswkDsc = GetDataset(cs, SQLs, "dsc")
            SQLs = ""

            Dim dsItm As DataSet
            dsItm = New DataSet
            SQLs = "select * from tbitem order by _itemcode"
            dsItm = GetDataset(cs, SQLs, "itm")
            SQLs = ""

            Dim dsJob As DataSet
            dsJob = New DataSet
            SQLs = "select * from tbjob order by _job"
            dsJob = GetDataset(cs, SQLs, "job")
            SQLs = ""

            Dim tempjobno, tempsuffix As String
            If InStr(WONo, "-") > 0 Then

                tempjobno = (Split(WONo, "-"))(0)
                If (Split(WONo, "-"))(1) = "" Then
                    ''tempsuffix = "*"
                    tempsuffix = "M"
                Else
                    tempsuffix = (Split(WONo, "-"))(1)
                End If

                'retrieve job route
                SQLs = "Select * from tbjobroute where _job='" & tempjobno & "' order by _operationNo"
                dsroute = GetDataset(cs, SQLs, "jobroute")
                If dsroute.Tables("jobroute").Rows.Count = 0 Then
                    MsgBox("No records to Print")
                    Exit Function
                End If

                'retrieve job suffix
                SQLs = "select * from tbjobtrans where _job='" & tempjobno & "' order by _oper_num"
                If st = False Then
                    SQLs = "select * from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' order by _oper_num"
                Else
                    SQLs = "select * from tbjobtransHis where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' order by _oper_num"
                End If
                dsjobtrans = GetDataset(cs, SQLs, "suffix")
                Dim DSPause As New DataSet
                SQLs = "select * from tbPause where _refID in(select _tansnum from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "')"
                DSPause = GetDataset(cs, SQLs, "suffix")


                If InStr(WONo, "-R") > 0 Then
                    If st = False Then
                        SQLs = "select * from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' and _Reworkst='R' order by _oper_num"
                    Else
                        SQLs = "select * from tbjobtransHis where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "' and _Reworkst='R' order by _oper_num"
                    End If
                    dsjobtrans = New DataSet
                    dsjobtrans = GetDataset(cs, SQLs, "suffix")

                    DSPause = New DataSet
                    SQLs = "select * from tbPause where _refID in(select _tansnum from tbjobtrans where _job='" & tempjobno & "' and _jobsuffix='" & tempsuffix & "')"
                    DSPause = GetDataset(cs, SQLs, "suffix")

                    SQLs = "select * from tbjob a,tbjobroute b where " & _
                           "a._job = b._job " & _
                           "and a._job='" & Split(WONo, "-")(0) & "' order by _operationNO"

                    '20090701
                    Dim dsMasterSTG As DataSet
                    dsMasterSTG = New DataSet
                    dsMasterSTG = GetDataset(cs, SQLs, "mst")

                    Dim dvstg As New DataView
                    dvstg.Table = dsMasterSTG.Tables(0)
                    dvstg.RowFilter = "_operationNo >= " & dsjobtrans.Tables("suffix").Rows(0).Item("_oper_num")
                    For Each i As DataRowView In dvstg
                        recCnt += 1

                        If recCnt = 1 Then
                            Dim drjob() As DataRow
                            drjob = dsJob.Tables("job").Select("_job='" & tempjobno & "'")
                            If drjob.Length > 0 Then

                                '20090528 if reccnt>1, the means the work qty retrieve already, the release qty get 1st record
                                If recCnt > 1 Then
                                Else
                                    wkoqty = drjob(0).Item("_qtyreleased")
                                    If dsjobtrans.Tables("suffix").Rows.Count > 0 Then
                                        wkoqty = dsjobtrans.Tables("suffix").Rows(0).Item("_qty_rele_qty")
                                    End If
                                End If

                                If drjob(0).Item("_stas") = "R" Then
                                    wkono = WONo '& "-R"
                                    wrknoR = wkono
                                Else
                                    wrknoR = wkono
                                    wkono = WONo
                                End If

                                Dim tempm() As String
                                tempm = Split(drjob(0).Item("_material"), "||")
                                Dim tempi As Integer
                                For tempi = 0 To tempm.Length - 1
                                    wkomtr += tempm(tempi) & vbCrLf
                                Next

                                'wkomtr = drjob(0).Item("_material")
                                wkofgcode = drjob(0).Item("_item")
                                Dim drprt() As DataRow
                                drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(drjob(0).Item("_item")) & "'")
                                If drprt.Length = 0 Then
                                    wkoptno = ""
                                    wkorevno = ""
                                    wkoprname = ""
                                Else
                                    'If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
                                    '    Dim temp() As String
                                    '    temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
                                    '    wkoprname = temp(0)

                                    '    If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
                                    '        temp = Split(temp(1), ")")
                                    '        wkoptno = temp(0)
                                    '        wkorevno = ""
                                    '    Else
                                    '        wkoptno = temp(1)
                                    '        wkorevno = ""
                                    '    End If
                                    'Else
                                    '    wkoptno = ""
                                    '    wkorevno = ""
                                    '    wkoprname = drprt(0).Item("_itemdescription")
                                    'End If

                                    '20100311 change to new fields
                                    wkoptno = drprt(0).Item("_Drawing_Nbr")
                                    wkorevno = drprt(0).Item("_Revision")
                                    wkoprname = drprt(0).Item("_itemdescription")
                                End If
                            End If
                            wkoeftdate = Format(Date.Now, "dd/MM/yyyy")
                        End If

                        'this part not sure need to add or not
                        Dim drstg() As DataRow
                        drstg = dsjobtrans.Tables("suffix").Select("_oper_num=" & i.Item("_operationno") & " ")

                        If drstg.Length = 0 Then
                            RowVal(0) = UCase(Trim(i.Item("_operationno")))
                            RowVal(1) = UCase(Trim(i.Item("_wc")))
                            RowVal(2) = Format(Date.FromOADate(i.Item("_jobdate")), "dd/MM/yyyy")
                            RowVal(3) = ""
                            RowVal(4) = ""
                            RowVal(5) = ""
                            RowVal(6) = "0"
                            RowVal(7) = "0"
                            RowVal(8) = ""
                            RowVal(9) = ""
                            RowVal(10) = ""

                            If dswkDsc.Tables(0).Rows.Count = 0 Then
                                RowVal(11) = ""
                            Else
                                Dim drn() As DataRow
                                drn = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(i.Item("_wc"))) & "'")
                                If drn.Length = 0 Then
                                    RowVal(11) = ""
                                Else
                                    RowVal(11) = drn(0).Item("_description")
                                End If
                            End If
                        Else
                            RowVal(0) = UCase(Trim(drstg(0).Item("_oper_num")))
                            RowVal(1) = UCase(Trim(drstg(0).Item("_wc")))
                            RowVal(2) = Format(Date.FromOADate(drstg(0).Item("_start_date")), "dd/MM/yyyy")
                            RowVal(3) = Format(Date.FromOADate(drstg(0).Item("_start_date")), "dd/MM/yyyy HH:mm")
                            If RowVal(3) = "30/12/1899 00:00" Then
                                RowVal(3) = ""
                            End If

                            RowVal(4) = Format(Date.FromOADate(drstg(0).Item("_end_date")), "dd/MM/yyyy HH:mm")
                            If RowVal(4) = "30/12/1899 00:00" Then
                                RowVal(4) = ""
                            End If
                            Dim DrPause As DataRow() = DSPause.Tables(0).Select("_refID=" & drstg(0).Item("_tansnum"))
                            Dim topausehr As Double
                            Dim intI As Integer = 0
                            If DrPause.Length > 0 Then

                                For intI = 0 To DrPause.Length - 1
                                    topausehr = topausehr + gettothrs(DrPause(intI).Item("_pstart"), DrPause(intI).Item("_pend"))
                                Next
                            End If

                            Dim tothrs As Double = gettothrs(drstg(0).Item("_start_date"), drstg(0).Item("_end_date"))
                            RowVal(5) = tothrs - topausehr

                            RowVal(6) = UCase(Trim(drstg(0).Item("_qty_complete")))
                            RowVal(7) = UCase(Trim(drstg(0).Item("_qty_scrapped")))

                            If UCase(Trim(drstg(0).Item("_emp_num"))) <> "" Then
                                If UCase(Trim(drstg(0).Item("_emp_num"))).Length > 4 Then
                                    RowVal(8) = Mid(UCase(Trim(drstg(0).Item("_emp_num"))), 1, 4)
                                Else
                                    RowVal(8) = UCase(Trim(drstg(0).Item("_emp_num")))
                                End If

                            Else
                                RowVal(8) = UCase(Trim(drstg(0).Item("_emp_num")))
                            End If

                            RowVal(9) = ""

                            If drstg(0).Item("_Reworkst") = "R" Then
                                RowVal(10) = "Rework"
                            Else
                                RowVal(10) = UCase(Trim(drstg(0).Item("_remarks")))
                            End If

                            If dswkDsc.Tables(0).Rows.Count = 0 Then
                                RowVal(11) = ""
                            Else
                                Dim drnew() As DataRow
                                drnew = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(drstg(0).Item("_wc"))) & "'")
                                If drnew.Length = 0 Then
                                    RowVal(11) = ""
                                Else
                                    RowVal(11) = drnew(0).Item("_description")
                                End If
                            End If
                        End If

                        pf.ds.Tables(0).Rows.Add(RowVal)
                    Next

                    conn.Close()

                    If pf.ds.Tables(0).Rows.Count > 0 Then
                        RetPrt = True
                        pf.Target = pf.PDamt
                        pf.ShowDialog()
                    Else
                        MsgBox("There are No Records Found")
                    End If
                    Return RetPrt
                    Exit Function    'for rework --------------------end
                End If


                '20090701
                Dim refR As Boolean = False  'for check the stage have any rework or not

                Dim stgno As Integer
                For Each v As DataRow In dsjobtrans.Tables("suffix").Rows
                    recCnt += 1
                    If recCnt = 1 Then
                        wkoqty = v.Item("_qty_rele_qty")
                    End If

                    '20090701
                    If v.Item("_reworkst") = "R" Then
                        refR = True
                        Exit For
                    End If

                    If v.Item("_Reworkst") = "R" Then
                        wkono = WONo & "-R"
                        wkono = WONo '& "-R"
                        wrknoR = wkono
                    Else
                        wrknoR = wkono
                        wkono = WONo
                    End If

                    'Dim tempm() As String
                    'tempm = Split(v.Item("_material"), "||")
                    'Dim tempi As Integer
                    'For tempi = 0 To tempm.Length - 1
                    '    wkomtr += tempm(tempi) & vbCrLf
                    'Next

                    wkomtr = v.Item("_item")
                    wkofgcode = v.Item("_item")

                    Dim drprt() As DataRow
                    drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(v.Item("_item")) & "'")
                    If drprt.Length = 0 Then
                        wkoptno = ""
                        wkorevno = ""
                        wkoprname = ""
                    Else
                        'If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
                        '    Dim temp() As String
                        '    temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
                        '    wkoprname = temp(0)
                        '    If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
                        '        temp = Split(temp(1), ")")
                        '        wkoptno = temp(0)
                        '        wkorevno = ""
                        '    Else
                        '        wkoptno = temp(1)
                        '        wkorevno = ""
                        '    End If
                        'Else
                        '    wkoptno = ""
                        '    wkorevno = ""
                        '    wkoprname = drprt(0).Item("_itemdescription")
                        'End If

                        '20100311 change to new field
                        wkoptno = drprt(0).Item("_Drawing_Nbr")
                        wkorevno = drprt(0).Item("_Revision")
                        wkoprname = drprt(0).Item("_itemdescription")
                    End If

                    wkoeftdate = Format(Date.Now, "dd/MM/yyyy")

                    RowVal(0) = UCase(Trim(v.Item("_oper_num")))
                    RowVal(1) = UCase(Trim(v.Item("_wc")))
                    RowVal(2) = Format(Date.FromOADate(v.Item("_start_date")), "dd/MM/yyyy")
                    RowVal(3) = Format(Date.FromOADate(v.Item("_start_date")), "dd/MM/yyyy HH:mm")
                    If RowVal(3) = "30/12/1899 00:00" Then
                        RowVal(3) = ""
                    End If

                    RowVal(4) = Format(Date.FromOADate(v.Item("_end_date")), "dd/MM/yyyy HH:mm")
                    If RowVal(4) = "30/12/1899 00:00" Then
                        RowVal(4) = ""
                    End If
                    Dim DrPause As DataRow() = DSPause.Tables(0).Select("_refID=" & v.Item("_tansnum"))
                    Dim topausehr As Double
                    Dim intI As Integer = 0
                    If DrPause.Length > 0 Then

                        For intI = 0 To DrPause.Length - 1
                            topausehr = topausehr + gettothrs(DrPause(intI).Item("_pstart"), DrPause(intI).Item("_pend"))
                        Next
                    End If

                    Dim tothrs As Double = gettothrs(v.Item("_start_date"), v.Item("_end_date"))
                    RowVal(5) = tothrs - topausehr

                    RowVal(6) = UCase(Trim(v.Item("_qty_complete")))
                    RowVal(7) = UCase(Trim(v.Item("_qty_scrapped")))
                    If UCase(Trim(v.Item("_emp_num"))) <> "" Then
                        If UCase(Trim(v.Item("_emp_num"))).Length > 4 Then
                            RowVal(8) = Mid(UCase(Trim(v.Item("_emp_num"))), 1, 4)
                        Else
                            RowVal(8) = UCase(Trim(v.Item("_emp_num")))
                        End If

                    Else
                        RowVal(8) = UCase(Trim(v.Item("_emp_num")))
                    End If

                    RowVal(9) = ""
                    If dsjobtrans.Tables("suffix").Rows.Count = recCnt Then
                        If st = False Then
                            RowVal(10) = ""
                        Else
                            RowVal(10) = "Rework"
                        End If
                        '20090528 if the rwk sts is R
                        If v.Item("_Reworkst") = "R" Then
                            RowVal(10) = "Rework"
                        End If
                    Else
                        RowVal(10) = UCase(Trim(v.Item("_remarks")))
                    End If

                    If dswkDsc.Tables(0).Rows.Count = 0 Then
                        RowVal(11) = ""
                    Else
                        Dim dr() As DataRow
                        dr = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(v.Item("_wc"))) & "'")
                        If dr.Length = 0 Then
                            RowVal(11) = ""
                        Else
                            RowVal(11) = dr(0).Item("_description")
                        End If
                    End If

                    pf.ds.Tables(0).Rows.Add(RowVal)
                    stgno = v.Item("_oper_num")
                Next

                '20090701
                If refR = True Then

                Else
                    Dim dsmax As DataSet
                    dsmax = New DataSet
                    If st = False Then
                        SQLs = "select * from tbjobtrans where _jobsuffixparent='" & tempsuffix & "' and _jobsuffix<>'" & tempsuffix & "' and _job='" & tempjobno & "'"
                    Else
                        SQLs = "select * from tbjobtransHis where _jobsuffixparent='" & tempsuffix & "' and _jobsuffix<>'" & tempsuffix & "' and _job='" & tempjobno & "'"
                    End If
                    dsmax = GetDataset(cs, SQLs, "max")

                    If dsmax.Tables("max").Rows.Count = 0 Then
                        Dim dvopno As New DataView
                        dvopno.Table = dsroute.Tables("jobroute")
                        dvopno.RowFilter = "_operationno> " & stgno & ""
                        For Each v As DataRowView In dvopno
                            recCnt += 1
                            If dsJob.Tables("job").Rows.Count = 0 Then
                                wkoqty = wkoqty 'v.Item("_qtyReleased")
                                If wrknoR <> "" Then
                                    wrknoR = wkono
                                Else
                                    wkono = WONo
                                End If
                                wkomtr = ""
                                wkofgcode = ""
                                wkoptno = ""
                                wkorevno = ""
                                wkoprname = ""
                                wkoeftdate = Format(Date.Now, "dd/MM/yyyy")
                                wkoqty = wkoqty
                            Else
                                Dim drjob() As DataRow
                                drjob = dsJob.Tables("job").Select("_job='" & tempjobno & "'")
                                If drjob.Length > 0 Then
                                    '20090528 if reccnt>1 the means before have trans record
                                    If recCnt > 1 Then
                                    Else
                                        wkoqty = drjob(0).Item("_qtyreleased")
                                    End If

                                    If drjob(0).Item("_stas") = "R" Then
                                        wkono = WONo '& "-R"
                                    Else
                                        wkono = WONo
                                    End If

                                    Dim tempm() As String
                                    tempm = Split(drjob(0).Item("_material"), "||")
                                    Dim tempi As Integer
                                    For tempi = 0 To tempm.Length - 1
                                        wkomtr += tempm(tempi) & vbCrLf
                                    Next

                                    'wkomtr = drjob(0).Item("_material")
                                    wkofgcode = drjob(0).Item("_item")
                                    Dim drprt() As DataRow
                                    drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(drjob(0).Item("_item")) & "'")
                                    If drprt.Length = 0 Then
                                        wkoptno = ""
                                        wkorevno = ""
                                        wkoprname = ""
                                    Else
                                        'If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
                                        '    Dim temp() As String
                                        '    temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
                                        '    wkoprname = temp(0)
                                        '    If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
                                        '        temp = Split(temp(1), ")")
                                        '        wkoptno = temp(0)
                                        '        wkorevno = ""
                                        '    Else
                                        '        wkoptno = temp(1)
                                        '        wkorevno = ""
                                        '    End If
                                        'Else
                                        '    wkoptno = ""
                                        '    wkorevno = ""
                                        '    wkoprname = drprt(0).Item("_itemdescription")
                                        'End If

                                        '20100311 change to new field
                                        wkoptno = drprt(0).Item("_Drawing_Nbr")
                                        wkorevno = drprt(0).Item("_Revision")
                                        wkoprname = drprt(0).Item("_itemdescription")
                                    End If
                                End If
                            End If
                            wkoeftdate = Format(Date.Now, "dd/MM/yyyy")

                            RowVal(0) = UCase(Trim(v.Item("_operationno")))
                            RowVal(1) = UCase(Trim(v.Item("_wc")))
                            RowVal(2) = ""
                            RowVal(3) = ""
                            RowVal(4) = ""
                            RowVal(5) = ""
                            RowVal(6) = "0"
                            RowVal(7) = "0"
                            RowVal(8) = ""
                            RowVal(9) = ""
                            RowVal(10) = "" 'UCase(Trim(v.Item("_remarks")))

                            If dswkDsc.Tables(0).Rows.Count = 0 Then
                                RowVal(11) = ""
                            Else
                                Dim dr() As DataRow
                                dr = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(v.Item("_wc"))) & "'")
                                If dr.Length = 0 Then
                                    RowVal(11) = ""
                                Else
                                    RowVal(11) = dr(0).Item("_description")
                                End If
                            End If

                            pf.ds.Tables(0).Rows.Add(RowVal)
                        Next
                    End If

                End If
            Else
                tempjobno = WONo
                tempsuffix = ""

                SQLs = "select * from tbjob a,tbjobroute b where " & _
                       "a._job = b._job " & _
                       "and a._job='" & WONo & "'"

                Dim com As New SqlCommand(SQLs, conn)
                Dim dr As SqlDataReader = com.ExecuteReader()

                Do While dr.Read()
                    recCnt += 1
                    If dsJob.Tables("job").Rows.Count = 0 Then
                        wkoqty = dr.Item("_qtyReleased")
                        wkono = WONo
                        wkomtr = ""
                        wkofgcode = ""
                        wkoptno = ""
                        wkorevno = ""
                        wkoprname = ""
                        wkoeftdate = Format(Date.Now, "dd/MM/yyyy")
                    Else
                        Dim drjob() As DataRow
                        drjob = dsJob.Tables("job").Select("_job='" & tempjobno & "'")
                        If drjob.Length > 0 Then

                            '20090528 if reccnt>1, the means the work qty retrieve already, the release qty get 1st record
                            If recCnt > 1 Then
                            Else
                                wkoqty = drjob(0).Item("_qtyreleased")
                            End If

                            If drjob(0).Item("_stas") = "R" Then
                                wkono = WONo '& "-R"
                                wrknoR = wkono
                            Else
                                wrknoR = wkono
                                wkono = WONo
                            End If

                            Dim tempm() As String
                            tempm = Split(drjob(0).Item("_material"), "||")
                            Dim tempi As Integer
                            For tempi = 0 To tempm.Length - 1
                                wkomtr += tempm(tempi) & vbCrLf
                            Next

                            'wkomtr = drjob(0).Item("_material")
                            wkofgcode = drjob(0).Item("_item")
                            Dim drprt() As DataRow
                            drprt = dsItm.Tables(0).Select("_itemcode='" & Trim(drjob(0).Item("_item")) & "'")
                            If drprt.Length = 0 Then
                                wkoptno = ""
                                wkorevno = ""
                                wkoprname = ""
                            Else
                                'If InStr(drprt(0).Item("_itemdescription"), "(") > 0 Then
                                '    Dim temp() As String
                                '    temp = Split(Trim(drprt(0).Item("_itemdescription")), "(")
                                '    wkoprname = temp(0)
                                '    If InStr(UCase(Trim(drprt(0).Item("_itemdescription"))), ")") Then
                                '        temp = Split(temp(1), ")")
                                '        wkoptno = temp(0)
                                '        wkorevno = ""
                                '    Else
                                '        wkoptno = temp(1)
                                '        wkorevno = ""
                                '    End If
                                'Else
                                '    wkoptno = ""
                                '    wkorevno = ""
                                '    wkoprname = drprt(0).Item("_itemdescription")
                                'End If

                                '20100311 change to new field
                                wkoptno = drprt(0).Item("_Drawing_Nbr")
                                wkorevno = drprt(0).Item("_Revision")
                                wkoprname = drprt(0).Item("_itemdescription")
                            End If
                        End If
                    End If
                    wkoeftdate = Format(Date.Now, "dd/MM/yyyy")

                    RowVal(0) = UCase(Trim(dr.Item("_operationno")))
                    RowVal(1) = UCase(Trim(dr.Item("_wc")))
                    RowVal(2) = Format(Date.FromOADate(dr.Item("_jobdate")), "dd/MM/yyyy")
                    RowVal(3) = ""
                    RowVal(4) = ""
                    RowVal(5) = ""
                    RowVal(6) = "0"
                    RowVal(7) = "0"
                    RowVal(8) = ""
                    RowVal(9) = ""
                    RowVal(10) = ""

                    If dswkDsc.Tables(0).Rows.Count = 0 Then
                        RowVal(11) = ""
                    Else
                        Dim drn() As DataRow
                        drn = dswkDsc.Tables(0).Select("_wc='" & UCase(Trim(dr.Item("_wc"))) & "'")
                        If drn.Length = 0 Then
                            RowVal(11) = ""
                        Else
                            RowVal(11) = drn(0).Item("_description")
                        End If
                    End If

                    pf.ds.Tables(0).Rows.Add(RowVal)
                Loop
                conn.Close()
            End If

            If pf.ds.Tables(0).Rows.Count > 0 Then
                If InStr(WONo, "-") > 0 Then
                    tempjobno = (Split(WONo, "-"))(0)
                Else
                    tempjobno = WONo
                End If

                Dim ds As DataSet
                ds = New DataSet
                SQLs = "select * from tbjob where _job='" & tempjobno & "'"
                ds = GetDataset(cs, SQLs, "max")

                wkomtr = ""
                If InStr(ds.Tables(0).Rows(0).Item("_material"), "||") > 0 Then
                    Dim tempm() As String
                    tempm = Split(ds.Tables(0).Rows(0).Item("_material"), "||")
                    Dim tempi As Integer
                    For tempi = 0 To tempm.Length - 1
                        If tempi = tempm.Length - 1 Then
                            wkomtr += tempm(tempi)
                        Else
                            wkomtr += tempm(tempi) & ","
                        End If
                        'wkomtr += tempm(tempi) & vbCrLf
                    Next
                Else
                    wkomtr = ds.Tables(0).Rows(0).Item("_material")
                End If

                RetPrt = True
                pf.Target = pf.PDamt
                pf.ShowDialog()
            Else
                MsgBox("There are No Records Found")
            End If

        Catch ex As Exception
            MsgBox("There is an error : " & ex.Message)
        Finally
            conn.Close()
        End Try
        Return RetPrt
    End Function

End Class
